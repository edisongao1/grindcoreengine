#pragma once
#include "IInputDeviceManager.h"
#include <gainput/gainput.h>
#ifdef _WIN32
#include <Windows.h>
#endif // _WIN32

namespace grind
{
	class CInputDeviceManager : public IInputDeviceManager, public gainput::InputListener
	{
		friend class CDeviceButtonListener;

	public:
		template<typename T>
		struct SEventListener
		{
			ListenerId		id;
			T				handler;
		};

		using TPressEventListenerList = std::list<SEventListener<TPressEventHandler>>;
		using TMoveEventListenerList = std::list<SEventListener<TMoveEventHandler>>;

		CInputDeviceManager(int clientWidth, int clientHeight);
		~CInputDeviceManager();
		virtual void Update(float fDelta) override;
		virtual bool IsButtonPressed(EInputDeviceType deviceType, InputKey key) const override;

#ifdef _WIN32
		void HandleMessage(const MSG& msg);
#endif
		virtual ListenerId AddButtonListener(EInputDeviceType deviceType, TPressEventHandler handler) override;
		virtual void RemoveButtonListener(EInputDeviceType deviceType, ListenerId id) override;
		virtual ListenerId AddMoveListener(EInputDeviceType deviceType, TMoveEventHandler handler) override;
		virtual void RemoveMoveListener(EInputDeviceType deviceType, ListenerId id) override;
		
		EInputDeviceType GetDeviceType(gainput::DeviceId deviceId) const
		{
			if (deviceId == mMouseDeviceId)
				return EInputDeviceType::Mouse;
			if (deviceId == mKeyboardDeviceId)
				return EInputDeviceType::Keyboard;
			if (deviceId == mGamepadDeviceId)
				return EInputDeviceType::Gamepad;
			return EInputDeviceType::Unknown;
		}

		// overwrite gainput::InputListener method:

		virtual bool OnDeviceButtonBool(gainput::DeviceId deviceId, gainput::DeviceButtonId deviceButton, bool oldValue, bool newValue) override;
		virtual bool OnDeviceButtonFloat(gainput::DeviceId deviceId, gainput::DeviceButtonId deviceButton, float oldValue, float newValue) override;

		virtual void SetGamepadViberate(float fLeftMotorSpeed, float fRightMotorSpeed, float fLastTime) override;
		virtual void StopGamepadViberate() override;
		virtual bool IsGamepadViberating() const override;
		virtual void ShowMouseCursor(bool bDisplay) override;
		virtual void SetMouseCursorPos(int x, int y) override;
		virtual float GetFloat(EInputDeviceType deviceType, InputKey key) override;
		virtual float GetFloatPrevious(EInputDeviceType deviceType, InputKey key) override;

		//void HandleMoveEvent(EInputKey key, const SInputMovementData& data)
		//{
		//	if (mPressEventHandlerMap[(int)])
		//}

		
	private:
		gainput::InputManager		mGainputManager;
		gainput::DeviceId			mKeyboardDeviceId;
		gainput::DeviceId			mMouseDeviceId;
		gainput::DeviceId			mGamepadDeviceId;
		gainput::InputDevicePad*	mGamepadDevice = nullptr;
		gainput::InputDevice*		mInputDevices[(int)EInputDeviceType::Count] = { 0 };

		//CDeviceButtonListener*		mDeviceInputListener;
		//std::unique_ptr<CDeviceButtonListener>	mDeviceButtonListener;
		gainput::ListenerId			mDeviceButtonListenerId;

		TPressEventListenerList		mButtonEventListenerLists[(int)EInputDeviceType::Count];
		TMoveEventListenerList		mMoveEventListenerLists[(int)EInputDeviceType::Count];

		//TPressEventListenerList		mPressEventListenerLists[KeyCount];
		//TMoveEventHandler		mMoveEventHandler;
		//TMoveEventListenerList		mMoveEventListenerList;
		std::atomic<uint32_t>		mNextListenerId;

		//bool		mButtonPressStates[(int)EInputDeviceType::Count][gainput::KeyCount_];
		
	public:
		void InitializeCursorResources();
		virtual void SetMouseCursorImage(ECursorImage eImage) override;
		HCURSOR				mCursorResources[(int)ECursorImage::_Count];


	};
}

