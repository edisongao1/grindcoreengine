#pragma once

namespace grind {

	template<size_t>
	struct LoadValueTypeFromEnum;

	template<typename T>
	struct StoreValueTypeAsEnum;

#ifndef DECLARE_VALUE_TYPE
#define DECLARE_VALUE_TYPE(T, N) \
template<> struct LoadValueTypeFromEnum<N> { using type = T; }; \
template<> struct StoreValueTypeAsEnum<T> { static const uint16_t value = N; };
#endif

	DECLARE_VALUE_TYPE(float, 1)
	DECLARE_VALUE_TYPE(int, 2)
	DECLARE_VALUE_TYPE(double, 3)
	DECLARE_VALUE_TYPE(void, 4)
	DECLARE_VALUE_TYPE(bool, 5)
	DECLARE_VALUE_TYPE(unsigned int, 6)
	DECLARE_VALUE_TYPE(long, 7)
	DECLARE_VALUE_TYPE(char, 8)
	DECLARE_VALUE_TYPE(const char*, 9)
	DECLARE_VALUE_TYPE(std::string, 10)

}
