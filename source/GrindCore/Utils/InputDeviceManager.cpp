#include "stdafx.h"
#include "InputDeviceManager.h"
#include "IEngineCore.h"

namespace grind
{
	CInputDeviceManager::CInputDeviceManager(int clientWidth, int clientHeight)
	{
		mNextListenerId.store(1);

		mGainputManager.SetDisplaySize(clientWidth, clientHeight);
		mMouseDeviceId = mGainputManager.CreateDevice<gainput::InputDeviceMouse>();
		mKeyboardDeviceId = mGainputManager.CreateDevice<gainput::InputDeviceKeyboard>();
		mGamepadDeviceId = mGainputManager.CreateDevice<gainput::InputDevicePad>();
		mGamepadDevice = static_cast<gainput::InputDevicePad*>(mGainputManager.GetDevice(mGamepadDeviceId));

		mInputDevices[(int)EInputDeviceType::Mouse] = mGainputManager.GetDevice(mMouseDeviceId);
		mInputDevices[(int)EInputDeviceType::Keyboard] = mGainputManager.GetDevice(mKeyboardDeviceId);
		mInputDevices[(int)EInputDeviceType::Gamepad] = mGamepadDevice;

		mDeviceButtonListenerId = mGainputManager.AddListener(this);
		//memset(mButtonPressStates, 0, sizeof(mButtonPressStates));


		//{
		//	HWND hwnd = (HWND)GetEngineCore()->GetMainWindow()->GetWindowHandle();
		//	RECT rc;
		//	GetClientRect(hwnd, &rc);
		//	POINT pt = { rc.left, rc.top };
		//	POINT pt2 = { rc.right, rc.bottom };
		//	ClientToScreen(hwnd, &pt);
		//	ClientToScreen(hwnd, &pt2);

		//	SetRect(&rc, pt.x, pt.y, pt2.x, pt2.y);
		//	ClipCursor(&rc);
		//}

		InitializeCursorResources();

		
	}

	CInputDeviceManager::~CInputDeviceManager()
	{
		mGainputManager.RemoveListener(mDeviceButtonListenerId);
	}

	void CInputDeviceManager::Update(float fDelta)
	{
		mGainputManager.Update();
	}

	bool CInputDeviceManager::IsButtonPressed(EInputDeviceType deviceType, InputKey key) const
	{
		auto inputState = mInputDevices[(int)deviceType]->GetInputState();
		return inputState->GetBool(key);
	}

#ifdef _WIN32
	void CInputDeviceManager::HandleMessage(const MSG& msg)
	{
		mGainputManager.HandleMessage(msg);
	}
#endif // _WIN32

	IInputDeviceManager::ListenerId CInputDeviceManager::AddButtonListener(EInputDeviceType deviceType, TPressEventHandler handler)
	{
		auto id = mNextListenerId.fetch_add(1);
		SEventListener<TPressEventHandler> listener;
		listener.id = id;
		listener.handler = std::move(handler);
		mButtonEventListenerLists[(int)deviceType].push_back(std::move(listener));
		return id;
	}

	void CInputDeviceManager::RemoveButtonListener(EInputDeviceType deviceType, ListenerId id)
	{
		auto& listenerList = mButtonEventListenerLists[(int)deviceType];
		for (auto it = listenerList.begin(); it != listenerList.end(); it++) {
			if (it->id == id) {
				listenerList.erase(it);
				break;
			}
		}
	}

	IInputDeviceManager::ListenerId CInputDeviceManager::AddMoveListener(EInputDeviceType deviceType, TMoveEventHandler handler)
	{
		auto id = mNextListenerId.fetch_add(1);
		SEventListener<TMoveEventHandler> listener;
		listener.id = id;
		listener.handler = std::move(handler);
		mMoveEventListenerLists[(int)deviceType].push_back(std::move(listener));
		return id;
	}

	void CInputDeviceManager::RemoveMoveListener(EInputDeviceType deviceType, ListenerId id)
	{
		auto& listenerList = mMoveEventListenerLists[(int)deviceType];
		for (auto it = listenerList.begin(); it != listenerList.end(); it++) {
			if (it->id == id) {
				listenerList.erase(it);
				break;
			}
		}
	}

	bool CInputDeviceManager::OnDeviceButtonBool(gainput::DeviceId deviceId, gainput::DeviceButtonId deviceButton,
		bool oldValue, bool newValue)
	{
		EInputDeviceType deviceType = GetDeviceType(deviceId);
		if (deviceType == EInputDeviceType::Unknown)
			return false;

		bool bPressed = true;
		assert(oldValue != newValue);
		if (oldValue == false && newValue == true)
			bPressed = true;
		else if (oldValue == true && newValue == false)
			bPressed = false;

		//mButtonPressStates[(int)deviceType][deviceButton] = bPressed;

		auto& listenerList = mButtonEventListenerLists[(int)deviceType];
		for (auto& it : listenerList) {
			it.handler(deviceButton, bPressed);
		}
		return false;
	}

	bool CInputDeviceManager::OnDeviceButtonFloat(gainput::DeviceId deviceId, gainput::DeviceButtonId deviceButton, 
		float oldValue, float newValue)
	{
		EInputDeviceType deviceType = GetDeviceType(deviceId);
		if (deviceType == EInputDeviceType::Unknown)
			return false;

		SInputMovementData data;
		data.DeviceType = deviceType;
		data.Key = deviceButton;
		data.Value = newValue;
		data.OldValue = oldValue;
		data.DeltaValue = newValue - oldValue;

		auto& listenerList = mMoveEventListenerLists[(int)deviceType];
		for (auto& it : listenerList) {
			it.handler(data);
		}
		return false;
	}

	void CInputDeviceManager::SetGamepadViberate(float fLeftMotorSpeed, float fRightMotorSpeed, float fLastTime)
	{
		if (!mGamepadDevice)
			return;
	}

	void CInputDeviceManager::StopGamepadViberate()
	{

	}

	bool CInputDeviceManager::IsGamepadViberating() const
	{
		return false;
	}

	void CInputDeviceManager::ShowMouseCursor(bool bDisplay)
	{
#ifdef _WIN32
		if (bDisplay) {
			while (ShowCursor(TRUE) <= 0);
		}
		else {
			while (ShowCursor(FALSE) > 0);
		}

		//ShowCursor((BOOL)bDisplay);
#endif // _WIN32
	}

	void CInputDeviceManager::SetMouseCursorPos(int x, int y)
	{
#ifdef _WIN32
		IWindow* pWindow = GetEngineCore()->GetMainWindow();
		int w = pWindow->GetWidth();
		int h = pWindow->GetHeight();
		Vector2i screenPos = pWindow->GetPosFromClientToScreen(Vector2i(w / 2, h / 2));
		SetCursorPos(screenPos.x, screenPos.y);
#endif // _WIN32
	}

	float CInputDeviceManager::GetFloat(EInputDeviceType deviceType, InputKey key)
	{
		return mInputDevices[(int)deviceType]->GetFloat(key);
	}

	float CInputDeviceManager::GetFloatPrevious(EInputDeviceType deviceType, InputKey key)
	{
		return mInputDevices[(int)deviceType]->GetFloatPrevious(key);
	}

	void CInputDeviceManager::InitializeCursorResources()
	{
#ifdef _WIN32
		HINSTANCE hInstance = NULL;
		mCursorResources[(int)ECursorImage::AppStarting] = LoadCursor(hInstance, IDC_APPSTARTING);
		mCursorResources[(int)ECursorImage::Arrow] = LoadCursor(hInstance, IDC_ARROW);
		mCursorResources[(int)ECursorImage::Cross] = LoadCursor(hInstance, IDC_CROSS);
		mCursorResources[(int)ECursorImage::Hand] = LoadCursor(hInstance, IDC_HAND);
		mCursorResources[(int)ECursorImage::Help] = LoadCursor(hInstance, IDC_HELP);
		mCursorResources[(int)ECursorImage::IBeam] = LoadCursor(hInstance, IDC_IBEAM);
		mCursorResources[(int)ECursorImage::Wait] = LoadCursor(hInstance, IDC_WAIT);
#endif
	}

	void CInputDeviceManager::SetMouseCursorImage(ECursorImage eImage)
	{
#ifdef _WIN32
		HCURSOR hCursor = mCursorResources[(int)eImage];
		SetCursor(hCursor);
#endif
	}

}

