#pragma once
#include "IFileSystem.h"

namespace grind
{
	class CFileSystem : public IFileSystem
	{
	public:
		CFileSystem();

		virtual FILE* OpenFile(const char* filename, const char* mode) override;
		virtual void CloseFile(FILE* fp) override;
		virtual void GetFileFullPath(const char* filename, char szFullPath[]) override;

		virtual const char* GetBasePath() const override { return mBasePath; }
		virtual void IterateFilesInDirectory(const char* dir, const char* pattern, std::function<void(const char*)> cb) override;
		virtual void IterateFilesInDirectory(const char* dir, const char* suffix, bool bSearchSubDir, std::function<void(const char*)> cb) override;
	private:
		char	mBasePath[MAX_PATH];
	};

}


