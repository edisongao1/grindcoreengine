#include "stdafx.h"
#include "FileSystem.h"
#include <io.h>

namespace grind
{
	CFileSystem::CFileSystem()
	{
		memset(mBasePath, 0, sizeof(mBasePath));
		//FILE* fp = fopen_s("resource_path.txt", "r");
		FILE *fp = nullptr;
		errno_t err = fopen_s(&fp, "resource_path.txt", "r");
		if (err == 0)
		{
			fgets(mBasePath, MAX_PATH, fp);
			auto len = strlen(mBasePath);
			if (mBasePath[len - 1] == '\n') {
				mBasePath[len - 1] = 0;
			}
			fclose(fp);
		}
	}

	FILE* CFileSystem::OpenFile(const char* filename, const char* mode)
	{
		char szFilePath[MAX_PATH];
		GetFileFullPath(filename, szFilePath);
		FILE* fp = nullptr;
		errno_t err = fopen_s(&fp, szFilePath, mode);
		if (err != 0)
			return nullptr;
		return fp;
	}

	void CFileSystem::CloseFile(FILE* fp)
	{
		if (fp) {
			fclose(fp);
		}
	}

	void CFileSystem::GetFileFullPath(const char* filename, char szFullPath[])
	{
		//sprintf_s(szFullPath, MAX_PATH, "%s/%s", mBasePath, filename);
		sprintf(szFullPath, "%s/%s", mBasePath, filename);
	}

	void CFileSystem::IterateFilesInDirectory(const char* dir, const char* suffix, bool bSearchSubDir, std::function<void(const char*)> cb)
	{
		char szDirPath[MAX_PATH];
		GetFileFullPath(dir, szDirPath);

		char szFilePathPattern[MAX_PATH];
		sprintf_s(szFilePathPattern, "%s/*", szDirPath);

		_finddata_t	fileInfo;
		intptr_t handle = _findfirst(szFilePathPattern, &fileInfo);

		if (handle == -1L) {
			return;
		}

		auto suffixLength = strlen(suffix);

		do {
			if (fileInfo.attrib & _A_SUBDIR)
			{
				if (bSearchSubDir && fileInfo.name[0] != '.')
				{
					char szSubDir[MAX_PATH];
					sprintf_s(szSubDir, "%s/%s", dir, fileInfo.name);
					IterateFilesInDirectory(szSubDir, suffix, bSearchSubDir, cb);
				}
			}
			else {
				auto len = strlen(fileInfo.name);
				auto pNameAtSuffixPos = fileInfo.name + (len - suffixLength);
				if (_stricmp(pNameAtSuffixPos, suffix) == 0)
				{
					char szFilePath[MAX_PATH];
					char szFullFilePath[MAX_PATH];
					sprintf_s(szFilePath, "%s/%s", dir, fileInfo.name);
					GetFileFullPath(szFilePath, szFullFilePath);
					cb(szFullFilePath);
				}
			}

		} while (!_findnext(handle, &fileInfo));
		_findclose(handle);
	}

	void CFileSystem::IterateFilesInDirectory(const char* dir, const char* pattern, std::function<void(const char*)> cb)
	{
		char szDirPath[MAX_PATH];
		GetFileFullPath(dir, szDirPath);

		char szFilePathPattern[MAX_PATH];
		sprintf_s(szFilePathPattern, "%s/%s", szDirPath, pattern);

		_finddata_t	fileInfo;
		intptr_t handle = _findfirst(szFilePathPattern, &fileInfo);

		if (handle == -1L) {
			return;
		}

		do {
			char szFilePath[MAX_PATH];
			char szFullFilePath[MAX_PATH];
			sprintf_s(szFilePath, "%s/%s", dir, fileInfo.name);
			GetFileFullPath(szFilePath, szFullFilePath);
			cb(szFullFilePath);
		} while (!_findnext(handle, &fileInfo));
		_findclose(handle);
	}

}

