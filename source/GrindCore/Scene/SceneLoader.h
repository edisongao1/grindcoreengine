#pragma once

#include "IEngineCore.h"
#include "GrindECS.h"
#include "tinyxml2.h"

namespace grind
{
	class CSceneManager;

	class CSceneLoader
	{
	public:
		CSceneLoader();
		CSceneManager* LoadFromXml(const char* szFilePath);
		//void LoadEntities(tinyxml2::XMLElement* EntitiesNode);

		void LoadEntities(tinyxml2::XMLElement* parent);
		void LoadEntity(tinyxml2::XMLElement* EntityNode);
		Entity* LoadStaticMeshEntity(const char* szName, tinyxml2::XMLElement* ComponentsNode);
		Entity* LoadCameraEntity(const char* szName, tinyxml2::XMLElement* ComponentsNode);
	private:
		void LoadTransformComponent(tinyxml2::XMLElement* node, TransformComponent& component);
		bool LoadMeshComponent(tinyxml2::XMLElement* node, MeshComponent& component);
		Entity* ExtendEntity(Entity* pEntity, tinyxml2::XMLElement* ComponentNode);

		CSceneManager* mSceneManager = nullptr;
		IRenderResourceManager* mRenderResourceManager = nullptr;
		IMeshManager* mMeshManager = nullptr;
		IMaterialManager* mMaterialManager = nullptr;
		ISceneLoadListener* mSceneLoadListener = nullptr;

		// temporary values
	private:
		std::string		mMainCameraName;
	};
}