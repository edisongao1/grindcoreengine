#include "Camera.h"
#include "IEngineCore.h"
#include <gainput/gainput.h>
#include "IConsole.h"

namespace grind
{

	void CCamera::Move(const Vector3f& dir, float fDist)
	{
		mPosition += dir * fDist;
	}

	void CCamera::Rotate(int localAxis, float radians)
	{
		//Matrix4x4 matRot;
		//matRot.setRotationAxisRadians(radians, mLocalAxes[localAxis]);
		//matRot.rotateVect(mLocalAxes[0]);
		//matRot.rotateVect(mLocalAxes[1]);
		//matRot.rotateVect(mLocalAxes[2]);
	}

	void CCamera::LookAt(const Vector3f& targetPos)
	{
		//Vector3f dir = (targetPos - mPosition).normalize();
		//Vector3f right = mLocalAxes[EAxis::Y].crossProduct(dir).normalize();
		//Vector3f up = dir.crossProduct(right).normalize();
		//mLocalAxes[(int)EAxis::X] = right;
		//mLocalAxes[(int)EAxis::Y] = up;
		//mLocalAxes[(int)EAxis::Z] = dir;

		Vector3f look = glm::normalize(targetPos - mPosition);
		Vector3f right = glm::normalize(glm::cross(mLocalAxes[(int)EAxis::Y], -look));
		Vector3f up = glm::normalize(glm::cross(-look, right));

		mLocalAxes[(int)EAxis::X] = right;
		mLocalAxes[(int)EAxis::Y] = up;
		mLocalAxes[(int)EAxis::Z] = -look;
	}

	void CCamera::Look(const Vector3f& lookDir)
	{
		//Vector3f dir = lookDir.getNormalized();
	}

	void CCamera::Update(float fDeltaTime)
	{
		/* adjust right, up, look vectors, to prevent accumulated error. */

		Vector3f backLook = mLocalAxes[(int)EAxis::Z];
		Vector3f up = glm::normalize(glm::cross(backLook, mLocalAxes[(int)EAxis::X]));
		Vector3f right = glm::normalize(glm::cross(up, backLook));

		mLocalAxes[(int)EAxis::X] = right;
		mLocalAxes[(int)EAxis::Y] = up;
		mLocalAxes[(int)EAxis::Z] = backLook;

		/* update the view-matrix, proj-matrix, view-proj-matrix */
		mViewMatrix = glm::lookAtRH(mPosition, mPosition - mLocalAxes[(int)EAxis::Z], mLocalAxes[(int)EAxis::Y]);

		if (!mIsOrthogonal)
		{
			mProjMatrix = glm::perspectiveFovRH_ZO(mFovAngleY, mProjectionSize.x, mProjectionSize.y, mNearZ, mFarZ);
		}
		mViewProjMatrix = mProjMatrix * mViewMatrix;
		mInvViewProjMatrix = glm::inverse(mViewProjMatrix);
		mFrustum.BuildFromViewProjMatrix(mViewProjMatrix, true);
	}

	Vector3f CFpsCamera::WORLD_UP(0, 1.0f, 0);

	CFpsCamera::CFpsCamera(const Vector3f& position,
		const Vector2f& projectionSize,
		float fov,
		float nearZ,
		float farZ,
		float fMaxUpAngle,
		float fMaxDownAngle)
		:ICamera(position, projectionSize, fov, nearZ, farZ, false)
		, IFpsCameraInterface(fMaxUpAngle, fMaxDownAngle)
	{
		IInputDeviceManager* pInputDeviceManager = GetEngineCore()->GetInputDeviceManager();
		pInputDeviceManager->AddMouseMoveListener([this](SInputMovementData& data) {
			this->OnMouseMovement(data);
		});
		pInputDeviceManager->AddKeyboardButtonListener([=](IInputDeviceManager::InputKey key, bool bPressed) {
			if (key == gainput::KeyQ && bPressed) {
				pInputDeviceManager->SetMouseCursorPos(0, 0);
			}
		});

		SetMoveEnabled(true);

		RECT rect;
		rect.left = 0;
		rect.right = 1920;
		rect.bottom = 1080;
		rect.top = 0;
		ClipCursor(&rect);
	}


	void CFpsCamera::Move(const Vector3f& dir, float fDist)
	{
		//switch (localAxis)
		//{
		//case EAxis::X:
		//	MoveRight(fDist);
		//	break;
		//case EAxis::Y:
		//	MoveUp(fDist);
		//	break;
		//case EAxis::Z:
		//	MoveForward(fDist);
		//	break;
		//default:
		//	break;
		//}
	}

	void CFpsCamera::MoveRight(float fDist)
	{
		//Vector3f right = WORLD_UP.crossProduct(mWalkDir).getNormalized();
		//mPosition += right * fDist;
		Vector3f right = glm::normalize(glm::cross(WORLD_UP, -mWalkDir));
		mPosition += right * fDist;
	}

	void CFpsCamera::MoveUp(float fDist)
	{
		mPosition += WORLD_UP * fDist;
	}

	void CFpsCamera::MoveForward(float fDist)
	{
		mPosition += mWalkDir * fDist;
	}

	void CFpsCamera::Rotate(int localAxis, float radians)
	{
		//if (localAxis == EAxis::X)
		//{
		//	mPitchAngle += radians;
		//	AdjustPitchAngle();
		//}
		//else if (localAxis == EAxis::Y)
		//{
		//	Matrix4x4 matRot;
		//	matRot.setRotationAxisRadians(radians, WORLD_UP);
		//	matRot.rotateVect(mWalkDir);
		//}

		if (localAxis == (int)EAxis::X)
		{
			mPitchAngle += radians;
			AdjustPitchAngle();
		}
		else if (localAxis == (int)EAxis::Y)
		{
			//Matrix4x4 matRot;
			//matRot.setRotationAxisRadians(radians, WORLD_UP);
			//matRot.rotateVect(mWalkDir);

			glm::quat q = glm::identity<glm::quat>();
			q = glm::rotate(q, radians, WORLD_UP);
			mWalkDir = q* mWalkDir;
		}
	}

	void CFpsCamera::LookAt(const Vector3f& targetPos)
	{
		Vector3f look = glm::normalize(targetPos - mPosition);
		Vector3f right = glm::normalize(glm::cross(mLocalAxes[(int)EAxis::Y], -look));
		Vector3f up = glm::normalize(glm::cross(-look, right));
		
		mPitchAngle = std::atan2f(look.y, std::sqrtf(look.x * look.x + look.z * look.z));
		if (mPitchAngle < 0)
			mPitchAngle += _2PI;
		if (mPitchAngle >= _2PI)
			mPitchAngle -= _2PI;

		///* 判断当前是抬头还是低头，如果是抬头，则angle角为负，否则为正
		//这里根据look向量与worldup向量的点积判断，如果点积为正，则为抬头，
		//如果点积为负，则为低头 */

		AdjustPitchAngle();
		mWalkDir = glm::normalize(Vector3f(look.x, 0, look.z));

		//mWalkDir = dir;
	}

	void CFpsCamera::Look(const Vector3f& lookdir)
	{
		//Vector3f look = lookdir.getNormalized();
		//Vector3f right = WORLD_UP.crossProduct(look).getNormalized();

		//Vector3f dir = right.crossProduct(WORLD_UP).getNormalized();

		//Vector3f degrees = look.getHorizontalAngle();
		//mPitchAngle = irr::core::degToRad(degrees.x);

		///* 判断当前是抬头还是低头，如果是抬头，则angle角为负，否则为正
		//这里根据look向量与worldup向量的点积判断，如果点积为正，则为抬头，
		//如果点积为负，则为低头 */

		//AdjustPitchAngle();
		//mWalkDir = dir;
	}

	void CFpsCamera::Creep()
	{

	}

	void CFpsCamera::Stand()
	{

	}

	void CFpsCamera::Jump()
	{

	}

	void CFpsCamera::SetMoveEnabled(bool bEnabled)
	{
		auto pInput = GetEngineCore()->GetInputDeviceManager();
		if (bEnabled) {
			//pInput->ShowMouseCursor(false);
			pInput->SetMouseCursorPos(0, 0);
			mMoveEnabled = true;
		}
		else {
		//	pInput->ShowMouseCursor(true);
			mMoveEnabled = false;
		}
	}

	void CFpsCamera::AdjustPitchAngle()
	{
		if (mPitchAngle > mMaxUpAngle)
			mPitchAngle = mMaxUpAngle;

		if (mPitchAngle < -mMaxDownAngle)
			mPitchAngle = -mMaxDownAngle;
	}

	void CFpsCamera::Update(float fDeltaTime)
	{
		auto pInput = GetEngineCore()->GetInputDeviceManager();
		float mouseX = pInput->GetFloat(EInputDeviceType::Mouse, gainput::MouseAxisX);
		float mouseY = pInput->GetFloat(EInputDeviceType::Mouse, gainput::MouseAxisY);

		float dx = mMouseMovementDelta.x;
		float dy = mMouseMovementDelta.y;

		if (dy * dy + dx * dx < 0.3f * 0.3f)
		{
			if (mMouseMovementDelta.x != 0.0f/* && fabs(mMouseMovementDelta.X) < 0.3f*/) {
				Rotate((int)EAxis::Y, -mMouseMovementDelta.x * mRotateSpeed);
			}
			if (mMouseMovementDelta.y != 0.0f/* && fabs(mMouseMovementDelta.Y) < 0.3f*/) {
				Rotate((int)EAxis::X, -mMouseMovementDelta.y * mRotateSpeed);
			}
		}

		Vector3f right = glm::normalize(glm::cross(mWalkDir, WORLD_UP));
		Quaternion q = glm::identity<Quaternion>();
		q = glm::rotate(q, mPitchAngle, right);

		Vector3f look = q * mWalkDir;
		Vector3f up = glm::normalize(glm::cross(-look, right));
		mViewMatrix = glm::lookAtRH(mPosition, mPosition + look, up);

		if (!mIsOrthogonal)
		{
			mProjMatrix = glm::perspectiveFovRH_ZO(mFovAngleY, mProjectionSize.x, mProjectionSize.y, mNearZ, mFarZ);
		}

		mViewProjMatrix =  mProjMatrix * mViewMatrix;
		mInvViewProjMatrix = glm::inverse(mViewProjMatrix);

		mMouseMovementDelta.x = mMouseMovementDelta.y = 0;
		if (mMoveEnabled)
		{
			if (mouseX < 0.1 || mouseY < 0.1 || mouseX > 0.9f || mouseY > 0.9f)
			{
				pInput->SetMouseCursorPos(0, 0);
			}
		}

		mFrustum.BuildFromViewProjMatrix(mViewProjMatrix, true);
	}

	void CFpsCamera::OnMouseMovement(SInputMovementData& data)
	{
		// data:
		if (data.Key == gainput::MouseAxisX)
		{
			mMouseMovementDelta.x = data.DeltaValue;
			//mMouseMovementDelta.X = data.Value - 0.5f;
			mLastMousePosition.x = data.Value;
			//IConsole::Print("camera move x: %f, %f", data.DeltaValue, data.Value);
		}
		else if (data.Key == gainput::MouseAxisY)
		{
			mMouseMovementDelta.y = data.DeltaValue;
			//mMouseMovementDelta.Y = data.Value - 0.5f;
			mLastMousePosition.y = data.Value;
			//IConsole::Print("camera move y: %f, %f", data.DeltaValue, data.Value);
		}
	}

}

