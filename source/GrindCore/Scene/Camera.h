#pragma once
#include "ICamera.h"

namespace grind
{
	class CCamera : public ICamera
	{
	public:
		CCamera(const Vector3f& position,
			const Vector2f& projectionSize,
			float fov,
			float nearZ,
			float farZ,
			bool bOrthogonal)
			:ICamera(position, projectionSize, fov, nearZ, farZ, bOrthogonal)
		{

		}

		virtual void Move(const Vector3f& dir, float fDist) override;

		virtual void Rotate(int localAxis, float radians) override;

		virtual void LookAt(const Vector3f& targetPos) override;

		virtual void Look(const Vector3f& lookDir) override;

		virtual void MoveRight(float fDist) override
		{
			Move(mLocalAxes[(int)EAxis::X], fDist);
		}
		virtual void MoveUp(float fDist) override
		{
			Move(mLocalAxes[(int)EAxis::Y], fDist);
		}
		virtual void MoveForward(float fDist) override 
		{
			Move(-mLocalAxes[(int)EAxis::Z], fDist);
		}

		virtual void Update(float fDeltaTime) override;

	};

	class CFpsCamera : public ICamera, public IFpsCameraInterface
	{
	public:
		CFpsCamera(const Vector3f& position,
			const Vector2f& projectionSize,
			float fov,
			float nearZ,
			float farZ,
			float fMaxUpAngle,
			float fMaxDownAngle);


		virtual void Move(const Vector3f& dir, float fDist) override;
		virtual void MoveRight(float fDist) override;
		virtual void MoveUp(float fDist) override;
		virtual void MoveForward(float fDist) override;
		virtual void Rotate(int localAxis, float radians) override;

		virtual void LookAt(const Vector3f& targetPos) override;
		virtual void Look(const Vector3f& dir) override;

		virtual void Creep() override;
		virtual void Stand() override;
		virtual void Jump() override;
		virtual void SetMoveEnabled(bool bEnabled) override;

		virtual void Update(float fDeltaTime) override;
	
		void OnMouseMovement(SInputMovementData& inputData);

	private:
		void AdjustPitchAngle();
		static Vector3f WORLD_UP;

		//IInputDeviceManager::ListenerId		mMouseMoveListenerId = 0;
		Vector2f							mMouseMovementDelta = Vector2f(0, 0);
		Vector2f							mLastMousePosition;
	};
}

