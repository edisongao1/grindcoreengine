#include "stdafx.h"
#include "SceneManager.h"
#include "../Core/WorkerThread.h"
#include "Camera.h"
#include "Math/IntersectionTest.h"
#include "Action/MeshEntityAction.h"
#include "Action/CameraAction.h"

namespace grind
{
	CSceneManager::CSceneManager()
	{
		mWorld = GetEngineCore()->GetWorld();
		mRenderResourceManager = GetEngineCore()->GetRenderResourceManager();
		mMeshManager = mRenderResourceManager->GetMeshManager();
		mMaterialManager = mRenderResourceManager->GetMaterialManager();
	}

	void CSceneManager::Initialize()
	{
		mMeshEntityAction = std::make_unique<CMeshEntityAction>(this);
		mCameraAction = std::make_unique<CCameraAction>(this);
	}

	Entity* CSceneManager::AddMeshEntity(const char* szName, const TransformComponent& transform, const MeshComponent& meshComponent)
	{
		Entity* pEntity = nullptr;
		// if it is static, pre-calculate aabb
		if (transform.bStatic)
		{
			BoundingBoxComponent boundingBoxComponent;
			IMesh* pMesh = meshComponent.pMesh;
			boundingBoxComponent.aabb = AABB::ComputeTransformedAABB(pMesh->GetAABB(), transform.ToMatrix());
			EntityContext* pContext = GetStaticEntityContext(transform.Position);
			pEntity = pContext->CreateEntity<TransformComponent, MeshComponent, BoundingBoxComponent>(
				transform, meshComponent, boundingBoxComponent);
		}
		else
		{
			EntityContext* pContext = GetMovableEntityContext();
			pEntity = pContext->CreateEntity<TransformComponent, MeshComponent>(transform, meshComponent);
		}
		return pEntity;
	}

	Entity* CSceneManager::AddCamera(const char* szName, const TransformComponent& transform,
		const CameraComponent& cameraComponent)
	{
		EntityContext* pContext = GetMovableEntityContext();
		Entity* pEntity = pContext->CreateEntity(transform, cameraComponent);
		return pEntity;
	}

	bool CSceneManager::MigrateEntity(Entity*& pEntity, EntityContext* pNewContext)
	{
		EntityContext* pOldContext = pEntity->GetContext();
		if (pOldContext == pNewContext)
			return pEntity;

		// migrate entity from one context to another
		Entity* pNewEntity = pNewContext->CreateEntity(pEntity->GetArchetype());
		pNewContext->CopyEntityData(pNewEntity, pEntity);

		EntityID oldEntityId = pEntity->GetEntityID();
		pEntity->Release();

		RefreshEntityReference(oldEntityId, pNewEntity);
		return pNewEntity;
	}

	void CSceneManager::RefreshEntityReference(EntityID oldEntityId, Entity* pNewEntity)
	{
		// check if the entity has a runtime name
		auto it = mEntityIdAndNameMap.find(oldEntityId);
		if (it != mEntityIdAndNameMap.end())
		{
			std::string name = it->second;
			mEntityIdAndNameMap.erase(it);

			// insert new item
			mEntityIdAndNameMap.insert({pNewEntity->GetEntityID(), name});

			// modify the item
			mNameAndEntityMap[name] = pNewEntity->GetEntityID();
		}
	}

	Entity* CSceneManager::GetEntityByName(const char* szEntityName)
	{
		auto it = mNameAndEntityMap.find(szEntityName);
		if (it == mNameAndEntityMap.end())
			return nullptr;
		EntityID eid = it->second;
		return mWorld->GetEntity(eid);
	}

	Entity* CSceneManager::GetMainCamera()
	{
		return mWorld->GetEntity(mMainCameraId);
	}

	bool CSceneManager::SetMainCamera(Entity* pEntity)
	{
		if (!pEntity->ContainAllComponents<TransformComponent, CameraComponent>())
			return false;
		mMainCameraId = pEntity->GetEntityID();
		return true;
	}

	void CSceneManager::OnNamedEntityAdded(const char* szEntityName, Entity* pEntity)
	{
		EntityID entityId = pEntity->GetEntityID();
		if (szEntityName != nullptr && szEntityName[0] == '@')
		{
			std::string strName(szEntityName);
			mEntityIdAndNameMap[entityId] = strName;
			mNameAndEntityMap[strName] = entityId;
		}
	}

	void CSceneManager::DestroyEntity(Entity* pEntity)
	{
		EntityID entityId = pEntity->GetEntityID();
		// check if the entity has a runtime name
		auto it = mEntityIdAndNameMap.find(entityId);
		if (it != mEntityIdAndNameMap.end())
		{
			std::string name = it->second;
			mEntityIdAndNameMap.erase(it);
			mNameAndEntityMap.erase(name);
		}

		pEntity->Release();
	}
}
