#pragma once

#include "IEngineCore.h"
#include "ISceneManager.h"
#include "RenderItem.h"

namespace grind
{
	using TCullStaticMeshJob = ParallelJob<true, CMeshRenderItemQueue, 
		TransformComponent, MeshComponent, BoundingBoxComponent>;
	
	class CWorkerThread;




	class CSceneManager : public ISceneManager
	{
		friend class CSceneLoader;
	public:
		//struct NamedSceneEntityKey
		//{
		//	EntityID			entityId = 0;
		//	EntityContext*		pEntityContext = nullptr;
		//	NamedSceneEntityKey(){}
		//	NamedSceneEntityKey(EntityID entityId, EntityContext* pContext)
		//	: entityId(entityId), pEntityContext(pContext){}
		//};

		CSceneManager();

		virtual void Initialize() override;
		virtual void CreateEntityContexts() = 0;
		//virtual EntityContext* GetEntityContext(const Vector3f& pos) = 0;
		virtual void CullEntities_WorkerThread(CWorkerThread* pWorkerThread) = 0;
		virtual void DestroyEntity(Entity* pEntity) override;


		virtual bool MigrateEntity(Entity*& pEntity, EntityContext* pNewContext) override;
		void RefreshEntityReference(EntityID oldEntityId, Entity* pNewEntity);
		virtual Entity* GetEntityByName(const char* szName) override;
		
		virtual Entity* GetMainCamera() override;
		virtual bool SetMainCamera(Entity* pEntity) override;

		virtual Entity* AddMeshEntity(const char* szName,
			const TransformComponent& transform,
			const MeshComponent& meshComponent) override;

		virtual Entity* AddCamera(const char* szName, 
			const TransformComponent& transform,
			const CameraComponent& cameraComponent) override;



	protected:
		void OnNamedEntityAdded(const char* szName, Entity* pEntity);

		World*						mWorld = nullptr;
		IRenderResourceManager*		mRenderResourceManager = nullptr;
		IMeshManager*				mMeshManager = nullptr;
		IMaterialManager*			mMaterialManager = nullptr;

		//std::unordered_map<std::string, NamedSceneEntityKey>		mNameAndEntityMap;
		std::unordered_map<std::string, EntityID>		mNameAndEntityMap;
		std::unordered_map<EntityID, std::string>		mEntityIdAndNameMap;
		EntityID					mMainCameraId = 0;
	};


}
