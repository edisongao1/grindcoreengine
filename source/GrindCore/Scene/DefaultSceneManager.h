#pragma once

#include "SceneManager.h"
#include "IGraphicsSystem.h"

namespace grind
{
	class CDefaultSceneManager : public CSceneManager
	{
	public:
		CDefaultSceneManager();
		~CDefaultSceneManager();

		//virtual void CullStaticMeshNodes() override;
		virtual void CullEntities() override;
		virtual void CullEntities_WorkerThread(CWorkerThread* pWorkerThread);

		//virtual EntityContext* GetEntityContext(const Vector3f& pos) override
		//{
		//	return mEntityContext;
		//}

		virtual EntityContext* GetStaticEntityContext(const Vector3f& pos) override
		{
			return mEntityContext;
		}

		virtual EntityContext* GetMovableEntityContext() override
		{
			return mEntityContext;
		}

	protected:
		void CreateEntityContexts();


		EntityContext*			mEntityContext = nullptr;

		TCullStaticMeshJob		mCullStaticMeshJob;
		void CullStaticMeshJobFunction(CMeshRenderItemQueue* renderItemQueue, Entity* pEntity,
			TransformComponent* pTransform, MeshComponent* pStaticMeshComponent,
			BoundingBoxComponent* pBoundingBox);

		SCameraViewTransformation	mCameraTransformation;
	};
}