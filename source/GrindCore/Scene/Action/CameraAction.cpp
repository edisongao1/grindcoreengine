#include "stdafx.h"
#include "CameraAction.h"
#include "IGraphicsSystem.h"
#include "IEngineCore.h"
#include "IRenderDevice.h"

namespace grind
{
	SCameraViewTransformation CCameraAction::GetCameraTransformations(Entity*pEntity)
	{
		SCameraViewTransformation info;
		
		TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
		CameraComponent* pCameraComponent = pEntity->GetComponent<CameraComponent>();

		IRenderDevice* pRenderDevice = GetEngineCore()->GetRenderDevice();
		unsigned int viewportWidth = pRenderDevice->GetSettings().ViewportWidth;
		unsigned int viewportHeight = pRenderDevice->GetSettings().ViewportHeight;
		
		// set scale to 1 by force
		pTransform->Scale.x = pTransform->Scale.y = pTransform->Scale.z = 1.0f;

		info.InvViewMatrix = pTransform->ToMatrix();
		info.ViewMatrix = glm::inverse(info.InvViewMatrix);

		// compute project matrix
		if (pCameraComponent->bOrthogonal)
		{
			//info.ProjMatrix = glm::orthoRH_ZO()
		}
		else
		{
			info.ProjMatrix = glm::perspectiveFovRH_ZO(pCameraComponent->Fov,
				(float)viewportWidth, (float)viewportHeight,
				pCameraComponent->NearZ, pCameraComponent->FarZ);
		}

		info.InvProjMatrix = glm::inverse(info.ProjMatrix);
		info.ViewProjMatrix = info.ProjMatrix * info.ViewMatrix;
		info.InvViewProjMatrix = glm::inverse(info.ViewProjMatrix);
		info.Position = pTransform->Position;
		info.FarZ = pCameraComponent->FarZ;
		info.NearZ = pCameraComponent->NearZ;

		info.mFrustum.BuildFromViewProjMatrix(info.ViewProjMatrix, true);

		return info;
	}

	void CCameraAction::Relocate(Entity*& pEntity, TransformComponent* pTransform)
	{
		//CameraComponent* pCameraComponent = pEntity->GetComponent<CameraComponent>();
		

	}

	AABB CCameraAction::GetWorldBoundingBox(Entity* pEntity) const
	{
		return AABB();
	}

	void CCameraAction::LookAt(Entity* pEntity, const Vector3f& target, const Vector3f& worldUp /*= Vector3f(0, 1, 0)*/)
	{
		TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
		
		Vector3f look = glm::normalize(target - pTransform->Position);
		Vector3f right = glm::normalize(glm::cross(worldUp, -look));
		Vector3f up = glm::normalize(glm::cross(-look, right));

		//Matrix4x4 rotation = glm::lookAtRH(pTransform->Position, target, worldUp);
		Matrix4x4 rotation = Matrix4x4(1.0f);
		rotation[0] = Vector4f(right.x, right.y, right.z, 0);
		rotation[1] = Vector4f(up.x, up.y, up.z, 0);
		rotation[2] = Vector4f(-look.x, -look.y, -look.z, 0);
		pTransform->Rotation = glm::quat_cast(rotation);
	}

}