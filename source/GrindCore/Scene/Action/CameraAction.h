#pragma once

#include "IEntityAction.h"

namespace grind
{
	class CCameraAction : public ICameraAction
	{
	public:
		CCameraAction(ISceneManager* pSceneManager) :ICameraAction(pSceneManager) {}
		virtual SCameraViewTransformation GetCameraTransformations(Entity*pEntity) override;
		virtual void Relocate(Entity*& pEntity, TransformComponent* pTransform);
		virtual AABB GetWorldBoundingBox(Entity* pEntity) const override;
		virtual void LookAt(Entity* pEntity, const Vector3f& target, const Vector3f& up = Vector3f(0, 1, 0)) override;

	};
}
