#pragma once

#include "IEntityAction.h"

namespace grind
{
	class CSceneManager;

	class CMeshEntityAction : public IMeshEntityAction
	{
	public:
		CMeshEntityAction(ISceneManager* pSceneManager) : IMeshEntityAction(pSceneManager){}
		virtual void Relocate(Entity*& pEntity, TransformComponent* pTransform);
		virtual AABB GetWorldBoundingBox(Entity* pEntity) const override;
	};
}
