#include "stdafx.h"
#include "MeshEntityAction.h"
#include "../SceneManager.h"

namespace grind
{
	
	void CMeshEntityAction::Relocate(Entity*& pEntity, TransformComponent* pTransform)
	{
		if (!pTransform->bStatic)
			return;

		EntityContext* pOldContext = pEntity->GetContext();
		EntityContext* pNewContext = mSceneManager->GetStaticEntityContext(pTransform->Position);		
		
		// migrate entity
		if (pOldContext != pNewContext)
		{
			mSceneManager->MigrateEntity(pEntity, pNewContext);
			pTransform = pEntity->GetComponent<TransformComponent>();
		}

		// update the aabb component
		BoundingBoxComponent* pBoundingBox = pEntity->GetComponent<BoundingBoxComponent>();
		MeshComponent* pMeshComponent = pEntity->GetComponent<MeshComponent>();
		IMesh* pMesh = pMeshComponent->pMesh;
		pBoundingBox->aabb = AABB::ComputeTransformedAABB(pMesh->GetAABB(), pTransform->ToMatrix());
	}

	AABB CMeshEntityAction::GetWorldBoundingBox(Entity* pEntity) const
	{
		TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
		if (pTransform->bStatic)
		{
			BoundingBoxComponent* pBoundingBox = pEntity->GetComponent<BoundingBoxComponent>();
			return pBoundingBox->aabb;
		}
		else
		{
			MeshComponent* pMeshComponent = pEntity->GetComponent<MeshComponent>();
			return AABB::ComputeTransformedAABB(pMeshComponent->pMesh->GetAABB(), pTransform->ToMatrix());
		}
	}

}

