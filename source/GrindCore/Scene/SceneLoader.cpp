#include "stdafx.h"
#include "SceneLoader.h"
#include "SceneManager.h"
#include "DefaultSceneManager.h"
#include "tinyxml2.h"
#include "Camera.h"
#include "GenericUtils.h"
#include "IGameFramework.h"
#include "../Graphics/ModelFileLoader.h"
#include "IMesh.h"

using namespace tinyxml2;

namespace grind
{
	template<typename T>
	T ReadChildAttribute(XMLElement* parent, const char* szChildNodeName)
	{
		XMLElement* childNode = parent->FirstChildElement(szChildNodeName);
		return StringParseUtils::Parse<T>(childNode->GetText());
	}

	CSceneLoader::CSceneLoader()
	{
		mRenderResourceManager = GetEngineCore()->GetRenderResourceManager();
		mMeshManager = mRenderResourceManager->GetMeshManager();
		mMaterialManager = mRenderResourceManager->GetMaterialManager();
		mSceneLoadListener = GetEngineCore()->GetGameFramework()->GetSceneLoadListener();
	}

	CSceneManager* CSceneLoader::LoadFromXml(const char* szFilePath)
	{
		IFileSystem* pFileSystem = GetEngineCore()->GetFileSystem();

		char szFileFullPath[256];
		pFileSystem->GetFileFullPath(szFilePath, szFileFullPath);

		//FILE* fp = pFileSystem->OpenFile(szFilePath, "r");
		//if (fp == nullptr)
		//{
		//	return nullptr;
		//}
		tinyxml2::XMLDocument doc;
		if (doc.LoadFile(szFileFullPath) != XML_SUCCESS)
		{
			return nullptr;
		}
		
		XMLElement* Root = doc.RootElement();
		XMLElement* ClassTypeNode = Root->FirstChildElement("ClassType");
		if (ClassTypeNode == nullptr)
		{
			return nullptr;
		}

		const char* szClassType = ClassTypeNode->GetText();
		if (_stricmp(szClassType, "Default") == 0)
		{
			mSceneManager = new CDefaultSceneManager();
		}
		else if (_stricmp(szClassType, "Section") == 0)
		{
			
		}
		else
		{
			return nullptr;
		}

		mSceneManager->Initialize();
		mSceneManager->CreateEntityContexts();

		mMainCameraName = std::string(ReadChildAttribute<const char*>(Root, "MainCamera"));
		
		XMLElement* EntitiesNode = Root->FirstChildElement("Entities");
		LoadEntities(EntitiesNode);

		//pFileSystem->CloseFile(fp);
		IWindow* pWindow = GetEngineCore()->GetMainWindow();
		float fov = 0.25f * PI;
		//mSceneManager->mMainCamera = new CFpsCamera(Vector3f(0, 0, 5.0f),
		//	Vector2f(pWindow->GetWidth(), pWindow->GetHeight()),
		//	fov, 0.1f, 1000.0f, glm::pi<float>() * 0.25f, glm::pi<float>() * 0.25f);
		//mSceneManager->mMainCamera->LookAt(Vector3f(0, 1, 0));

		//auto x1 = Generic::IsAnyOf(1, 2);
		//auto x2 = Generic::IsAnyOf(1, 2, 3, 4);
		//auto x3 = Generic::IsAnyOf(1, 2, 3, 1, 5);

		//auto x4 = Generic::IsAnyOf(1.0f, 2.0f);
		//auto x5 = Generic::IsAnyOf(1.0f, 2.0f, 3.0f, 4.0f);
		//auto x6 = Generic::IsAnyOf(1.0f, 2.0f, 3.0f, 1.0f, 5.0f);
		//auto x7 = Generic::IsAnyOf(1.0f, 1.0f);

		//auto x8 = Generic::IsAnyOf("Hello", "Hello");
		//auto x9 = Generic::IsAnyOf("Hello", "World");
		//auto x10 = Generic::IsAnyOf("Hello", "abc", "def", "k111");
		//auto x11 = Generic::IsAnyOf("Hello", "abc", "Hello", "k111");
		return mSceneManager;
		
	}

	void CSceneLoader::LoadEntities(tinyxml2::XMLElement* parent)
	{
		XMLElement* EntityNode = parent->FirstChildElement();
		while (EntityNode)
		{
			LoadEntity(EntityNode);
			EntityNode = EntityNode->NextSiblingElement();
		}
	}

	void CSceneLoader::LoadEntity(tinyxml2::XMLElement* EntityNode)
	{
		const char* szType = EntityNode->Name();
		XMLElement* NameNode = EntityNode->FirstChildElement("Name");
		const char* szName = NameNode->GetText();
		XMLElement* ComponentsNode = EntityNode->FirstChildElement("Components");
		Entity* pEntity = nullptr;
		if (strcmp(szType, "MeshEntity") == 0) {
			pEntity = LoadStaticMeshEntity(szName, ComponentsNode);
		}
		else if (strcmp(szType, "CameraEntity") == 0) {
			pEntity = LoadCameraEntity(szName, ComponentsNode);
		}
		else {
			pEntity = mSceneLoadListener->OnProcessEntityXML(szName, szType, ComponentsNode, mSceneManager);
		}

		if (pEntity)
		{
			if (szName != nullptr && szName[0] == '@')
			{
				mSceneManager->OnNamedEntityAdded(szName, pEntity);
			}
		}
	}

	Entity* CSceneLoader::LoadStaticMeshEntity(const char* szName, tinyxml2::XMLElement* ComponentsNode)
	{
		TransformComponent transform;
		MeshComponent meshComponent;

		XMLElement* TransformNode = ComponentsNode->FirstChildElement("Transform");
		LoadTransformComponent(TransformNode, transform);

		XMLElement* MeshNode = ComponentsNode->FirstChildElement("Mesh");
		LoadMeshComponent(MeshNode, meshComponent);

		Entity* pEntity = mSceneManager->AddMeshEntity(szName, transform, meshComponent);
		
		XMLElement* nextComponentNode = MeshNode->NextSiblingElement();
		if (nextComponentNode)
		{
			pEntity = ExtendEntity(pEntity, nextComponentNode);
		}
		return pEntity;
	}

	Entity* CSceneLoader::LoadCameraEntity(const char* szName, tinyxml2::XMLElement* ComponentsNode)
	{
		TransformComponent transform;
		CameraComponent cameraComponent;

		XMLElement* TransformNode = ComponentsNode->FirstChildElement("Transform");
		LoadTransformComponent(TransformNode, transform);
		transform.bStatic = false; // set static to false by force

		// create camera component
		XMLElement* CameraNode = ComponentsNode->FirstChildElement("Camera");
		cameraComponent.bOrthogonal = ReadChildAttribute<bool>(CameraNode, "Orthogonal");
		cameraComponent.NearZ = ReadChildAttribute<float>(CameraNode, "NearZ");
		cameraComponent.FarZ = ReadChildAttribute<float>(CameraNode, "FarZ");
		cameraComponent.Fov = ReadChildAttribute<float>(CameraNode, "Fov");
		cameraComponent.ProjectionSize = ReadChildAttribute<Vector2f>(CameraNode, "ProjectionSize");

		Entity* pEntity = mSceneManager->AddCamera(szName, transform, cameraComponent);
		XMLElement* nextComponentNode = CameraNode->NextSiblingElement();
		if (nextComponentNode)
		{
			pEntity = ExtendEntity(pEntity, nextComponentNode);
		}

		if (mMainCameraName == std::string(szName))
		{
			mSceneManager->SetMainCamera(pEntity);
		}
		return pEntity;
	}

	void CSceneLoader::LoadTransformComponent(tinyxml2::XMLElement* node, TransformComponent& transform)
	{
		//XMLElement* ActiveNode = node->FirstChildElement("Active");
		//transform.bActive = StringParseUtils::Parse<bool>(ActiveNode->GetText());
		//XMLElement* StaticNode = node->FirstChildElement("Static");
		//transform.bStatic = StringParseUtils::Parse<bool>(StaticNode->GetText());

		//XMLElement* PositionNode = node->FirstChildElement("Position");
		//transform.Position = StringParseUtils::Parse<Vector3f>(PositionNode->GetText());
		//XMLElement* RotationNode = node->FirstChildElement("Rotation");
		//Vector4f q = StringParseUtils::Parse<Vector4f>(RotationNode->GetText());
		//transform.Rotation = Quaternion(q.w, q.x, q.y, q.z);
		//XMLElement* ScaleNode = node->FirstChildElement("Scale");
		//transform.Scale = StringParseUtils::Parse<Vector3f>(ScaleNode->GetText());

		transform.bActive = ReadChildAttribute<bool>(node, "Active");
		transform.bStatic = ReadChildAttribute<bool>(node, "Static");
		transform.Position = ReadChildAttribute<Vector3f>(node, "Position");
		transform.Rotation = ReadChildAttribute<Vector3f>(node, "Rotation");
		transform.Scale = ReadChildAttribute<Vector3f>(node, "Scale");
	}

	bool CSceneLoader::LoadMeshComponent(tinyxml2::XMLElement* node, MeshComponent& meshComponent)
	{
		// <Resource>
		//XMLElement* ResourceNode = node->FirstChildElement("Resource");
		const char* szResourcePath = ReadChildAttribute<const char*>(node, "Resource");
		IMesh* pMesh = mMeshManager->GetMesh(szResourcePath);
		if (pMesh == nullptr)
		{
			// try to create one
			GRIND_ERROR_LOG("Failed in creating MeshEntity: mesh '%s' was not found!", szResourcePath);
			return false;
		}
		meshComponent.pMesh = pMesh;

		// <Visible>
		meshComponent.bVisible = ReadChildAttribute<bool>(node, "Visible");

		// <ReceiveShadow>
		meshComponent.bReceiveShadow = ReadChildAttribute<bool>(node, "ReceiveShadow");

		// <CastShadow>
		meshComponent.bCastShadow = ReadChildAttribute<bool>(node, "CastShadow");

		// <Materials>
		XMLElement* MaterialsNode = node->FirstChildElement("Materials");
		XMLElement* MaterialNode = MaterialsNode->FirstChildElement("Material");

		int iSubMeshCount = pMesh->GetSubMeshCount();
		meshComponent.pMaterialList = (IMaterial**)malloc(sizeof(IMaterial*) * iSubMeshCount);
		int i = 0;
		while (MaterialNode && i < iSubMeshCount)
		{
			const char* szMaterialName = MaterialNode->GetText();
			meshComponent.pMaterialList[i] = mMaterialManager->GetMaterial(szMaterialName);
			MaterialNode = MaterialNode->NextSiblingElement("Material");
			i += 1;
		}

		return true;
	}

	Entity* CSceneLoader::ExtendEntity(Entity* pEntity, tinyxml2::XMLElement* ComponentNode)
	{
		if (ComponentNode == nullptr)
			return pEntity;

		while (ComponentNode)
		{
			pEntity = mSceneLoadListener->OnProcessEntityComponentXML(pEntity, ComponentNode, mSceneManager);
			ComponentNode = ComponentNode->NextSiblingElement();
		}
		return pEntity;
	}

}

