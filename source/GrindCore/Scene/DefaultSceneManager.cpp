#include "stdafx.h"
#include "DefaultSceneManager.h"
#include "../Core/WorkerThread.h"
#include "Camera.h"
#include "Math/IntersectionTest.h"

namespace grind
{
	CDefaultSceneManager::CDefaultSceneManager()
	{

	}

	CDefaultSceneManager::~CDefaultSceneManager()
	{
		if (mEntityContext) {
			mEntityContext->Release();
			mEntityContext = nullptr;
		}
	}

	void CDefaultSceneManager::CullEntities()
	{
		IEngineCore* pEngineCore = GetEngineCore();
		IGraphicsSystem* pGraphicsSystem = pEngineCore->GetIGraphicsSystem();
		mCameraTransformation = pGraphicsSystem->GetSceneCameraTransformation();

		int iWorkerCount = pEngineCore->GetWorkerThreadCount();
		mCullStaticMeshJob.Prepare(mEntityContext, iWorkerCount);

		pEngineCore->FireWorkerThreads(ERequestType::CullScene);
	}

	void CDefaultSceneManager::CullEntities_WorkerThread(CWorkerThread* pWorkerThread)
	{
		CMeshRenderItemQueue* pOpaqueMeshRenderQueue = pWorkerThread->GetOpaqueMeshRenderQueue();
		pOpaqueMeshRenderQueue->Reset();
		mCullStaticMeshJob.Execute(pOpaqueMeshRenderQueue);
	}

	void CDefaultSceneManager::CreateEntityContexts()
	{
		World* pWorld = GetEngineCore()->GetWorld();
		mEntityContext = pWorld->CreateContext();

		mCullStaticMeshJob.SetJobFunction(std::bind(&CDefaultSceneManager::CullStaticMeshJobFunction, this,
			std::placeholders::_1, std::placeholders::_2, std::placeholders::_3,
			std::placeholders::_4, std::placeholders::_5));
	}

	void CDefaultSceneManager::CullStaticMeshJobFunction(CMeshRenderItemQueue* renderItemQueue,
		Entity* pEntity, TransformComponent* pTransform,
		MeshComponent* pStaticMeshComponent, BoundingBoxComponent* pBoundingBox)
	{
		const Frustum& frustum = mCameraTransformation.mFrustum;
		if (!IntersectionTest::Frustum_AABB(frustum, pBoundingBox->aabb)) {
			return;
		}

		// check aabb first
		Matrix4x4 worldMatrix = pTransform->ToMatrix();

		IMesh* pMesh = pStaticMeshComponent->pMesh;
		if (pMesh == nullptr)
			return;

		for (int i = 0; i < pMesh->GetSubMeshCount(); i++)
		{
			SMeshRenderItem* renderItem = renderItemQueue->GetNextToWrite();
			renderItem->iSubMeshIndex = i;
			renderItem->pMaterial = pStaticMeshComponent->pMaterialList[i];
			renderItem->pMesh = pStaticMeshComponent->pMesh;
			renderItem->WorldMatrix = worldMatrix;
		}
	}

}