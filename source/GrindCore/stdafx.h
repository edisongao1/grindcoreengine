// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

#ifndef PCH_H
#define PCH_H

#ifdef WIN32
#include <Windows.h>
#include <WinBase.h>

#endif // WIN32

// TODO: add headers that you want to pre-compile here
#include <assert.h>
#include <atomic>
#include <type_traits>
#include <thread>
#include <functional>
#include <stdio.h>
#include <tchar.h>
#include <memory>
#include <iostream>
#include <chrono>
#include <future>
#include <io.h>

#endif //PCH_H
