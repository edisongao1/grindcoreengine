#ifndef __WIN32_WINDOW_H__
#define __WIN32_WINDOW_H__

#include "IWindow.h"

#ifdef WIN32

#include "Windows.h"

namespace grind
{
	class CWin32Window : public IWindow
	{
	public:
		virtual bool Initialize(SWindowSettings& settings) override;

		LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
		
		virtual void StartLoop() override;

		virtual intptr_t GetWindowHandle() const override
		{
			return (intptr_t)m_hFrameWnd;
		}

		virtual void SetCaption(const char* szText) override;

		virtual Vector2i GetPosFromScreenToClient(const Vector2i& spos) const override;
		virtual Vector2i GetPosFromClientToScreen(const Vector2i& cpos) const override;
	private:
		HWND			m_hParentWnd;
		HWND			m_hFrameWnd;
		HWND			m_hBufferWnd;
		HINSTANCE		m_hInstance;
		WNDPROC			m_DefinedWndProc;
	};
}

#endif // WIN32
#endif // __WIN32_WINDOW_H__
