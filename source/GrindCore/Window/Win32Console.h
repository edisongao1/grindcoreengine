#pragma once
#include "IConsole.h"

namespace grind
{
	class CWin32Console : public IConsole
	{
	public:
		CWin32Console();
		virtual ~CWin32Console();

		virtual void PrintLine(const char* s,
			EConsoleTextColor color = EConsoleTextColor::WHITE,
			bool bColorIntensity = false) override;

		virtual void PrintLineW(const wchar_t* s,
			EConsoleTextColor color = EConsoleTextColor::WHITE,
			bool bColorIntensity = false) override;

		void Run();

	private:
		WORD _GetTextColorAttribute(EConsoleTextColor color, bool bIntensify);
		void PrintInputText();
		
		HANDLE				m_hIn;
		HANDLE				m_hOut;
		HWND				mHwnd;
		std::thread			mThread;
		int					mScreenWidth;
		int					mScreenHeight;
		char				mReadInputText[1024];
		int					mReadInputTextPos;
		volatile bool		mStopped;
	};
}
