#include "stdafx.h"
#include "Win32Console.h"
#include "../Utils/ThreadUtils.h"

namespace grind
{
	static const char CURSOR_STR[] = ">>";
	static const int CURSOR_STR_LEN = strlen(CURSOR_STR);

	CWin32Console::CWin32Console()
		:mThread(std::mem_fn(&CWin32Console::Run), this)
		, mStopped(false)
	{
		//g_pConsole = this;

		SetThreadName(mThread, "Console Input Thread");

		m_hOut = GetStdHandle(STD_OUTPUT_HANDLE);
		m_hIn = GetStdHandle(STD_INPUT_HANDLE);
		mHwnd = GetConsoleWindow();

		CONSOLE_SCREEN_BUFFER_INFO bInfo;
		GetConsoleScreenBufferInfo(m_hOut, &bInfo);
		mScreenWidth = bInfo.dwSize.X;
		mScreenHeight = bInfo.dwSize.Y;

		::SetWindowLong(mHwnd, GWL_STYLE, GetWindowLong(mHwnd, GWL_STYLE)&~(WS_SIZEBOX | WS_MAXIMIZEBOX));
		
	}

	CWin32Console::~CWin32Console()
	{
		mStopped = true;
		INPUT_RECORD dummyInput;
		dummyInput.EventType = KEY_EVENT;
		dummyInput.Event.KeyEvent.bKeyDown = TRUE;
		dummyInput.Event.KeyEvent.dwControlKeyState = 0;
		dummyInput.Event.KeyEvent.uChar.UnicodeChar = 'u';
		dummyInput.Event.KeyEvent.wRepeatCount = 1;
		dummyInput.Event.KeyEvent.wVirtualKeyCode = 'U';
		dummyInput.Event.KeyEvent.wVirtualScanCode = MapVirtualKey('U', MAPVK_VK_TO_VSC);

		DWORD dwNumberOfEventsWritten;
		WriteConsoleInput(m_hIn, &dummyInput, 1, &dwNumberOfEventsWritten);
		if (mThread.joinable()) {
			mThread.join();
		}
	}

	void CWin32Console::PrintLineW(const wchar_t* s, EConsoleTextColor color, bool bColorIntensity)
	{
		static wchar_t szLine[1024] = { 0 };

		CONSOLE_SCREEN_BUFFER_INFO bInfo;
		//COORD			crHome = { 0, 0 }, crPos;
		COORD crPos;

		GetConsoleScreenBufferInfo(m_hOut, &bInfo);
		crPos.X = 0;
		crPos.Y = bInfo.dwCursorPosition.Y;

		DWORD dwNumberOfCharsWritten;
		FillConsoleOutputCharacterA(m_hOut, ' ', mScreenWidth, crPos, &dwNumberOfCharsWritten);
		SetConsoleCursorPosition(m_hOut, crPos);

		SetConsoleTextAttribute(m_hOut, _GetTextColorAttribute(color, bColorIntensity));
		wsprintf(szLine, L"%s\n", s);
		wprintf(szLine);
		PrintInputText();

#if _WIN32
		OutputDebugStringW(szLine);
#endif
	}

	void CWin32Console::PrintLine(const char* s, EConsoleTextColor color /*= EConsoleTextColor::WHITE*/, bool bColorIntensity /*= false*/)
	{
		static char szLine[1024] = { 0 };
		CONSOLE_SCREEN_BUFFER_INFO bInfo;
		COORD			crPos;

		GetConsoleScreenBufferInfo(m_hOut, &bInfo);
		crPos.X = 0;
		crPos.Y = bInfo.dwCursorPosition.Y;

		DWORD dwNumberOfCharsWritten;
		FillConsoleOutputCharacterA(m_hOut, ' ', mScreenWidth, crPos, &dwNumberOfCharsWritten);
		SetConsoleCursorPosition(m_hOut, crPos);

		SetConsoleTextAttribute(m_hOut, _GetTextColorAttribute(color, bColorIntensity));
		sprintf_s(szLine, "%s", s);
		puts(szLine);
		PrintInputText();
#if _WIN32
		OutputDebugStringA(szLine);
#endif
	}

	void CWin32Console::PrintInputText()
	{
		CONSOLE_SCREEN_BUFFER_INFO bInfo;
		COORD			crPos;

		GetConsoleScreenBufferInfo(m_hOut, &bInfo);
		crPos.X = 0;
		crPos.Y = bInfo.dwCursorPosition.Y;
		SetConsoleCursorPosition(m_hOut, crPos);
		SetConsoleTextAttribute(m_hOut, _GetTextColorAttribute(EConsoleTextColor::GREEN, true));
		printf("%s%s\n", CURSOR_STR, mReadInputText);

		crPos.X = mReadInputTextPos + CURSOR_STR_LEN;
		crPos.Y = bInfo.dwCursorPosition.Y;
		SetConsoleCursorPosition(m_hOut, crPos);
	}

	void CWin32Console::Run()
	{
		DWORD			dwRes = 0;
		INPUT_RECORD	keyRec;
		CONSOLE_SCREEN_BUFFER_INFO bInfo;
		COORD			crPos;
		char			ch;

		printf("");
		crPos.X = crPos.Y = 0;
		SetConsoleCursorPosition(m_hOut, crPos);
		memset(mReadInputText, 0, sizeof(mReadInputText));
		mReadInputTextPos = 0;

		printf("%s", CURSOR_STR);
		crPos.X = CURSOR_STR_LEN;
		crPos.Y = 0;
		SetConsoleCursorPosition(m_hOut, crPos);

		while (!mStopped)
		{
			ReadConsoleInput(m_hIn, &keyRec, 1, &dwRes);
			if (mStopped)
				break;

			SetConsoleTextAttribute(m_hOut, _GetTextColorAttribute(EConsoleTextColor::GREEN, true));
			if (keyRec.EventType == KEY_EVENT)
			{
				if (keyRec.Event.KeyEvent.bKeyDown)
				{
					switch (keyRec.Event.KeyEvent.wVirtualKeyCode)
					{
					case VK_RETURN:
						RecordCommandLine(mReadInputText);
						printf("\n");
						memset(mReadInputText, 0, sizeof(mReadInputText));
						mReadInputTextPos = 0;
						PrintInputText();
						break;

					case VK_SPACE:
						printf(" ");
						mReadInputText[mReadInputTextPos++] = ' ';
						break;

					case VK_BACK:
						GetConsoleScreenBufferInfo(m_hOut, &bInfo);
						crPos = bInfo.dwCursorPosition;
						if (crPos.X > CURSOR_STR_LEN)
						{
							crPos.X -= 1;
						}
						SetConsoleCursorPosition(m_hOut, crPos);
						printf(" ");
						SetConsoleCursorPosition(m_hOut, crPos);
						if (mReadInputTextPos > 0)
						{
							mReadInputText[--mReadInputTextPos] = '\0';
						}
						break;
					case VK_LEFT:
						if (mReadInputTextPos > 0)
						{
							mReadInputTextPos -= 1;
							GetConsoleScreenBufferInfo(m_hOut, &bInfo);
							crPos = bInfo.dwCursorPosition;
							if (crPos.X >= CURSOR_STR_LEN)
							{
								crPos.X -= 1;
							}
							SetConsoleCursorPosition(m_hOut, crPos);
						}
						break;
					case VK_RIGHT:
						if (mReadInputTextPos < strlen(mReadInputText))
						{
							mReadInputTextPos += 1;
							GetConsoleScreenBufferInfo(m_hOut, &bInfo);
							crPos = bInfo.dwCursorPosition;
							crPos.X += 1;
							SetConsoleCursorPosition(m_hOut, crPos);
						}
						break;
					default:
						break;
					}

					ch = keyRec.Event.KeyEvent.uChar.AsciiChar;
					if (ch > 0x20 && ch < 0x7e)
					{
						putchar(ch);
						mReadInputText[mReadInputTextPos++] = ch;
					}
				}
			}
		}
	}
	

	WORD CWin32Console::_GetTextColorAttribute(EConsoleTextColor color, bool bIntensify)
	{
		WORD wAttribute = 0;
		switch (color)
		{
		case EConsoleTextColor::RED:
			wAttribute = FOREGROUND_RED;
			break;
		case EConsoleTextColor::GREEN:
			wAttribute = FOREGROUND_GREEN;
			break;
		case EConsoleTextColor::BLUE:
			wAttribute = FOREGROUND_BLUE;
			break;
		case EConsoleTextColor::YELLOW:
			wAttribute = FOREGROUND_RED | FOREGROUND_GREEN;
			break;
		case EConsoleTextColor::CYAN:
			wAttribute = FOREGROUND_GREEN | FOREGROUND_BLUE;
			break;
		case EConsoleTextColor::PINK:
			wAttribute = FOREGROUND_RED | FOREGROUND_BLUE;
			break;
		case EConsoleTextColor::WHITE:
			wAttribute = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
			break;
		case EConsoleTextColor::BLACK:
			wAttribute = 0;
			break;
		default:
			break;
		}

		if (bIntensify) {
			wAttribute = wAttribute | FOREGROUND_INTENSITY;
		}

		return wAttribute;
	}
}