#include "stdafx.h"
#include "Win32Window.h"
#include "../Utils/InputDeviceManager.h"
#include "IEngineCore.h"

#ifdef WIN32


namespace grind
{
	static CWin32Window* g_pWin32Window = nullptr;

	static LRESULT __stdcall _WndProc(HWND, UINT, WPARAM, LPARAM);


	bool CWin32Window::Initialize(SWindowSettings& settings)
	{
		if (settings.WindowsProcedure)
			m_DefinedWndProc = (WNDPROC)(settings.WindowsProcedure);

		WNDCLASSEX wc;
		DEVMODE dmScreenSettings;

		// Get an external pointer to this object.	
		g_pWin32Window = this;

		// Get the instance of this application.
		m_hInstance = GetModuleHandleW(NULL);


		IConfiguration* pConfiguration = GetEngineCore()->GetConfiguration();
		const char* szTitle = pConfiguration->GetProperty<const char*>("window_title");
		std::wstring wstrTitle = StringParseUtils::AnsiToWString(szTitle);

		if (settings.FrameWindowHandle == 0) {
			// Give the application a name.
			static TCHAR className[] = TEXT("GameWindow");

			// Setup the windows class with default settings.
			wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC | CS_DBLCLKS;
			wc.lpfnWndProc = grind::_WndProc;
			wc.cbClsExtra = 0;
			wc.cbWndExtra = sizeof(CWin32Window*);
			wc.hInstance = m_hInstance;
			wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
			wc.hIconSm = wc.hIcon;
			wc.hCursor = LoadCursor(NULL, IDC_ARROW);
			//wc.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);
			HBRUSH bkBrush = CreateSolidBrush(RGB(240, 240, 240));

			wc.hbrBackground = bkBrush;
			wc.lpszMenuName = NULL;
			wc.lpszClassName = className;
			wc.cbSize = sizeof(WNDCLASSEX);

			// Register the window class.
			RegisterClassEx(&wc);

			// Determine the resolution of the clients desktop screen.
			if (settings.Style & eWS_FULLRESOLUTION)
			{
				settings.Width = GetSystemMetrics(SM_CXSCREEN);
				settings.Height = GetSystemMetrics(SM_CYSCREEN);
			}

			// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
			if (settings.Style & eWS_FULLSCREEN)
			{
				// If full screen set the screen to maximum size of the users desktop and 32bit.
				memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
				dmScreenSettings.dmSize = sizeof(dmScreenSettings);
				dmScreenSettings.dmPelsWidth = (unsigned long)settings.Width;
				dmScreenSettings.dmPelsHeight = (unsigned long)settings.Height;
				dmScreenSettings.dmBitsPerPel = 32;
				dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

				// Change the display settings to full screen.
				ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

				// Create the window with the screen settings and get the handle to it.
				m_hFrameWnd = CreateWindowEx(WS_EX_APPWINDOW, className, wstrTitle.c_str(),
					WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
					0, 0, settings.Width,
					settings.Height, NULL, NULL, m_hInstance, static_cast<LPVOID>(this));
			}
			else
			{
				int posX = (GetSystemMetrics(SM_CXSCREEN) - settings.Width) / 2;
				int posY = (GetSystemMetrics(SM_CYSCREEN) - settings.Height) / 2;

				RECT R = { 0, 0, static_cast<LONG>(settings.Width), static_cast<LONG>(settings.Height) };
				AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);

				// Create the window with the screen settings and get the handle to it.
				m_hFrameWnd = CreateWindowEx(WS_EX_APPWINDOW, className, wstrTitle.c_str(),
					WS_OVERLAPPEDWINDOW, posX, posY, R.right - R.left, R.bottom - R.top,
					NULL, NULL, m_hInstance, static_cast<LPVOID>(this));
			}

			// Bring the window up on the screen and set it as main focus.

			//mSettings.Window.ParentHandle = (u32)mHwnd;
			m_hBufferWnd = m_hFrameWnd;
			m_hParentWnd = m_hFrameWnd;
			ShowWindow(m_hFrameWnd, SW_SHOW);
			SetForegroundWindow(m_hFrameWnd);
			SetFocus(m_hFrameWnd);
		}
		else // if the user sets the parent hwnd
		{
			m_hParentWnd = (HWND)settings.ParentHandle;
			m_hFrameWnd = (HWND)settings.FrameWindowHandle;
			if (m_hParentWnd == 0) {
				m_hParentWnd = m_hFrameWnd;
			}
		}

		//m_CreationParams.FrameWindowHandle = (u32)mHwnd;
		//m_CreationParams.BackBufferWindowHandle = (u32)mHwnd;

		if (!(settings.Style & eWS_FULLSCREEN))
		{
			HWND hBufferWnd = CreateWindow(TEXT("static"), NULL, WS_CHILD | WS_VISIBLE | SS_WHITERECT,
				0, 0, settings.Width, settings.Height,
				m_hParentWnd, (HMENU)9, m_hInstance, NULL);

			// m_CreationParams.BackBufferWindowHandle = (u32)hBufferWnd;
			m_hBufferWnd = hBufferWnd;
		}

		settings.BackBufferWindowHandle = (intptr_t)m_hBufferWnd;
		settings.FrameWindowHandle = (intptr_t)m_hFrameWnd;
		m_settings = settings;

		return true;
	}


	LRESULT __stdcall _WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		// Store instance pointer while handling the first message
		if (msg == WM_NCCREATE)
		{
			CREATESTRUCT* pCS = reinterpret_cast<CREATESTRUCT*>(lParam);
			LPVOID pThis = pCS->lpCreateParams;
			SetWindowLongPtrW(hwnd, 0, reinterpret_cast<LONG_PTR>(pThis));
		}

		// At this point the instance pointer will always be available
		CWin32Window* pWnd = reinterpret_cast<CWin32Window*>(GetWindowLongPtrW(hwnd, 0));
		// see Note 1a below
		if (pWnd) {
			return pWnd->WndProc(hwnd, msg, wParam, lParam);
		}

		return DefWindowProc(hwnd, msg, wParam, lParam);
	}

	LRESULT CALLBACK CWin32Window::WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		if (m_DefinedWndProc)
		{
			m_DefinedWndProc(hwnd, msg, wParam, lParam);
		}

		switch (msg)
		{
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
			/*
		case WM_ACTIVATE:
			if (mInputDriver && mSettings.Input.Driver == EIDT_DIRECTINPUT8)
			{
				CDInput8Driver* input = dynamic_cast<CDInput8Driver*>(mInputDriver);
				POINT p;
				GetCursorPos(&p);
				input->setCurrentMousePos(p.x, p.y);
			}
			break;
			*/
		default:
			return DefWindowProc(hwnd, msg, wParam, lParam);
		}

		return 0;
	}

	void CWin32Window::StartLoop()
	{
		MSG msg;
		bool stopped = false;

		auto pInputDeviceManager = dynamic_cast<CInputDeviceManager*>(GetEngineCore()->GetInputDeviceManager());

		while (!stopped) {
			while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				if (msg.message == WM_QUIT)
					stopped = true;
				TranslateMessage(&msg);
				DispatchMessage(&msg);
				pInputDeviceManager->HandleMessage(msg);
			}

			if (stopped)
				break;

			if (m_frameFunction && !m_frameFunction())
				stopped = true;
		}
	}


	void CWin32Window::SetCaption(const char* szText)
	{
		SetWindowTextA(m_hFrameWnd, szText);
	}

	Vector2i CWin32Window::GetPosFromScreenToClient(const Vector2i& spos) const
	{
		POINT point = { spos.x, spos.y };
		ScreenToClient(m_hFrameWnd, &point);
		Vector2i cpos;
		cpos.x = point.x;
		cpos.y = point.y;
		return cpos;
	}

	Vector2i CWin32Window::GetPosFromClientToScreen(const Vector2i& cpos) const
	{
		POINT point = { cpos.x, cpos.y };
		ClientToScreen(m_hFrameWnd, &point);
		Vector2i spos;
		spos.x = point.x;
		spos.y = point.y;
		return spos;
	}

}

#endif // WIN32
