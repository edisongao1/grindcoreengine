#include "stdafx.h"
#include "ModelFileLoader.h"
#include "IEngineCore.h"


namespace grind
{

	bool CModelFileLoader::LoadStaticMesh(const char* szFileName, SMeshData& meshData, uint32_t fvf_flags)
	{
		IFileSystem* pFileSystem = GetEngineCore()->GetFileSystem();
		char szFullPath[256];
		pFileSystem->GetFileFullPath(szFileName, szFullPath);

		Assimp::Importer importer;

		unsigned int importFlags = aiProcess_Triangulate |
			aiProcess_JoinIdenticalVertices |
			aiProcess_GenBoundingBoxes |
			aiProcess_GenSmoothNormals |
			aiProcess_SortByPType;

		bool bContainTangent = false;
		bool bContainTexcoord1 = false;

		if (fvf_flags & E_FVF_Flag::ContainTangent)
		{
			bContainTangent = true;
			importFlags |= aiProcess_CalcTangentSpace;
		}
		// And have it read the given file with some example postprocessing
		// Usually - if speed is not the most important aspect for you - you'll
		// probably to request more postprocessing than we do in this example.
		const aiScene* scene = importer.ReadFile(szFullPath, importFlags);
		aiMesh* mesh = scene->mMeshes[0];

		if (scene->mNumMeshes == 0)
			return false;

		uint8_t vertexSize = 3 + 3; // position + normal
		// tangent
		if (fvf_flags & E_FVF_Flag::ContainTangent)
			vertexSize += 3;

		// texcoord0
		vertexSize += 2;

		// texcoord1
		if ((fvf_flags & E_FVF_Flag::ContainTexcoord1) && mesh->HasTextureCoords(1))
		{
			bContainTexcoord1 = true;
			vertexSize += 2;
		}
		else {
			fvf_flags &= (~E_FVF_Flag::ContainTexcoord1);
		}

		vertexSize *= sizeof(float);
		unsigned int vertexCount = ComputeMeshVertexCount(scene);
		unsigned int indiceCount = ComputeMeshIndiceCount(scene);

		meshData.Fvf = EFVF::Object3D;
		meshData.FvfFlag = fvf_flags;

		meshData.VertexCount = vertexCount;
		meshData.VertexSize = vertexSize;

		meshData.IndiceCount = indiceCount;
		// if 16 bits are enough for indice buffer
		if (vertexCount <= 65536)
			meshData.IndexSize = 2;
		else
			meshData.IndexSize = 4;

		meshData.VertexData = (byte*)std::malloc(meshData.VertexSize * meshData.VertexCount);
		meshData.IndiceData = (byte*)std::malloc(meshData.IndexSize * meshData.IndiceCount);
		
		ReadMeshVerticesData(scene, meshData);
		ReadMeshIndicesData(scene, meshData);
		FillSubMeshes(scene, meshData);

		return true;
	}

	unsigned int CModelFileLoader::ComputeMeshIndiceCount(const aiScene* scene)
	{
		unsigned int totalCount = 0;
		if (scene->mNumMeshes == 0)
			return 0;

		for (int i = 0; i < scene->mNumMeshes; i++) {
			aiMesh* mesh = scene->mMeshes[i];
			totalCount += mesh->mNumFaces * 3;
		}
		return totalCount;
	}

	unsigned int CModelFileLoader::ComputeMeshVertexCount(const aiScene* scene)
	{
		unsigned int totalCount = 0;
		if (scene->mNumMeshes == 0)
			return 0;

		for (int i = 0; i < scene->mNumMeshes; i++) {
			aiMesh* mesh = scene->mMeshes[i];
			totalCount += mesh->mNumVertices;
		}
		return totalCount;
	}

	void CModelFileLoader::ReadMeshVerticesData(const aiScene* scene, SMeshData& meshData)
	{
		bool bContainTangent = (meshData.FvfFlag & E_FVF_Flag::ContainTangent);
		bool bContainTexcoord1 = (meshData.FvfFlag & E_FVF_Flag::ContainTexcoord1);
		byte* pVertexData = meshData.VertexData;

		for (int i = 0; i < scene->mNumMeshes; i++)
		{
			aiMesh* mesh = scene->mMeshes[i];
			for (int j = 0; j < mesh->mNumVertices; j++)
			{
				// position
				{
					Vector3f position;
					position.x = mesh->mVertices[j].x;
					position.y = mesh->mVertices[j].y;
					position.z = mesh->mVertices[j].z;

					*(Vector3f*)pVertexData = position;
					pVertexData += sizeof(Vector3f);
				}
				
				// normal
				{
					Vector3f normal;
					normal.x = mesh->mNormals[j].x;
					normal.y = mesh->mNormals[j].y;
					normal.z = mesh->mNormals[j].z;

					*(Vector3f*)pVertexData = normal;
					pVertexData += sizeof(Vector3f);
				}
				
				// tangent
				if (bContainTangent)
				{
					Vector3f tangent;
					tangent.x = mesh->mTangents[j].x;
					tangent.y = mesh->mTangents[j].y;
					tangent.z = mesh->mTangents[j].z;

					*(Vector3f*)pVertexData = tangent;
					pVertexData += sizeof(Vector3f);
				}

				// texcoord0:
				{
					Vector2f texcoord;
					texcoord.x = mesh->mTextureCoords[0][j].x;
					texcoord.y = mesh->mTextureCoords[0][j].y;

					*(Vector2f*)pVertexData = texcoord;
					pVertexData += sizeof(Vector2f);
				}

				// texcoord1:
				if (bContainTexcoord1)
				{
					Vector2f texcoord1;
					texcoord1.x = mesh->mTextureCoords[1][j].x;
					texcoord1.y = mesh->mTextureCoords[1][j].y;

					*(Vector2f*)pVertexData = texcoord1;
					pVertexData += sizeof(Vector2f);
				}
			}
		}
	}

	template<typename T>
	void _ReadMeshIndicesData(const aiScene* scene, T* indicesData)
	{
		T* indices = (T*)indicesData;
		for (int i = 0; i < scene->mNumMeshes; i++)
		{
			aiMesh* mesh = scene->mMeshes[i];
			for (int j = 0; j < mesh->mNumFaces; j++)
			{
				indices[0] = (T)mesh->mFaces[j].mIndices[0];
				indices[1] = (T)mesh->mFaces[j].mIndices[1];
				indices[2] = (T)mesh->mFaces[j].mIndices[2];
				indices += 3;
			}
		}
	}

	void CModelFileLoader::ReadMeshIndicesData(const aiScene* scene, SMeshData& meshData)
	{
		if (meshData.IndexSize == 2)
		{
			_ReadMeshIndicesData<uint16_t>(scene, (uint16_t*)meshData.IndiceData);
		}
		else
		{
			_ReadMeshIndicesData<uint32_t>(scene, (uint32_t*)meshData.IndiceData);
		}		
	}

	void CModelFileLoader::FillSubMeshes(const aiScene* scene, SMeshData& meshData)
	{
		//meshData.aabb.MinEdge = Vector3f(-1000.0f, -1000.0f, -1000.0f);
		//meshData.aabb.MaxEdge = Vector3f(1000.0f, 1000.0f, 1000.0f);
		unsigned int startIndex = 0;
		meshData.aabb.Reset();
		for (int i = 0; i < scene->mNumMeshes; i++)
		{
			aiMesh* mesh = scene->mMeshes[i];
			meshData.SubMeshes[i].StartIndexLocation = startIndex;
			meshData.SubMeshes[i].IndexCount = mesh->mNumFaces * 3;
			aiAABB aabb = mesh->mAABB;
			meshData.SubMeshes[i].aabb.MinEdge = Vector3f(aabb.mMin.x, aabb.mMin.y, aabb.mMin.z);
			meshData.SubMeshes[i].aabb.MaxEdge = Vector3f(aabb.mMax.x, aabb.mMax.y, aabb.mMax.z);
			meshData.aabb.AddInternalBox(meshData.SubMeshes[i].aabb);
			startIndex += mesh->mNumFaces * 3;
		}
		meshData.SubMeshCount = scene->mNumMeshes;
	}

}
