#pragma once
#include "IMesh.h"
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

namespace grind
{
	class CModelFileLoader
	{
	public:
		bool LoadStaticMesh(const char* szFileName, SMeshData& meshData, uint32_t fvf_flags);
	private:
		unsigned int ComputeMeshIndiceCount(const aiScene* scene);
		unsigned int ComputeMeshVertexCount(const aiScene* scene);
		void ReadMeshVerticesData(const aiScene* scene, SMeshData& meshData);
		void ReadMeshIndicesData(const aiScene* scene, SMeshData& meshData);
		void FillSubMeshes(const aiScene* scene, SMeshData& meshData);
	};
}

