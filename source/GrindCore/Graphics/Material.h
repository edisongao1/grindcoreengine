#pragma once
#include "IMaterial.h"
#include "IRenderPass.h"
#include "tinyxml2.h"

using namespace tinyxml2;
namespace grind
{
	class IRenderResourceManager;

	class CTechnique : public ITechnique
	{
	public:
		friend class CRenderResourceManager;
		CTechnique() {
			int a = 1;
		}
		virtual IRenderPass* CreateRenderPass(ETechniquePassStage stage,
			const std::vector<std::string>& defines) override;

		virtual SMaterialRenderPass* GetMaterialRenderPass(uint64_t mask) override;
	private:
		std::unordered_map<uint64_t, std::unique_ptr<SMaterialRenderPass>> mMaterialRenderPassMap;
		//std::unordered_map<std::string, SMaterialVariableInfo>	mMaterialVariableInfos;
		
	};

	class CMaterial : public IMaterial
	{
	public:
		friend class CMaterialManager;

		CMaterial(const std::string& name, ITechnique* pTech)
			:IMaterial(name, pTech)
		{
			mAtomicIsCreatingResource.clear();
		}

		static void StaticInitialize();
		
		void Initialize();

		void InitializeGPUResources();

		virtual IRenderPass* GetRenderPass(ETechniquePassStage stage, uint64_t mask) override;

		void SetRenderPass(ETechniquePassStage stage, uint64_t runningMask64, IRenderPass* pPass);
		
		virtual void FlushParams() override;

		void Copy(CMaterial* pAnotherMaterial);

		virtual void TryCreateMaterialResource() override;

	private:
		DECLARE_ATOMIC_FLAG(mAtomicIsCreatingResource)
	};

	struct SCreateMaterialResourceRequest
	{
		CMaterial*		pMaterial;
	};

	class CMaterialManager : public IMaterialManager
	{
	public:
		CMaterialManager(IRenderResourceManager* pRenderResourceManager);
		virtual IMaterial* GetMaterial(const std::string& name) override;
		virtual void InitializeAfterGraphicsCreated() override;
		virtual IMaterialResourceManager* GetMaterialResourceManager() override;
		virtual IMaterial* CopyMaterial(IMaterial* pMaterial, const char* pName = 0) override;
		//virtual SMaterialTemplate* GetMaterialTemplate(const std::string& name) override;
	private:
		//void LoadMaterialTemplates();
		//void ParseMaterialTemplate(tinyxml2::XMLElement* node);
		//void ParseMaterialTemplateVariable(tinyxml2::XMLElement* node, SMaterialVariableInfo& variableInfo);
		//void ParseMaterialTemplateFlag(tinyxml2::XMLElement* node, SMaterialFlagInfo& flagInfo);



		void LoadAllMaterials();
		void ParseMaterial(tinyxml2::XMLElement* node);
		void ParseMaterialRenderStateVariable(tinyxml2::XMLElement* node, CMaterial* pMaterial);
		//void ParseMaterialVariable(tinyxml2::XMLElement* node, SMaterialTemplate* pMatTemplate, SMaterialVariable& matVariable);
	private:
		std::unordered_map<std::string, std::unique_ptr<CMaterial>>			mMaterialMap;		
		IRenderResourceManager*		mRenderResourceManager;
		static int	s_matGeneratorId;
	};
}

