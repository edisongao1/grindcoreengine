#include "stdafx.h"
#include "Mesh.h"
#include "../Core/EngineCore.h"
#include "ModelFileLoader.h"

namespace grind
{
	CMeshManager::CMeshManager()
	{
		auto queue = GetEngineCore()->GetAsynchronizeTaskQueue();
		queue->SetRequestHandler<SCreateMeshBufferFinishedRequest>(ETaskType::CreateMeshBufferFinished,
			[](SCreateMeshBufferFinishedRequest* req, STaskContext* context) {

			IMesh* pMesh = req->pMesh;
			pMesh->OnCreateBufferCompleted(req->pMeshBuffer);

		});
	}

	IMesh* CMeshManager::GetMesh(const std::string& name) const
	{
		auto it = mMeshMap.find(name);
		if (it == mMeshMap.end())
			return nullptr;
		return it->second.get();
	}

	IMesh* CMeshManager::CreateMesh(const std::string& name, const SMeshData& data)
	{
		auto meshPtr = std::make_unique<CMesh>(name);
		meshPtr->mMeshData = data;
		meshPtr->mState = EMeshState::Loaded;
		//meshPtr->mSubMeshes[0].StartIndexLocation = 0;
		//meshPtr->mSubMeshes[0].IndexCount = meshPtr->mMeshData.IndiceCount;
		//meshPtr->mSubMeshCount = 1;

		CMesh* pMesh = meshPtr.get();
		mMeshMap.insert({ name, std::move(meshPtr) });
		return pMesh;
	}

	bool CMeshManager::LoadStaticMeshData(const char* szFileName, SMeshData& meshData, uint32_t fvf_flags)
	{
		CModelFileLoader modelFileLoader;
		return modelFileLoader.LoadStaticMesh(szFileName, meshData, fvf_flags);
	}

	void CMesh::TryCreate()
	{
		if (mState != EMeshState::NotLoaded && mState != EMeshState::Loaded)
			return;

		if (mAtomicIsCreating.test_and_set() == false)
		{
			// if mesh data has already been loaded, just create buffer resource
			if (mState == EMeshState::Loaded)
			{
				mState = EMeshState::BufferCreating;
				auto queue = GetEngineCore()->GetAsynchronizeTaskQueue();
				SCreateMeshBufferRequest req;
				req.pMesh = this;
				queue->PushRequest<ETaskExecuteTime::LoadRenderResources>(ETaskType::CreateMeshBuffer, req);
			}
			else
			{
				// TODO:

			}
		}
	}

	void CMesh::OnCreateBufferFailed()
	{
		mState = EMeshState::BufferCreateFailed;
		mAtomicIsCreating.clear();
	}

	void CMesh::OnCreateBufferCompleted(IMeshBuffer* pMeshBuffer)
	{
		mState = EMeshState::BufferCreated;
		pMeshBuffer->OnCreateCompleted();
		mMeshBuffer = pMeshBuffer;
		// can release the mesh data now.
		if (mMeshData.VertexData) {
			std::free(mMeshData.VertexData);
			mMeshData.VertexData = nullptr;
		}
		if (mMeshData.IndiceData) {
			std::free(mMeshData.IndiceData);
			mMeshData.IndiceData = nullptr;
		}
		mAtomicIsCreating.clear();
	}

	void CMesh::ReleaseBuffer()
	{

	}

	void CMesh::ReleaseDataAndBuffer()
	{

	}

}
