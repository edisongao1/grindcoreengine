#pragma once
#include "IMesh.h"
#include "Common.h"

namespace grind
{
	class CMesh : public IMesh
	{
		friend class CMeshManager;
	public:
		CMesh(const std::string& name) :IMesh(name)
		{
			mAtomicIsCreating.clear();
		}
		virtual void TryCreate() override;
		virtual void OnCreateBufferFailed() override;
		virtual void OnCreateBufferCompleted(IMeshBuffer* pMeshBuffer) override;

		virtual void ReleaseBuffer() override;
		virtual void ReleaseDataAndBuffer() override;
	private:
		DECLARE_ATOMIC_FLAG(mAtomicIsCreating)
		//std::atomic_flag			mAtomicIsCreating = ATOMIC_FLAG_INIT;
	};

	class CMeshManager : public IMeshManager
	{
	public:
		CMeshManager();
		virtual IMesh* GetMesh(const std::string& name) const override;
		virtual IMesh* CreateMesh(const std::string& name, const SMeshData& data) override;
		
		virtual bool LoadStaticMeshData(const char* szFileName, SMeshData& meshData, uint32_t fvf_flags) override;

	private:
		std::unordered_map<std::string, std::unique_ptr<CMesh>>		mMeshMap;
	};
}