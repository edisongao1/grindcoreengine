#include "stdafx.h"

#include "../Core/EngineCore.h"
#include "Material.h"
#include "IRenderResourceFactory.h"


namespace grind
{
	struct SCreateRenderPassRequest
	{
		CMaterial*				pMaterial = nullptr;
		ETechniquePassStage		PassStage;
		uint64_t				RunningMask64;
	};

	struct SCreateRenderPassResult
	{
		CMaterial*				pMaterial = nullptr;
		ETechniquePassStage		PassStage;
		uint64_t				RunningMask64;
		IRenderPass*			pRenderPass = nullptr;
	};

	//struct SCreateMaterialResourceRequest

	void CMaterial::StaticInitialize()
	{
		auto pTaskQueue = GetAsynchronizeTaskQueue();
		pTaskQueue->SetRequestHandler<SCreateRenderPassRequest>(
			ETaskType::CreateRenderPass,
			[](SCreateRenderPassRequest* req, STaskContext* context) {
			auto pRenderResourceMgr = GetEngineCore()->GetRenderResourceManager();

			CMaterial* pMaterial = req->pMaterial;
			const auto& matFlags = pMaterial->GetMaterialShaderFlags();

			//根据mask码反推出Shader的宏列表
			std::vector<std::string> runningFlags;
			pRenderResourceMgr->GetRunningMacroStringsFromMask(req->RunningMask64, runningFlags);

			runningFlags.insert(runningFlags.end(), matFlags.begin(), matFlags.end());

			auto pTech = pMaterial->GetTechnique();
			IRenderPass* pPass = pTech->CreateRenderPass(req->PassStage, runningFlags);

			SCreateRenderPassResult result;
			result.pMaterial = pMaterial;
			result.PassStage = req->PassStage;
			result.RunningMask64 = req->RunningMask64;
			result.pRenderPass = pPass;

			context->pQueue->PushRequest<ETaskExecuteTime::BeforeRenderScene>(ETaskType::CreateRenderPassResult, result);
		});

		pTaskQueue->SetRequestHandler<SCreateRenderPassResult>(
			ETaskType::CreateRenderPassResult,
			[](SCreateRenderPassResult* res) {
			CMaterial* pMaterial = res->pMaterial;
			pMaterial->SetRenderPass(res->PassStage, res->RunningMask64, res->pRenderPass);
		});

		// ETaskType::CreateMaterialResource
		pTaskQueue->SetRequestHandler<SCreateMaterialResourceRequest>(
			ETaskType::CreateMaterialResource, 
			[](SCreateMaterialResourceRequest* req, STaskContext* context) {
			CMaterial* pMaterial = req->pMaterial;
			pMaterial->InitializeGPUResources();


		});
	}

	void CMaterial::Initialize()
	{
		auto pResourceManager = GetEngineCore()->GetRenderResourceManager();
		mShaderFlagsMask = pResourceManager->GetShaderMacroMask(mShaderFlags);
		mMaterialRenderPass = mTechnique->GetMaterialRenderPass(mShaderFlagsMask);
		
		// TODO: replace this with memory pool later
		auto cbSize = mTechnique->GetMaterialBufferSize();
		if (cbSize != 0)
			mMaterialBufferCPU = (byte*)malloc(cbSize);
	}

	void CMaterial::Copy(CMaterial* pAnotherMaterial)
	{
		memcpy(pAnotherMaterial->mRenderStateDescs, &mRenderStateDescs[0], sizeof(mRenderStateDescs));
		pAnotherMaterial->mShaderFlags = mShaderFlags;
		memcpy(pAnotherMaterial->mTextures, &mTextures, sizeof(mTextures));

		pAnotherMaterial->Initialize();
		memcpy(pAnotherMaterial->mMaterialBufferCPU, mMaterialBufferCPU, GetConstBufferSize());

		// TODO:
		if (mMaterialResourceState == EMaterialResourceState::Created) {
			pAnotherMaterial->InitializeGPUResources();
		}
	}

	void CMaterial::TryCreateMaterialResource()
	{
		if (mMaterialResourceState != EMaterialResourceState::NotCreated)
			return;

		bool bReady = true;

		// 先查看一下纹理资源是否已经就绪
		for (int i = 0; i < mTechnique->GetMaterialTextureCount(); i++) {
			auto pTexture = mTextures[i];
			if (pTexture) {
				if (pTexture->GetState() == ETextureState::Loaded) {
					continue;
				}
				bReady = false;
				if (pTexture->GetState() == ETextureState::NotLoaded) {
					pTexture->LoadFromFile();
				}
			}
		}

		if (bReady)
		{
			if (mAtomicIsCreatingResource.test_and_set() == false) {
				mMaterialResourceState = EMaterialResourceState::Creating;
				auto taskQueue = GetEngineCore()->GetAsynchronizeTaskQueue();

				SCreateMaterialResourceRequest req;
				req.pMaterial = this;
				taskQueue->PushRequest<ETaskExecuteTime::BeforeRenderScene>(ETaskType::CreateMaterialResource, req);
			}
		}
	}

	void CMaterial::InitializeGPUResources()
	{
		auto pManagerResourceManager = GetEngineCore()->GetRenderResourceManager()->GetMaterialResourceManager();
		auto cbSize = mTechnique->GetMaterialBufferSize();
		mGPUResourceId = pManagerResourceManager->AllocateMaterialResources(cbSize, mMaterialBufferCPU, mTechnique->GetMaterialTextureCount(), mTextures);
		mMaterialResourceState = EMaterialResourceState::Created;
		mAtomicIsCreatingResource.clear();
	}

	IRenderPass* CMaterial::GetRenderPass(ETechniquePassStage stage, uint64_t mask)
	{
		uint8_t runningMask = static_cast<uint8_t>(mask >> 56);
		IRenderPass* pRenderPass = mMaterialRenderPass->GetRenderPass(stage, runningMask);
		if (pRenderPass)
			return pRenderPass;

		if (mMaterialRenderPass->CanConstructRenderPassNow(stage, runningMask))
		{
			// add to construct queue
			auto pTaskQueue = GetAsynchronizeTaskQueue();
			SCreateRenderPassRequest request;
			request.pMaterial = this;
			request.PassStage = stage;
			request.RunningMask64 = mask;
			pTaskQueue->PushRequest<ETaskExecuteTime::AsyncImmediately>(ETaskType::CreateRenderPass, request);
		}
		return nullptr;
	}

	void CMaterial::SetRenderPass(ETechniquePassStage stage, uint64_t runningMask64, IRenderPass* pPass)
	{
		uint8_t runningMask = static_cast<uint8_t>(runningMask64 >> 56);
		mMaterialRenderPass->SetRenderPass(stage, runningMask, pPass);
	}



	void CMaterial::FlushParams()
	{
		//if (!mGPUResourceId.IsCreated)
		//	return;
		if (mMaterialResourceState != EMaterialResourceState::Created)
			return;

		//auto pMaterialManager = GetEngineCore()->GetRenderResourceManager()->GetMaterialManager();
		auto pManagerResourceManager = GetEngineCore()->GetRenderResourceManager()->GetMaterialResourceManager();
		pManagerResourceManager->SetConstBufferData(mGPUResourceId, mMaterialBufferCPU);
	}


	IRenderPass* CTechnique::CreateRenderPass(ETechniquePassStage stage,
		const std::vector<std::string>& defines)
	{
		auto pResourceFactory = GetEngineCore()->GetRenderResourceManager()->GetRenderResourceFactory();
		return pResourceFactory->CreateRenderPass(&mPassMetas[stage], defines);
	}

	SMaterialRenderPass* CTechnique::GetMaterialRenderPass(uint64_t mask)
	{
		auto it = mMaterialRenderPassMap.find(mask);
		if (it != mMaterialRenderPassMap.end())
			return it->second.get();

		auto ptr = std::make_unique<SMaterialRenderPass>();
		auto p = ptr.get();
		mMaterialRenderPassMap.insert(std::make_pair(mask, std::move(ptr)));
		return p;
	}




}
