#pragma once
#include "IGraphicsSystem.h"

namespace grind
{
	class CWorkerThread;

	class CGraphicsSystem: public IGraphicsSystem
	{
	public:
		void BeginFrame();
		void EndFrame();

		void RenderScene_WorkerThread(CWorkerThread* pWorkerThread);

		int GetDrawcallCount() const { return mDrawcallCount; }

	protected:
		int mDrawcallCount = 0;
	};
}
