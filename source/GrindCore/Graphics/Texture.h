#pragma once
#include "ITexture.h"

namespace grind
{
	struct DDS_PIXELFORMAT
	{
		uint32_t    size;
		uint32_t    flags;
		uint32_t    fourCC;
		uint32_t    RGBBitCount;
		uint32_t    RBitMask;
		uint32_t    GBitMask;
		uint32_t    BBitMask;
		uint32_t    ABitMask;
	};

	struct DDS_HEADER
	{
		uint32_t        size;
		uint32_t        flags;
		uint32_t        height;
		uint32_t        width;
		uint32_t        pitchOrLinearSize;
		uint32_t        depth; // only if DDS_HEADER_FLAGS_VOLUME is set in flags
		uint32_t        mipMapCount;
		uint32_t        reserved1[11];
		DDS_PIXELFORMAT ddspf;
		uint32_t        caps;
		uint32_t        caps2;
		uint32_t        caps3;
		uint32_t        caps4;
		uint32_t        reserved2;
	};



	class CTexture : public ITexture
	{
	public:
		friend class CTextureManager;

		CTexture(const std::string& filename, ETextureType eType)
			:ITexture(filename, eType)
		{
			mAtomicIsLoading.clear();
		}

		virtual void InitializeTextureResource(void* p) override;
		virtual void OnCompleteLoadResource(ITextureResource* pTextureResource) override;
		virtual void OnFailLoadResource() override;
		virtual void LoadFromFile() override;

	private:
		//std::atomic_flag	mAtomicIsLoading = ATOMIC_FLAG_INIT;
		DECLARE_ATOMIC_FLAG(mAtomicIsLoading)
	};

	class CTextureManager : public ITextureManager
	{
	public:
		CTextureManager();
		virtual ITexture* GetTexture(const std::string& name) override;
		virtual ITexture* CreateTexture(const std::string& name, ETextureType eType) override;
		
		// TODO: for test
		//virtual void InitializeAfterGraphicsCreated(void*);
		virtual void OnCompleteLoadResource() override;

		static HRESULT LoadTextureDataFromFile(const char* szFileName, 
			uint8_t** ddsData,
			DDS_HEADER** header,
			uint8_t** bitData,
			size_t* bitSize);

	private:
		using TTextureMap = std::unordered_map<std::string, std::unique_ptr<ITexture>>;
		TTextureMap		mTextureMap;
	};
}
