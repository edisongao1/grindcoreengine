#pragma once

#include "IRenderResourceManager.h"
#include "tinyxml2.h"
#include "Material.h"

namespace grind
{
	class CRenderResourceManager : public IRenderResourceManager
	{
	public:
		friend class CTechnique;

		using TTechniqueMap = std::unordered_map<std::string, std::unique_ptr<CTechnique>>;
		
		// 记录每个shader包含的宏，包括tech, material, running三种宏
		struct SValidateShaderMacros
		{
			std::vector<std::string>	MacroList;
			uint64_t					Mask;
		};
		
		using TValidateShaderMacrosMap = std::unordered_map<std::string, SValidateShaderMacros>;

	public:
		CRenderResourceManager();
		virtual void Initialize() override;
		virtual void PreloadResources() override;
		virtual IMaterialResourceManager* GetMaterialResourceManager() override;
		virtual ITechnique* GetTechniqueByName(const std::string& name) override;
		virtual void GetAllTechniques(std::vector<ITechnique*>& vecTechniques) const override;
		virtual uint64_t GetValidateShaderMacrosMask(const std::string& shaderName) const override;
	private:
		void LoadTechniques();
		void ParseTechnique(tinyxml2::XMLElement* TechNode);

		void ParseShaderMacroMaskFromString(const char* s, std::vector<std::string>& macroStrings);
		
		void LoadMacroDefinitions();

		//void LoadShaderMacroMasks();
		ETechniquePassStage GetPassStageFromString(const char* s);
		void ParseTechniquePass(tinyxml2::XMLElement* PassNode, CTechnique& pTechData);
		void ParsePassConstantBuffer(tinyxml2::XMLElement* CBNode, SRenderPassMeta& passMeta);
		void ParseMaterialBuffer(tinyxml2::XMLElement* root, CTechnique* pTech);
		void ParseMaterialTextures(tinyxml2::XMLElement* root, CTechnique* pTech);
		void ParsePassRenderState(tinyxml2::XMLElement* RenderStateNode, SRenderPassMeta& passMeta);

	private:
		TTechniqueMap					mTechniqueMap;
		TValidateShaderMacrosMap		mValidateShaderMacroMap;

	};
}

