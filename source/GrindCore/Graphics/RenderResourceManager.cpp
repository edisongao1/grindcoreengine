#include "stdafx.h"
#include "RenderResourceManager.h"
#include "IEngineCore.h"
#include "GrindEnums.h"
#include "Material.h"
#include "Texture.h"
#include "Mesh.h"
#include "IRenderResourceFactory.h"
#include "ModelFileLoader.h"

using namespace tinyxml2;

namespace grind
{
	static const char* SHADER_TYPES[] = { "VS", "PS", "GS", "HS", "DS", "CS" };

	static void ConvertToHexFromString(const char* s, uint64_t& num)
	{
		sscanf_s(s, "%I64x", &num);
	}


	void CRenderResourceManager::Initialize()
	{
		LoadMacroDefinitions();
		LoadTechniques();
		mTextureManager = std::make_unique<CTextureManager>();
		mMaterialManager = std::make_unique<CMaterialManager>(this);
		mMeshManager = std::make_unique<CMeshManager>();
	}

	void CRenderResourceManager::PreloadResources()
	{
		SGeometryData<SVertex_P3_N3_TG3_T2> meshGeom;
		GeometryGenerator::CreateBox(meshGeom, 2, 2, 2, 1);
		SMeshData meshData = meshGeom.GetMeshData();
		mMeshManager->CreateMesh("box", meshData);

		CModelFileLoader modelFileLoader;
		SMeshData meshData1;
		modelFileLoader.LoadStaticMesh("Models/ln_daoguan01_house03.fbx", meshData1,
			E_FVF_Flag::ContainTangent);
		mMeshManager->CreateMesh("house", meshData1);

		CModelFileLoader modelFileLoader2;
		SMeshData meshData2;
		modelFileLoader2.LoadStaticMesh("Models/cokecan.fbx", meshData2,
			E_FVF_Flag::ContainTangent);
		mMeshManager->CreateMesh("cokecan", meshData2);
	}

	CRenderResourceManager::CRenderResourceManager()
	{
		
	}

	IMaterialResourceManager* CRenderResourceManager::GetMaterialResourceManager()
	{
		return mRenderResourceFactory->GetMaterialResourceManager();
	}

	ITechnique* CRenderResourceManager::GetTechniqueByName(const std::string& name)
	{
		auto it = mTechniqueMap.find(name);
		if (it == mTechniqueMap.end())
			return nullptr;
		return it->second.get();
	}

	void CRenderResourceManager::GetAllTechniques(std::vector<ITechnique*>& vecTechniques) const
	{
		vecTechniques.clear();
		for (auto& it : mTechniqueMap) {
			vecTechniques.push_back(it.second.get());
		}
	}

	uint64_t CRenderResourceManager::GetValidateShaderMacrosMask(const std::string& shaderName) const
	{
		auto it = mValidateShaderMacroMap.find(shaderName);
		if (it == mValidateShaderMacroMap.end())
			return 0;
		return it->second.Mask;
	}

	void CRenderResourceManager::LoadTechniques()
	{
		auto pFileSystem = GetEngineCore()->GetFileSystem();
		pFileSystem->IterateFilesInDirectory("Shaders", "*.technique.xml", [this](const char* filepath) {
			tinyxml2::XMLDocument doc;
			auto err = doc.LoadFile(filepath);
			if (err != XML_SUCCESS)
				return;
			XMLElement* root = doc.RootElement();
			XMLElement* TechNode = root->FirstChildElement("Technique");
			while (TechNode) {
				ParseTechnique(TechNode);
				TechNode = TechNode->NextSiblingElement("Technique");
			}
		});
	}

	void CRenderResourceManager::ParseShaderMacroMaskFromString(const char* s, std::vector<std::string>& macroStrings) {
		if (s == nullptr)
			return;
		
		const char* p1 = s;
		const char* p2 = nullptr;

		while (*p1 != '\0') {
			while (*p1 == ' ' || *p1 == '|') p1++;
			if (*p1 == '\0')
				break;
			p2 = p1;
			while (*p2 != ' ' && *p2 != '|' && *p2 != '\0') p2++;
			std::string str(p1, p2 - p1);
			macroStrings.push_back(str);
			p1 = p2;
		}

		std::sort(macroStrings.begin(), macroStrings.end());
	}

	void CRenderResourceManager::ParseTechnique(tinyxml2::XMLElement* TechNode)
	{
		//static const char* SHADER_TYPES[] = { "VS", "PS", "GS", "HS", "DS", "CS" };		
		//STechniqueData techData;
		auto pTech = std::make_unique<CTechnique>();

		const char* szName = nullptr;
		if (TechNode->QueryStringAttribute("name", &szName) != XML_SUCCESS) {
			// error:
		}

		pTech->mName = szName;
		
		XMLElement* FVFNode = TechNode->FirstChildElement("FVF");
		if (FVFNode == nullptr) {
			// error:
		}
		pTech->mFVF = StringToFVF(FVFNode->GetText());

		XMLElement* MaterialFlagsNode = TechNode->FirstChildElement("MaterialFlags");
		if (MaterialFlagsNode) {
			XMLElement* FlagNode = MaterialFlagsNode->FirstChildElement("Flag");
			while (FlagNode) {
				FlagNode = FlagNode->NextSiblingElement("Flag");
			}
		}

		XMLElement* MaterialBufferNode = TechNode->FirstChildElement("MaterialBuffer");
		if (MaterialBufferNode) {
			ParseMaterialBuffer(MaterialBufferNode, pTech.get());
		}

		XMLElement* MaterialTexturesNode = TechNode->FirstChildElement("MaterialTextures");
		if (MaterialTexturesNode) {
			ParseMaterialTextures(MaterialTexturesNode, pTech.get());
		}


		XMLElement* PassNode = TechNode->FirstChildElement("Pass");
		while (PassNode) {
			ParseTechniquePass(PassNode, *pTech.get());
			PassNode = PassNode->NextSiblingElement("Pass");
		}

		mTechniqueMap.insert(std::make_pair(szName, std::move(pTech)));
	}

	void CRenderResourceManager::ParseMaterialBuffer(tinyxml2::XMLElement* root, CTechnique* pTech)
	{
		int iRegisterId = 0;
		int iRegisterOffset = 0;

		XMLElement* VariableNode = root->FirstChildElement();
		while (VariableNode) {
			const char* szType = VariableNode->Name();
			const char* szDefault = nullptr;
			const char* szName = nullptr;
			if (VariableNode->QueryStringAttribute("name", &szName) != XML_SUCCESS) {
				// error:
			}
			VariableNode->QueryStringAttribute("default", &szDefault);

			ITechnique::SMaterialBufferVariableMeta variableMeta;
			int iVarSize = 0;
			if (_stricmp(szType, "float") == 0) {
				variableMeta.Variable.Type = EVariableType::Float;
				variableMeta.Variable.Value.vFloat = StringParseUtils::Parse<float>(szDefault);
				iVarSize = 1;
			}
			else if (_stricmp(szType, "float2") == 0) {
				variableMeta.Variable.Type = EVariableType::Float2;
				variableMeta.Variable.Value.vFloat2 = StringParseUtils::Parse<Vector2f>(szDefault);
				iVarSize = 2;
			}
			else if (_stricmp(szType, "float3") == 0) {
				variableMeta.Variable.Type = EVariableType::Float3;
				variableMeta.Variable.Value.vFloat3 = StringParseUtils::Parse<Vector3f>(szDefault);
				iVarSize = 3;
			}
			else if (_stricmp(szType, "float4") == 0) {
				variableMeta.Variable.Type = EVariableType::Float4;
				variableMeta.Variable.Value.vFloat4 = StringParseUtils::Parse<Vector4f>(szDefault);
				iVarSize = 4;
			}
			else if (_stricmp(szType, "int") == 0) {
				variableMeta.Variable.Type = EVariableType::Int;
				variableMeta.Variable.Value.vInt = StringParseUtils::Parse<int>(szDefault);
				iVarSize = 1;
			}
			else if (_stricmp(szType, "int2") == 0) {
				variableMeta.Variable.Type = EVariableType::Int2;
				variableMeta.Variable.Value.vInt2 = StringParseUtils::Parse<Vector2i>(szDefault);
				iVarSize = 2;
			}
			else if (_stricmp(szType, "int3") == 0) {
				variableMeta.Variable.Type = EVariableType::Int3;
				variableMeta.Variable.Value.vInt3 = StringParseUtils::Parse<Vector3i>(szDefault);
				iVarSize = 3;
			}
			else if (_stricmp(szType, "int4") == 0) {
				variableMeta.Variable.Type = EVariableType::Int4;
				variableMeta.Variable.Value.vInt4 = StringParseUtils::Parse<Vector4i>(szDefault);
				iVarSize = 4;
			}
			else {
				// error:
			}
			
			if (iRegisterOffset + iVarSize > 4) {
				iRegisterId += 1;
				variableMeta.Offset = (4 * iRegisterId) * 4;
				iRegisterOffset = iVarSize;
			}
			else
			{
				variableMeta.Offset = (4 * iRegisterId + iRegisterOffset) * 4;
				iRegisterOffset += iVarSize;
			}

			pTech->mMaterialVariableMetaMap.insert(std::make_pair(std::string(szName), variableMeta));
			VariableNode = VariableNode->NextSiblingElement();
		}
		if (iRegisterOffset != 0) {
			iRegisterId += 1;
		}
		pTech->mMaterialBufferSize = (4 * iRegisterId) * 4;
	}

	void CRenderResourceManager::ParseMaterialTextures(tinyxml2::XMLElement* root, CTechnique* pTech)
	{
		XMLElement* TextureNode = root->FirstChildElement();
		int slot = 0;
		while (TextureNode) {
			const char* szType = TextureNode->Name();
			const char* szDefault = nullptr;
			const char* szName = nullptr;
			if (TextureNode->QueryStringAttribute("name", &szName) != XML_SUCCESS) {
				// error:
			}

			TextureNode->QueryStringAttribute("default", &szDefault);
			ITechnique::SMaterialTextureMeta textureMeta;
			if (szDefault) {
				textureMeta.DefaultPath = std::string(szDefault);
			}

			textureMeta.Type = StringToTextureType(szType);
			assert(textureMeta.Type != ETextureType::Unknown);

			textureMeta.Slot = slot;

			slot += 1;

			pTech->mMaterialTextureMetaMap.insert(std::make_pair(std::string(szName), textureMeta));
			TextureNode = TextureNode->NextSiblingElement();
		}
	}

	void CRenderResourceManager::LoadMacroDefinitions()
	{
		//static const char* SHADER_TYPES[] = { "VS", "PS", "GS", "HS", "DS", "CS" };
		//static uint8_t	ALL_SHADER_MASK = 0x3f;

		auto pFileSystem = GetEngineCore()->GetFileSystem();
		char szFilePath[MAX_PATH];
		pFileSystem->GetFileFullPath("Shaders/MacroDefinitions.xml", szFilePath);

		tinyxml2::XMLDocument doc;
		if (doc.LoadFile(szFilePath) != XML_SUCCESS)
			return;

		XMLElement* root = doc.RootElement();

		// load macros' list
		XMLElement* MacrosListNode = root->FirstChildElement("MacroList");
		if (MacrosListNode) 
		{
			XMLElement* MacroNode = MacrosListNode->FirstChildElement("Macro");
			while (MacroNode) 
			{
				const char* szName = nullptr;
				const char* szMask = nullptr;
				//const char* szShaderType = nullptr;
				if (MacroNode->QueryStringAttribute("name", &szName) != XML_SUCCESS) {
					// error:
				}
				if (MacroNode->QueryStringAttribute("mask", &szMask) != XML_SUCCESS) {
					// error:
				}
				//if (MacroNode->QueryStringAttribute("shaderTypes", &szShaderType) != XML_SUCCESS) {
				//	// error:
				//}
				uint64_t mask;
				if (szMask[0] == '0' && szMask[1] == 'x') {
					szMask = szMask + 2;
					ConvertToHexFromString(szMask, mask);
				}
				else {
					int bit = StringParseUtils::Parse<int>(szMask);
					mask = (1ULL << bit);
				}
				mShaderMacroMaskMap.insert(std::make_pair(std::string(szName), mask));
				MacroNode = MacroNode->NextSiblingElement("Macro");
			}
		}

		XMLElement* ShaderListNode = root->FirstChildElement("Shaders");
		if (ShaderListNode)
		{
			XMLElement* ShaderNode = ShaderListNode->FirstChildElement("Shader");
			while (ShaderNode)
			{
				const char* szName = nullptr;
				if (ShaderNode->QueryStringAttribute("name", &szName) != XML_SUCCESS) {
					// error:
				}
				std::vector<std::string> macroStringList;
				ParseShaderMacroMaskFromString(ShaderNode->GetText(), macroStringList);
				uint64_t mask = GetShaderMacroMask(macroStringList);
				SValidateShaderMacros shaderMacros;
				shaderMacros.MacroList = std::move(macroStringList);
				shaderMacros.Mask = mask;
				mValidateShaderMacroMap.insert(std::make_pair(std::string(szName), shaderMacros));

				ShaderNode = ShaderNode->NextSiblingElement("Shader");
			}
		}
		//int a = 1;
	}

	//void CRenderResourceManager::LoadShaderMacroMasks()
	//{
	//	static const char* SHADER_TYPES[] = { "VS", "PS", "GS", "HS", "DS", "CS" };
	//	static uint8_t	ALL_SHADER_MASK = 0x3f;

	//	auto pFileSystem = GetEngineCore()->GetFileSystem();
	//	char szFilePath[MAX_PATH];
	//	pFileSystem->GetFileFullPath("Shaders/MacroDefinitions.xml", szFilePath);

	//	tinyxml2::XMLDocument doc;
	//	if (doc.LoadFile(szFilePath) != XML_SUCCESS)
	//		return;

	//	XMLElement* root = doc.RootElement();
	//	
	//}

	ETechniquePassStage CRenderResourceManager::GetPassStageFromString(const char* s)
	{
		if (_stricmp(s, "Forward") == 0) {
			return ePass_Forward;
		}
		// TODO:
		return ePass_Forward;
	}

	void CRenderResourceManager::ParseTechniquePass(tinyxml2::XMLElement* PassNode, CTechnique& techData)
	{
		const char* szStage = nullptr;
		if (PassNode->QueryStringAttribute("stage", &szStage) != XML_SUCCESS) {
			// error:
		}
		ETechniquePassStage stage = GetPassStageFromString(szStage);
		auto& passData = techData.mPassMetas[stage];
		passData.Valid = true;
		passData.Stage = stage;
		passData.TechinqueName = techData.mName;
		passData.FrameConstBufferType = EFrameConstBuffer::None;
		passData.ObjectConstBufferType = EObjectConstBuffer::None;
		
		auto FileNode = PassNode->FirstChildElement("File");
		if (FileNode == nullptr) {
			// error:
		}
		const char* szFileName = FileNode->GetText();
		passData.ShaderFileName = szFileName;

		XMLElement* DefinesNode = PassNode->FirstChildElement("Defines");
		if (DefinesNode) {
			std::vector<std::string> macros;
			ParseShaderMacroMaskFromString(DefinesNode->GetText(), passData.Defines);	
		}

		XMLElement* ConstBuffersNode = PassNode->FirstChildElement("ConstBuffers");
		if (ConstBuffersNode) {
			XMLElement* CBNode = ConstBuffersNode->FirstChildElement("CB");
			while (CBNode) {
				ParsePassConstantBuffer(CBNode, passData);
				CBNode = CBNode->NextSiblingElement("CB");
			}
		}

		for (int i = 0; i < SHADER_TYPE_COUNT; i++) {
			auto ShaderNode = PassNode->FirstChildElement(SHADER_TYPES[i]);
			if (ShaderNode == nullptr)
				continue;
			passData.Shaders[i].Valid = true;
			passData.Shaders[i].EntryPoint = ShaderNode->FirstChildElement("Entry")->GetText();
		}

		XMLElement* RenderStateListNode = PassNode->FirstChildElement("RenderStates");
		if (RenderStateListNode) {
			auto RenderStateNode = RenderStateListNode->FirstChildElement();
			while (RenderStateNode) {
				ParsePassRenderState(RenderStateNode, passData);
				RenderStateNode = RenderStateNode->NextSiblingElement();
			}
		}
		//int a = 1;
	}

	void CRenderResourceManager::ParsePassConstantBuffer(tinyxml2::XMLElement* CBNode, SRenderPassMeta& passMeta)
	{
		//const char* szName = nullptr;
		const char* szType = nullptr;
		const char* szStruct = nullptr;

		if (CBNode->QueryStringAttribute("type", &szType) != XML_SUCCESS) {
			// error:
		}
		if (CBNode->QueryStringAttribute("struct", &szStruct) != XML_SUCCESS) {
			// error:
		}

		if (_stricmp(szType, "object") == 0) {
			passMeta.ObjectConstBufferType = StringToObjectConstBufferType(szStruct);
		}
		else if (_stricmp(szType, "frame") == 0) {
			passMeta.FrameConstBufferType = StringToFrameConstBufferType(szStruct);
		}
	}

	void CRenderResourceManager::ParsePassRenderState(tinyxml2::XMLElement* RenderStateNode, SRenderPassMeta& passMeta)
	{
		const char* szName = RenderStateNode->Name();
		const char* szValue = RenderStateNode->GetText();
		//bool bUndetermined = false;
		if (szValue[0] == '@') {
			szValue = szValue + 1;
			passMeta.mUndeterminedRenderStates.insert(std::make_pair(szValue, szName));
			return;
		}
		passMeta.mRenderStateDesc.SetAttributeFromString(szName, szValue);
	}

}
