#include "stdafx.h"
#include "Texture.h"
#include "IEngineCore.h"
#include "IRenderResourceFactory.h"
#include "AsynchronizeTaskQueue.h"
#include <d3d11_1.h>

#define DDS_FOURCC      0x00000004  // DDPF_FOURCC

namespace grind
{
	//CDummyTexture* g_pDummyNullTexture = new CDummyTexture();
	struct SReadDDSRequest
	{
		CTexture* pTexture;
	};

	struct DDS_HEADER_DXT10
	{
		DXGI_FORMAT     dxgiFormat;
		uint32_t        resourceDimension;
		uint32_t        miscFlag; // see D3D11_RESOURCE_MISC_FLAG
		uint32_t        arraySize;
		uint32_t        miscFlags2;
	};

	const uint32_t DDS_MAGIC = 0x20534444; // "DDS "

	CTextureManager::CTextureManager()
	{
		auto taskQueue = GetEngineCore()->GetAsynchronizeTaskQueue();

		taskQueue->SetRequestHandler<SReadDDSRequest>(ETaskType::ReadDDSData, 
			[](SReadDDSRequest* req, STaskContext* context) {

			ITexture* pTexture = req->pTexture;

			uint8_t* ddsData = nullptr;
			DDS_HEADER* ddsHeader = nullptr;
			uint8_t* bitData = nullptr;
			size_t bitSize = 0;

			char szFullFilePath[MAX_PATH];
			GetEngineCore()->GetFileSystem()->GetFileFullPath(pTexture->GetFileName().c_str(), szFullFilePath);

			auto hr = LoadTextureDataFromFile(szFullFilePath, &ddsData, &ddsHeader, &bitData, &bitSize);
			if (FAILED(hr)) {
				// return fail
			}

			SCreateTextureResourceRequest ctrReq;
			ctrReq.pTexture = pTexture;
			ctrReq.pddsData = ddsData;
			ctrReq.pBitData = bitData;
			ctrReq.bitSize = bitSize;
			ctrReq.pddsHeader = reinterpret_cast<uint8_t*>(ddsHeader);

			// test:
			std::this_thread::sleep_for(std::chrono::milliseconds(1 * 1000));
			
			context->pQueue->PushRequest<ETaskExecuteTime::LoadRenderResources>(
				ETaskType::CreateTextureResource, ctrReq);
		});

		taskQueue->SetRequestHandler<SCreateTextureResourceFinishedRequest>(
			ETaskType::CreateTextureResourceFinished,
			[](SCreateTextureResourceFinishedRequest* req, STaskContext* context)
		{
			//auto pTexture = dynamic_cast<CTexture*>(req->pTexture);
			ITexture* pTexture = req->pTexture;
			ITextureResource* pTextureResource = req->pTextureResource;
			pTexture->OnCompleteLoadResource(pTextureResource);
		});

	}

	ITexture* CTextureManager::GetTexture(const std::string& name)
	{
		auto it = mTextureMap.find(name);
		if (it == mTextureMap.end())
			return nullptr;
		return it->second.get();
	}

	ITexture* CTextureManager::CreateTexture(const std::string& name, ETextureType eType)
	{
		auto it = mTextureMap.find(name);
		if (it != mTextureMap.end()) {
			// warning:
			return it->second.get();
		}

		std::unique_ptr<ITexture> pTexture = std::make_unique<CTexture>(name, eType);
		auto p = pTexture.get();
		mTextureMap.insert(std::make_pair(name, std::move(pTexture)));
		return p;
	}

	//void CTextureManager::InitializeAfterGraphicsCreated(void* p)
	//{
	//	for (auto& it : mTextureMap) {
	//		it.second->InitializeTextureResource(p);
	//	}
	//}

	void CTextureManager::OnCompleteLoadResource()
	{
		//for (auto& it : mTextureMap) {
			//it.second->OnCompleteLoad();
		//}
	}

	void CTexture::InitializeTextureResource(void* p)
	{
		//auto pResourceFactory = GetEngineCore()->GetRenderResourceManager()->GetRenderResourceFactory();
		//auto pTextureResource = pResourceFactory->CreateTextureResource(this, p);
		//mTextureResource = pTextureResource;
	}

	// thread safe: BeforeRenderScene
	void CTexture::OnCompleteLoadResource(ITextureResource* pTextureResource)
	{
		mTextureResource.reset(pTextureResource);
		mTextureResource->OnCompleteLoadResource();
		mState = ETextureState::Loaded;
		mAtomicIsLoading.clear();
	}

	// not thread safe
	void CTexture::OnFailLoadResource()
	{
		mState = ETextureState::LoadFailed;
		mAtomicIsLoading.clear();
	}

	void CTexture::LoadFromFile()
	{
		if (mState != ETextureState::NotLoaded)
			return;

		if (mAtomicIsLoading.test_and_set() == false)
		{
			mState = ETextureState::Loading;
			// 加载纹理第一步：使用Task线程读取DDS文件的数据
			auto taskQueue = GetEngineCore()->GetAsynchronizeTaskQueue();

			SReadDDSRequest req;
			req.pTexture = this;
			taskQueue->PushRequest<ETaskExecuteTime::AsyncImmediately>(ETaskType::ReadDDSData, req);
		}
	}

	HRESULT CTextureManager::LoadTextureDataFromFile(const char* szFileName,
		uint8_t** ddsData,
		DDS_HEADER** header,
		uint8_t** bitData,
		size_t* bitSize)
	{
		std::wstring wstrFileName = StringParseUtils::AnsiToWString(szFileName);

		if (!header || !bitData || !bitSize)
		{
			return E_POINTER;
		}
		// open the file
#if (_WIN32_WINNT >= _WIN32_WINNT_WIN8)
		ScopedHandle hFile(safe_handle(CreateFile2(wstrFileName.c_str(),
			GENERIC_READ,
			FILE_SHARE_READ,
			OPEN_EXISTING,
			nullptr)));
#else
		ScopedHandle hFile(safe_handle(CreateFileW(wstrFileName.c_str(),
			GENERIC_READ,
			FILE_SHARE_READ,
			nullptr,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			nullptr)));
#endif

		if (!hFile)
		{
			return HRESULT_FROM_WIN32(GetLastError());
		}

		// Get the file size
		LARGE_INTEGER FileSize;
		memset(&FileSize, 0, sizeof(LARGE_INTEGER));

#if (_WIN32_WINNT >= _WIN32_WINNT_VISTA)
		FILE_STANDARD_INFO fileInfo;
		if (!GetFileInformationByHandleEx(hFile.get(), FileStandardInfo, &fileInfo, sizeof(fileInfo)))
		{
			return HRESULT_FROM_WIN32(GetLastError());
		}
		FileSize = fileInfo.EndOfFile;
#else
		GetFileSizeEx(hFile.get(), &FileSize);
#endif

		// File is too big for 32-bit allocation, so reject read
		if (FileSize.HighPart > 0)
		{
			return E_FAIL;
		}

		// Need at least enough data to fill the header and magic number to be a valid DDS
		if (FileSize.LowPart < (sizeof(DDS_HEADER) + sizeof(uint32_t)))
		{
			return E_FAIL;
		}

		// create enough space for the file data
		//ddsData.reset(new (std::nothrow) uint8_t[FileSize.LowPart]);
		*ddsData = new (std::nothrow) uint8_t[FileSize.LowPart];
		if (*ddsData == nullptr)
		{
			return E_OUTOFMEMORY;
		}

		// read the data in
		DWORD BytesRead = 0;
		if (!ReadFile(hFile.get(),
			*ddsData,
			FileSize.LowPart,
			&BytesRead,
			nullptr
		))
		{
			return HRESULT_FROM_WIN32(GetLastError());
		}

		if (BytesRead < FileSize.LowPart)
		{
			return E_FAIL;
		}

		// DDS files always start with the same magic number ("DDS ")
		uint32_t dwMagicNumber = *(const uint32_t*)(*ddsData);
		if (dwMagicNumber != DDS_MAGIC)
		{
			return E_FAIL;
		}

		auto hdr = reinterpret_cast<DDS_HEADER*>(*ddsData + sizeof(uint32_t));

		// Verify header to validate DDS file
		if (hdr->size != sizeof(DDS_HEADER) ||
			hdr->ddspf.size != sizeof(DDS_PIXELFORMAT))
		{
			return E_FAIL;
		}

		// Check for DX10 extension
		bool bDXT10Header = false;
		if ((hdr->ddspf.flags & DDS_FOURCC) &&
			(MAKEFOURCC('D', 'X', '1', '0') == hdr->ddspf.fourCC))
		{
			// Must be long enough for both headers and magic value
			if (FileSize.LowPart < (sizeof(DDS_HEADER) + sizeof(uint32_t) + sizeof(DDS_HEADER_DXT10)))
			{
				return E_FAIL;
			}

			bDXT10Header = true;
		}

		// setup the pointers in the process request
		*header = hdr;
		ptrdiff_t offset = sizeof(uint32_t) + sizeof(DDS_HEADER)
			+ (bDXT10Header ? sizeof(DDS_HEADER_DXT10) : 0);
		*bitData = *ddsData + offset;
		*bitSize = FileSize.LowPart - offset;

		return S_OK;
	}
}
