#include "stdafx.h"
#include "GraphicsSystem.h"
#include "IEngineCore.h"
#include "IRenderDevice.h"
#include "IRenderContext.h"
#include "../Core/WorkerThread.h"
#include "IEntityAction.h"

namespace grind
{

	void CGraphicsSystem::BeginFrame()
	{
		// calculate camera transformation
		ISceneManager* pSceneManager = GetEngineCore()->GetISceneManager();
		ICameraAction* pCameraAction = pSceneManager->GetCameraAction();
		Entity* pMainCamera = pSceneManager->GetMainCamera();
		mSceneCameraTransformation = pCameraAction->GetCameraTransformations(pMainCamera);

		IRenderDevice* pRenderDevice = GetEngineCore()->GetRenderDevice();
		pRenderDevice->BeginFrame();
		for (int i = 0; i < pRenderDevice->GetRenderContextCount(); i++)
		{
			IRenderContext* pRenderContext = pRenderDevice->GetRenderContext(i);
			pRenderContext->BeginFrame();
		}
	}

	void CGraphicsSystem::EndFrame()
	{
		IRenderDevice* pRenderDevice = GetEngineCore()->GetRenderDevice();
		pRenderDevice->EndFrame();

		mDrawcallCount = 0;
		for (int i = 0; i < pRenderDevice->GetRenderContextCount(); i++)
		{
			IRenderContext* pRenderContext = pRenderDevice->GetRenderContext(i);
			pRenderContext->EndFrame();
			mDrawcallCount += pRenderContext->GetDrawcallCount();
		}
	}

	void CGraphicsSystem::RenderScene_WorkerThread(CWorkerThread* pWorkerThread)
	{
		CMeshRenderItemQueue* pOpaqueMeshItemQueue = pWorkerThread->GetOpaqueMeshRenderQueue();
		IRenderContext* pRenderContext = pWorkerThread->GetRenderContext();

		pRenderContext->RenderMeshItemQueue(pOpaqueMeshItemQueue);
	}

}