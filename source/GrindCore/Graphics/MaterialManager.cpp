#include "stdafx.h"
#include "Material.h"
#include "tinyxml2.h"
#include "IEngineCore.h"
#include "StringParseUtils.h"
#include "IConsole.h"

using namespace tinyxml2;

namespace grind
{
	int CMaterialManager::s_matGeneratorId = 0;

	CMaterialManager::CMaterialManager(IRenderResourceManager* pRenderResourceManager)
		:mRenderResourceManager(pRenderResourceManager)
	{
		//SRenderStatesDesc desc;
		//auto p1 = &desc;
		//auto p2 = &desc.CullBack;

		//IConsole::Print("SRenderStatesDesc size: %d, p1=%p, p2=%p", sizeof(SRenderStatesDesc), p1, p2);

		//LoadMaterialTemplates();
		LoadAllMaterials();

		CMaterial::StaticInitialize();
	}

	IMaterial* CMaterialManager::GetMaterial(const std::string& name)
	{
		auto it = mMaterialMap.find(name);
		if (it == mMaterialMap.end())
			return nullptr;
		return it->second.get();
	}

	void CMaterialManager::LoadAllMaterials()
	{
		auto pFileSystem = GetEngineCore()->GetFileSystem();
		pFileSystem->IterateFilesInDirectory("Materials", "material.xml", true, [this](const char* filepath) {
			tinyxml2::XMLDocument doc;
			auto err = doc.LoadFile(filepath);
			if (err != XML_SUCCESS)
				return;
			XMLElement* root = doc.RootElement();
			XMLElement* node = root->FirstChildElement("Material");
			while (node) {
				ParseMaterial(node);
				node = node->NextSiblingElement("Material");
			}
		});
	}

	void CMaterialManager::ParseMaterial(tinyxml2::XMLElement* rootNode)
	{
		const char* szName = nullptr;
		if (rootNode->QueryStringAttribute("name", &szName) != XML_SUCCESS) {
			// error:
		}
		XMLElement* TechNode = rootNode->FirstChildElement("Technique");
		if (TechNode == nullptr) {
			// error:
		}

		const char* szTechnique = TechNode->GetText();
		auto pTech = mRenderResourceManager->GetTechniqueByName(szTechnique);

		// 如果已经加载过了
		if (mMaterialMap.find(szName) != mMaterialMap.end())
			return;

		std::unique_ptr<CMaterial> pMat = std::make_unique<CMaterial>(szName, pTech);

		// parse flags
		XMLElement* FlagsNode = rootNode->FirstChildElement("Flags");
		if (FlagsNode) {
			std::vector<std::string> vecFlagStrings;
			StringParseUtils::SplitStringIgnoreSpace(FlagsNode->GetText(), '|', vecFlagStrings);
			pMat->mShaderFlags = std::move(vecFlagStrings);
		}

		// generate flag mask
		pMat->Initialize();

		// parse const buffer variables
		XMLElement* VariablesNode = rootNode->FirstChildElement("Variables");
		if (VariablesNode) {
			XMLElement* VariableNode = VariablesNode->FirstChildElement();
			while (VariableNode) {
				const char* szName = nullptr;
				const char* szType = VariableNode->Name();
				const char* szValue = VariableNode->GetText();
				VariableNode->QueryStringAttribute("name", &szName);
				if (_stricmp(szType, "float") == 0) {
					pMat->SetParam(szName, StringParseUtils::Parse<float>(szValue));
				}
				else if (_stricmp(szType, "float2") == 0) {
					pMat->SetParam(szName, StringParseUtils::Parse<Vector2f>(szValue));
				}
				else if (_stricmp(szType, "float3") == 0) {
					pMat->SetParam(szName, StringParseUtils::Parse<Vector3f>(szValue));
				}
				else if (_stricmp(szType, "float4") == 0) {
					pMat->SetParam(szName, StringParseUtils::Parse<Vector4f>(szValue));
				}
				else if (_stricmp(szType, "int") == 0) {
					pMat->SetParam(szName, StringParseUtils::Parse<int>(szValue));
				}
				else if (_stricmp(szType, "int2") == 0) {
					pMat->SetParam(szName, StringParseUtils::Parse<Vector2i>(szValue));
				}
				else if (_stricmp(szType, "int3") == 0) {
					pMat->SetParam(szName, StringParseUtils::Parse<Vector3i>(szValue));
				}
				else if (_stricmp(szType, "int4") == 0) {
					pMat->SetParam(szName, StringParseUtils::Parse<Vector4i>(szValue));
				}

				VariableNode = VariableNode->NextSiblingElement();
			}
		}

		// parse textures
		auto pTextureManager = mRenderResourceManager->GetTextureManager();

		XMLElement* TextureListNode = rootNode->FirstChildElement("Textures");
		if (TextureListNode) {
			XMLElement* TextureNode = TextureListNode->FirstChildElement();
			while (TextureNode) {
				const char* szType = TextureNode->Name();
				const char* szName = nullptr;
				if (TextureNode->QueryStringAttribute("name", &szName) != XML_SUCCESS) {
					// error:
				}
				ETextureType eTextureType = StringToTextureType(szType);
				const auto pTexMeta = pTech->GetMaterialTextureMeta(szName);
				assert(pTexMeta != nullptr);
				assert(pTexMeta->Type == eTextureType);
				const char* szFileName = TextureNode->GetText();
				if (_stricmp(szFileName, "NULL") != 0) {
					auto pTexture = pTextureManager->GetTexture(szFileName);
					if (pTexture == nullptr) {
						pTexture = pTextureManager->CreateTexture(szFileName, eTextureType);
					}
					pMat->mTextures[pTexMeta->Slot] = pTexture;
				}
				TextureNode = TextureNode->NextSiblingElement();
			}
		}

		// parse render states
		for (int i = 0; i < ePass_Count; i++) {
			const auto& passMeta = pTech->GetRenderPassMeta(i);
			pMat->mRenderStateDescs[i] = passMeta.mRenderStateDesc;
		}

		XMLElement* RenderStateListNode = rootNode->FirstChildElement("RenderStates");
		if (RenderStateListNode) {
			XMLElement* RenderStateVariableNode = RenderStateListNode->FirstChildElement("RenderStateVariable");
			while (RenderStateVariableNode) {
				ParseMaterialRenderStateVariable(RenderStateVariableNode, pMat.get());
				RenderStateVariableNode = RenderStateVariableNode->NextSiblingElement("RenderStateVariable");
			}
		}
		for (int i = 0; i < ePass_Count; i++) {
			pMat->mRenderStateDescs[i].ComputeHash();
		}

		mMaterialMap.insert(std::make_pair(szName, std::move(pMat)));
	}

	void CMaterialManager::ParseMaterialRenderStateVariable(tinyxml2::XMLElement* node, CMaterial* pMaterial)
	{
		const char* szName = nullptr;
		if (node->QueryStringAttribute("name", &szName) != XML_SUCCESS) {
			return;
		}
		const char* szValue = node->GetText();

		auto pTech = pMaterial->GetTechnique();
		for (int i = 0; i < ePass_Count; i++) {
			const auto& passMeta = pTech->GetRenderPassMeta(i);
			auto it = passMeta.mUndeterminedRenderStates.find(szName);
			if (it != passMeta.mUndeterminedRenderStates.end()) {
				const char* szRenderStateAttributeName = it->second.c_str();
				pMaterial->mRenderStateDescs[i].SetAttributeFromString(szRenderStateAttributeName, szValue);
			}
		}
	}

	void CMaterialManager::InitializeAfterGraphicsCreated()
	{
		//for (auto& it : mMaterialMap) {
		//	//it.second->InitializeGPUResources();
		//}
	}

	IMaterialResourceManager* CMaterialManager::GetMaterialResourceManager()
	{
		return mRenderResourceManager->GetMaterialResourceManager();
	}

	IMaterial* CMaterialManager::CopyMaterial(IMaterial* p, const char* pName /*= 0*/)
	{
		CMaterial* pMaterial = dynamic_cast<CMaterial*>(p);
		char szName2[256] = { 0 };
		if (pName) {
			strcpy_s(szName2, pName);
		}
		else {
			sprintf_s(szName2, "%s_%d", pMaterial->GetName().c_str(), ++s_matGeneratorId);
		}

		std::string strName2(szName2);

		if (mMaterialMap.find(strName2) != mMaterialMap.end())
			return nullptr;

		auto pTech = pMaterial->GetTechnique();
		std::unique_ptr<CMaterial> pMaterial2 = std::make_unique<CMaterial>(strName2, pTech);
		pMaterial->Copy(pMaterial2.get());

		auto p2 = pMaterial2.get();
		mMaterialMap.insert(std::make_pair(strName2, std::move(pMaterial2)));
		return p2;
	}

}


