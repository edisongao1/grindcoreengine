#include "stdafx.h"

#include "EngineCore.h"
#include "WorkerThread.h"
#include "IRenderDevice.h"
#include "Configuration.h"
#include "IGameFramework.h"
#include "GrindECS.h"

#include "../Window/Win32Window.h"
#include "../Window/Win32Console.h"
#include "../Utils/FileSystem.h"
#include "../Graphics/RenderResourceManager.h"
#include "../Utils/InputDeviceManager.h"
#include "../Scene/Camera.h"
#include "../Scene/SceneManager.h"
#include "../Scene/SceneLoader.h"
#include "../Graphics/GraphicsSystem.h"

#include <gainput/gainput.h>

namespace grind
{
	IEngineCore*	g_pEngineCore = nullptr;
	IConsole*		g_pConsole = nullptr;

	CEngineCore::CEngineCore()
	{
		assert(g_pEngineCore == nullptr);
		g_pEngineCore = this;
		// initialize ECS world
		mWorld = new FastECS::World();
		mFileSystem = std::make_unique<CFileSystem>();
		mConfiguration = std::make_unique<CConfiguration>();
		mConfiguration->ReadConfigProperties("config.ini");

#ifdef WIN32
		mConsole = std::make_unique<CWin32Console>();
		g_pConsole = mConsole.get();
#endif
		mTimer = std::make_unique<CTimer>();
		mAsynchronizeTaskQueue = std::make_unique<CAsynchronizeTaskQueue>();

		mGraphicsSystem = new CGraphicsSystem();

		mRenderResourceManager = std::make_unique<CRenderResourceManager>();
		mRenderResourceManager->Initialize();
	}

	CEngineCore::~CEngineCore()
	{
		SAFE_DELETE(mSceneManager);
		SAFE_DELETE(mGraphicsSystem);
	}

	void CEngineCore::Stop()
	{
		SAFE_DELETE(mSceneManager);

		FireWorkerThreads(ERequestType::QuitGame);
		for (auto i = 0; i < mWorkerThreadCount; i++) {
			if (mWorkerThreads[i]) {
				delete mWorkerThreads[i];
			}
		}
		CTaskThread::DestroyThreads();
		for (auto i = 0; i < mTaskThreadCount; i++) {
			if (mTaskThreads[i]) {
				delete mTaskThreads[i];
			}
		}
	}

	void CEngineCore::Initialize()
	{
		mWorkerThreadCount = mConfiguration->GetProperty<int>("worker_thread_count");
		if (mWorkerThreadCount == 0)
			mWorkerThreadCount = std::thread::hardware_concurrency();
		
		int iWorkerThreadMaxCount = mConfiguration->GetProperty<int>("worker_thread_max_count");
		iWorkerThreadMaxCount = std::min<int>(iWorkerThreadMaxCount, MAX_WORKDER_THREAD_COUNT);
		
		if (mWorkerThreadCount > iWorkerThreadMaxCount) {
			mWorkerThreadCount = iWorkerThreadMaxCount;
		}

		mTaskThreadCount = mConfiguration->GetProperty<int>("task_thread_count");
		if (mTaskThreadCount > MAX_TASK_THREAD_COUNT) {
			mTaskThreadCount = MAX_TASK_THREAD_COUNT;
		}

		mConsole->RegisterCommand("add", [](IConsole::SConsoleCommand* pCommand) {
			const char* szCommand = pCommand->GetArg(0);
			float a = pCommand->GetArg<float>(1);
			float b = pCommand->GetArg<float>(2);
			IConsole::Print("COMMAND result: %s %f", szCommand, a + b);
		});

		mInputDeviceManager->AddKeyboardButtonListener([](IInputDeviceManager::InputKey key, bool bPressed) {
			if (key == gainput::KeyA) {
				if (bPressed) {
					IConsole::Print("KeyA is Down!");
				}
				else {
					IConsole::Print("KeyA is Up!");
				}
			}
		});

		mInputDeviceManager->AddGamepadButtonListener([](IInputDeviceManager::InputKey key, bool bPressed) {
			if (key == gainput::PadButtonA) {
				if (bPressed) {
					IConsole::Print("PadButtonA is Down");
				}
				else {
					IConsole::Print("PadButtonA is Up");
				}
			}
		});

		mInputDeviceManager->AddGamepadMoveListener([](SInputMovementData& data) {
			if (data.Key == gainput::PadButtonLeftStickX) {
				IConsole::Print("PadButtonLeftStickX moves %f", data.Value);
			}
			else if (data.Key == gainput::PadButtonLeftStickY) {
				IConsole::Print("PadButtonLeftStickY moves %f", data.Value);
			}
			else if (data.Key == gainput::PadButtonRightStickX) {
				IConsole::Print("PadButtonRightStickX moves %f", data.Value);
			}
			else if (data.Key == gainput::PadButtonRightStickY) {
				IConsole::Print("PadButtonRightStickY moves %f", data.Value);
			}
		});
	} 

	bool CEngineCore::CreateMainWindow()
	{
		if (mMainWindow)
			return false;

		SWindowSettings settings;
		settings.Width = mConfiguration->GetProperty<int>("client_width");
		settings.Height = mConfiguration->GetProperty<int>("client_height");
		settings.Style = 0;
		if (mConfiguration->GetProperty<bool>("full_screen")) {
			settings.Style |= eWS_FULLSCREEN;
		}
		if (mConfiguration->GetProperty<bool>("full_resolution")) {
			settings.Style |= eWS_FULLRESOLUTION;
		}


#ifdef WIN32
		//mMainWindow = new CWin32Window();
		mMainWindow = std::make_unique<CWin32Window>();
		mMainWindow->Initialize(settings);
		mMainWindow->SetFrameFunction(std::bind(&CEngineCore::UpdateFrame, this));

		mInputDeviceManager = std::make_unique<CInputDeviceManager>(settings.Width, settings.Height);
#endif // WIN32


		float fAspectRadio = (float)settings.Width / (float)settings.Height;
		float fov = 0.25f * PI;
		//mCamera = new CCamera(Vector3f(0, 0.5f, 5.0f), Vector2f(settings.Width, settings.Height), fov, 0.1f, 1000.0f, false);
		//mCamera = new CFpsCamera(Vector3f(0, 0, -5.0f), Vector2f(settings.Width, settings.Height), 
		//	fov, 0.1f, 1000.0f, glm::pi<float>() * 0.25, glm::pi<float>() * 0.25);
		//mCamera->LookAt(Vector3f(0, 1, 0));

		return true;
	}

	void CEngineCore::UpdateCamera(float fDeltaTime)
	{
		//ICamera* pCamera = mSceneManager->GetMainCamera();

		//if (mInputDeviceManager->IsKeyboardPressed(gainput::KeyW)) {
		//	pCamera->MoveForward(1.0f * fDeltaTime);
		//}
		//if (mInputDeviceManager->IsKeyboardPressed(gainput::KeyS)) {
		//	pCamera->MoveForward(-1.0f * fDeltaTime);
		//}
		//if (mInputDeviceManager->IsKeyboardPressed(gainput::KeyD)) {
		//	pCamera->MoveRight(1.0f * fDeltaTime);
		//}
		//if (mInputDeviceManager->IsKeyboardPressed(gainput::KeyA)) {
		//	pCamera->MoveRight(-1.0f * fDeltaTime);
		//}
		//if (mInputDeviceManager->IsKeyboardPressed(gainput::KeyR)) {
		//	pCamera->MoveUp(1.0f * fDeltaTime);
		//}
		//if (mInputDeviceManager->IsKeyboardPressed(gainput::KeyF)) {
		//	pCamera->MoveUp(-1.0f * fDeltaTime);
		//}
		//pCamera->Update(fDeltaTime);
	}

	bool CEngineCore::UpdateFrame()
	{
		GetTimer()->Tick();
		float fDeltaTime = mTimer->GetTickDelta();

		mConsole->RunCachedCommands();
		mInputDeviceManager->Update(fDeltaTime);

		mGameFramework->OnFrameUpdate(fDeltaTime);

		UpdateCamera(fDeltaTime);

		if (mInputDeviceManager->IsKeyboardPressed(gainput::KeyEscape))
			return false;

		//mRenderDevice->BeginFrame();
		mGraphicsSystem->BeginFrame();
		mAsynchronizeTaskQueue->ProcessTasks(ETaskExecuteTime::BeforeRenderScene_SyncResourceFrame);
		mAsynchronizeTaskQueue->ProcessTasks(ETaskExecuteTime::BeforeRenderScene);
		mSceneManager->CullEntities();

		FireWorkerThreads(ERequestType::LoadRenderResources);
		FireWorkerThreads(ERequestType::RenderScene);

		mGraphicsSystem->EndFrame();
		//mRenderDevice->EndFrame();

		DisplayPerformanceStats();
		return true;
	}

	void CEngineCore::StartLoop()
	{
		for (int i = 0; i < mTaskThreadCount; i++) {
			mTaskThreads[i] = new CTaskThread(i);
		}

		//mWorkerThreadCount = 1;
		//mBarrier = new CLockedBarrier(mWorkerThreadCount + 1);
		mBarrier = std::make_shared<CLockedBarrier>(mWorkerThreadCount + 1);
		for (int i = 0; i < mWorkerThreadCount; i++) {
			mWorkerThreads[i] = new CWorkerThread(i, this);
		}

		// init RenderContext
		//mRequestType = ERequestType::INIT_RENDER_CONTEXT;
		//FireWorkerThreads();
		//mGraphicsDevice->FlushCommandQueue();
		//mRenderDevice->FlushCommandQueue();

		// create scene manager

		mRenderResourceManager->PreloadResources();

		LoadScene();

		mTimer->Reset();
		mRequestType = ERequestType::Unknown;
		if (mMainWindow)
		{
			mMainWindow->StartLoop();
		}
	}

	void CEngineCore::Release()
	{
		delete this;
	}

	void CEngineCore::FireWorkerThreads(ERequestType eRequestType)
	{
		mRequestType = eRequestType;
		mBarrier->Sync();
		mBarrier->Sync();
		mRequestType = ERequestType::Unknown;
	}

	ISceneManager* CEngineCore::GetISceneManager()
	{
		return mSceneManager;
	}

	IGraphicsSystem* CEngineCore::GetIGraphicsSystem()
	{
		return mGraphicsSystem;
	}

	void CEngineCore::LoadScene()
	{
		//const char* szSceneManagerType = mConfiguration->GetProperty("scene_manager", "default");
		//if (_stricmp(szSceneManagerType, "default") == 0)
		//{
		//	mSceneManager = new CDefaultSceneManager();
		//}
		//else if (_stricmp(szSceneManagerType, "section") == 0)
		//{

		//}

		//mSceneManager->Load("hello");

		CSceneLoader sceneLoader;
		mSceneManager = sceneLoader.LoadFromXml("Scenes/sample01.scene.xml");
		mGameFramework->OnSceneLoaded(mSceneManager);
	}

	void CEngineCore::DisplayPerformanceStats()
	{
		static int frameCnt = 0;
		static float timeElapsed = 0.0f;
		static float fMaxTimeInterval = 0.1f;

		frameCnt++;
		float t = mTimer->GetElapseTime();
		
		if ((t - timeElapsed) >= fMaxTimeInterval)
		{
			float fps = (float)frameCnt / fMaxTimeInterval; // fps = frameCnt / 1
			float mspf = 1000.0f / fps;

			std::string fpsStr = std::to_string(fps);
			std::string mspfStr = std::to_string(mspf);

			auto pInput = GetEngineCore()->GetInputDeviceManager();
			float mouseX = pInput->GetFloat(EInputDeviceType::Mouse, gainput::MouseAxisX);
			int drawcallCount = mGraphicsSystem->GetDrawcallCount();

			const char* szTitle = mConfiguration->GetProperty<const char*>("window_title");
			std::string windowText = std::string(szTitle) +
				"    fps: " + fpsStr +
				"   mspf: " + mspfStr + "   DP: " + std::to_string(drawcallCount);

			mMainWindow->SetCaption(windowText.c_str());

			// Reset for next average.
			frameCnt = 0;
			timeElapsed += fMaxTimeInterval;
		}
	}

	int CEngineCore::GetResourceFrameIndex() const
	{
		return mRenderDevice->GetResourceFrameIndex();
	}

}

