#include "stdafx.h"
#include "TaskThread.h"
#include "IEngineCore.h"
#include "../Utils/ThreadUtils.h"
#include "AsynchronizeTaskQueue.h"

namespace grind
{
	struct SEmptyTaskRequest {};
	int CTaskThread::s_ThreadCount = 0;

	CTaskThread::CTaskThread(int id)
		:mId(id)
		, mThread(std::mem_fn(&CTaskThread::Run), this)
	{
		s_ThreadCount += 1;
		SetThreadName(mThread, "Task Thread (%d)", mId);
	}

	CTaskThread::~CTaskThread()
	{
		if (mThread.joinable()) {
			mThread.join();
		}
	}

	//void CTaskThread::StaticInitialize()
	//{
	//	IAsynchronizeTaskQueue* queue = GetEngineCore()->GetAsynchronizeTaskQueue();
	//	queue->SetRequestHandler<SEmptyTaskRequest>(ETaskType::QuitGame, [](SEmptyTaskRequest*) {
	//		
	//	});
	//}

	void CTaskThread::DestroyThreads()
	{
		auto queue = GetEngineCore()->GetAsynchronizeTaskQueue();
		for (int i = 0; i < s_ThreadCount; i++) {
			queue->PushRequest<ETaskExecuteTime::AsyncImmediately>(ETaskType::QuitGame, SEmptyTaskRequest());
		}
	}

	void CTaskThread::Run()
	{
		auto queue = GetEngineCore()->GetAsynchronizeTaskQueue();

		mStopped = false;
		int idleTimes = 0;

		while (!mStopped)
		{
			STaskRequest req;
			bool ret = queue->TryPopRequest<ETaskExecuteTime::AsyncImmediately>(req);
			if (ret) {
				idleTimes = 0;
				if (req.Type == ETaskType::QuitGame) {
					mStopped = true;
				}
				else {
					queue->ProcessRequest(req);
				}
			}
			else {
				idleTimes += 1;
				if (idleTimes > 20) {
					idleTimes = 0;
					std::this_thread::sleep_for(std::chrono::milliseconds(100));
				}
			}
		}
	}

}


