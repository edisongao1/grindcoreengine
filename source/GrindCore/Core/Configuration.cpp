#include "stdafx.h"
#include "Configuration.h"
#include "IEngineCore.h"
#include "../Utils/cpp-properties/PropertiesParser.h"

#define DefineConfigProperty(key, type, defaultValue) \
	AddProperty<type>(key, defaultValue);

namespace grind
{
	CConfiguration::CConfiguration()
	{
		// for window
		AddProperty<int>("client_width", 800);
		AddProperty<int>("client_height", 600);
		AddProperty<const char*>("window_title", "GameApp");
		AddProperty<bool>("full_resolution", false);
		AddProperty<bool>("full_screen", false);

		// for render system
		AddProperty<const char*>("renderer", "Null");
		AddProperty<bool>("renderer_debug_enabled", false);
		AddProperty<int>("msaa_sample_count", 1);

		// for game framework
		AddProperty<const char*>("game_framework", "");

		// for scene
		AddProperty<const char*>("scene_manager", "default");

		// for engine core
		AddProperty<int>("worker_thread_count", 0);
		AddProperty<int>("worker_thread_max_count", 32);
		AddProperty<int>("task_thread_count", 2);
	}

	void CConfiguration::ReadConfigProperties(const char* szConfigFileName)
	{
		auto pFileSystem = GetEngineCore()->GetFileSystem();
		char szConfigFilePath[MAX_PATH];
		pFileSystem->GetFileFullPath(szConfigFileName, szConfigFilePath);
		cppproperties::Properties props = cppproperties::PropertiesParser::Read(szConfigFilePath);
		std::vector<std::string> propertyNames = props.GetPropertyNames();
		for (auto key : propertyNames) {
			std::string valueStr = props.GetProperty(key);
			//IConsole::Print("%s, %s", key.c_str(), valueStr.c_str());
			SetPropertyByString(key.c_str(), valueStr.c_str());
		}
	}
	


}

