#pragma once
#include "Barrier.h"
#include "IRenderContext.h"
#include "RenderItem.h"

namespace grind
{
	class CEngineCore;
	class IRenderDevice;

	class CWorkerThread
	{
	public:
		CWorkerThread(int id, CEngineCore* pEngineCore);

		~CWorkerThread();

		void Run();
		void ProcessRequest();
		
		//CMeshRenderItemQueue* GetStaticMeshRenderQueue() { return &mStaticMeshRenderQueue; }
		
		CMeshRenderItemQueue* GetOpaqueMeshRenderQueue() { return &mOpaqueMeshRenderQueue; }
		IRenderContext* GetRenderContext() { return mRenderContext; }

	private:
		int						mId;
		bool					mStopped = false;
		//CLockedBarrier*			mBarrier;
		CEngineCore*			mEngineCore;
		std::shared_ptr<CLockedBarrier>		mBarrier;
		IRenderContext*			mRenderContext;
		IRenderDevice*			mRenderDevice;

		//CMeshRenderItemQueue	mStaticMeshRenderQueue;
		CMeshRenderItemQueue	mOpaqueMeshRenderQueue;

		std::thread				mThread;
		//std::shared_ptr<IRenderDevice>	mGraphicsDevice;
		
	};
}

