#ifndef __GRINDCORE_H__
#define __GRINDCORE_H__

#ifdef GrindCore_EXPORTS
#define GRINDCORE_API extern "C" __declspec(dllexport)
#else
#define GRINDCORE_API extern "C" __declspec(dllimport)
#endif

#include "IEngineCore.h"
#include "IWindow.h"

namespace grind 
{
	class IEngineCore;

	// CreateApp
	GRINDCORE_API IEngineCore* CreateEngineCore();

	GRINDCORE_API IWindow* CreateAppWindow(const SWindowSettings& settings);
}

#endif // __GRINDCORE_H__
