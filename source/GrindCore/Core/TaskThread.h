#pragma once

namespace grind
{
	class CTaskThread
	{
	public:
		CTaskThread(int id);
		~CTaskThread();

		//static void StaticInitialize();
		static void DestroyThreads();

		void Run();
		
	private:
		int						mId;
		bool					mStopped = false;
		std::thread				mThread;

		static int s_ThreadCount;
	};
}
