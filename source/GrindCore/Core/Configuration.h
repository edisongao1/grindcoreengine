#pragma once

#include "IConfiguration.h"




namespace grind
{
	class CConfiguration : public IConfiguration
	{
	public:
		CConfiguration();
		void ReadConfigProperties(const char* szConfigFileName);
	};
}


