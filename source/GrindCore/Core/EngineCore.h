#pragma once

#include "IEngineCore.h"
#include "Barrier.h"
#include "WorkerThread.h"
#include "TaskThread.h"
#include "AsynchronizeTaskQueue.h"
#include "Timer.h"

namespace grind
{
	class CSceneManager;
	class CGraphicsSystem;

	class CEngineCore : public IEngineCore
	{
		friend class CWorkerThread;
		enum { MAX_WORKDER_THREAD_COUNT = 32 };
		enum { MAX_TASK_THREAD_COUNT = 8};

	public:
		CEngineCore();
		virtual ~CEngineCore();

		virtual void Initialize() override;
		virtual bool CreateMainWindow() override;
		bool UpdateFrame();
		virtual void StartLoop() override;
		virtual void Release() override;
		virtual void Stop() override;
		void DisplayPerformanceStats();
		virtual int GetResourceFrameIndex() const override;
		virtual CAsynchronizeTaskQueue* GetAsynchronizeTaskQueue() override
		{
			return mAsynchronizeTaskQueue.get();
		}

		//virtual ICamera* GetCamera() override { return mCamera; }

		void UpdateCamera(float fDeltaTime);
		virtual void FireWorkerThreads(ERequestType eRequestType) override;
		virtual ISceneManager* GetISceneManager() override;
		CSceneManager* GetSceneManager() { return mSceneManager; }
		//virtual IGraphicsSystem* GetIGraphicsSystem() override;
		CGraphicsSystem* GetGraphicsSystem() { return mGraphicsSystem; }
		virtual IGraphicsSystem* GetIGraphicsSystem() override;

	private:
		

		volatile ERequestType		mRequestType = ERequestType::Unknown;

		std::unique_ptr<CAsynchronizeTaskQueue>		mAsynchronizeTaskQueue;
		CWorkerThread*	mWorkerThreads[MAX_WORKDER_THREAD_COUNT] = { 0 };
		CTaskThread*	mTaskThreads[MAX_TASK_THREAD_COUNT] = { 0 };
		std::shared_ptr<CLockedBarrier>	mBarrier;
		//ICamera*	mCamera = nullptr;


		void LoadScene();
		CSceneManager* mSceneManager = nullptr;
		CGraphicsSystem* mGraphicsSystem = nullptr;
	};

	inline CAsynchronizeTaskQueue* GetAsynchronizeTaskQueue() 
	{
		auto pEngineCore = (CEngineCore*)GetEngineCore();
		return (CAsynchronizeTaskQueue*)pEngineCore->GetAsynchronizeTaskQueue();
	}

}

