#include "stdafx.h"
#include "WorkerThread.h"
#include "EngineCore.h"
#include "IRenderDevice.h"
#include "../Utils/ThreadUtils.h"
#include "IConsole.h"
#include "Common.h"
#include "../Scene/SceneManager.h"
#include "../Graphics/GraphicsSystem.h"

namespace grind
{
	CWorkerThread::CWorkerThread(int id, CEngineCore* pEngineCore)
		: mId(id)
		, mEngineCore(pEngineCore)
		, mBarrier(pEngineCore->mBarrier)
		, mRenderContext(nullptr)
		, mRenderDevice(pEngineCore->mRenderDevice)
		, mThread(std::mem_fn(&CWorkerThread::Run), this)
	{
		SetThreadName(mThread, "Worker Thread (%d)", mId);
		mRenderContext = mRenderDevice->GetRenderContext(id);

		//std::chrono::milliseconds t(12345);
		//auto t2 = std::chrono::duration_cast<std::chrono::seconds>(t).count();
		//auto t3 = std::chrono::duration_cast<std::chrono::duration<float>>(t).count();
		//int a = 1;
	}

	CWorkerThread::~CWorkerThread()
	{
		if (mThread.joinable()) {
			mThread.join();
		}
	}

	void CWorkerThread::Run()
	{
		srand((unsigned int)(time(NULL) + mId));
		IConsole::Print("CWorkerThread::Run %d", mId);

		mStopped = false;
		while (!mStopped)
		{
			mBarrier->Sync();
			ProcessRequest();
			mBarrier->Sync();
		}
	}

	void CWorkerThread::ProcessRequest()
	{
		auto requestType = mEngineCore->mRequestType;
		switch (requestType)
		{
		case ERequestType::QuitGame:
			mStopped = true;
			break;
		case ERequestType::InitRenderContext:
			//InitRenderContext();
			break;
		case ERequestType::RenderScene:
			//mRenderContext->Render();
			mEngineCore->GetGraphicsSystem()->RenderScene_WorkerThread(this);
			break;
		case ERequestType::LoadRenderResources:
		{
			auto taskQueue = GetEngineCore()->GetAsynchronizeTaskQueue();
			STaskContext context;
			context.pRenderContext = mRenderContext;

			mRenderContext->BeginCommands();
			taskQueue->ProcessTasks(ETaskExecuteTime::LoadRenderResources, &context);
			mRenderContext->EndCommands();
		}
		case ERequestType::Unknown:
			// throw error
			break;
		case ERequestType::PrepareSceneResources:
			break;
		case ERequestType::CullScene:
			mEngineCore->GetSceneManager()->CullEntities_WorkerThread(this);
			break;
		}
	}

	//void CWorkerThread::InitRenderContext()
	//{
	//	mRenderContext = mRenderDevice->CreateRenderContext(mId);
	//	mRenderContext->Init();
	//}

	//void CWorkerThread::OnCreate()
	//{
	//	mRenderContext = mRenderDevice->CreateRenderContext(mId);
	//	mRenderContext->Init();
	//}

}
