//#pragma GCC diagnostic push
//_Pragma("GCC diagnostic ignored \"-Wmicrosoft-include\"")


#include "stdafx.h"
#include "Core/GrindCore.h"
#include "IGraphicsFactory.h"
#include "IGameFramework.h"
#include "GrindException.h"
#include <Windows.h>

using CreateAppWindowPtr = grind::IWindow*(*)(const grind::SWindowSettings&);

using namespace grind;
//using CreateAppWindowPtr = void(*)(bool);

//#define SCREEN_WIDTH 800
//#define SCREEN_HEIGHT 600

int main()
{
	IEngineCore* pEngineCore = nullptr;
	IWindow* pWindow = nullptr;
	IGraphicsFactory* pGraphicsFactory = nullptr;
	IRenderDevice* pRenderDevice = nullptr;
	IConfiguration* pConfiguration = nullptr;
	IGameFramework* pGameFramework = nullptr;

	// create engine
	{
		HINSTANCE hCoreDll = LoadLibraryA("GrindCore");
		if (hCoreDll == 0) {
			throw CException("Failed in loading enginecore module.");
		}

		auto pCreateEngineCore = (IEngineCore::TCreateFunction)GetProcAddress(hCoreDll, "CreateEngineCore");
		pEngineCore = pCreateEngineCore();
		pConfiguration = pEngineCore->GetConfiguration();
		
		// create game framework
		const char* szGameFramework = pConfiguration->GetProperty("game_framework", "");
		if (_stricmp(szGameFramework, "") == 0) {
			throw CException("a property 'game_framework' must be defined in configuration file.");
		}

		HINSTANCE hGameDll = LoadLibraryA(szGameFramework);
		if (hGameDll == 0) {
			throw CException("Failed in loading game framework.");
		}

		auto pCreateGameFramework = (IGameFramework::TCreateFunction)GetProcAddress(hGameDll, "CreateGameFramework");
		TSetGlobalEnvironmnet pSetGlobalEnv = (TSetGlobalEnvironmnet)GetProcAddress(hGameDll, "SetGlobalEnvironment");

		pGameFramework = pCreateGameFramework();
		pSetGlobalEnv(pEngineCore); // call SetGlobalEnvironment
		pEngineCore->_SetGameFramework(pGameFramework);
		pGameFramework->OnCreate();

		pEngineCore->CreateMainWindow();
		pWindow = pEngineCore->GetMainWindow();

		pGameFramework->OnAfterCreateWindow();
	}
	
	// create renderer system
	{
		const char* szRendererType = pConfiguration->GetProperty<const char*>("renderer");
		const char* szModuleName = "";
		if (_stricmp(szRendererType, "D3D12") == 0)
			szModuleName = "GrindD3D12";

		HINSTANCE hRendererDll = LoadLibraryA(szModuleName);
		if (hRendererDll == 0) {
			throw CException("Failed in loading rendering module.");
		}

		TSetGlobalEnvironmnet pSetGlobalEnv = (TSetGlobalEnvironmnet)GetProcAddress(hRendererDll, "SetGlobalEnvironment");
		pSetGlobalEnv(pEngineCore);

		IGraphicsFactory::TCreateFunction pCreateFactory = (IGraphicsFactory::TCreateFunction)GetProcAddress(hRendererDll, "CreateFactory");
		pGraphicsFactory = pCreateFactory();
		pRenderDevice = pGraphicsFactory->CreateRenderDevice();
		pEngineCore->_SetRenderDevice(pRenderDevice);
	}

	pGameFramework->OnBeforeInitialize();

	pEngineCore->Initialize();
	pRenderDevice->Initialize();

	pGameFramework->OnAfterInitialize();

	//pWindow->SetFrameFunction([pGraphicsDevice]() {
	//	//printf("Hello\n");
	//	pGraphicsDevice->Render();
	//	return true;
	//});

	//pWindow->StartLoop();
	pEngineCore->StartLoop();

	pGameFramework->OnExitGame();

	pEngineCore->Stop();

	pGraphicsFactory->ReleaseRenderDevice(pRenderDevice);

	pGameFramework->OnDestroy();
	pGameFramework->Release();

	pEngineCore->Release();

	//std::cout << CreateApp("abc") << std::endl;
	//system("pause");
	return 0;
}
