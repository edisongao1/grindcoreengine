#pragma once

#include "IEngineCore.h"
#include "IGameFramework.h"

using namespace grind;

class CSampleSceneLoadListener;
class CCameraSystem;

class CSampleGameFramework : public IGameFramework
{
public:
	virtual void OnCreate() override;
	virtual void OnBeforeInitialize() override;
	virtual void OnAfterInitialize() override;
	virtual void OnFrameUpdate(float dt) override;
	virtual void OnExitGame() override;
	virtual void OnDestroy() override;
	virtual void OnAfterCreateWindow() override;
	virtual void OnSceneLoaded(ISceneManager* pSceneManager) override;
	
	virtual ISceneLoadListener* GetSceneLoadListener() override;

private:
	CSampleSceneLoadListener*	mSceneLoadListener = nullptr;
	CCameraSystem*				mCameraSystem = nullptr;

};


extern "C" __declspec(dllexport) IGameFramework * CreateGameFramework();
extern "C" __declspec(dllexport) void SetGlobalEnvironment(IEngineCore * pEngineCore);


