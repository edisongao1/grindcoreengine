#include "Camera.h"
#include <gainput/gainput.h>

Vector3f CFpsCameraAction::GetWalkDir(Entity* pEntity)
{
	TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
	Vector3f look = -GetLocalAxis(pEntity, EAxis::Z);
	Vector3f right1 = glm::cross(look, mWorldUp);
	Vector3f walkDir = glm::cross(mWorldUp, right1);
	return glm::normalize(walkDir);
}

float CFpsCameraAction::GetPitchRadians(Entity* pEntity)
{
	Vector3f look = -GetLocalAxis(pEntity, EAxis::Z);
	Vector3f walkDir = GetWalkDir(pEntity);	
	float radians = glm::angle(walkDir, look);
	return look.y > 0 ? radians : -radians;
}

void CFpsCameraAction::SetPitchRadians(Entity* pEntity, float radians)
{
	Vector3f right = GetLocalAxis(pEntity, EAxis::X);
	Vector3f walkDir = GetWalkDir(pEntity);
	Vector3f look = glm::rotate(walkDir, radians, right);
	Vector3f up = glm::cross(right, look);

	Vector3f localAxes[3];
	localAxes[(int)EAxis::X] = right;
	localAxes[(int)EAxis::Y] = up;
	localAxes[(int)EAxis::Z] = -look;
	SetLocalAxes(pEntity, localAxes);
}

void CFpsCameraAction::MoveRight(Entity* pEntity, float fDist)
{
	TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
	Vector3f look = -GetLocalAxis(pEntity, EAxis::Z);
	Vector3f right = glm::normalize(glm::cross(look, mWorldUp));
	pTransform->Position += right * fDist;
}

void CFpsCameraAction::MoveUp(Entity* pEntity, float fDist)
{
	TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
	pTransform->Position += mWorldUp * fDist;
}

void CFpsCameraAction::MoveForward(Entity* pEntity, float fDist)
{
	TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
	Vector3f walkDir = GetWalkDir(pEntity);
	pTransform->Position += walkDir * fDist;
}

void CFpsCameraAction::Rotate(Entity* pEntity, EAxis axis, float radians)
{
	if (axis == EAxis::X)
	{
		float fPitchRadians = GetPitchRadians(pEntity);
		fPitchRadians += radians;
		if (fPitchRadians > mMaxUpAngle)
			fPitchRadians = mMaxUpAngle;

		if (fPitchRadians < -mMaxDownAngle)
			fPitchRadians = -mMaxDownAngle;

		SetPitchRadians(pEntity, fPitchRadians);
	}
	else if (axis == EAxis::Y)
	{
		TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
		Quaternion q1 = pTransform->Rotation;
		Quaternion q2 = glm::identity<Quaternion>();
		q2 = glm::rotate(q2, radians, mWorldUp);
		pTransform->Rotation = q2 * q1;

		float fPitchRadians = GetPitchRadians(pEntity);
		Vector3f walkDir = GetWalkDir(pEntity);
		Quaternion yawQuat = glm::identity<Quaternion>();
		yawQuat = glm::rotate(yawQuat, radians, mWorldUp);
		walkDir = yawQuat * walkDir;

		Vector3f right = glm::normalize(glm::cross(walkDir, mWorldUp));
		Quaternion pitchQuat = glm::identity<Quaternion>();
		pitchQuat = glm::rotate(pitchQuat, fPitchRadians, right);
		Vector3f look = pitchQuat * walkDir;

		Vector3f up = glm::normalize(glm::cross(right, look));

		Vector3f localAxes[3];
		localAxes[(int)EAxis::X] = right;
		localAxes[(int)EAxis::Y] = up;
		localAxes[(int)EAxis::Z] = -look;
		SetLocalAxes(pEntity, localAxes);
	}
}


CCameraSystem::CCameraSystem(ISceneManager* pSceneManager)
	:mSceneManager(pSceneManager)
{
	mFpsCameraAction = std::make_unique<CFpsCameraAction>(pSceneManager,
		glm::pi<float>() * 0.25f,
		glm::pi<float>() * 0.25f);

	IInputDeviceManager* pInput = GetEngineCore()->GetInputDeviceManager();
	mMouseMoveListenerId = pInput->AddMouseMoveListener(std::bind(&CCameraSystem::OnMouseMovement, this, std::placeholders::_1));
	mKeyboardListenerId = pInput->AddKeyboardButtonListener(
		std::bind(&CCameraSystem::OnKeyboardButton, this, std::placeholders::_1, std::placeholders::_2));

	mMouseButtonListenerId = pInput->AddMouseButtonListener(std::bind(&CCameraSystem::OnMouseButton,
		this, std::placeholders::_1, std::placeholders::_2));

	SetCameraMode(ECameraMode::FpsCamera);
}

CCameraSystem::~CCameraSystem()
{
	IInputDeviceManager* pInput = GetEngineCore()->GetInputDeviceManager();
	pInput->RemoveMouseMoveListener(mMouseMoveListenerId);
	pInput->RemoveKeyboardButtonListener(mKeyboardListenerId);
	pInput->RemoveMouseButtonListener(mMouseButtonListenerId);
}

void CCameraSystem::Update(float fDeltaTime)
{
	if (mCameraMode == ECameraMode::FpsCamera)
	{
		UpdateFpsCamera(fDeltaTime);
	}
	else if (mCameraMode == ECameraMode::EditorCamera)
	{
		UpdateEditorCamera(fDeltaTime);
	}
}

void CCameraSystem::SetCameraMode(ECameraMode cameraMode)
{
	IInputDeviceManager* pInput = GetEngineCore()->GetInputDeviceManager();

	mCameraMode = cameraMode;
	if (mCameraMode == ECameraMode::FpsCamera)
	{
		pInput->ShowMouseCursor(false);
	}
	else if (mCameraMode == ECameraMode::EditorCamera)
	{
		pInput->ShowMouseCursor(true);
	}
}

void CCameraSystem::UpdateFpsCamera(float fDeltaTime)
{
	Entity* pCameraEntity = mSceneManager->GetMainCamera();

	// translate
	IInputDeviceManager* pInput = GetEngineCore()->GetInputDeviceManager();
	if (pInput->IsKeyboardPressed(gainput::KeyW)) {
		mFpsCameraAction->MoveForward(pCameraEntity, mFpsTranslateSpeed * fDeltaTime);
	}
	if (pInput->IsKeyboardPressed(gainput::KeyS)) {
		mFpsCameraAction->MoveForward(pCameraEntity, -mFpsTranslateSpeed * fDeltaTime);
	}
	if (pInput->IsKeyboardPressed(gainput::KeyD)) {
		mFpsCameraAction->MoveRight(pCameraEntity, mFpsTranslateSpeed * fDeltaTime);
	}
	if (pInput->IsKeyboardPressed(gainput::KeyA)) {
		mFpsCameraAction->MoveRight(pCameraEntity, -mFpsTranslateSpeed * fDeltaTime);
	}
	if (pInput->IsKeyboardPressed(gainput::KeyR)) {
		mFpsCameraAction->MoveUp(pCameraEntity, mFpsTranslateSpeed * fDeltaTime);
	}
	if (pInput->IsKeyboardPressed(gainput::KeyF)) {
		mFpsCameraAction->MoveUp(pCameraEntity, -mFpsTranslateSpeed * fDeltaTime);
	}

	// rotate
	float dx = mMouseMovementDelta.x;
	float dy = mMouseMovementDelta.y;

	// the movement should not be too large
	if (dy * dy + dx * dx < 0.3f * 0.3f)
	{
		if (mMouseMovementDelta.x != 0.0f/* && fabs(mMouseMovementDelta.X) < 0.3f*/) {
			mFpsCameraAction->Rotate(pCameraEntity, EAxis::Y, -mMouseMovementDelta.x * mFpsRotateSpeed);
		}
		if (mMouseMovementDelta.y != 0.0f/* && fabs(mMouseMovementDelta.Y) < 0.3f*/) {
			mFpsCameraAction->Rotate(pCameraEntity, EAxis::X, -mMouseMovementDelta.y * mFpsRotateSpeed);
		}
	}

	// reset
	mMouseMovementDelta.x = 0;
	mMouseMovementDelta.y = 0;

	float mouseX = pInput->GetFloat(EInputDeviceType::Mouse, gainput::MouseAxisX);
	float mouseY = pInput->GetFloat(EInputDeviceType::Mouse, gainput::MouseAxisY);

	// set mouse cursor to the center of screen
	// if it is on the edge of the screen.
	if (mouseX < 0.1 || mouseY < 0.1 || mouseX > 0.9f || mouseY > 0.9f)
	{
		pInput->SetMouseCursorPos(0, 0);
	}
}

void CCameraSystem::UpdateEditorCamera(float fDeltaTime)
{
	Entity* pCameraEntity = mSceneManager->GetMainCamera();
	//ICameraAction* pCameraAction = mSceneManager->GetCameraAction();
	IInputDeviceManager* pInput = GetEngineCore()->GetInputDeviceManager();
	TransformComponent* pTransform = pCameraEntity->GetComponent<TransformComponent>();

	if (pInput->IsMousePressed(gainput::MouseButtonMiddle))
	{
		Vector2f mousePos;
		mousePos.x = pInput->GetFloat(EInputDeviceType::Mouse, gainput::MouseAxisX);
		mousePos.y = pInput->GetFloat(EInputDeviceType::Mouse, gainput::MouseAxisY);

		Vector2f trans = (mousePos - mLastMousePosition) * mEditorTranslateScale;
		pTransform->Position = mLastCameraPosition + Vector3f(-trans.x, trans.y, 0);
	}
}

void CCameraSystem::OnMouseMovement(SInputMovementData& data)
{
	// data:
	if (data.Key == gainput::MouseAxisX)
	{
		mMouseMovementDelta.x = data.DeltaValue;
	}
	else if (data.Key == gainput::MouseAxisY)
	{
		mMouseMovementDelta.y = data.DeltaValue;
	}
	else if (data.Key == gainput::MouseButtonWheelDown)
	{
		mMouseMovementDelta.z = data.DeltaValue;
	}
	else if (data.Key == gainput::MouseButtonWheelUp)
	{
		mMouseMovementDelta.z = data.DeltaValue;
	}
	//data.Key == gainput::MouseButtonMiddle
}

void CCameraSystem::OnKeyboardButton(IInputDeviceManager::InputKey key, bool bPressed)
{
	IConsole::Print("OnKeyboardButton %d %d", key, bPressed);

	if (key == gainput::KeyC && bPressed) 
	{
		ECameraMode cameraMode = ECameraMode(((int)mCameraMode + 1) % (int)ECameraMode::_Count);
		SetCameraMode(cameraMode);
	}
}

void CCameraSystem::OnMouseButton(IInputDeviceManager::InputKey key, bool bPressed)
{
	IInputDeviceManager* pInput = GetEngineCore()->GetInputDeviceManager();
	Entity* pEntity = mSceneManager->GetMainCamera();
	TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();

	if (key == gainput::MouseButtonMiddle && bPressed)
	{
		if (bPressed) 
		{
			//pInput->SetMouseCursorImage(ECursorImage::Hand);
			mLastMousePosition.x = pInput->GetFloat(EInputDeviceType::Mouse, gainput::MouseAxisX);
			mLastMousePosition.y = pInput->GetFloat(EInputDeviceType::Mouse, gainput::MouseAxisY);
			mLastCameraPosition = pTransform->Position;
		}
		else
		{
			//pInput->SetMouseCursorImage(ECursorImage::Arrow);
		}
	}
}

//void CCameraSystem::ChangeMouseCursor(const char* szCursor)
//{
//#ifdef _WIN32
//	HCURSOR hCursor = GetMouseCursor(szCursor);
//	
//#endif
//}
//
//
//#ifdef _WIN32
//
//HCURSOR CCameraSystem::GetMouseCursor(const char* szCursor)
//{
//	auto it = mCursorsCache.find(szCursor);
//	if (it != mCursorsCache.end())
//		return it->second;
//
//	HINSTANCE hInstance = GetModuleHandle(NULL);
//	HCURSOR hCursor = LoadCursorA(hInstance, szCursor);
//	mCursorsCache.insert({std::string(szCursor), hCursor});
//	return hCursor;
//}
//
//#endif
//
