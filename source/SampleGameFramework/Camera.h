#pragma once

#include "IEngineCore.h"
using namespace grind;

enum class ECameraMode
{
	EditorCamera,
	FpsCamera,
	_Count,
};

class CFpsCameraAction : public IEntityAction
{
public:
	CFpsCameraAction(ISceneManager* pSceneManager, float maxUpAngle, float maxDownAngle)
		: IEntityAction(pSceneManager), mMaxUpAngle(maxUpAngle), mMaxDownAngle(maxDownAngle)
	{

	}

	virtual AABB GetWorldBoundingBox(Entity* pEntity) const { return AABB(); }
	virtual void Relocate(Entity*& pEntity, TransformComponent* pTransform) {}

	Vector3f GetWalkDir(Entity* pEntity);
	float GetPitchRadians(Entity* pEntity);
	void SetPitchRadians(Entity* pEntity, float radians);

	void MoveRight(Entity* pEntity, float fDist);
	void MoveUp(Entity* pEntity, float fDist);
	void MoveForward(Entity* pEntity, float fDist);
	void Rotate(Entity* pEntity, EAxis axis, float radians);

protected:
	Vector3f	mWorldUp = Vector3f(0, 1.0f, 0);
	float		mMaxUpAngle;
	float		mMaxDownAngle;
};

class CCameraSystem
{
public:
	CCameraSystem(ISceneManager* pSceneManager);
	~CCameraSystem();
	void Update(float fDeltaTime);
	
	void SetCameraMode(ECameraMode cameraMode);
	ECameraMode GetCameraMode() const { return mCameraMode; }

private:
	void UpdateFpsCamera(float fDeltaTime);
	void UpdateEditorCamera(float fDeltaTime);
	
	void OnMouseMovement(SInputMovementData& data);
	void OnKeyboardButton(IInputDeviceManager::InputKey key, bool bPressed);
	void OnMouseButton(IInputDeviceManager::InputKey key, bool bPressed);
	

	ECameraMode			mCameraMode = ECameraMode::FpsCamera;

	ISceneManager*							mSceneManager;
	std::unique_ptr<CFpsCameraAction>		mFpsCameraAction;
	Vector3f							mMouseMovementDelta = Vector3f(0, 0, 0);
	IInputDeviceManager::ListenerId		mMouseMoveListenerId;
	IInputDeviceManager::ListenerId		mKeyboardListenerId;
	IInputDeviceManager::ListenerId		mMouseButtonListenerId;
	
	float								mFpsRotateSpeed = 0.5f; // radians per second
	float								mFpsTranslateSpeed = 1.0f;

	float								mEditorTranslateScale = 1.0f;

// for editor camera
	Vector2f							mLastMousePosition;
	Vector3f							mLastCameraPosition;

	

//	void ChangeMouseCursor(const char* szCursor);
//#ifdef _WIN32
//	HCURSOR	GetMouseCursor(const char* s);
//	std::map<std::string, HCURSOR>		mCursorsCache;
//#endif
};


