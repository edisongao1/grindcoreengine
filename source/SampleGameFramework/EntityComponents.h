#pragma once

#include "GrindECS.h"
#include "GrindMath.h"

using namespace grind;

DefineComponentWithID(RotatorComponent, 65)
{
	Vector3f	Angles;
	Vector3f	Speeds;
};
