#include "stdafx.h"
#include "SampleSceneLoadListener.h"
#include "EntityComponents.h"
#include "StringParseUtils.h"

using namespace tinyxml2;

Entity* CSampleSceneLoadListener::OnProcessEntityXML(const char* szName,
	const char* szClassType, tinyxml2::XMLElement* ComponentsNode, ISceneManager* pSceneManager)
{
	return nullptr;
}

Entity* CSampleSceneLoadListener::OnProcessEntityComponentXML(Entity* pEntity,
	tinyxml2::XMLElement* ComponentNode, ISceneManager* pSceneManager)
{
	const char* szComponentType = ComponentNode->Name();
	if (strcmp(szComponentType, "Rotator") == 0)
	{
		pEntity = AddRotatorComponent(pEntity, ComponentNode, pSceneManager);
	}
	return pEntity;
}


Entity* CSampleSceneLoadListener::AddRotatorComponent(Entity* pEntity,
	tinyxml2::XMLElement* ComponentNode,
	ISceneManager* pSceneManager)
{
	RotatorComponent rotatorComponent;
	XMLElement* AnglesNode = ComponentNode->FirstChildElement("Angles");
	rotatorComponent.Angles = StringParseUtils::Parse<Vector3f>(AnglesNode->GetText());

	XMLElement* SpeedsNode = ComponentNode->FirstChildElement("Speeds");
	rotatorComponent.Speeds = StringParseUtils::Parse<Vector3f>(SpeedsNode->GetText());

	Entity* pNewEntity = pEntity->Extend(rotatorComponent);
	pEntity->Release();
	return pNewEntity;
}
