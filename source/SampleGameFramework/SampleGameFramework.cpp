#include "stdafx.h"
#include "SampleGameFramework.h"
#include "SampleSceneLoadListener.h"
#include "GrindECS.h"
#include "EntityComponents.h"
#include "IRenderResourceManager.h"
#include "IMesh.h"
#include "IMaterial.h"
#include "Camera.h"

using namespace grind;

namespace grind
{
	IEngineCore* g_pEngineCore = nullptr;
	IConsole* g_pConsole = nullptr;
}

void SetGlobalEnvironment(IEngineCore* pEngineCore)
{
	g_pEngineCore = pEngineCore;
	g_pConsole = pEngineCore->GetConsole();
}

IGameFramework* CreateGameFramework()
{
	return new CSampleGameFramework();
}

void CSampleGameFramework::OnCreate()
{
	mSceneLoadListener = new CSampleSceneLoadListener();
}

void CSampleGameFramework::OnBeforeInitialize()
{

}

static void RemoveMeshEntity(const char* szEntityName)
{
	ISceneManager* pSceneManager = GetEngineCore()->GetISceneManager();
	Entity* pEntity = pSceneManager->GetEntityByName(szEntityName);
	if (pEntity)
		pSceneManager->DestroyEntity(pEntity);
}

void CSampleGameFramework::OnAfterInitialize()
{
	IConsole* pConsole = GetEngineCore()->GetConsole();
	// 控制台输入'hide xxx'，则隐藏对应的物体
	pConsole->RegisterCommand("hide", [](IConsole::SConsoleCommand* pCommand) {
		const char* szCommand = pCommand->GetArg(0);
		const char* szEntityName = pCommand->GetArg(1);
		RemoveMeshEntity(szEntityName);
		IConsole::Print("Hide entity: %s", szEntityName);
	});
}

static void MoveMeshEntity(float deltaTime)
{
	static float moveDirection = 1.0f;
	ISceneManager* pSceneManager = GetEngineCore()->GetISceneManager();

	Entity* pEntity = pSceneManager->GetEntityByName("@cokecan01");
	if (pEntity == nullptr)
		return;
	TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
	Vector3f position = pTransform->Position;
	position.x += moveDirection * deltaTime;
	if (position.x > 5.0f && moveDirection > 0)
		moveDirection = -1.0f;
	else if (position.x < -5.0f && moveDirection < 0)
		moveDirection = 1.0f;
	
	IMeshEntityAction* pMeshEntityAction = pSceneManager->GetMeshEntityAction();
	pMeshEntityAction->SetPosition(pEntity, position);
}

void CSampleGameFramework::OnFrameUpdate(float deltaTime)
{
	World* pWorld = GetEngineCore()->GetWorld();
	ISceneManager* pSceneManager = GetEngineCore()->GetISceneManager();

	pWorld->ForEach<TransformComponent, RotatorComponent>([deltaTime]
		(Entity* pEntity, TransformComponent* pTransform, RotatorComponent* pRotator) 
		{
			pRotator->Angles += pRotator->Speeds * deltaTime;
			pTransform->Rotation = Quaternion(pRotator->Angles);
		});


	MoveMeshEntity(deltaTime);

	mCameraSystem->Update(deltaTime);
}

void CSampleGameFramework::OnExitGame()
{

}

void CSampleGameFramework::OnDestroy()
{
	delete mSceneLoadListener;
}

void CSampleGameFramework::OnAfterCreateWindow()
{

}

void CSampleGameFramework::OnSceneLoaded(ISceneManager* pSceneManager)
{
	IConsole::Print(IConsole::EConsoleTextColor::PINK, true,
		"CSampleGameFramework::OnSceneLoaded");

	Entity* pCamera = pSceneManager->GetMainCamera();
	ICameraAction* pCameraAction = pSceneManager->GetCameraAction();
	pCameraAction->SetPosition(pCamera, Vector3f(0, 2.0f, 3));
	pCameraAction->LookAt(pCamera, Vector3f(0, 0, 0));

	static const float CUBE_INTERVAL = 0.5f;
	static const int CUBE_NUM_X = 10;
	static const int CUBE_NUM_Y = 8;

	IRenderResourceManager* pResourceMgr = GetEngineCore()->GetRenderResourceManager();
	IMeshManager* pMeshManager = pResourceMgr->GetMeshManager();
	IMaterialManager* pMaterialManager = pResourceMgr->GetMaterialManager();

	float fStartX = -(CUBE_NUM_X - 1) * CUBE_INTERVAL * 0.5f;
	float fStartY = -(CUBE_NUM_Y - 1) * CUBE_INTERVAL * 0.5f;

	for (int i = 0; i < CUBE_NUM_Y; i++) {
		for (int j = 0; j < CUBE_NUM_X; j++) {
			int index = i * CUBE_NUM_X + j;

			float x = fStartX + j * CUBE_INTERVAL;
			float y = fStartY + i * CUBE_INTERVAL;

			IMesh* pMesh = pMeshManager->GetMesh("box");
			IMaterial* pPbrMat = pMaterialManager->GetMaterial("PBRCube");

			Vector3f position(x, y, -2.0f);
			Quaternion rotation = glm::identity<Quaternion>();
			Vector3f scale(0.15f, 0.15f, 0.15f);

			TransformComponent transform;
			transform.Position = position;
			transform.Rotation = rotation;
			transform.Scale = scale;

			Matrix4x4 worldMatrix = transform.ToMatrix();

			MeshComponent staticMeshComponent;
			//staticMeshComponent.WorldMatrix = transform.ToMatrix();
			staticMeshComponent.pMesh = pMesh;
			staticMeshComponent.pMaterialList = (IMaterial**)malloc(sizeof(IMaterial*) * 1);
			staticMeshComponent.pMaterialList[0] = pPbrMat;
			//staticMeshComponent.iSubMeshCount = 1;

			BoundingBoxComponent boundingBoxComponent;
			boundingBoxComponent.aabb = AABB::ComputeTransformedAABB(pMesh->GetAABB(), worldMatrix);

			EntityContext* pContext = pSceneManager->GetStaticEntityContext(position);
			Entity* pEntity = pContext->CreateEntity(transform, staticMeshComponent, boundingBoxComponent);
		}
	}

	mCameraSystem = new CCameraSystem(pSceneManager);
}

ISceneLoadListener* CSampleGameFramework::GetSceneLoadListener()
{
	return mSceneLoadListener;
}


