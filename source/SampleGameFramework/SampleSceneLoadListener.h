#pragma once
#include "ISceneManager.h"
using namespace grind;

class CSampleSceneLoadListener : public ISceneLoadListener
{
public:
	virtual Entity* OnProcessEntityXML(const char* szName,
		const char* szClassType,
		tinyxml2::XMLElement* ComponentsNode,
		ISceneManager* pSceneManager) override;

	virtual Entity* OnProcessEntityComponentXML(Entity* pEntity,
		tinyxml2::XMLElement* ComponentNode,
		ISceneManager* pSceneManager) override;

private:
	Entity* AddRotatorComponent(Entity* pEntity,
		tinyxml2::XMLElement* ComponentNode,
		ISceneManager* pSceneManager);
};

