#pragma once

#include "Common.h"
#include "IWindow.h"
#include "IConsole.h"
#include "IFileSystem.h"
#include "IRenderResourceManager.h"
#include "IInputDeviceManager.h"
#include "Timer.h"
#include "IConfiguration.h"
#include "ISceneManager.h"

namespace FastECS
{
	class World;
}

namespace grind
{
	class ICamera;
	class IEngineCore;
	class IAsynchronizeTaskQueue;

	extern IEngineCore* g_pEngineCore;

	inline IEngineCore* GetEngineCore()
	{
		return g_pEngineCore;
	}
	using TSetGlobalEnvironmnet = void(*)(IEngineCore*);

	class IWindow;
	struct SWindowSettings;
	class IRenderDevice;
	class CAsynchronizeTaskQueue;
	class IGameFramework;
	class IGraphicsSystem;

	enum class ERequestType
	{
		Unknown,
		InitRenderContext,
		QuitGame,
		RenderScene,
		LoadRenderResources,
		PrepareSceneResources,
		CullScene,
	};

	class IEngineCore
	{
	public:
		using TCreateFunction = IEngineCore * (*)();

		IEngineCore()
		: mMainWindow(nullptr)
			, mConsole(nullptr)
		{

		}

		virtual ~IEngineCore() {}

		virtual void Initialize() = 0;
		virtual void StartLoop() = 0;
		virtual void Stop() = 0;
		virtual void Release() = 0;

		virtual bool CreateMainWindow() = 0;
		IWindow* GetMainWindow() { return mMainWindow.get(); }
		IRenderDevice* GetRenderDevice() { return mRenderDevice; }
		IConsole*	GetConsole() { return mConsole.get(); }
		IGameFramework* GetGameFramework() { return mGameFramework; }

		void _SetRenderDevice(IRenderDevice* p) { mRenderDevice = p; }
		void _SetGameFramework(IGameFramework* p) { mGameFramework = p; }

		IFileSystem* GetFileSystem() { return mFileSystem.get(); }
		IRenderResourceManager* GetRenderResourceManager() { return mRenderResourceManager.get(); }

		int GetWorkerThreadCount() const { return mWorkerThreadCount; }
		int GetTaskThreadCount() const { return mTaskThreadCount; }
		virtual CAsynchronizeTaskQueue* GetAsynchronizeTaskQueue() = 0;
		CTimer* GetTimer() { return mTimer.get(); }
		virtual int GetResourceFrameIndex() const = 0;
		IInputDeviceManager* GetInputDeviceManager() { return mInputDeviceManager.get(); }
		IConfiguration* GetConfiguration() { return mConfiguration.get(); }
		// for test:
		//virtual ICamera* GetCamera() = 0;
		virtual void FireWorkerThreads(ERequestType eRequestType) = 0;

		virtual ISceneManager* GetISceneManager() = 0;
		virtual IGraphicsSystem* GetIGraphicsSystem() = 0;
		FastECS::World* GetWorld() { return mWorld; }
		

	protected:
		//IWindow*			mMainWindow;
		//IConsole*			mConsole;
		//IRenderDevice*	mGraphicsDevice;
		
		int					mWorkerThreadCount = 0;
		int					mTaskThreadCount = 0;
		
		std::unique_ptr<IWindow>				mMainWindow;
		std::unique_ptr<IConsole>				mConsole;
		std::unique_ptr<IFileSystem>			mFileSystem;
		std::unique_ptr<IConfiguration>			mConfiguration;
		std::unique_ptr<IRenderResourceManager>		mRenderResourceManager;
		std::unique_ptr<CTimer>						mTimer;
		std::unique_ptr<IInputDeviceManager>		mInputDeviceManager;
		IRenderDevice*							mRenderDevice = nullptr;
		IGameFramework*							mGameFramework = nullptr;
		FastECS::World* mWorld;
	};
}



