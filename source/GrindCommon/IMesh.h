//***************************************************************************************
// GeometryGenerator.h by Frank Luna (C) 2011 All Rights Reserved.
//   
// Defines a static class for procedurally generating the geometry of 
// common mathematical objects.
//
// All triangles are generated "outward" facing.  If you want "inward" 
// facing triangles (for example, if you want to place the camera inside
// a sphere to simulate a sky), you will need to:
//   1. Change the Direct3D cull mode or manually reverse the winding order.
//   2. Invert the normal.
//   3. Update the texture coordinates and tangent vectors.
//***************************************************************************************

#pragma once

#include "GrindMath.h"
#include "GrindEnums.h"

namespace grind
{
	struct SSubMesh
	{
		unsigned int StartIndexLocation = 0;
		unsigned int IndexCount = 0;
		AABB		 aabb;
	};

	struct SMeshData
	{
		byte*			VertexData = nullptr;
		byte*			IndiceData = nullptr;
		EFVF			Fvf = EFVF::Unknown;
		uint8_t			FvfFlag = 0;
		uint8_t			VertexSize = 0;
		uint8_t			IndexSize = 2;  // only 2 or 4 (16 or 32 bits)
		size_t			VertexCount = 0;
		size_t			IndiceCount = 0;
		AABB			aabb;

		int				SubMeshCount = 0;
		SSubMesh		SubMeshes[32];

		void Release()
		{
			if (VertexData) {
				free(VertexData);
			}
			if (IndiceData) {
				free(IndiceData);
			}
		}
	};

	struct SVertex_P3_N3_T2
	{
		Vector3f		Pos;
		Vector3f		Normal;
		Vector2f		BaseTC;

		SVertex_P3_N3_T2():Pos(0), Normal(0), BaseTC(0){}

		SVertex_P3_N3_T2(Vector3f p, Vector3f n, Vector2f uv)
			:Pos(p), Normal(n), BaseTC(uv)
		{

		}

		SVertex_P3_N3_T2(Vector3f p, Vector3f n, Vector3f t, Vector2f uv)
			:SVertex_P3_N3_T2(p, n, uv)
		{

		}

		SVertex_P3_N3_T2 Lerp(const SVertex_P3_N3_T2& v, float k) const
		{
			SVertex_P3_N3_T2 vertex;
			//vertex.Pos = Pos.getInterpolated(v.Pos, k);
			//glm::
			//vertex.Normal = Normal.getInterpolated(v.Normal, k);
			//vertex.BaseTC = BaseTC.getInterpolated(v.BaseTC, k);
			vertex.Pos = glm::lerp(this->Pos, v.Pos, k);
			vertex.Normal = glm::lerp(this->Normal, v.Normal, k);
			vertex.BaseTC = glm::lerp(this->BaseTC, v.BaseTC, k);
			return vertex;
		}

		static constexpr uint8_t GetFVFFlags()
		{
			return 0;
		}
	};

	struct SVertex_P3_N3_TG3_T2
	{
		Vector3f		Pos;
		Vector3f		Normal;
		Vector3f		Tangent;
		Vector2f		BaseTC;

		SVertex_P3_N3_TG3_T2():Pos(0), Normal(0), Tangent(0), BaseTC(0){}

		SVertex_P3_N3_TG3_T2(Vector3f p, Vector3f n, Vector3f t, Vector2f uv)
			:Pos(p), Normal(n), Tangent(t), BaseTC(uv)
		{

		}

		SVertex_P3_N3_TG3_T2 Lerp(const SVertex_P3_N3_TG3_T2& v, float k) const
		{
			SVertex_P3_N3_TG3_T2 vertex;
			//vertex.Pos = Pos.getInterpolated(v.Pos, k);
			//vertex.Normal = Normal.getInterpolated(v.Normal, k);
			//vertex.BaseTC = BaseTC.getInterpolated(v.BaseTC, k);
			//vertex.Tangent = Tangent.getInterpolated(v.Tangent, k);
			vertex.Pos = glm::lerp(this->Pos, v.Pos, k);
			vertex.Normal = glm::lerp(this->Normal, v.Normal, k);
			vertex.BaseTC = glm::lerp(this->BaseTC, v.BaseTC, k);
			vertex.Tangent = glm::lerp(this->Tangent, v.Tangent, k);

			return vertex;
		}

		static constexpr uint8_t GetFVFFlags()
		{
			return E_FVF_Flag::ContainTangent;
		}
	};

	template<typename Vertex, typename IndiceType = uint16_t, 
		typename = std::enable_if_t<std::is_same_v<IndiceType, uint16_t> || std::is_same_v<IndiceType, uint32_t>>>
	struct SGeometryData
	{
		std::vector<Vertex> Vertices;
		std::vector<IndiceType> Indices;
		
		//const SGeometryData meshData(const SGeometryData& )

		template<typename T>
		bool CheckIndiceType() const
		{
			return std::is_same<IndiceType, T>::value;
		}

		inline SMeshData GetMeshData()
		{
			SMeshData meshData;
			meshData.Fvf = EFVF::Object3D;
			meshData.FvfFlag = Vertex::GetFVFFlags();
			meshData.VertexSize = sizeof(Vertex);
			meshData.IndexSize = sizeof(IndiceType);
			meshData.VertexCount = Vertices.size();
			meshData.IndiceCount = Indices.size();
			meshData.VertexData = (byte*)std::malloc(meshData.VertexSize * meshData.VertexCount);
			meshData.IndiceData = (byte*)std::malloc(meshData.IndexSize * meshData.IndiceCount);
			meshData.aabb = ComputeAABB();
			meshData.SubMeshCount = 1;
			meshData.SubMeshes[0].StartIndexLocation = 0;
			meshData.SubMeshes[0].IndexCount = Indices.size();

			std::memcpy(meshData.VertexData, this->Vertices.data(), meshData.VertexSize * meshData.VertexCount);
			std::memcpy(meshData.IndiceData, this->Indices.data(), meshData.IndexSize * meshData.IndiceCount);
			return meshData;
		}

	private:
		inline AABB ComputeAABB()
		{
			AABB aabb;
			for (int i = 0; i < Vertices.size(); i++) {
				aabb.AddInternalPoint(Vertices[i].Pos);
			}
			return aabb;
		}
	};

	namespace GeometryGenerator
	{
		template<typename Vertex>
		inline Vertex MidPoint(const Vertex& v0, const Vertex& v1);

		template<typename Vertex, typename IndiceType>
		inline void Subdivide(_Inout_ SGeometryData<Vertex, IndiceType>& meshData);

		template<typename Vertex, typename IndiceType>
		inline void CreateBox(__out SGeometryData<Vertex, IndiceType>& meshData,
			float width, float height, float depth, int numSubdivisions = 0);


		template<typename Vertex, typename IndiceType>
		inline void CreateSphere(__out SGeometryData<Vertex, IndiceType>& meshData,
			float radius, int sliceCount, int stackCount);

		template<typename Vertex, typename IndiceType>
		inline void CreateGeosphere(__out SGeometryData<Vertex, IndiceType>& meshData,
			float radius, int numSubdivisions);

		template<typename Vertex, typename IndiceType>
		inline void CreateCylinder(__out SGeometryData<Vertex, IndiceType>& meshData, 
			float bottomRadius, float topRadius, float height, int sliceCount, int stackCount);

		template<typename Vertex, typename IndiceType>
		inline void CreateGrid(__out SGeometryData<Vertex, IndiceType>& meshData,
			float width, float depth, int m, int n);
	}




	class IMeshResource;

	enum class EMeshState
	{
		NotLoaded, 
		Loading, // 
		Loaded, // loaded the data, but not created the resource
		LoadFailed, // 
		BufferCreating, // 
		BufferCreateFailed, //
		BufferCreated,
	};

	class IMeshBuffer;

	class IMesh
	{
	public:


	public:
		IMesh(const std::string& name)
			:mName(name)
		{

		}
		virtual ~IMesh()
		{

		}

		const std::string& GetName() const { return mName; }
		bool IsCreated() const { return mState == EMeshState::BufferCreated; }
		virtual void TryCreate() = 0;
		const SMeshData* GetMeshData() { return &mMeshData; }
		virtual void OnCreateBufferFailed() = 0;
		virtual void OnCreateBufferCompleted(IMeshBuffer* pMeshBuffer) = 0;
		void _SetState(EMeshState state) { mState = state; }
		IMeshBuffer* GetMeshBuffer() { return mMeshBuffer; }
		const SSubMesh* GetSubMesh(int subMeshIndex) const { return &mMeshData.SubMeshes[subMeshIndex]; }
		int GetSubMeshCount() const { return mMeshData.SubMeshCount; }
		virtual void ReleaseBuffer() = 0;
		virtual void ReleaseDataAndBuffer() = 0;

		uint8_t GetFVFFlags() const { return mMeshData.FvfFlag; }
		const AABB GetAABB() const { return mMeshData.aabb; }
	protected:
		std::string				mName;
		EMeshState				mState = EMeshState::NotLoaded;
		IMeshBuffer*			mMeshBuffer = nullptr;
		SMeshData				mMeshData;
		//int						mSubMeshCount = 0;
		//SSubMesh				mSubMeshes[32]; // maximum of submesh is 32
	};

	class IMeshBuffer
	{
	public:
		IMeshBuffer(IMesh* pMesh):mMesh(pMesh){}
		virtual void OnCreateCompleted() = 0;
	protected:
		IMesh*		mMesh = nullptr;
	};

	class IMeshManager
	{
	public:
		virtual ~IMeshManager(){}
		virtual IMesh* GetMesh(const std::string& name) const = 0;
		virtual IMesh* CreateMesh(const std::string& name, const SMeshData& data) = 0;
		
		virtual bool LoadStaticMeshData(const char* szFileName, SMeshData& meshData, uint32_t fvf_flags) = 0;
	};

	struct SCreateMeshBufferRequest
	{
		IMesh*		pMesh;
	};

	struct SCreateMeshBufferFinishedRequest
	{
		IMesh*			pMesh;
		IMeshBuffer*	pMeshBuffer;
	};
}

#include "GeometryGenerator.inl"


