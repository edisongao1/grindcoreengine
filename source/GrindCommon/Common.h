#pragma once

#define _ENABLE_ATOMIC_ALIGNMENT_FIX 

#include <assert.h>
#include <atomic>
#include <type_traits>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <map>
#include <array>
#include <functional>

using byte = unsigned char;

#if __cplusplus > 201703L
#define _CPP20_ENABLED 1
#endif

#ifndef DECLARE_ATOMIC_FLAG
#ifdef _CPP20_ENABLED
#define DECLARE_ATOMIC_FLAG(x) std::atomic_flag x;
#else
#define DECLARE_ATOMIC_FLAG(x) std::atomic_flag x = ATOMIC_FLAG_INIT;
#endif
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(x) { if(x){ delete x; x = 0; } }
#endif

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(x) { if(x){ x->Release(); x = 0; } }
#endif

#define __RETURN_IF_STRING_MATCH_ENUM(s, enumClass, enumType) \
	if (_stricmp(s, #enumType) == 0) \
		return enumClass::enumType;

enum { FRAME_RESOURCE_COUNT = 3};

namespace grind
{
	template<typename T>
	struct release_deleter
	{
		void operator()(T* obj)
		{
			SAFE_RELEASE(obj);
		}
	};

	template<typename T>
	using com_unique_ptr = std::unique_ptr<T, release_deleter<T>>;


	// wrapper for std::atomic_flag

}
