#pragma once

#include "GrindECS.h"

namespace grind
{
	struct SMeshRenderItem
	{
		Matrix4x4		WorldMatrix;
		IMesh* pMesh = nullptr;
		int				iSubMeshIndex = 0;
		IMaterial* pMaterial = nullptr;
	};

	template<typename T, int Size>
	struct TRenderItemChunk
	{
	public:
		int mWriteIndex = 0;
		int mReadIndex = 0;

		T mElements[Size];
		TRenderItemChunk* pNext = nullptr;

		static int GetSize() { return Size; }
		void Reset()
		{
			mReadIndex = 0;
			mWriteIndex = 0;
		}

		T* GetNextToWrite()
		{
			assert(mWriteIndex < Size);
			return &mElements[mWriteIndex++];
		}

		T* GetNextToRead()
		{
			if (mReadIndex >= mWriteIndex)
				return nullptr;
			return &mElements[mReadIndex++];
		}

		bool IsFull() const
		{
			return mWriteIndex == Size;
		}
	};

	template<typename T, int ChunkSize>
	class TRenderItemQueue
	{
	public:
		using RenderItemChunk = TRenderItemChunk<T, ChunkSize>;
		
		TRenderItemQueue()
		{
			mHead = new RenderItemChunk();
			mReadChunk = mHead;
			mWriteChunk = mHead;
		}

		~TRenderItemQueue()
		{
			RenderItemChunk* p = mHead;
			while (p) {
				auto p1 = p->pNext;
				delete p;
				p = p1;
			}
		}

		T* GetNextToWrite()
		{
			if (mWriteChunk->IsFull())
			{
				if (!mWriteChunk->pNext)
					mWriteChunk->pNext = new RenderItemChunk();

				mWriteChunk = mWriteChunk->pNext;
			}
			return mWriteChunk->GetNextToWrite();
		}

		T* GetNextToRead()
		{
			T* pElement = mReadChunk->GetNextToRead();
			if (pElement == nullptr && mReadChunk != mWriteChunk) {
				mReadChunk = mReadChunk->pNext;
				pElement = mReadChunk->GetNextToRead();
			}
			return pElement;
		}

		void Reset()
		{
			mReadChunk = mHead;
			mWriteChunk = mHead;
			RenderItemChunk* p = mHead;
			while (p) {
				p->Reset();
				p = p->pNext;
			}
		}

		void ResetRead()
		{
			mReadChunk = mHead;
			mHead->mReadIndex = 0;
		}

	private:
		RenderItemChunk* mHead = nullptr;

		RenderItemChunk* mReadChunk = nullptr;
		RenderItemChunk* mWriteChunk = nullptr;
	};

	using CMeshRenderItemQueue = TRenderItemQueue<SMeshRenderItem, 1024>;

}

