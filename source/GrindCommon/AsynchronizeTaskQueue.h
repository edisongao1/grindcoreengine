#pragma once
#include "LockFreeQueue.h"
#include "IEngineCore.h"

namespace grind
{
	//enum { FRAME_RESOURCE_COUNT = 3 };

	enum class ETaskType : uint32_t {
		CreateRenderPass,
		CreateRenderPassResult,
		CreatePipelineState,
		CreatePipelineStateResult,
		
		// for texture creation
		ReadDDSData,
		CreateTextureResource,
		CreateTextureResourceFinished,
		CreateTextureResult,

		// for material
		CreateMaterialResource,

		// for mesh
		LoadMeshData,
		// LoadedMeshDataFinished,
		CreateMeshBuffer,

		//CreateMeshBufferFailed,
		CreateMeshBufferFinished,

		QuitGame,
		Count,
	};

	enum class ETaskExecuteTime 
	{
		// without SyncResourceFrame
		AsyncImmediately,
		LoadRenderResources,
		BeforeRenderScene,
		AfterRenderScene,
		// with Sync SyncResourceFrame

		//LoadRenderResources_SyncResourceFrame,
		BeforeRenderScene_SyncResourceFrame,
		AfterRenderScene_SyncResourceFrame,
		Count,
	};

	struct STaskRequest
	{
		ETaskType		Type;
		byte			Content[4096];
	};

	class CAsynchronizeTaskQueue;
	class IRenderContext;

	struct STaskContext
	{
		CAsynchronizeTaskQueue*	pQueue = nullptr;
		IRenderContext*			pRenderContext = nullptr;
	};

	using STaskResult = STaskRequest;

	class CAsynchronizeTaskQueue
	{
	public:
		enum { REQUEST_QUEUE_SIZE = 4096 };
		enum { NoResourceSync_Count = (int)ETaskExecuteTime::AfterRenderScene + 1};
		enum { ResourceSync_Count = (int)ETaskExecuteTime::Count - NoResourceSync_Count};
		enum { TaskTypeCount = (int)ETaskType::Count};

		using TLockFreeRequestQueue = TLockFreeQueue<STaskRequest, REQUEST_QUEUE_SIZE>;
		//using TResultQueue = TRequestQueue;

		CAsynchronizeTaskQueue()
		{
			mDefaultTaskContext.pQueue = this;
		}

		template<ETaskExecuteTime eExecuteTime, typename T>
		void PushRequest(ETaskType type, T&& req)
		{
			PushRequestIntoQueue(GetRequestQueue<eExecuteTime>(), type, std::forward<T>(req));
		}

		template<typename T>
		void SetRequestHandler(ETaskType taskType, std::function<void(T*, STaskContext* queue)> cb)
		{
			mRequestHandlers[(uint32_t)taskType] = [cb](STaskRequest* req, STaskContext* context) {
				T* msg = reinterpret_cast<T*>(req->Content);
				cb(msg, context);
			};
		}

		template<typename T>
		void SetRequestHandler(ETaskType taskType, std::function<void(T*)> cb)
		{
			mRequestHandlers[(uint32_t)taskType] = [cb](STaskRequest* req, STaskContext* context) {
				T* msg = reinterpret_cast<T*>(req->Content);
				cb(msg);
			};
		}

		void ProcessRequest(STaskRequest& req, STaskContext* context = nullptr) 
		{
			if (context) {
				context->pQueue = this;
			}
			else {
				context = &mDefaultTaskContext;
			}

			uint32_t type = (uint32_t)req.Type;
			if (mRequestHandlers[type]) {
				mRequestHandlers[type](&req, context);
			}
		}

		template<ETaskExecuteTime eExecuteTime>
		bool TryPopRequest(STaskRequest& req)
		{
			return GetRequestQueue<eExecuteTime>().try_pop(req);
		}

		void ProcessTasks(ETaskExecuteTime eExecuteTime, STaskContext* context = nullptr)
		{
			if (context) {
				context->pQueue = this;
			}
			else {
				context = &mDefaultTaskContext;
			}

			auto& queue = GetRequestQueue(eExecuteTime);
			STaskRequest req;
			while (queue.try_pop(req)) {
				uint32_t type = (uint32_t)req.Type;
				if (mRequestHandlers[type]) {
					mRequestHandlers[type](&req, context);
				}
			}
		}

	protected:


		template<ETaskExecuteTime eExecuteTime>
		TLockFreeRequestQueue& GetRequestQueue()
		{
			if constexpr ((int)eExecuteTime < NoResourceSync_Count)
			{
				return mRequestQueues[(int)eExecuteTime];
			}
			else
			{
				int frameIndex = GetEngineCore()->GetResourceFrameIndex();
				int i = (int)eExecuteTime - NoResourceSync_Count;
				return mRequestSyncQueues[i][frameIndex];
			}
		}

		TLockFreeRequestQueue& GetRequestQueue(ETaskExecuteTime eExecuteTime)
		{
			if ((int)eExecuteTime < NoResourceSync_Count)
			{
				return mRequestQueues[(int)eExecuteTime];
			}
			else
			{
				int frameIndex = GetEngineCore()->GetResourceFrameIndex();
				int i = (int)eExecuteTime - NoResourceSync_Count;
				return mRequestSyncQueues[i][frameIndex];
			}
		}

		template<typename T>
		static void PushRequestIntoQueue(TLockFreeRequestQueue& queue, ETaskType type, T&& req)
		{
			STaskRequest task;
			task.Type = type;
			std::decay_t<T>* reqContent = reinterpret_cast<std::decay_t<T>*>(task.Content);
			*reqContent = std::forward<T>(req);
			queue.try_push(std::move(task));
		}

		STaskContext				mDefaultTaskContext;

		TLockFreeRequestQueue		mRequestQueues[NoResourceSync_Count];
		TLockFreeRequestQueue		mRequestSyncQueues[ResourceSync_Count][FRAME_RESOURCE_COUNT];

		std::function<void(STaskRequest*, STaskContext*)>	mRequestHandlers[TaskTypeCount];
		//std::function<void(STaskResult* result)>				mResultHandlers[(uint32_t)ETaskType::Count];
	};

}
