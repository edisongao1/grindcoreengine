#pragma once

#include "GrindEnums.h"
#include "StringParseUtils.h"
#include <unordered_map>
#include <string>

#pragma warning(push)
#pragma warning(disable: 4996)

namespace grind
{
	class IConfiguration
	{
	public:
		struct ConfigProperty
		{
			EVariableType		Type;
			union {
				bool			vBool;
				float			vFloat;
				Vector2f		vFloat2;
				Vector3f		vFloat3;
				Vector4f		vFloat4;
				int				vInt;
				Vector2i		vInt2;
				Vector3i		vInt3;
				Vector4i		vInt4;
				char			vChars[128];
			} Value;
		};		
	public:
		template<typename T>
		bool Get(const char* key, T& value) const;

		bool Get(const char* key, char value[]) const
		{
			auto it = mPropertiesMap.find(key);
			if (it == mPropertiesMap.end())
				return false;
			if (it->second.Type != EVariableType::CharSequence)
				return false;
			std::strcpy(value, it->second.Value.vChars);
			return true;
		}

		template<>
		bool Get<std::string>(const char* key, std::string& value) const
		{
			auto it = mPropertiesMap.find(key);
			if (it == mPropertiesMap.end())
				return false;
			if (it->second.Type != EVariableType::CharSequence)
				return false;
			value.assign(it->second.Value.vChars);
			return true;
		}

		template<>
		bool Get<int>(const char* key, int& value) const
		{
			auto it = mPropertiesMap.find(key);
			if (it == mPropertiesMap.end())
				return false;
			if (it->second.Type != EVariableType::Int)
				return false;
			value = it->second.Value.vInt;
			return true;
		}

		template<>
		bool Get<float>(const char* key, float& value) const
		{
			auto it = mPropertiesMap.find(key);
			if (it == mPropertiesMap.end())
				return false;
			if (it->second.Type != EVariableType::Float)
				return false;
			value = it->second.Value.vFloat;
			return true;
		}

		template<>
		bool Get<bool>(const char* key, bool& value) const
		{
			auto it = mPropertiesMap.find(key);
			if (it == mPropertiesMap.end())
				return false;
			if (it->second.Type != EVariableType::Bool)
				return false;
			value = it->second.Value.vBool;
			return true;
		}

		template<typename T = const char*>
		T GetProperty(const char* key, T defaultVal) const
		{
			T value;
			if (Get<T>(key, value))
				return value;
			return defaultVal;
		}

		template<>
		const char* GetProperty<const char*>(const char* key, const char* defaultVal) const
		{
			auto it = mPropertiesMap.find(key);
			if (it == mPropertiesMap.end())
				return defaultVal;
			if (it->second.Type != EVariableType::CharSequence)
				return defaultVal;
			return it->second.Value.vChars;
		}

		template<typename T>
		T GetProperty(const char* key)
		{
			return GetProperty<T>(key, GetDefaultValueOfType<T>());
		}

		template<typename T>
		bool SetProperty(const char* key, T value)
		{
			auto it = mPropertiesMap.find(key);
			if (it == mPropertiesMap.end())
				return false;
			if (it->second.Type != GetVariableType<T>())
				return false;
			ConfigProperty prop = BuildConfigProperty<T>(value);
			mPropertiesMap[std::string(key)] = prop;
			return true;
		}

		bool SetPropertyByString(const char* key, const char* szValue)
		{
			auto it = mPropertiesMap.find(key);
			if (it == mPropertiesMap.end())
				return false;
			auto type = it->second.Type;
			switch (type)
			{
			case EVariableType::Float:
				SetProperty<float>(key, StringParseUtils::Parse<float>(szValue));
				break;
			case EVariableType::Int:
				SetProperty<int>(key, StringParseUtils::Parse<int>(szValue));
				break;
			case EVariableType::CharSequence:
				SetProperty<const char*>(key, szValue);
				break;
			case EVariableType::Bool:
				SetProperty<bool>(key, StringParseUtils::Parse<bool>(szValue));
				break;
			default:
				break;
			}
			return true;
		}
		
		virtual void ReadConfigProperties(const char* szConfigFileName) = 0;


		template<typename T>
		void AddProperty(const char* key, T value)
		{
			ConfigProperty prop = BuildConfigProperty(value);
			mPropertiesMap.insert({ std::string(key), prop });
		}

	protected:

		template<typename T>
		ConfigProperty BuildConfigProperty(T value);

		template<>
		ConfigProperty BuildConfigProperty(const char* value)
		{
			ConfigProperty prop;
			prop.Type = EVariableType::CharSequence;
			strcpy_s(prop.Value.vChars, value);
			return prop;
		}

		template<>
		ConfigProperty BuildConfigProperty(int value)
		{
			ConfigProperty prop;
			prop.Type = EVariableType::Int;
			prop.Value.vInt = value;
			return prop;
		}

		template<>
		ConfigProperty BuildConfigProperty(float value)
		{
			ConfigProperty prop;
			prop.Type = EVariableType::Float;
			prop.Value.vFloat = value;
			return prop;
		}

		template<>
		ConfigProperty BuildConfigProperty(bool value)
		{
			ConfigProperty prop;
			prop.Type = EVariableType::Bool;
			prop.Value.vBool = value;
			return prop;
		}

		std::unordered_map<std::string, ConfigProperty>		mPropertiesMap;
	};
}


#pragma warning(pop)
