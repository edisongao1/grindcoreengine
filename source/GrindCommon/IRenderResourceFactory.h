#pragma once

namespace grind
{
	class IRenderPass;
	struct SRenderPassMeta;
	class IMaterialResourceManager;

	class IRenderResourceFactory
	{
	public:
		virtual ~IRenderResourceFactory() {}
		virtual IMaterialResourceManager* GetMaterialResourceManager() = 0;
		
		virtual IRenderPass* CreateRenderPass(SRenderPassMeta* passMeta,
			const std::vector<std::string>& matFlags) = 0;
	};
}

