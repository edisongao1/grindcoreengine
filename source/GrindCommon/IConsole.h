#pragma once

#include "LockFreeQueue.h"
#include <atomic>

namespace grind
{
	class IConsole;
	extern IConsole* g_pConsole;
	//class IEngineCore;
	//extern IEngineCore* g_pEngineCore;

	class IConsole
	{
	public:
		virtual ~IConsole() {};
		enum { MAX_COMMANDSTRING_LEN = 128 };
		enum { MAX_COMMAND_COUNT = 16};

		struct SConsoleCommand
		{
			char Args[MAX_COMMAND_COUNT][MAX_COMMANDSTRING_LEN];
			SConsoleCommand()
			{
				memset(Args, 0, sizeof(Args));
			}

			template<typename T = const char*>
			T GetArg(int i);

			template<>
			const char* GetArg<const char*>(int i)
			{
				return Args[i];
			}

			template<>
			int GetArg<int>(int i)
			{
				return atoi(Args[i]);
			}

			template<>
			float GetArg<float>(int i)
			{
				return (float)atof(Args[i]);
			}
		};

		//using TCommandHandler = void(*)(SConsoleCommand* pCommand);
		using TCommandHandler = std::function<void(SConsoleCommand* pCommand)>;

	public:
		enum class EConsoleTextColor
		{
			GREEN = 1,
			RED,
			BLUE,
			YELLOW,
			CYAN,
			PINK,
			WHITE,
			BLACK,
		};

		template<typename ...T>
		static void Print(const char* format, T... args)
		{
			Print(EConsoleTextColor::WHITE, false, format, args...);
		}

		template<typename ...T>
		static void Print(EConsoleTextColor color, bool bBold, const char* format, T... args)
		{
			static std::atomic_flag	outputFlag;
			thread_local static char buff[1024];
			sprintf_s(buff, format, args...);

			while (outputFlag.test_and_set(std::memory_order_acquire));
			//g_pEngineCore->GetConsole()->PrintLine(buff, color, bBold);
			g_pConsole->PrintLine(buff, color, bBold);
			outputFlag.clear(std::memory_order_release);
		}

		template<typename ...T>
		static void PrintW(const wchar_t* format, T... args)
		{
			PrintW(EConsoleTextColor::WHITE, false, format, args...);
		}

		template<typename ...T>
		static void PrintW(EConsoleTextColor color, bool bBold, const wchar_t* format, T... args)
		{
			static std::atomic_flag	outputFlag;
			thread_local static wchar_t buff[1024];
			swprintf_s(buff, format, args...);

			while (outputFlag.test_and_set(std::memory_order_acquire));
			g_pConsole->PrintLineW(buff, color, bBold);
			outputFlag.clear(std::memory_order_release);
		}

		virtual void PrintLine(const char* s,
			EConsoleTextColor color = EConsoleTextColor::WHITE,
			bool bColorIntensity = false) = 0;

		virtual void PrintLineW(const wchar_t* s,
			EConsoleTextColor color = EConsoleTextColor::WHITE,
			bool bColorIntensity = false) = 0;

		template<typename T>
		void RegisterCommand(const char* szCommand, T&& func)
		{
			if (mCommandHandlers.find(szCommand) == mCommandHandlers.end()) 
			{
				mCommandHandlers[szCommand] = std::forward<T>(func);
			}
		}

		// must call from main thread
		void RunCachedCommands()
		{
			SConsoleCommand command;
			while (mCommandQueue.try_pop(command)) 
			{
				const char* szCommand = command.Args[0];
				auto it = mCommandHandlers.find(szCommand);
				if (it != mCommandHandlers.end())
				{
					it->second(&command);
				}
				else if (szCommand[0] != '\0')
				{
					Print(EConsoleTextColor::RED, true, "Can't find command %s", szCommand);
				}
			}
		}

	protected:
		
		void RecordCommandLine(const char* szLine)
		{
			SConsoleCommand command;

			const char* p1 = szLine;
			const char* p2 = szLine;

			int i = 0;
			while (*p1 != '\n' && *p1 != '\0')
			{
				while (*p2 != ' ' && *p2 != '\0' && *p2 != '\n')
					p2 += 1;

				assert(i < MAX_COMMAND_COUNT);
				strncpy_s(command.Args[i++], p1, p2 - p1);

				while (*p2 == ' ')
					p2 += 1;
				p1 = p2;
			}

			mCommandQueue.try_push(command);
		}

		std::unordered_map<std::string, TCommandHandler>		mCommandHandlers;
		TLockFreeQueue<SConsoleCommand, 32>						mCommandQueue;
		
	};

#define GRIND_DEBUG_LOG(...) grind::IConsole::Print(grind::IConsole::EConsoleTextColor::WHITE, false, __VA_ARGS__)
#define GRIND_WARNING_LOG(...) grind::IConsole::Print(grind::IConsole::EConsoleTextColor::YELLOW, true, __VA_ARGS__)
#define GRIND_ERROR_LOG(...) grind::IConsole::Print(grind::IConsole::EConsoleTextColor::RED, true, __VA_ARGS__)

}


