#pragma once

#include "Common.h"

namespace grind
{
	enum class ETextureType {
		Unknown,
		Texture1D,
		Texture2D,
		Texture3D,
	};

	inline ETextureType StringToTextureType(const char* str)
	{
		__RETURN_IF_STRING_MATCH_ENUM(str, ETextureType, Texture1D);
		__RETURN_IF_STRING_MATCH_ENUM(str, ETextureType, Texture2D);
		__RETURN_IF_STRING_MATCH_ENUM(str, ETextureType, Texture3D);
		return ETextureType::Unknown;
	}

	class ITextureResource;
	
	enum ETextureState
	{
		NotLoaded, // 未加载
		Loading, // 加载中
		Loaded, // 已加载
		LoadFailed, // 加载失败
	};

	class ITexture;
	struct SCreateTextureResourceRequest
	{
		ITexture*	pTexture;

		uint8_t*	pddsData;
		uint8_t*	pddsHeader;
		uint8_t*	pBitData;
		size_t		bitSize;
	};

	struct SCreateTextureResourceFinishedRequest
	{
		ITexture*				pTexture;
		ITextureResource*		pTextureResource;
	};

	class ITexture
	{
	public:
		ITexture(const std::string& filename, ETextureType eType)
			:mFileName(filename), mType(eType)
		{
			//int a = 1;
		}

		virtual ~ITexture()
		{
			//int a = 1;
		}

		ITextureResource* GetTextureResource() { return mTextureResource.get(); }

		//const std::string& GetName() const { return mName; }
		const std::string& GetFileName() const { return mFileName; }
		ETextureType GetTextureType() const { return mType; }
		virtual bool IsNull() const { return false; }
		virtual void InitializeTextureResource(void* p) = 0;
		virtual void OnCompleteLoadResource(ITextureResource* pTextureResource) = 0;
		virtual void OnFailLoadResource() = 0;

		virtual void LoadFromFile() = 0;

		ETextureState GetState() const { return mState; }
		void _SetState(ETextureState state) { mState = state; }
	
	protected:
		//std::string			mName;
		std::string			mFileName;
		ETextureType		mType;
		//ITextureResource*	mTextureResource = nullptr;
		com_unique_ptr<ITextureResource>	mTextureResource;

		ETextureState		mState = ETextureState::NotLoaded;
	};

	//class CDummyTexture : public ITexture
	//{
	//public:
	//	CDummyTexture(): ITexture("Dummy", ETextureType::Unknown){}
	//	virtual bool IsNull() const override { return true; }
	//};

	class ITextureResource
	{
	public:
		ITextureResource(ITexture* pTexture) 
			:mTexture(pTexture)
		{

		}
		virtual ~ITextureResource(){}

		virtual void OnCompleteLoadResource() = 0;
		void Release()
		{
			delete this;
		}
	protected:
		ITexture*					mTexture = nullptr;
	};

	class ITextureManager
	{
	public:
		virtual ~ITextureManager() {}

		virtual ITexture* CreateTexture(const std::string& name, ETextureType eType) = 0;
		virtual ITexture* GetTexture(const std::string& name) = 0;
		// TODO: for test
		//virtual void InitializeAfterGraphicsCreated(void*) = 0;
		virtual void OnCompleteLoadResource() = 0;
	};
}
