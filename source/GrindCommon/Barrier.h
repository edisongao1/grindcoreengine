#pragma once

namespace grind
{

	class CLockedBarrier
	{
	public:
		CLockedBarrier(int totalCount)
			:mTotalCount(totalCount)
		{
			mIndex = 0;
			mCurrentCount[0] = mCurrentCount[1] = 0;
		}

		void Sync()
		{
			std::unique_lock<std::mutex> lock(mMutex);
			int idx = mIndex;
			mCurrentCount[idx] += 1;
			if (mCurrentCount[idx] <= mTotalCount - 1)
			{
				mCondition.wait(lock, [this, idx]() {
					return mCurrentCount[idx] == 0;
				});
			}
			else
			{
				mCurrentCount[idx] = 0;
				mIndex = !idx;
				mCondition.notify_all();
			}
		}
		
		~CLockedBarrier()
		{
		}

	private:
		int							mIndex;
		int							mTotalCount;
		int							mCurrentCount[2];
		std::condition_variable		mCondition;
		std::mutex					mMutex;
	};

}
