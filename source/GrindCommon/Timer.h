#pragma once

namespace grind
{
	class CTimer
	{
	public:
		using TimePoint = std::chrono::time_point<std::chrono::steady_clock>;
		using TimeInterval = std::chrono::nanoseconds;

		CTimer()
		{
			Reset();
		}

		void Tick()
		{
			if (mStopped)
				return;

			TimePoint currTime = std::chrono::steady_clock::now();
			mCurrTime = currTime;

			mTickDelta = currTime - mPrevTime;
			mPrevTime = mCurrTime;
			//mTickDeltaSeconds = std::chrono::duration_cast<std::chrono::duration<float>>(mTickDelta).count();
		}

		
		void Pause()
		{
			if (!mStopped)
			{
				mStopTime = std::chrono::steady_clock::now();
				mStopped = true;
			}
		}

		void Resume()
		{
			if (mStopped)
			{
				auto currTime = std::chrono::steady_clock::now();
				mPausedTime += (currTime - mStopTime);
				mStopped = false;
			}
		}

		void Reset()
		{
			auto curTime = std::chrono::steady_clock::now();
			mBaseTime = curTime;
			mPrevTime = curTime;
			mCurrTime = curTime;
			mPausedTime = TimeInterval(0);
			mStopped = false;
		}

		template<typename Unit = std::ratio<1>, typename T = float>
		T GetTickDelta() const 
		{
			return std::chrono::duration_cast<std::chrono::duration<T, Unit>>(mTickDelta).count();
		}

		template<typename Unit = std::ratio<1>, typename T = float>
		T GetFrameTime() const 
		{
			return std::chrono::duration_cast<std::chrono::duration<T, Unit>>(mCurrTime - mBaseTime).count();
		}

		template<typename Unit = std::ratio<1>, typename T = float>
		T GetNowTime() const
		{
			auto nowTime = std::chrono::steady_clock::now();
			return std::chrono::duration_cast<std::chrono::duration<T, Unit>>(nowTime - mBaseTime).count();
		}

		template<typename Unit = std::ratio<1>, typename T = float>
		T GetElapseTime() const
		{
			if (mStopped) 
			{
				return std::chrono::duration_cast<std::chrono::duration<T, Unit>>((mStopTime - mPausedTime) - mBaseTime).count();
			}
			else
			{
				return std::chrono::duration_cast<std::chrono::duration<T, Unit>>((mCurrTime - mPausedTime) - mBaseTime).count();
			}
		}

	protected:
		bool				mStopped = false;
		TimeInterval		mTickDelta;
		//float				mTickDeltaSeconds = 0;
		TimePoint			mBaseTime;
		TimeInterval		mPausedTime;
		TimePoint			mStopTime;
		TimePoint			mPrevTime;
		TimePoint			mCurrTime;
	};

}
