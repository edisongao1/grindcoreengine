#pragma once

#include "Common.h"

namespace grind
{
	enum class EFrustumPlane {
		Left, Right, Top, Bottom, Near, Far,
	};

	struct Frustum
	{
		Vector4f		Planes[6];
		inline void BuildFromViewProjMatrix(const Matrix4x4& viewProjMatrix, bool bD3D);
	};

	void Frustum::BuildFromViewProjMatrix(const Matrix4x4& viewProjMatrix, bool bD3D)
	{
		Matrix4x4 m = glm::transpose(viewProjMatrix);
		// Left clipping plane
		//Planes[0].x = comboMatrix._41 + comboMatrix._11;
		//Planes[0].y = comboMatrix._42 + comboMatrix._12;
		//Planes[0].z = comboMatrix._43 + comboMatrix._13;
		//Planes[0].w = comboMatrix._44 + comboMatrix._14;
		Planes[0].x = m[3][0] + m[0][0];
		Planes[0].y = m[3][1] + m[0][1];
		Planes[0].z = m[3][2] + m[0][2];
		Planes[0].w = m[3][3] + m[0][3];

		// Right clipping plane
		//p_planes[1].a = comboMatrix._41 - comboMatrix._11;
		//p_planes[1].b = comboMatrix._42 - comboMatrix._12;
		//p_planes[1].c = comboMatrix._43 - comboMatrix._13;
		//p_planes[1].d = comboMatrix._44 - comboMatrix._14;
		Planes[1].x = m[3][0] - m[0][0];
		Planes[1].y = m[3][1] - m[0][1];
		Planes[1].z = m[3][2] - m[0][2];
		Planes[1].w = m[3][3] - m[0][3];


		// Top clipping plane
		/*Planes[2].x = comboMatrix._41 - comboMatrix._21;
		Planes[2].y = comboMatrix._42 - comboMatrix._22;
		Planes[2].z = comboMatrix._43 - comboMatrix._23;
		Planes[2].w = comboMatrix._44 - comboMatrix._24;*/
		Planes[2].x = m[3][0] - m[1][0];
		Planes[2].y = m[3][1] - m[1][1];
		Planes[2].z = m[3][2] - m[1][2];
		Planes[2].w = m[3][3] - m[1][3];

		// Bottom clipping plane
		//p_planes[3].a = comboMatrix._41 + comboMatrix._21;
		//p_planes[3].b = comboMatrix._42 + comboMatrix._22;
		//p_planes[3].c = comboMatrix._43 + comboMatrix._23;
		//p_planes[3].d = comboMatrix._44 + comboMatrix._24;
		Planes[3].x = m[3][0] + m[1][0];
		Planes[3].y = m[3][1] + m[1][1];
		Planes[3].z = m[3][2] + m[1][2];
		Planes[3].w = m[3][3] + m[1][3];

		// Near clipping plane:
		// for D3D, the homogeneous z is between 0 and 1
		// for OpenGL, the homogeneous z is between -1 and 1
		if (bD3D)
		{
			Planes[4].x = m[2][0];
			Planes[4].y = m[2][1];
			Planes[4].z = m[2][2];
			Planes[4].w = m[2][3];
		}
		else
		{
			//p_planes[4].a = comboMatrix._41 + comboMatrix._31;
			//p_planes[4].b = comboMatrix._42 + comboMatrix._32;
			//p_planes[4].c = comboMatrix._43 + comboMatrix._33;
			//p_planes[4].d = comboMatrix._44 + comboMatrix._34;
			Planes[4].x = m[3][0] + m[2][0];
			Planes[4].y = m[3][1] + m[2][1];
			Planes[4].z = m[3][2] + m[2][2];
			Planes[4].w = m[3][3] + m[2][3];
		}

		// Far clipping plane
		Planes[5].x = m[3][0] - m[2][0];
		Planes[5].y = m[3][1] - m[2][1];
		Planes[5].z = m[3][2] - m[2][2];
		Planes[5].w = m[3][3] - m[2][3];
	}

}

