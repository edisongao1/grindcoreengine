#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/compatibility.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/vector_angle.hpp>

namespace grind
{
	using Matrix4x4 = glm::mat4x4;
	using Matrix3x3 = glm::mat3x3;
	using Vector2f = glm::vec2;
	using Vector3f = glm::vec3;
	using Vector4f = glm::vec4;
	using Vector2i = glm::ivec2;
	using Vector3i = glm::ivec3;
	using Vector4i = glm::ivec4;
	using Vector2u = glm::uvec2;
	using Vector3u = glm::uvec3;
	using Vector4u = glm::uvec4;
	using Quaternion = glm::quat;

	//const float PI = float(3.141592653589793238462643383279502884197169399375);
	const float PI = glm::pi<float>();
	const float INV_PI = 1.0f / PI;
	const float _2PI = 2.0f * PI;
	const float INV_2PI = 1.0f / _2PI;

	const float MIN_FLOAT = std::numeric_limits<float>::min();
	const float MAX_FLOAT = std::numeric_limits<float>::max();

	const float INF_FLOAT = MAX_FLOAT;

	// the negative infinity is this, if std::numeric_limits<float>::is_iec559 is true
	const float NEG_INF_FLOAT = -MAX_FLOAT;

	template<class T>
	inline bool fcmp_tpl(T a, T b, T fEpsilon = FLT_EPSILON)
	{
		return fabs(a - b) <= fEpsilon;
	}



	inline float sqrt_tpl(float a) { return sqrtf(a); }
	inline double sqrt_tpl(double a) { return sqrt(a); }
	inline float sqrt_tpl(int i) { return sqrtf(static_cast<float>(i)); }

	inline float recip_tpl(float a) { return 1.0f / a; }
	inline double recip_tpl(double a) { return 1.0 / a; }
	inline float recip_tpl(int i) { return 1.0f / (float)i; }

	inline float fabs_tpl(float a) { return fabs(a); }
	inline int fabs_tpl(int a) { return abs(a); }
	inline double fabs_tpl(double a) { return fabs(a); }

	inline float rand_float()
	{
		return (float)(rand()) / (float)RAND_MAX;
	}

	inline float rand_float(float a, float b)
	{
		return a + rand_float() * (b - a);
	}

	inline int rand_int(int a, int b)
	{
		return a + rand() % ((b - a) + 1);
	}

	//inline float radians(float angle)
	//{
	//	static const float k = PI / 180.0f;
	//	return angle * k;
	//}

	template<typename T>
	inline T degree_to_radians(T angle) {
		return glm::radians(angle);
	}

	template<typename T>
	inline T radians_to_degree(T radians) {
		return glm::degrees(radians);
	}

	template<typename T>
	inline float clamp_tpl(T x, T minx, T maxx)
	{
		return std::max<T>(minx, std::min<T>(x, maxx));
	}
}