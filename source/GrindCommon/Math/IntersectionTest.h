#pragma once
#include "AABB.h"
#include "Frustum.h"


namespace grind
{
	enum class EIntersectRelation
	{
		Back,
		Front,
		Intersected,
		Inside,
		Outside,
	};

	namespace IntersectionTest
	{
		inline EIntersectRelation Plane_AABB(const Vector4f& plane, const AABB& aabb)
		{
			Vector4f nearPoint(aabb.MaxEdge, 1.0f);
			Vector4f farPoint(aabb.MinEdge, 1.0f);

			if (plane.x > 0)
			{
				nearPoint.x = aabb.MinEdge.x;
				farPoint.x = aabb.MaxEdge.x;
			}

			if (plane.y > 0)
			{
				nearPoint.y = aabb.MinEdge.y;
				farPoint.y = aabb.MaxEdge.y;
			}

			if (plane.z > 0)
			{
				nearPoint.z = aabb.MinEdge.z;
				farPoint.z = aabb.MaxEdge.z;
			}

			if (glm::dot(plane, nearPoint) > 0)
				return EIntersectRelation::Front;

			if (glm::dot(plane, farPoint) > 0)
				return EIntersectRelation::Intersected;

			return EIntersectRelation::Back;
		}

		template<typename R = bool>
		inline R Frustum_AABB(const Frustum& frustum, const AABB& aabb);


		template<>
		inline bool Frustum_AABB<bool>(const Frustum& frustum, const AABB& aabb)
		{
			for (int i = 0; i < 6; i++)
			{
				if (Plane_AABB(frustum.Planes[i], aabb) == EIntersectRelation::Back)
					return false;
			}
			return true;
		}

		template<>
		inline EIntersectRelation Frustum_AABB<EIntersectRelation>(const Frustum& frustum, const AABB& aabb)
		{
			bool bCompleteInside = true;
			bool bCompleteOutside = false;
			for (int i = 0; i < 6; i++)
			{
				EIntersectRelation relation = Plane_AABB(frustum.Planes[i], aabb);
				if (relation != EIntersectRelation::Front)
					bCompleteInside = false;
				if (relation == EIntersectRelation::Back)
					bCompleteOutside = true;
			}

			if (bCompleteOutside)
				return EIntersectRelation::Outside;
			if (bCompleteInside)
				return EIntersectRelation::Inside;

			return EIntersectRelation::Intersected;
		}
	}
}