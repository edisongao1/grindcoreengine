#pragma once

#include "Common.h"

namespace grind
{
	struct AABB
	{
		//! Default Constructor.
		AABB()
			: MinEdge(INF_FLOAT, INF_FLOAT, INF_FLOAT)
			, MaxEdge(-INF_FLOAT, -INF_FLOAT, -INF_FLOAT) {}

		//! Constructor with min edge and max edge.
		AABB(const Vector3f& min, const Vector3f& max) : MinEdge(min), MaxEdge(max) {}

		//! Constructor with only one point.
		AABB(const Vector3f& init) : MinEdge(init), MaxEdge(init) {}
		//! Constructor with min edge and max edge as single values, not vectors.
		AABB(float minx, float miny, float minz, float maxx, float maxy, float maxz) 
			: MinEdge(minx, miny, minz), MaxEdge(maxx, maxy, maxz) {}

		inline bool operator==(const AABB& other) const { return (MinEdge == other.MinEdge && other.MaxEdge == MaxEdge); }
		inline bool operator!=(const AABB& other) const { return !(MinEdge == other.MinEdge && other.MaxEdge == MaxEdge); }

		void Reset()
		{
			MinEdge = Vector3f(INF_FLOAT, INF_FLOAT, INF_FLOAT);
			MaxEdge = Vector3f(-INF_FLOAT, -INF_FLOAT, -INF_FLOAT);
		}

		//! Adds a point to the bounding box
		/** The box grows bigger, if point is outside of the box.
		\param x x coordinate of the point to add to this box.
		\param y y coordinate of the point to add to this box.
		\param z z coordinate of the point to add to this box. */
		void AddInternalPoint(float x, float y, float z)
		{
			if (x > MaxEdge.x) MaxEdge.x = x;
			if (y > MaxEdge.y) MaxEdge.y = y;
			if (z > MaxEdge.z) MaxEdge.z = z;

			if (x < MinEdge.x) MinEdge.x = x;
			if (y < MinEdge.y) MinEdge.y = y;
			if (z < MinEdge.z) MinEdge.z = z;
		}

		//! Adds a point to the bounding box
		/** The box grows bigger, if point was outside of the box.
		\param p: Point to add into the box. */
		void AddInternalPoint(const Vector3f& p)
		{
			AddInternalPoint(p.x, p.y, p.z);
		}

		void AddInternalBox(const AABB& b)
		{
			AddInternalPoint(b.MaxEdge);
			AddInternalPoint(b.MinEdge);
		}

		Vector3f GetExtent() const
		{
			return MaxEdge - MinEdge;
		}

		Vector3f GetCenter() const
		{
			return (MinEdge + MaxEdge) * 0.5f;
		}

		//! Stores all 8 edges of the box into an array
		/** \param edges: Pointer to array of 8 edges. */
		void GetEdges(Vector3f* edges) const
		{
			const Vector3f middle = GetCenter();
			const Vector3f diag = middle - MaxEdge;

			/*
			Edges are stored in this way:
			Hey, am I an ascii artist, or what? :) niko.
				   /3--------/7
				  / |       / |
				 /  |      /  |
				1---------5   |
				|  /2- - -|- -6
				| /       |  /
				|/        | /
				0---------4/
			*/

			edges[0] = Vector3f(middle.x + diag.x, middle.y + diag.y, middle.z + diag.z);
			edges[1] = Vector3f(middle.x + diag.x, middle.y - diag.y, middle.z + diag.z);
			edges[2] = Vector3f(middle.x + diag.x, middle.y + diag.y, middle.z - diag.z);
			edges[3] = Vector3f(middle.x + diag.x, middle.y - diag.y, middle.z - diag.z);
			edges[4] = Vector3f(middle.x - diag.x, middle.y + diag.y, middle.z + diag.z);
			edges[5] = Vector3f(middle.x - diag.x, middle.y - diag.y, middle.z + diag.z);
			edges[6] = Vector3f(middle.x - diag.x, middle.y + diag.y, middle.z - diag.z);
			edges[7] = Vector3f(middle.x - diag.x, middle.y - diag.y, middle.z - diag.z);
		}

		//! The near edge
		Vector3f MinEdge;

		//! The far edge
		Vector3f MaxEdge;

		inline static AABB ComputeTransformedAABB(const AABB& aabb, const Matrix4x4& matrix)
		{
			AABB transformedAabb;
			Vector3f edges[8];
			aabb.GetEdges(edges);
			
			for (int i = 0; i < 8; i++)
			{
				Vector3f v = matrix * Vector4f(edges[i], 1.0f);
				transformedAabb.AddInternalPoint(v);
			}
			return transformedAabb;
		}

	};
}
