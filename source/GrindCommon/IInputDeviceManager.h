#pragma once
#include <functional>

namespace grind
{
	enum class EInputDeviceType 
	{
		Mouse,		///< A mouse/cursor input device featuring one pointer.
		Keyboard,		///< A keyboard input device.
		Gamepad,			///< A joypad/gamepad input device.
		Count,
		Unknown = 99
	};

	enum class ECursorImage
	{
		AppStarting,
		Arrow,
		Cross,
		Hand,
		Help,
		IBeam,
		Wait,
		_Count,
	};

	struct SInputMovementData
	{
		EInputDeviceType	DeviceType;
		unsigned int		Key;
		float				Value;
		float				OldValue;
		float				DeltaValue;
	};

	class IInputDeviceManager
	{
	public:
		virtual ~IInputDeviceManager(){}
		using InputKey = unsigned int;

		using TPressEventHandler = std::function<void(InputKey, bool)>;

		using TMoveEventHandler = std::function<void(SInputMovementData&)>;
		//using TMoveEventHandlerMap = std::array<TMoveEventHandler, (size_t)EInputKey::KeyCount_>;
		using ListenerId = uint32_t;


		//virtual ListenerId AddPressEventListener( EInputKey key, TPressEventHandler handler) = 0;
		virtual ListenerId AddButtonListener(EInputDeviceType deviceType, TPressEventHandler handler) = 0;
		virtual void RemoveButtonListener(EInputDeviceType deviceType, ListenerId id) = 0;
		virtual ListenerId AddMoveListener(EInputDeviceType deviceType, TMoveEventHandler handler) = 0;
		virtual void RemoveMoveListener(EInputDeviceType deviceType, ListenerId id) = 0;
		virtual void Update(float fDeltaTime) = 0;
		virtual bool IsButtonPressed(EInputDeviceType deviceType, InputKey key) const = 0;

		// 手柄震动相关：
		virtual void SetGamepadViberate(float fLeftMotorSpeed, float fRightMotorSpeed, float fLastTime) = 0;
		virtual void StopGamepadViberate() = 0;
		virtual bool IsGamepadViberating() const = 0;

		bool IsKeyboardPressed(InputKey key) const
		{
			return IsButtonPressed(EInputDeviceType::Keyboard, key);
		}

		bool IsMousePressed(InputKey key) const 
		{
			return IsButtonPressed(EInputDeviceType::Mouse, key);
		}

		bool IsGamepadPressed(InputKey key) const
		{
			return IsButtonPressed(EInputDeviceType::Gamepad, key);
		}

		ListenerId AddMouseButtonListener(TPressEventHandler handler)
		{
			return AddButtonListener(EInputDeviceType::Mouse, handler);
		}
		ListenerId AddMouseMoveListener(TMoveEventHandler handler)
		{
			return AddMoveListener(EInputDeviceType::Mouse, handler);
		}
		ListenerId AddKeyboardButtonListener(TPressEventHandler handler)
		{
			return AddButtonListener(EInputDeviceType::Keyboard, handler);
		}
		ListenerId AddGamepadButtonListener(TPressEventHandler handler)
		{
			return AddButtonListener(EInputDeviceType::Gamepad, handler);
		}
		ListenerId AddGamepadMoveListener(TMoveEventHandler handler)
		{
			return AddMoveListener(EInputDeviceType::Gamepad, handler);
		}

		void RemoveMouseButtonListener(ListenerId id)
		{
			RemoveButtonListener(EInputDeviceType::Mouse, id);
		}
		void RemoveMouseMoveListener(ListenerId id)
		{
			RemoveMoveListener(EInputDeviceType::Mouse, id);
		}
		void RemoveKeyboardButtonListener(ListenerId id)
		{
			RemoveButtonListener(EInputDeviceType::Keyboard, id);
		}
		void RemoveGamepadButtonListener(ListenerId id)
		{
			RemoveButtonListener(EInputDeviceType::Gamepad, id);
		}
		void RemoveGamepadMoveListener(ListenerId id)
		{
			RemoveMoveListener(EInputDeviceType::Gamepad, id);
		}

		virtual float GetFloat(EInputDeviceType deviceType, InputKey key) = 0;
		virtual float GetFloatPrevious(EInputDeviceType deviceType, InputKey key) = 0;

		virtual void ShowMouseCursor(bool bDisplay) = 0;
		virtual void SetMouseCursorPos(int x, int y) = 0;

		virtual void SetMouseCursorImage(ECursorImage eImage) = 0;

		// 存储gainput::DeviceButtonId与EInputButtonType的对应关系
		//EInputDeviceButton			mMouseButtonIdMap[64];
		//EInputDeviceButton			mGamepadButtonIdMap[64];
	};

}