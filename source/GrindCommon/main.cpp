// GrindCommon.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "stdafx.h"
#include <iostream>
#include "LockFreeQueue.h"
#include "MemoryPool.h"
#include <vector>
#include "GrindMath.h"
//#include "Ogre/OgreVector4.h"
#include "IMesh.h"
#include "Timer.h"
#include "IInputDeviceManager.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>

using namespace grind;

class Person {
public:
	Person(int age, const std::string& name) :age(age), name(name)
	{

	}
protected:
	int age;
	std::string name;
};

class Student : public Person {
public:
	Student(int age, const std::string& name, int grade) :Person(age, name), grade(grade)
	{
		std::printf("student constructor\n");
	}
	void printInfo()
	{
		std::printf("name:%s, age:%d, grade:%d\n", name.c_str(), age, grade);
	}
	~Student()
	{
		std::printf("student destructor\n");
	}
protected:
	int grade;
};

void TestLockFreeMemoryPool()
{
	LockFreeMemoryPool<5, 16, 32, 64> pool;
	printf("pool.ALLOCATOR_COUNT: %d\n", pool.ALLOCATOR_COUNT);

	void* p1 = pool.Allocate<4>();
	void* p2 = pool.Allocate<30>();
	void* p3 = pool.Allocate<63>();

	std::vector<Student*> v;
	for (int i = 0; i < 1000; i++) {
		Student* p = pool.CreateObject<Student>(i, "abc", i * 10);
		v.push_back(p);
	}

	std::thread thread1([&pool]() {
		for (int i = 0; i < 1000; i++) {
			pool.CreateObject<Student>(i, "def", i * 4);
		}
	});

	for (int i = 0; i < v.size(); i++) {
		Student* p = v[i];
		pool.DestroyObject<Student>(p);
	}

	thread1.join();

	Student* p4 = pool.CreateObject<Student>(20, "abc", 1);
	p4->printInfo();
	pool.DestroyObject<Student>(p4);

	pool.Free(p1);
	pool.Free(p2);
	pool.Free(p3);
}

int main()
{
	TLockFreeQueue<int, 32> q;
	for (int i = 0; i < 10; i++) {
		q.try_push(i);
	}

	for (int i = 0; i < 10; i++) {
		int x;
		if (q.try_pop(x)) {
			std::cout << x << std::endl;
		}
	}

	TestLockFreeMemoryPool();

	glm::vec4 v1(0, 1, 1, 2);
	glm::vec4 v2(1.2f, 2, 0, 3);
	glm::vec4 v3 = v1 + v2;

	printf("v3=(%f, %f, %f, %f)\n", v3.x, v3.y, v3.z, v3.w);

	system("pause");
	return 0;
}
