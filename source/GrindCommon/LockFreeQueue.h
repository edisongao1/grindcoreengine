#pragma once
#include <atomic>

namespace grind {

	/// refer to article--https://www.codeproject.com/Articles/153898/Yet-another-implementation-of-a-lock-free-circular
	template<typename T, unsigned int MAX_SIZE>
	class TLockFreeQueue
	{
	public:
		TLockFreeQueue()
			: m_uReadIndex(0)
			, m_uWriteIndex(0)
			, m_uMaxReadIndex(0)
		{

		}

		~TLockFreeQueue()
		{

		}


		unsigned capacity() const { return MAX_SIZE; }

		template<typename U>
		bool try_push(U&& data)
		{
			uint32_t writeIndex, readIndex, newWriteIndex;

			do {
				writeIndex = m_uWriteIndex.load();
				readIndex = m_uReadIndex.load();
				newWriteIndex = (writeIndex + 1) % MAX_SIZE;
				// if queue is full
				if (newWriteIndex == readIndex) {
					return false;
				}
			} while (!m_uWriteIndex.compare_exchange_strong(writeIndex, newWriteIndex));

			m_QueueItems[writeIndex] = std::forward<U>(data);

			while (!m_uMaxReadIndex.compare_exchange_strong(writeIndex, newWriteIndex)) {
				//std::yield();
				std::this_thread::yield();
			}
			return true;
		}

		bool try_pop(T& data)
		{
			uint32_t readIndex, newReadIndex, maxReadIndex;

			do {
				readIndex = m_uReadIndex.load();
				maxReadIndex = m_uMaxReadIndex.load();

				// empty
				if (readIndex == maxReadIndex) {
					return false;
				}

				data = m_QueueItems[readIndex];

				newReadIndex = (readIndex + 1) % MAX_SIZE;
				if (m_uReadIndex.compare_exchange_strong(readIndex, newReadIndex)) {
					return true;
				}

			} while (1);

			return true;
		}

		bool unsafe_pop(T& data)
		{
			uint32_t readIndex = m_uReadIndex.load();
			uint32_t maxReadIndex = m_uMaxReadIndex.load();
			// empty
			if (readIndex == maxReadIndex) {
				return false;
			}
			data = m_QueueItems[readIndex];
			uint32_t newReadIndex = (readIndex + 1) % MAX_SIZE;
			m_uReadIndex.store(newReadIndex);
			return true;
		}

		//void pop_all();
		size_t size() const
		{
			uint32_t writeIndex = m_uWriteIndex.load();
			uint32_t readIndex = m_uReadIndex.load();
			if (writeIndex > readIndex) {
				return writeIndex - readIndex;
			}
			else {
				return MAX_SIZE + writeIndex - readIndex;
			}
		}

		template<typename U>
		void write_at(U&& data, uint32_t writeIndex)
		{
			m_QueueItems[writeIndex] = std::forward<U>(data);
		}

	private:
		T							m_QueueItems[MAX_SIZE];
		std::atomic<uint32_t>		m_uReadIndex;
		std::atomic<uint32_t>		m_uWriteIndex;
		std::atomic<uint32_t>		m_uMaxReadIndex;
	};

}