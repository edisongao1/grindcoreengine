file(GLOB HEADER_FILES "${CMAKE_CURRENT_SOURCE_DIR}/*.h" "${CMAKE_CURRENT_SOURCE_DIR}/*.hpp")
file(GLOB SOURCE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")
file(GLOB INL_FILES "${CMAKE_CURRENT_SOURCE_DIR}/*.inl")
file(GLOB IRRLICHT_FILES "${CMAKE_CURRENT_SOURCE_DIR}/Irrlicht/*.h")
#file(GLOB OGRE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/Ogre/*.*")
file(GLOB HASH_FILES "${CMAKE_CURRENT_SOURCE_DIR}/Hash/*.h")
file(GLOB FASTECS_FILES "${CMAKE_CURRENT_SOURCE_DIR}/FastECS/*.hpp")
file(GLOB MATH_FILES "${CMAKE_CURRENT_SOURCE_DIR}/Math/*.h" "${CMAKE_CURRENT_SOURCE_DIR}/Math/*.hpp")


set(MODULE_NAME GrindCommon)
set(PRECOMPILED_CPP_FILE ${CMAKE_CURRENT_SOURCE_DIR}/stdafx.cpp)

source_group("" FILES ${HEADER_FILES} ${SOURCE_FILES} ${INL_FILES})
source_group("Irrlicht" FILES ${IRRLICHT_FILES})
source_group("Hash" FILES ${HASH_FILES})
source_group("FastECS" FILES ${FASTECS_FILES})
source_group("Math" FILES ${MATH_FILES})

#source_group("Ogre" FILES ${OGRE_FILES})

include_directories(${PROJECT_SOURCE_DIR}/thirdparty/glm)
include_directories(${PROJECT_SOURCE_DIR}/thirdparty/tinyxml2)

add_library(${MODULE_NAME} 
			${HEADER_FILES}
			${SOURCE_FILES}
			${INL_FILES}
			${IRRLICHT_FILES}
			${HASH_FILES}
			${FASTECS_FILES}
			${MATH_FILES}
)

add_precompiled_header(${MODULE_NAME} "stdafx.h" SOURCE_CXX ${PRECOMPILED_CPP_FILE} FORCEINCLUDE)

set_property(TARGET ${MODULE_NAME} PROPERTY FOLDER Engine)

add_dependencies(${MODULE_NAME} glm_shared)
#link_directories(${PROJECT_BINARY_DIR}/bin)
#target_link_libraries(${MODULE_NAME} glm_shared)

