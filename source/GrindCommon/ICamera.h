#pragma once
#include "GrindMath.h"
#include "IInputDeviceManager.h"
#include "GrindEnums.h"

namespace grind
{


	enum class ECameraAction
	{
		//EFCA_MOVE_FORWARD = 0,
		//EFCA_MOVE_BACK,
		//EFCA_MOVE_LEFT,
		//EFCA_MOVE_RIGHT,
		//EFCA_JUMP,
		//EFCA_CREEP,
		//EFCA_COUNT,
		MoveForward,
		Back,
		Left,
		Right,
		Jump,
		Creep,
		Count_
	};

	struct SCameraMovementParams
	{
		using TMovementKey = std::pair<EInputDeviceType, IInputDeviceManager::InputKey>;
		
	};

	class ICamera
	{
	public:
		ICamera(const Vector3f& position,
			const Vector2f& projectionSize,
			float fov,
			float nearZ,
			float farZ,
			bool bOrthogonal)
			: mPosition(position)
			, mFovAngleY(fov)
			, mProjectionSize(projectionSize)
			, mNearZ(nearZ)
			, mFarZ(farZ)
			, mIsOrthogonal(bOrthogonal)
		{
			mLocalAxes[(int)EAxis::X] = Vector3f(1, 0, 0);
			mLocalAxes[(int)EAxis::Y] = Vector3f(0, 1, 0);
			mLocalAxes[(int)EAxis::Z] = Vector3f(0, 0, 1);
		}

		virtual ~ICamera(){}
		void SetPosition(const Vector3f& pos)  { mPosition = pos; }
		const Vector3f& GetPosition() const { return mPosition; }

		
		virtual void Rotate(int localAxis, float radians) = 0;

		virtual void Move(const Vector3f& dir, float fDist) = 0;
		virtual void MoveRight(float fDist) = 0;
		virtual void MoveUp(float fDist) = 0;
		virtual void MoveForward(float fDist) = 0;

		virtual void LookAt(const Vector3f& targetPos) = 0;
		virtual void Look(const Vector3f& dir) = 0;

		virtual void Update(float fDeltaTime) = 0;

		//virtual void lookAt(const XMFLOAT3& lookat, const XMFLOAT3& up) = 0;
		//virtual void look(const XMFLOAT3& dir, const XMFLOAT3& up) = 0;

		const Matrix4x4& GetViewTransform() const { return mViewMatrix; }
		const Matrix4x4& GetProjTransform() const { return mProjMatrix; }
		const Matrix4x4& GetViewProjTransform() const { return mViewProjMatrix; }
		const Matrix4x4& GetInverseViewProjTransform() const { return mInvViewProjMatrix; }

		const Frustum& GetFrustum() const { return mFrustum; }

	protected:
		Vector3f		mPosition;
		//Vector3f		mRight;		/* 摄像机的右向量 */
		//Vector3f		mUp;		/* 摄像机的上向量 */
		//Vector3f		mLook;		/* 摄像机的前向量 */
		Vector3f		mLocalAxes[3];

		Matrix4x4			mViewMatrix;
		Matrix4x4			mProjMatrix;
		Matrix4x4			mViewProjMatrix;
		Matrix4x4			mInvViewProjMatrix;
		Frustum				mFrustum;

		float			mFovAngleY;
		//float			mAspectRatio;
		//float			mProjectionWidth;
		//float			mProjectionHeight;
		Vector2f		mProjectionSize;

		float			mNearZ;
		float			mFarZ;
		float			mViewWidth;
		float			mViewHeight;

		bool			mIsOrthogonal = false;
	};

	class IFpsCameraInterface
	{
	public:
		IFpsCameraInterface(
			float maxUpAngle,
			float maxDownAngle)
			:mMaxUpAngle(maxUpAngle),
			mMaxDownAngle(maxDownAngle) 
		{

		}

		bool IsCreeping() const { return mIsCreeping; }

		void SetWalkSpeed(float speed) { mWalkSpeed = speed; }
		void SetStrafeSpeed(float speed) { mStrafeSpeed = speed; }
		float GetWalkSpeed() const { return mWalkSpeed; }
		float GetStrafeSpeed() const { return mStrafeSpeed; }
		void SetRotateSpeed(float speed) { mRotateSpeed = speed; }
		float GetRotateSpeed() const { return mRotateSpeed; }
		void SetJumpSpeed(float speed) { mJumpSpeed = speed; }
		float GetJumpSpeed() const { return mJumpSpeed; }

		void SetStandHeight(float height) { mStandHeight = height; }
		float GetStandHeight() const { return mStandHeight; }
		void SetCreepHeight(float height) { mCreepHeight = height; }
		float GetCreepHeight() const { return mCreepHeight; }

		virtual void Creep() = 0;
		virtual void Stand() = 0;
		virtual void Jump() = 0;

		virtual void SetMoveEnabled(bool bEnabled) = 0;
		
	protected:
		bool								mMoveEnabled = true;

		float								mMaxUpAngle;			/* 最大抬头角度 */
		float								mMaxDownAngle;			/* 最大低头角度 */

		float								mWalkSpeed = 4.0f;
		float								mStrafeSpeed = 4.0f;
		float								mRotateSpeed = 0.5f; // radians per second

		float								mStandHeight = 1.8f;
		float								mCreepHeight = 0.9f;
		float								mCrouchSpeed = 4.0f;
		float								mJumpSpeed = 5.0f;
	
		//float								mHeight;
		//float								mActionMappings[EFCA_COUNT];

		bool								mAutoUpdate;
		bool								mIsCreeping = false;

		float								mVerticalVelocity = 0;
		float								mPitchAngle = 0;			/* 俯仰角度，即视线偏离行走方向的角度 */
		Vector3f							mWalkDir = Vector3f(0, 0, 1);
	};
}