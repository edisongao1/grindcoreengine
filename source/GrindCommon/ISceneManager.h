#pragma once

#include "GrindECS.h"
#include "tinyxml2.h"
#include "IEntityAction.h"

namespace grind
{
	class ICamera;

	class ISceneManager;

	class ISceneLoadListener
	{
	public:
		virtual Entity* OnProcessEntityXML(
			const char* szName,
			const char* szClassType,
			tinyxml2::XMLElement* ComponentsNode, 
			ISceneManager* pSceneManager) = 0;

		virtual Entity* OnProcessEntityComponentXML(Entity* pEntity,
			tinyxml2::XMLElement* ComponentNode, 
			ISceneManager* pSceneManager) = 0;

		/*virtual Entity* LoadEntity(const char* szName, tinyxml2::XMLElement* ComponentsNode) = 0;
		virtual Entity* AddComponentToEntity(Entity* pEntity, tinyxml2::XMLElement* ComponentNode) = 0;*/
	};

	class ISceneManager
	{
	public:
		virtual ~ISceneManager(){}
		virtual void Initialize() = 0;
		virtual Entity* GetEntityByName(const char* szName) = 0;

		virtual void CullEntities() = 0;

		virtual Entity* GetMainCamera() = 0;
		virtual bool SetMainCamera(Entity* pEntity) = 0;
		//ICamera* GetMainCamera() { return mMainCamera; }
		//virtual EntityContext* GetEntityContext(const Vector3f& pos) = 0;

		virtual EntityContext* GetStaticEntityContext(const Vector3f& pos) = 0;
		virtual EntityContext* GetMovableEntityContext() = 0;

		virtual Entity* AddMeshEntity(const char* szName, 
			const TransformComponent& transform,
			const MeshComponent& staticMesh) = 0;

		virtual Entity* AddCamera(const char* szName,
			const TransformComponent& transform,
			const CameraComponent& cameraComponent) = 0;

		IMeshEntityAction* GetMeshEntityAction() { return mMeshEntityAction.get(); }

		ICameraAction* GetCameraAction() { return mCameraAction.get(); }

		// migrate an entity from one context to another
		// return if it has been migrated
		virtual bool MigrateEntity(Entity*& pEntity, EntityContext* pNewContext) = 0;

		virtual void DestroyEntity(Entity* pEntity) = 0;
	
	protected:
		//ICamera* mMainCamera = nullptr;
		std::unique_ptr<IMeshEntityAction>		mMeshEntityAction = nullptr;
		std::unique_ptr<ICameraAction>			mCameraAction = nullptr;
	};

	// migrate an entity from one context to another
	// return the new entity
	//inline Entity* MigrateEntity(Entity* pEntity, EntityContext* pNewContext)
	//{
	//	EntityContext* pOldContext = pEntity->GetContext();
	//	if (pOldContext == pNewContext)
	//		return pEntity;

	//	// migrate entity from one context to another
	//	Entity* pNewEntity = pNewContext->CreateEntity(pEntity->GetArchetype());
	//	pNewContext->CopyEntityData(pNewEntity, pEntity);
	//	pEntity->Release();
	//	return pNewEntity;
	//}
}

