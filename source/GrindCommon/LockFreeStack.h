#pragma once

namespace grind {

	template<typename T>
	class LockFreePointerStack
	{
#define TAG_BITS_INC 0x0001000000000000LL
#define TAG_BITS_MASK 0xFFFF000000000000LL

		//enum : uint64_t {
		//	TAG_BITS_INC = 0x0001000000000000LL,
		//	TAG_BITS_MASK = 0xFFFF000000000000LL
		//};

	public:
		LockFreePointerStack() : m_head(0) {

		}

		void push(T* p) {
			intptr_t oldVal = m_head.load();
			intptr_t newVal;
			do {
				newVal = (intptr_t)p;

				// update tags
				intptr_t tags = oldVal & TAG_BITS_MASK;
				tags = (tags + TAG_BITS_INC) & TAG_BITS_MASK;
				newVal |= tags;

				// update next
				p->next = oldVal & ~TAG_BITS_MASK;
			} while (!m_head.compare_exchange_weak(oldVal, newVal));
		}

		T* pop()
		{
			intptr_t oldVal = m_head.load();
			intptr_t newVal;
			T* ptr = nullptr;
			do {
				// extract ptr from oldVal
				ptr = reinterpret_cast<T*>(oldVal & ~TAG_BITS_MASK);
				if (ptr == nullptr)
					break;

				// update next
				newVal = ptr->next;

				// update tags
				intptr_t tags = oldVal & TAG_BITS_MASK;
				tags = (tags + TAG_BITS_INC) & TAG_BITS_MASK;
				newVal |= tags;
			} while (!m_head.compare_exchange_weak(oldVal, newVal));
			return ptr;
		}

	public:
		std::atomic<intptr_t>		m_head;
	};

	template<unsigned BlockSize, unsigned BlockCount>
	class FixedSizeLockFreeMemoryAllocator
	{
	public:
		FixedSizeLockFreeMemoryAllocator()
			: m_pMem(nullptr)
			, m_avail(0)
		{
			m_pMem = (byte*)malloc(BlockSize * BlockCount);
			for (int i = 0; i < BlockCount; i++) {
				byte* p = m_pMem + i * BlockSize;
				*reinterpret_cast<int*>(p) = i + 1;
			}
		}

		~FixedSizeLockFreeMemoryAllocator()
		{
			free(m_pMem);
		}

		void* Allocator() {
			// trying to allocate until succeed
			void* ptr = nullptr;
			do {
				ptr = TryAllocate();
			} while (ptr == nullptr);
			return ptr;
		}

		void* TryAllocate()
		{
			byte* ptr = nullptr;
			int oldAvail = m_avail.load();
			int newAvail;
			do {
				// no available block
				if (oldAvail >= BlockCount) {
					return nullptr;
				}

				ptr = m_pMem + oldAvail * BlockSize;

				// update next avail
				newAvail = *reinterpret_cast<int*>(ptr);

			} while (!m_avail.compare_exchange_weak(oldAvail, newAvail));
			return ptr;
		}

		void Free(void* p)
		{
			byte* ptr = (byte*)p;
			int oldAvail = m_avail.load();
			int newAvail;
			do {
				newAvail = static_cast<int>(ptr - m_pMem) / BlockSize;
				*reinterpret_cast<int*>(ptr) = oldAvail;
			} while (!m_avail.compare_exchange_weak(oldAvail, newAvail));
		}

		unsigned GetCapacity() const {
			return BlockCount;
		}

	private:
		byte*				m_pMem;
		std::atomic<int>	m_avail;
	};

	void TestFixedSizeLockFreeMemoryAllocator();


}