#pragma once

namespace grind
{

	class ThreadLocalBlockAllocator
	{
	public:
		struct SDescriptor
		{
			byte* sb;
			ThreadLocalBlockAllocator* heap;
			SDescriptor* next;
			unsigned avail; // next available index
			unsigned count; // count of blocks available
		};

	public:
		inline ThreadLocalBlockAllocator();
		inline ~ThreadLocalBlockAllocator();

		unsigned int						m_blocksize;
		unsigned int						m_sbsize;

		inline unsigned int GetBlockSize() const { return m_blocksize; }
		inline void Init(unsigned int blocksize, unsigned int sbsize);
		inline void* Allocate();
		inline void Free(void* p);

	private:
		SDescriptor* m_active;
		SDescriptor* m_partialList;
		inline SDescriptor* CreateChunk();
		inline void ReleaseChunk(SDescriptor* pDesc);
		inline void* AllocateFromChunk(SDescriptor* pChunk);
	};

	ThreadLocalBlockAllocator::ThreadLocalBlockAllocator()
		:m_active(nullptr)
		, m_partialList(nullptr)
	{

	}

	ThreadLocalBlockAllocator::~ThreadLocalBlockAllocator()
	{
		if (m_active)
		{
			ReleaseChunk(m_active);
		}

		SDescriptor* p = m_partialList;
		while (p != nullptr) {
			SDescriptor* next = p->next;
			ReleaseChunk(p);
			p = next;
		}
	}

	void ThreadLocalBlockAllocator::Init(unsigned int blocksize, unsigned int sbsize)
	{
		m_blocksize = blocksize;
		m_sbsize = sbsize;
	}

	void* ThreadLocalBlockAllocator::Allocate()
	{
		void* ptr = nullptr;
		if (m_active)
		{
			ptr = AllocateFromChunk(m_active);
			if (m_active->count == 0) {
				m_active = nullptr;
			}
			return ptr;
		}

		// allocate from partialList
		if (m_partialList) {
			m_active = m_partialList;
			m_partialList = m_partialList->next;
			return Allocate();
		}

		// allocate a new chunk
		m_active = CreateChunk();
		return Allocate();
	}

	void ThreadLocalBlockAllocator::Free(void* p)
	{
		byte* ptr = (byte*)p - sizeof(intptr_t);
		intptr_t addr = *reinterpret_cast<intptr_t*>(ptr);
		SDescriptor* pChunk = reinterpret_cast<SDescriptor*>(addr);

		*reinterpret_cast<unsigned*>(ptr) = pChunk->avail;
		pChunk->avail = static_cast<unsigned>(pChunk->sb - ptr) / m_blocksize;
		pChunk->count += 1;

		// from full to unfull
		if (pChunk->count == 1)
		{
			// put it into partialList
			pChunk->next = m_partialList;
			m_partialList = pChunk;
		}
	}

	ThreadLocalBlockAllocator::SDescriptor* ThreadLocalBlockAllocator::CreateChunk()
	{
		SDescriptor* pChunk = new SDescriptor;
		pChunk->next = nullptr;
		pChunk->sb = (byte*)malloc(m_blocksize * m_sbsize);
		for (unsigned i = 0; i < m_sbsize; i++) {
			byte* p = pChunk->sb + m_blocksize * i;
			*reinterpret_cast<unsigned*>(p) = i + 1;
		}
		pChunk->heap = this;
		pChunk->avail = 0;
		pChunk->count = m_sbsize;

		//printf("CreateChunk %p\n", pChunk);
		return pChunk;
	}

	void ThreadLocalBlockAllocator::ReleaseChunk(SDescriptor* pDesc)
	{
		free(pDesc->sb);
		delete pDesc;
	}

	void* ThreadLocalBlockAllocator::AllocateFromChunk(SDescriptor* pChunk)
	{
		byte* ptr = m_active->sb + m_blocksize * m_active->avail;
		m_active->avail = *reinterpret_cast<unsigned*>(ptr);
		*reinterpret_cast<intptr_t*>(ptr) = (intptr_t)m_active;
		m_active->count -= 1;
		return ptr + sizeof(intptr_t);
	}

}
