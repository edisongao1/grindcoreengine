#pragma once

#include "LockFreeBlockAllocator.h"
#include "ThreadLocalBlockAllocator.h"

namespace grind {

	template<bool, unsigned RequestSize, unsigned index, unsigned size, unsigned... Sizes>
	struct AllocatorIndex;

	template<unsigned RequestSize, unsigned index, unsigned size, unsigned... Sizes>
	struct AllocatorIndex<true, RequestSize, index, size, Sizes...>
	{
		static const int value = index - 1;
	};

	template<unsigned RequestSize, unsigned index, unsigned size, unsigned... Sizes>
	struct AllocatorIndex<false, RequestSize, index, size, Sizes...> : public AllocatorIndex<RequestSize <= size, RequestSize, index + 1, Sizes...>
	{

	};

	template<unsigned RequestSize, unsigned index, unsigned size>
	struct AllocatorIndex<false, RequestSize, index, size>
	{
		static const int value = (RequestSize <= size) ? index : -1;
	};

	template<typename TBlockAllocator, unsigned ChunkSize, unsigned... BlockSizes>
	class MemoryPoolBase
	{
	public:
		MemoryPoolBase()
		{
			InitAllocators<BlockSizes...>();
		}

		void* Allocate(unsigned size)
		{
			for (int i = 0; i < ALLOCATOR_COUNT; i++) {
				if (size + sizeof(intptr_t) <= m_allocators[i].GetBlockSize()) {
					return m_allocators[i].Allocate();
				}
			}
			return AllocateFromSystem(size);
		}

		void* AllocateFromSystem(unsigned size)
		{
			byte* ptr = (byte*)malloc(size + sizeof(intptr_t));
			*reinterpret_cast<intptr_t*>(ptr) = 0;
			return ptr + sizeof(intptr_t);
		}

		template<unsigned size>
		void* Allocate()
		{
			int index = AllocatorIndex<false, size + sizeof(intptr_t), 0, BlockSizes...>::value;
			if (index < 0) {
				return AllocateFromSystem(size);
			}
			return m_allocators[index].Allocate();
		}

		template<typename T, typename... Args>
		T* CreateObject(Args&&... args)
		{
			//constexpr unsigned size = sizeof(T) + sizeof(intptr_t);
			void* pMem = Allocate<sizeof(T)>();
			T* pObject = new (pMem) T(std::forward<Args>(args)...);
			return pObject;
		}

		template<typename TActualClass, typename TPointer>
		void DestroyObject(TPointer* pObject)
		{
			pObject->~TActualClass();
			Free<sizeof(TActualClass)>(pObject);
		}

		template<unsigned size>
		void Free(void* p)
		{
			int index = AllocatorIndex<false, size + sizeof(intptr_t), 0, BlockSizes...>::value;
			if (index < 0) {
				byte* ptr = (byte*)p - sizeof(intptr_t);
				free(ptr);
				return;
			}
			m_allocators[index].Free(p);
		}

		void Free(void* p)
		{
			byte* ptr = (byte*)p - sizeof(intptr_t);
			intptr_t addr = *reinterpret_cast<intptr_t*>(ptr);
			typename TBlockAllocator::SDescriptor* pDesc = reinterpret_cast<typename TBlockAllocator::SDescriptor*>(addr);
			if (pDesc == nullptr) {
				free(ptr);
				return;
			}
			pDesc->heap->Free(p);
		}


		const static int ALLOCATOR_COUNT = sizeof...(BlockSizes);
		const static int CHUNK_SIZE = ChunkSize;

	protected:

		template<unsigned size, unsigned... Sizes>
		typename std::enable_if_t<sizeof...(Sizes) != 0> InitAllocators()
		{
			m_allocators[ALLOCATOR_COUNT - sizeof...(Sizes) - 1].Init(size, CHUNK_SIZE);
			InitAllocators<Sizes...>();
		}

		template<unsigned size>
		void InitAllocators()
		{
			m_allocators[ALLOCATOR_COUNT - 1].Init(size, CHUNK_SIZE);
		}

		TBlockAllocator m_allocators[ALLOCATOR_COUNT];
	};

	template<unsigned ChunkSize, unsigned... BlockSizes>
	using LockFreeMemoryPool = MemoryPoolBase<LockFreeBlockAllocator, ChunkSize, BlockSizes...>;

	template<unsigned ChunkSize, unsigned... BlockSizes>
	using ThreadLocalMemoryPool = MemoryPoolBase<ThreadLocalBlockAllocator, ChunkSize, BlockSizes...>;


	//template<unsigned ChunkSize, unsigned... BlockSizes>
	//class LockFreeMemoryPool : public MemoryPoolBase<LockFreeBlockAllocator, ChunkSize, BlockSizes...>
	//{
	//public:
	//	void Free(void* p)
	//	{
	//		byte* ptr = (byte*)p - sizeof(intptr_t);
	//		intptr_t addr = *reinterpret_cast<intptr_t*>(ptr);
	//		LockFreeBlockAllocator::SDescriptor* pDesc = reinterpret_cast<LockFreeBlockAllocator::SDescriptor*>(addr);
	//		if (pDesc == nullptr) {
	//			free(ptr);
	//			return;
	//		}
	//		pDesc->heap->Free(p);
	//	}
	//};
	//
	//template<unsigned ChunkSize, unsigned... BlockSizes>
	//class ThreadLocalMemoryPool : public MemoryPoolBase<ThreadLocalBlockAllocator, ChunkSize, BlockSizes...>
	//{
	//public:
	//	void Free(void* p)
	//	{
	//		byte* ptr = (byte*)p - sizeof(intptr_t);
	//		intptr_t addr = *reinterpret_cast<intptr_t*>(ptr);
	//		ThreadLocalBlockAllocator::SChunk* pDesc = reinterpret_cast<ThreadLocalBlockAllocator::SChunk*>(addr);
	//		if (pDesc == nullptr) {
	//			free(ptr);
	//			return;
	//		}
	//		pDesc->heap->Free(p);
	//	}
	//};

}