#pragma once

#include "LockFreeStack.h"

namespace grind {

	class LockFreeBlockAllocator
	{
	public:
		struct SAnchor
		{
			uint16_t avail;
			uint16_t count;
			unsigned int state : 2;
			unsigned int tags : 30;
		};

		struct SDescriptor
		{
			std::atomic<SAnchor> anchor;
			intptr_t next;
			LockFreeBlockAllocator* heap;
			unsigned int sz;
			unsigned int maxcount;
			byte* sb;

			enum ESuperBlockState
			{
				EMPTY = 0,
				ACTIVE,
				PARTIAL,
				FULL,
			};
		};

		using SDescriptorList = LockFreePointerStack<SDescriptor>;

#define CREDIT_BITS_INC 0x0001000000000000LL
#define CREDIT_BITS_MASK 0xFFFF000000000000LL
#define POINTER_BITS_MASK (~CREDIT_BITS_MASK)
#define MAX_CREDIT_COUNT 0xFFFF
#define ACTIVE_POINTER_BITS (48)

		//enum :uint64_t {
		//	CREDIT_BITS_INC = 0x0001000000000000LL,
		//	CREDIT_BITS_MASK = 0xFFFF000000000000LL,
		//	POINTER_BITS_MASK = (~CREDIT_BITS_MASK),
		//	MAX_CREDIT_COUNT = 0xFFFF,
		//	ACTIVE_POINTER_BITS = (48),
		//};

	public:
		inline LockFreeBlockAllocator();
		inline ~LockFreeBlockAllocator();

		std::atomic<intptr_t>				m_active;
		std::atomic<SDescriptor*>			m_partial;
		SDescriptorList						m_partialList;
		unsigned int						m_blocksize;
		unsigned int						m_sbsize;

		unsigned int GetBlockSize() const { return m_blocksize; }

		inline void Init(unsigned int blocksize, unsigned int sbsize);
		inline void* Allocate();
		inline void Free(void* p);

		inline void UpdateActive(SDescriptor* pDesc, intptr_t credits);

	public:
		inline SDescriptor* CreateDescriptor(unsigned state, unsigned count, unsigned avail = 0);
		inline void ReleaseDescriptor(SDescriptor* p);

		inline void* AllocateFromActive();
		inline void* AllocateFromPartial();
		inline void* AllocateFromNewSuperBlock();

		inline SDescriptor* GetPartial();
		inline SDescriptor* ListGetPartial();

		inline void PutPartial(SDescriptor* pDesc);
		inline void ListPutPartial(SDescriptor* pDesc);
	};

	void TestLockFreeBlockAllocator();


	LockFreeBlockAllocator::LockFreeBlockAllocator()
		: m_active(0)
		, m_partial(nullptr)
	{

	}

	LockFreeBlockAllocator::~LockFreeBlockAllocator()
	{
		intptr_t oldActive = m_active.load();
		intptr_t pDescAddr = oldActive & POINTER_BITS_MASK;
		if (pDescAddr != 0) {
			SDescriptor* pDesc = reinterpret_cast<SDescriptor*>(pDescAddr);
			ReleaseDescriptor(pDesc);
		}

		SDescriptor* pPartial = m_partial.load();
		if (pPartial != nullptr) {
			ReleaseDescriptor(pPartial);
		}

		while (1)
		{
			SDescriptor* p = m_partialList.pop();
			if (p == nullptr)
				break;
			ReleaseDescriptor(p);
		}
	}

	void LockFreeBlockAllocator::Init(unsigned int blocksize, unsigned int sbsize)
	{
		m_blocksize = blocksize;
		m_sbsize = sbsize;
		assert(m_sbsize <= MAX_CREDIT_COUNT);
	}

	void* LockFreeBlockAllocator::Allocate()
	{
		void* ptr = nullptr;
		while (1) {
			ptr = AllocateFromActive();
			if (ptr) break;
			ptr = AllocateFromPartial();
			if (ptr) break;
			ptr = AllocateFromNewSuperBlock();
			if (ptr) break;
		}
		return ptr;
	}


	LockFreeBlockAllocator::SDescriptor* LockFreeBlockAllocator::CreateDescriptor(unsigned state, unsigned count, unsigned avail)
	{
		SDescriptor* desc = new SDescriptor();

		desc->heap = this;
		desc->maxcount = m_sbsize;
		desc->sz = m_blocksize;
		desc->next = 0;

		byte* sb = (byte*)malloc(desc->sz * desc->maxcount);
		// update the next pointers of all blocks
		for (unsigned i = 0; i < desc->maxcount - 1; i++) {
			byte* p = sb + i * desc->sz;
			*reinterpret_cast<unsigned*>(p) = i + 1;
		}
		// last block' next
		//byte* p = sb + (desc->maxcount - 1) * desc->sz;
		//*reinterpret_cast<intptr_t*>(p) = 0;

		desc->sb = sb;

		SAnchor anchor;
		anchor.avail = avail;
		anchor.state = state;
		anchor.tags = 0;
		anchor.count = count;
		desc->anchor.store(anchor);

		//printf("CreateDescriptor: %p\n", desc);

		return desc;
	}

	void LockFreeBlockAllocator::ReleaseDescriptor(SDescriptor* p)
	{
		free(p->sb);
		delete p;
	}

	void* LockFreeBlockAllocator::AllocateFromActive()
	{
		intptr_t oldActive = m_active.load();
		intptr_t newActive;
		intptr_t pDescAddr;
		intptr_t credits;
		do {
			// get pointer
			pDescAddr = oldActive & POINTER_BITS_MASK;
			if (pDescAddr == 0)
				return nullptr;

			// get counter
			credits = oldActive >> ACTIVE_POINTER_BITS;

			// last block in active
			if (credits == 0) {
				newActive = 0;
			}
			else {
				// update counter
				newActive = ((credits - 1) << ACTIVE_POINTER_BITS) | pDescAddr;
			}

		} while (!m_active.compare_exchange_weak(oldActive, newActive));

		SDescriptor* pDesc = reinterpret_cast<SDescriptor*>(pDescAddr);

		SAnchor oldAnchor = pDesc->anchor.load();
		SAnchor newAnchor = oldAnchor;
		byte* addr = nullptr;
		do {
			newAnchor = oldAnchor;
			addr = pDesc->sb + oldAnchor.avail * pDesc->sz;
			newAnchor.avail = *reinterpret_cast<unsigned*>(addr);
			newAnchor.tags++;
			if (credits == 0) {
				if (oldAnchor.count == 0) {
					newAnchor.state = SDescriptor::FULL;
				}
				else {
					newAnchor.count = 0;
				}
			}
		} while (!pDesc->anchor.compare_exchange_weak(oldAnchor, newAnchor));

		//oldAnchor.count -= 1;
		if (credits == 0 && oldAnchor.count > 1) {
			UpdateActive(pDesc, oldAnchor.count - 1);
		}

		*reinterpret_cast<intptr_t*>(addr) = (intptr_t)pDesc;
		return addr + sizeof(intptr_t);
	}

	LockFreeBlockAllocator::SDescriptor* LockFreeBlockAllocator::GetPartial()
	{
		SDescriptor* pOldDesc = m_partial.load();
		do {
			if (pOldDesc == nullptr) {
				return ListGetPartial();
			}
		} while (!m_partial.compare_exchange_weak(pOldDesc, nullptr));
		return pOldDesc;
	}

	LockFreeBlockAllocator::SDescriptor* LockFreeBlockAllocator::ListGetPartial()
	{
		return m_partialList.pop();
	}

	void LockFreeBlockAllocator::PutPartial(SDescriptor* pDesc)
	{
		SDescriptor* pre = m_partial.load();
		while (!m_partial.compare_exchange_weak(pre, pDesc));
		if (pre != nullptr) {
			ListPutPartial(pre);
		}
	}

	void LockFreeBlockAllocator::ListPutPartial(SDescriptor* pDesc)
	{
		m_partialList.push(pDesc);
	}

	void* LockFreeBlockAllocator::AllocateFromPartial()
	{
		SDescriptor* pDesc = GetPartial();
		if (!pDesc)
			return nullptr;

		SAnchor oldAnchor = pDesc->anchor.load();
		SAnchor newAnchor;
		byte* addr = nullptr;
		do {
			newAnchor = oldAnchor;
			addr = pDesc->sb + pDesc->sz * oldAnchor.avail;
			newAnchor.avail = *reinterpret_cast<unsigned*>(addr);
			newAnchor.tags++;
			if (oldAnchor.count == 1) {
				newAnchor.state = SDescriptor::FULL;
			}
			else {
				newAnchor.state = SDescriptor::ACTIVE;
			}
			newAnchor.count = 0;
		} while (!pDesc->anchor.compare_exchange_weak(oldAnchor, newAnchor));

		// regard this desc as active, if its state is still partial
		if (newAnchor.state == SDescriptor::ACTIVE) {
			UpdateActive(pDesc, oldAnchor.count - 1);
		}

		if (addr == nullptr)
			return nullptr;

		*reinterpret_cast<intptr_t*>(addr) = (intptr_t)pDesc;
		return addr + sizeof(intptr_t);
	}

	void* LockFreeBlockAllocator::AllocateFromNewSuperBlock()
	{
		//SDescriptor* pOldDesc = m_partial.load();
		SDescriptor* pNewDesc = CreateDescriptor(SDescriptor::ACTIVE, 0, 1);
		intptr_t newActive = ((intptr_t)(m_sbsize - 2) << ACTIVE_POINTER_BITS) | (intptr_t)pNewDesc;
		intptr_t oldActive = 0;

		if (m_active.compare_exchange_strong(oldActive, newActive)) {
			byte* addr = pNewDesc->sb;
			*reinterpret_cast<intptr_t*>(addr) = (intptr_t)pNewDesc;
			return addr + sizeof(intptr_t);
		}
		else
		{
			// release desc, and return null
			ReleaseDescriptor(pNewDesc);
			return nullptr;
		}
	}

	void LockFreeBlockAllocator::UpdateActive(SDescriptor* pDesc, intptr_t credits)
	{
		intptr_t newActive = ((credits - 1) << ACTIVE_POINTER_BITS) | (intptr_t)pDesc;
		intptr_t oldActive = 0;
		if (m_active.compare_exchange_strong(oldActive, newActive))
			return;

		// if it fails, put pDesc to partial. return the count to it
		SAnchor oldAnchor = pDesc->anchor.load();
		SAnchor newAnchor;
		do {
			newAnchor = oldAnchor;
			newAnchor.count += static_cast<uint16_t>(credits);
			newAnchor.tags += 1;
			newAnchor.state = SDescriptor::PARTIAL;
		} while (pDesc->anchor.compare_exchange_weak(oldAnchor, newAnchor));

		PutPartial(pDesc);
	}



	void LockFreeBlockAllocator::Free(void* p)
	{
		byte* ptr = (byte*)p - sizeof(intptr_t);
		intptr_t pDescAddr = *reinterpret_cast<intptr_t*>(ptr);
		SDescriptor* pDesc = (SDescriptor*)pDescAddr;
		unsigned sbindex = static_cast<unsigned>(ptr - pDesc->sb) / pDesc->sz;

		SAnchor oldAnchor = pDesc->anchor.load();
		SAnchor newAnchor;

		do {
			newAnchor = oldAnchor;
			*reinterpret_cast<unsigned*>(ptr) = oldAnchor.avail;
			newAnchor.avail = sbindex;

			if (oldAnchor.state == SDescriptor::FULL) {
				newAnchor.state = SDescriptor::PARTIAL;
			}

			if (oldAnchor.count == pDesc->maxcount - 1) {
				newAnchor.state = SDescriptor::EMPTY;
			}

			newAnchor.count++;
			newAnchor.tags++;
		} while (!pDesc->anchor.compare_exchange_weak(oldAnchor, newAnchor));

		if (oldAnchor.state == SDescriptor::FULL) {
			PutPartial(pDesc);
		}
	}

}