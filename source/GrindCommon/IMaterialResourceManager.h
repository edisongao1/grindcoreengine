#pragma once

#include "MemoryAllocator.h"

namespace grind
{
	struct SMaterialConstBufferIndex
	{
		int					SizeIndex = 0;
		SChunkBlockIndex	SubIndex;
	};

	struct SMaterialDescriptorSetIndex
	{
		int					DescriptorCount = 0;
		SChunkBlockIndex	SubIndex;
	};

	struct SMaterialResourceId
	{
		//bool							IsCreated = false;
		SMaterialDescriptorSetIndex		DescriptorSetIndex;
		SMaterialConstBufferIndex		ConstBufferIndex;
		uint64_t						Hash = 0;

		bool IsValid() const { return DescriptorSetIndex.DescriptorCount > 0; }
		void ComputeHash()
		{
			// 5bit : DescriptorCount
			// 20bit : DescriptorSetIndex's SubIndex
			// 5bit: SizeIndex
			// 20bit : ConstBufferIndex's SubIndex
			Hash = (uint64_t)DescriptorSetIndex.DescriptorCount << 45 | DescriptorSetIndex.SubIndex.GetHash() << 25
				| (uint64_t)ConstBufferIndex.SizeIndex << 20 | ConstBufferIndex.SubIndex.GetHash();
		}
		
		bool operator==(const SMaterialResourceId& id) const {
			return Hash == id.Hash;
		}
		bool operator!=(const SMaterialResourceId& id) const {
			return Hash != id.Hash;
		}
	};

	class ITexture;

	class IMaterialResourceManager
	{
	public:
		virtual ~IMaterialResourceManager(){}

		virtual SMaterialResourceId	AllocateMaterialResources(
			int cbSize, void* cbData,
			int texCount, ITexture* textures[]
		) = 0;

		virtual void SetConstBufferData(SMaterialResourceId resourceId, void* data) = 0;
	};
}

