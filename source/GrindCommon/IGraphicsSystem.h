#pragma once

#include "GrindMath.h"

namespace grind
{
	struct SCameraViewTransformation
	{
		Vector3f		Position;

		Matrix4x4		ViewMatrix;
		Matrix4x4		InvViewMatrix;
		Matrix4x4		ProjMatrix;
		Matrix4x4		InvProjMatrix;
		Matrix4x4		ViewProjMatrix;
		Matrix4x4		InvViewProjMatrix;
		float			NearZ;
		float			FarZ;


		Frustum			mFrustum;
	};

	class IGraphicsSystem
	{
	public:
		const SCameraViewTransformation& GetSceneCameraTransformation()
		{
			return mSceneCameraTransformation;
		}

	protected:
		SCameraViewTransformation mSceneCameraTransformation;
	};
}

