#pragma once

namespace grind
{
	template<typename T>
	class ThreadLocalPointerStack
	{
	public:
		ThreadLocalPointerStack() : m_head(0) {

		}

		void push(T* p) 
		{
			p->next = m_head;
			m_head = (intptr_t)p;
		}

		T* pop() 
		{
			if (m_head == 0) {
				return nullptr;
			}
			T* p = reinterpret_cast<T*>(m_head);
			m_head = p->next;
		}

	public:
		intptr_t		m_head;
	};
}
