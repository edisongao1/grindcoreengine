#pragma once

namespace grind
{
	template<typename T>
	struct TContainerItemWithNextPointer
	{
		T* pObject;
		intptr_t							next;
	};

	struct SChunkBlockIndex
	{
		int			BufferIndex = 0;
		int			BlockIndex = 0;

		uint64_t GetHash() const {
			return ((uint64_t)BufferIndex << 10) | (uint64_t)BlockIndex;
		}

		SChunkBlockIndex() {}
		SChunkBlockIndex(int bufferIndex, int blockIndex)
			:BufferIndex(bufferIndex), BlockIndex(blockIndex) {}
	};



	template<int BlockCount, typename = std::enable_if<(BlockCount <= 1024)>>
	class TBlockArray
	{
	public:
		static int GetBlockCount() {
			return BlockCount;
		}

		int AllocateBlock()
		{
			if (IsFull())
				return -1;

			int i = mCurrentAllocateIndex;
			while (mBlockAllocated[i]) {
				i = (i + 1) % BlockCount;
			}

			mBlockAllocated[i] = true;
			mCurrentAllocateIndex = (i + 1) % BlockCount;
			mAllocateBlockCount += 1;
			return i;
		}

		void ReleaseBlock(int i)
		{
			assert(mBlockAllocated[i]);

			mBlockAllocated[i] = false;
			if (mBlockAllocated[mCurrentAllocateIndex]) {
				mCurrentAllocateIndex = i;
			}
			mAllocateBlockCount -= 1;
		}

		bool IsFull() const
		{
			return mAllocateBlockCount == BlockCount;
		}

	protected:
		bool						mBlockAllocated[BlockCount] = { false };
		int							mAllocateBlockCount = 0;
		int							mCurrentAllocateIndex = 0;
	};

	template<typename TChunkBuffer, int ChunkCount,
		typename = std::enable_if<(ChunkCount <= 1024), void>>
		class TBlockBufferAllocator
	{
	public:
		using TChunkBufferPtr = std::unique_ptr<TChunkBuffer>;
		TBlockBufferAllocator()
		{

		}

		virtual ~TBlockBufferAllocator() {}

		void Release(SChunkBlockIndex index)
		{
			assert(index.BufferIndex < mValidChunkCount);
			mChunks[index.BufferIndex]->ReleaseBlock(index.BlockIndex);
			mCurrentChunkIndex = index.BufferIndex;
		}

		SChunkBlockIndex Allocate()
		{
			if (mValidChunkCount == 0)
			{
				mChunks[0].reset(CreateChunkBuffer());
				mValidChunkCount = 1;
			}

			int i = mCurrentChunkIndex;
			while (mChunks[i]->IsFull()) {
				i = (i + 1) % mValidChunkCount;
				if (i == mCurrentChunkIndex)
					break;
			}

			// 如果现有的Chunk中已经找不出空位了, 则需要创建新的Chunk
			if (mChunks[i]->IsFull()) {
				i = mValidChunkCount;
				mChunks[i].reset(CreateChunkBuffer());
				mValidChunkCount += 1;
			}

			int blockIndex = mChunks[i]->AllocateBlock();
			// 如果这个Chunk还没满，则把分配位置指向
			if (!mChunks[i]->IsFull()) {
				mCurrentChunkIndex = i;
			}

			return SChunkBlockIndex(i, blockIndex);
		}

		virtual TChunkBuffer* CreateChunkBuffer() = 0;

	protected:
		TChunkBufferPtr		mChunks[ChunkCount];
		int					mCurrentChunkIndex = 0;
		int					mValidChunkCount = 0;
	};

	template<typename T, int Size>
	class TSequentialFixedQueue
	{
	public:
		static int GetSize() {
			return Size;
		}

		void Reset()
		{
			mHead = 0;
		}

		void Push(const T& element) 
		{
			//assert(mCurrentIndex < Size);
			//mElements[mCurrentIndex++] = element;
		}

		void Pop();

		int mHead = 0;
		int mTail = 0;
		T mElements[Size];
		TSequentialFixedQueue* pNext;
	};
}

