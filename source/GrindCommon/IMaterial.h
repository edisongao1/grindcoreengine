#pragma once
#include "GrindEnums.h"
#include "GrindMath.h"
#include "StringParseUtils.h"
#include "Common.h"

#include "Hash/CRC.h"
#include "IMaterialResourceManager.h"
#include "ITexture.h"

namespace grind
{
	//enum class EVariableType 
	//{
	//	Float,
	//	Float2,
	//	Float3,
	//	Float4,
	//	Int,
	//	Int2,
	//	Int3,
	//	Int4,
	//	Color4,
	//	Color3,
	//};

	//template<typename T>
	//inline EVariableType GetVariableType();

	//template<> inline EVariableType GetVariableType<float>()		{ return EVariableType::Float; }
	//template<> inline EVariableType GetVariableType<Vector2f>()	{ return EVariableType::Float2; }
	//template<> inline EVariableType GetVariableType<Vector3f>()	{ return EVariableType::Float3; }
	//template<> inline EVariableType GetVariableType<Vector4f>()	{ return EVariableType::Float4; }
	//template<> inline EVariableType GetVariableType<int>()			{ return EVariableType::Int; }
	//template<> inline EVariableType GetVariableType<Vector2i>() { return EVariableType::Int2; }
	//template<> inline EVariableType GetVariableType<Vector3i>() { return EVariableType::Int3; }
	//template<> inline EVariableType GetVariableType<Vector4i>() { return EVariableType::Int4; }

	struct EShaderRunningMacro
	{
		enum : uint64_t {
			ReceiveShadow = (1ULL << 56),
			ReceiveGI = (1ULL << 57),
			Tangent = (1ULL << 58),
		};
	};


	// 2 bits
	enum class EDepthTest : uint8_t
	{
		Disabled = 0,
		Less,
		LessEqual,
		Equal,
		Greater,
		NotEqual,
		GreaterEqual,
		Never,
		Always,
	};

	// 2 bits
	enum class EBlendState : uint8_t
	{
		Disabled = 0,
		SrcAlphaAdd = 1,
		ColorAdd = 2,
	};

	enum class ECullMode : uint8_t
	{
		Off,
		Back,
		Front,
	};

	inline EDepthTest StringToDepthTest(const char* str) {
		__RETURN_IF_STRING_MATCH_ENUM(str, EDepthTest, Disabled);
		__RETURN_IF_STRING_MATCH_ENUM(str, EDepthTest, Less);
		__RETURN_IF_STRING_MATCH_ENUM(str, EDepthTest, LessEqual);
		__RETURN_IF_STRING_MATCH_ENUM(str, EDepthTest, Equal);
		__RETURN_IF_STRING_MATCH_ENUM(str, EDepthTest, Greater);
		__RETURN_IF_STRING_MATCH_ENUM(str, EDepthTest, NotEqual);
		__RETURN_IF_STRING_MATCH_ENUM(str, EDepthTest, GreaterEqual);
		__RETURN_IF_STRING_MATCH_ENUM(str, EDepthTest, Never);
		__RETURN_IF_STRING_MATCH_ENUM(str, EDepthTest, Always);
		return EDepthTest::Disabled;
	}

	inline EBlendState StringToBlendState(const char* str) {
		__RETURN_IF_STRING_MATCH_ENUM(str, EBlendState, Disabled);
		__RETURN_IF_STRING_MATCH_ENUM(str, EBlendState, SrcAlphaAdd);
		__RETURN_IF_STRING_MATCH_ENUM(str, EBlendState, ColorAdd);
		return EBlendState::Disabled;
	}

	inline ECullMode StringToCullMode(const char* str) {
		__RETURN_IF_STRING_MATCH_ENUM(str, ECullMode, Off);
		__RETURN_IF_STRING_MATCH_ENUM(str, ECullMode, Back);
		__RETURN_IF_STRING_MATCH_ENUM(str, ECullMode, Front);
		return ECullMode::Back;
	}

	struct SRenderStatesDesc
	{
		// 判断是否是常见状态
		// 常见状态使用数组进行快速索引
		// 非常见状态使用map索引
		bool					IsCommon = true;	
		uint8_t					CommonHash = 0;
		uint32_t				FullHash = 0;

		ECullMode				CullBack = ECullMode::Back;
		EDepthTest				DepthTest = EDepthTest::LessEqual;
		bool					DepthWriteEnabled = true;
		EBlendState				BlendState = EBlendState::Disabled;
		bool					WireFrame = false;
		uint8_t					_pad[3] = { 0 };

		SRenderStatesDesc()
		{
			ComputeHash();
		}

		void ComputeHash()
		{
			IsCommon = true;
			if ((uint8_t)DepthTest > 0x03 ||
				(uint8_t)BlendState > 1 ||
				(uint8_t)CullBack > 1)
			{
				IsCommon = false;
			}

			if (IsCommon)
			{
				CommonHash = ((uint8_t)CullBack & 0x01) |
					((uint8_t)DepthTest & 0x01) << 1 |
					((uint8_t)DepthWriteEnabled & 0x01) << 3 |
					((uint8_t)BlendState & 0x01) << 4 |
					((uint8_t)WireFrame & 0x01) << 5;
			}
			else
			{
				FullHash = CRC::Calculate(&this->CullBack, 
					sizeof(SRenderStatesDesc) - sizeof(uint64_t),
					CRC::CRC_32());
			}
		}

		void SetAttributeFromString(const char* szName, const char* szValue) 
		{
			if (_stricmp(szName, "CullMode") == 0) {
				CullBack = StringToCullMode(szValue);
			}
			else if (_stricmp(szName, "DepthTest") == 0) {
				DepthTest = StringToDepthTest(szValue);
			}
			else if (_stricmp(szName, "DepthWriteEnabled") == 0) {
				DepthWriteEnabled = StringParseUtils::Parse<bool>(szValue);
			}
			else if (_stricmp(szName, "BlendState") == 0) {
				BlendState = StringToBlendState(szValue);
			}
			else if (_stricmp(szName, "WireFrame") == 0) {
				WireFrame = StringParseUtils::Parse<bool>(szValue);
			}
		}
	};


	class ITechnique;


	//struct STechniqueData;
	class ITechnique;

	union UMaterialVariableValue
	{
		float			vFloat;
		Vector2f		vFloat2;
		Vector3f		vFloat3;
		Vector4f		vFloat4;
		int				vInt;
		Vector2i		vInt2;
		Vector3i		vInt3;
		Vector4i		vInt4;
		Vector3f		vColor3;
		Vector4f		vColor4;
		UMaterialVariableValue() {}
	};

	struct SMaterialVariable
	{
		EVariableType					Type;
		UMaterialVariableValue			Value;

		SMaterialVariable() = default;
		SMaterialVariable(const SMaterialVariable& var)
			:Type(var.Type)
		{
			*this = var;
		}

		SMaterialVariable& operator=(const SMaterialVariable& var) 
		{
			Type = var.Type;
			switch (var.Type)
			{
			case EVariableType::Float:
				Value.vFloat = var.Value.vFloat;
				break;
			case EVariableType::Float2:
				Value.vFloat2 = var.Value.vFloat2;
				break;
			case EVariableType::Float3:
				Value.vFloat3 = var.Value.vFloat3;
				break;
			case EVariableType::Float4:
				Value.vFloat4 = var.Value.vFloat4;
				break;
			case EVariableType::Int:
				Value.vInt = var.Value.vInt;
				break;
			case EVariableType::Int2:
				Value.vInt2 = var.Value.vInt2;
				break;
			case EVariableType::Int3:
				Value.vInt3 = var.Value.vInt3;
				break;
			case EVariableType::Int4:
				Value.vInt4 = var.Value.vInt4;
				break;
			case EVariableType::Color4:
				Value.vColor4 = var.Value.vColor4;
				break;
			case EVariableType::Color3:
				Value.vColor3 = var.Value.vColor3;
				break;
			default:
				break;
			}
			return *this;
		}

		template<typename T> T GetValue() const;
		template<> float GetValue() const { assert(Type == EVariableType::Float); return Value.vFloat; }
		template<> Vector2f GetValue() const { assert(Type == EVariableType::Float2); return Value.vFloat2; }
		template<> Vector3f GetValue() const { assert(Type == EVariableType::Float3); return Value.vFloat3; }
		template<> Vector4f GetValue() const { assert(Type == EVariableType::Float4); return Value.vFloat4; }
		template<> int GetValue() const { assert(Type == EVariableType::Int); return Value.vInt; }
		template<> Vector2i GetValue() const { assert(Type == EVariableType::Int2); return Value.vInt2; }
		template<> Vector3i GetValue() const { assert(Type == EVariableType::Int3); return Value.vInt3; }
		template<> Vector4i GetValue() const { assert(Type == EVariableType::Int4); return Value.vInt4; }
		
		void SetValueFromString(const char* str)
		{
			switch (Type)
			{
			case EVariableType::Float:
				Value.vFloat = StringParseUtils::Parse<float>(str);
				break;
			case EVariableType::Float2:
				Value.vFloat2 = StringParseUtils::Parse<Vector2f>(str);
				break;
			case grind::EVariableType::Float3:
				Value.vFloat3 = StringParseUtils::Parse<Vector3f>(str);
				break;
			case grind::EVariableType::Float4:
				Value.vFloat4 = StringParseUtils::Parse<Vector4f>(str);
				break;
			case grind::EVariableType::Int:
				Value.vInt = StringParseUtils::Parse<int>(str);
				break;
			case grind::EVariableType::Int2:
				Value.vInt2 = StringParseUtils::Parse<Vector2i>(str);
				break;
			case grind::EVariableType::Int3:
				Value.vInt3 = StringParseUtils::Parse<Vector3i>(str);
				break;
			case grind::EVariableType::Int4:
				Value.vInt4 = StringParseUtils::Parse<Vector4i>(str);
				break;
			default:
				break;
			}
		}
	};

	//struct SMaterialTemplate;
	struct SMaterialRenderPass;
	class IRenderPass;

	enum class EMaterialResourceState {
		NotCreated, // 纹理和变量等未准备好
		//ReadyButNotCreated, // 纹理和变量等已准备就绪，但尚未创建纹理资源 
		Creating, // 创建中
		Created, // 已创建
		CreateFailed, // 创建失败
	};

	class IMaterial
	{
	public:
		enum { MAX_TEXTURE_COUNT = 15 };
		IMaterial(const std::string& name, ITechnique* pTech)
			:mName(name), mTechnique(pTech)
		{
		}

		virtual ~IMaterial(){}
		const std::string& GetName() const { return mName; }

		virtual IRenderPass* GetRenderPass(ETechniquePassStage stage, uint64_t mask) = 0;

		virtual void FlushParams() = 0;

		template<typename T>
		inline void SetParam(const std::string& name, const T& x);

		template<>
		inline void SetParam(const std::string& name, const SMaterialVariable& val);

		template<typename T>
		T GetParam(const std::string& name) const;

		const std::vector<std::string>& GetMaterialShaderFlags() const { return mShaderFlags; }

		inline ITechnique* GetTechnique() { return mTechnique; }

		SMaterialRenderPass* GetMaterialRenderPass() { return mMaterialRenderPass;}

		const SMaterialResourceId GetMaterialResourceId() { return mGPUResourceId; }

		inline uint32_t GetConstBufferSize() const;

		inline int GetTextureCount() const;

		EMaterialResourceState GetMaterialResourceState() const { return mMaterialResourceState; }

		virtual void TryCreateMaterialResource() = 0;

		bool IsCreated() const { return mMaterialResourceState == EMaterialResourceState::Created; }

		const SRenderStatesDesc& GetRenderStateDesc(ETechniquePassStage stage) const { return mRenderStateDescs[stage]; }

	protected:
		std::string				mName;
		ITechnique*				mTechnique = nullptr;
		SMaterialRenderPass*	mMaterialRenderPass = nullptr;
		SRenderStatesDesc		mRenderStateDescs[ePass_Count];
		std::vector<std::string>	mShaderFlags;
		uint64_t					mShaderFlagsMask = 0;
		byte*						mMaterialBufferCPU = nullptr;
		ITexture*					mTextures[MAX_TEXTURE_COUNT] = { 0 };
		SMaterialResourceId			mGPUResourceId;
		EMaterialResourceState		mMaterialResourceState = EMaterialResourceState::NotCreated;
	};

	//////////////////////////////////////////////////////////////////////////
	// Material Templates



	struct SMaterialVariableInfo
	{
		std::string				Name;
		std::string				ShaderVariableName;
		EVariableType	Type;
		//MaterialVariableValue	DefaultValue;
		SMaterialVariable		DefaultValue;

		SMaterialVariableInfo() = default;
		SMaterialVariableInfo(const SMaterialVariableInfo& info)
		:Name(info.Name)
			,ShaderVariableName(info.ShaderVariableName)
			,Type(info.Type)
			,DefaultValue(info.DefaultValue)
		{

		}

	};

	struct SMaterialFlagInfo
	{
		std::string				DisplayString;
		std::string				MacroString;
		bool					Default;
	};

	// encapsulate all passes with the same techinque and material flags
	struct SMaterialRenderPass
	{
	public:
		struct SRenderPassMapItem
		{
			IRenderPass*			RenderPass = nullptr;

#ifdef _CPP20_ENABLED
			std::atomic_flag		IsConstructing;
#else
			std::atomic_flag		IsConstructing = ATOMIC_FLAG_INIT;
#endif
		};

		//SMaterialTemplate*			mMatTemplate = nullptr;
		SRenderPassMapItem			mRenderPassMaps[ePass_Count][1 << 8];

	public:
		SMaterialRenderPass()
		{
			memset(mRenderPassMaps, 0, sizeof(mRenderPassMaps));
		}

		IRenderPass* GetRenderPass(ETechniquePassStage stage, uint8_t runningMask)
		{
			auto pRenderPass = mRenderPassMaps[stage][runningMask].RenderPass;
			return pRenderPass;
		}

		void SetRenderPass(ETechniquePassStage stage, uint8_t runningMask, IRenderPass* pPass)
		{
			assert(mRenderPassMaps[stage][runningMask].RenderPass == nullptr);
			mRenderPassMaps[stage][runningMask].RenderPass = pPass;
			mRenderPassMaps[stage][runningMask].IsConstructing.clear();
		}

		bool CanConstructRenderPassNow(ETechniquePassStage stage, uint8_t runningMask)
		{
			return mRenderPassMaps[stage][runningMask].IsConstructing.test_and_set() == false;
		}
	};

	struct SMaterialConstBufferVariableMeta
	{
		std::string				Name;
		uint32_t				Offset;
		EVariableType	DataType;
	};

	struct SMaterialConstBufferLayout
	{

	};

	using TShaderMacroList = std::vector<std::string>;

	class IRenderResourceFactory;
	//struct SShaderMacroMask
	//{
	//	char			MacroText[48];
	//	uint8_t			ShaderTypes;
	//	uint64_t		Mask;
	//};

	struct STechniquePassShaderData
	{
		std::string		EntryPoint;
		bool			Valid = false;
		//uint64_t		MacrosMask = 0;
	};

	struct SRenderPassMeta
	{
		std::string					TechinqueName;
		ETechniquePassStage			Stage;

		std::string					ShaderFileName;
		bool						Valid = false;
		STechniquePassShaderData	Shaders[SHADER_TYPE_COUNT];
		std::vector<std::string>	Defines;


		bool						bMetaInfoExtracted = false;
		enum class EObjectConstBuffer			ObjectConstBufferType;
		enum class  EFrameConstBuffer			FrameConstBufferType;

		SRenderStatesDesc						mRenderStateDesc;
		std::map<std::string, std::string>		mUndeterminedRenderStates;
	};

	class ITechnique
	{
	public:
		struct SMaterialBufferVariableMeta
		{
			SMaterialVariable		Variable;
			int						Offset;
		};
		struct SMaterialTextureMeta
		{
			ETextureType			Type;
			int						Slot;
			std::string				DefaultPath;
		};
	public:
		virtual ~ITechnique(){}

		const std::string&	GetName() const { return mName; }
		EFVF				GetFVF() const { return mFVF; }

		virtual IRenderPass* CreateRenderPass(ETechniquePassStage stage, const std::vector<std::string>& defines) = 0;
		virtual SMaterialRenderPass* GetMaterialRenderPass(uint64_t mask) = 0;

		const SMaterialBufferVariableMeta* GetMaterialVariableMeta(const std::string& name) const
		{
			auto it = mMaterialVariableMetaMap.find(name);
			if (it == mMaterialVariableMetaMap.end())
				return nullptr;
			return &(it->second);
		}

		const SMaterialTextureMeta* GetMaterialTextureMeta(const std::string& name) const
		{
			auto it = mMaterialTextureMetaMap.find(name);
			if (it == mMaterialTextureMetaMap.end())
				return nullptr;
			return &(it->second);
		}

		uint32_t GetMaterialBufferSize() const { return mMaterialBufferSize; }
		int GetMaterialTextureCount() const { return (int)mMaterialTextureMetaMap.size(); }

		const SRenderPassMeta& GetRenderPassMeta(int stage) const { return mPassMetas[stage]; }

	protected:
		std::string			mName;
		EFVF				mFVF = EFVF::Object3D;

		std::unordered_map<std::string, SMaterialBufferVariableMeta>	mMaterialVariableMetaMap;
		uint32_t			mMaterialBufferSize = 0;

		std::unordered_map<std::string, SMaterialTextureMeta>			mMaterialTextureMetaMap;
		SRenderPassMeta		mPassMetas[ePass_Count];
	};

	template<typename T>
	void IMaterial::SetParam(const std::string& name, const T& x)
	{
		auto pVarMeta = mTechnique->GetMaterialVariableMeta(name);
		if (!pVarMeta)
			return;
		assert(pVarMeta->Variable.Type == GetVariableType<T>());
		*reinterpret_cast<T*>(mMaterialBufferCPU + pVarMeta->Offset) = x;
	}

	template<>
	void IMaterial::SetParam(const std::string& name, const SMaterialVariable& val)
	{
		switch (val.Type)
		{
		case EVariableType::Float: SetParam(name, val.Value.vFloat); break;
		case EVariableType::Float2: SetParam(name, val.Value.vFloat2); break;
		case EVariableType::Float3: SetParam(name, val.Value.vFloat3); break;
		case EVariableType::Float4: SetParam(name, val.Value.vFloat4); break;
		case EVariableType::Int:  SetParam(name, val.Value.vInt); break;
		case EVariableType::Int2: SetParam(name, val.Value.vInt2); break;
		case EVariableType::Int3: SetParam(name, val.Value.vInt3); break;
		case EVariableType::Int4: SetParam(name, val.Value.vInt4); break;
		default: break;
		}
	}

	template<typename T>
	T IMaterial::GetParam(const std::string& name) const
	{
		auto pVarMeta = mTechnique->GetMaterialVariableMeta(name);
		if (!pVarMeta)
			return T(0);
		assert(pVarMeta->Variable.Type == GetVariableType<T>());
		return *reinterpret_cast<T*>(mMaterialBufferCPU + pVarMeta->Offset);
	}

	uint32_t IMaterial::GetConstBufferSize() const
	{
		return mTechnique->GetMaterialBufferSize();
	}

	int IMaterial::GetTextureCount() const
	{
		return mTechnique->GetMaterialTextureCount();
	}

	//////////////////////////////////////////////////////////////////////////
	// Material Manager

	class IMaterialResourceManager;

	class IMaterialManager
	{
	public:
		virtual ~IMaterialManager(){}
		virtual IMaterialResourceManager* GetMaterialResourceManager() = 0;
		virtual IMaterial* GetMaterial(const std::string& name) = 0;
		virtual void InitializeAfterGraphicsCreated() = 0;
		virtual IMaterial* CopyMaterial(IMaterial* pMaterial, const char* pName = 0) = 0;
		//virtual SMaterialTemplate* GetMaterialTemplate(const std::string& name) = 0;


	};
}
