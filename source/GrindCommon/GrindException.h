#pragma once

#include <string>
#include <exception>

namespace grind
{
	class CException : public std::exception {
	public:
		CException() {}
		CException(const std::string& msg) throw() : message(msg) {}

		virtual ~CException() throw() {}

		const std::string& str() const throw() { return message; }

		virtual const char* what() const throw() { return message.c_str(); }

	private:
		std::string message;
	};


}