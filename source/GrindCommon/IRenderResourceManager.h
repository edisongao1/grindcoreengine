#pragma once

#include "GrindEnums.h"
#include "IMaterial.h"
#include "IRenderPass.h"
#include "IMesh.h"

namespace grind
{

	class ITechnique;
	//using TShaderMacroMaskMap = std::unordered_map<std::string, SShaderMacroMask>;
	using TShaderMacroMaskMap = std::unordered_map<std::string, uint64_t>;

	class IRenderResourceManager
	{
	public:
		virtual ~IRenderResourceManager() {  }
		virtual void Initialize() = 0;
		virtual void PreloadResources() = 0;
		virtual void GetAllTechniques(std::vector<ITechnique*>& vecTechniques) const = 0;

		virtual ITechnique* GetTechniqueByName(const std::string& name) = 0;
		void _SetRenderResourceFactory(IRenderResourceFactory* p) {
			mRenderResourceFactory = p;
		}

		IRenderResourceFactory* GetRenderResourceFactory()
		{
			return mRenderResourceFactory;
		}

		virtual IMaterialResourceManager* GetMaterialResourceManager() = 0;


		std::vector<std::string> FilterShaderMacroStrings(const std::vector<std::string>& defines, uint64_t validateMask)
		{
			std::vector<std::string> filteredDefines;
			for (size_t i = 0; i < defines.size(); i++) {
				auto it = mShaderMacroMaskMap.find(defines[i]);
				if (it != mShaderMacroMaskMap.end()) {
					uint64_t mask = it->second;
					if (mask & validateMask) {
						filteredDefines.push_back(defines[i]);
					}
				}
			}
			return filteredDefines;
		}

		inline uint64_t  GetShaderMacroMask(const std::vector<std::string>& defines) {
			uint64_t mask = 0;
			for (size_t i = 0; i < defines.size(); i++) {
				auto it = mShaderMacroMaskMap.find(defines[i]);
				if (it != mShaderMacroMaskMap.end()) {
					mask |= it->second;
				}
			}
			return mask;
		}

		// 根据mask码反推出Shader的宏列表
		inline void GetRunningMacroStringsFromMask(uint64_t mask, std::vector<std::string>& defines)
		{
			const static std::string RECEIVE_SHADOW("_RT_RECEIVE_SHADOW");
			const static std::string RECEIVE_GI("_RT_RECEIVE_GI");
			const static std::string TANGENT("_RT_TANGENT");

			if (mask & EShaderRunningMacro::ReceiveShadow) {
				defines.push_back(RECEIVE_SHADOW);
			}
			if (mask & EShaderRunningMacro::ReceiveGI) {
				defines.push_back(RECEIVE_GI);
			}
			if (mask & EShaderRunningMacro::Tangent) {
				defines.push_back(TANGENT);
			}
		}

		IMaterialManager* GetMaterialManager() { return mMaterialManager.get(); }
		ITextureManager* GetTextureManager() { return mTextureManager.get(); }
		IMeshManager* GetMeshManager() { return mMeshManager.get(); }

		virtual uint64_t GetValidateShaderMacrosMask(const std::string& shaderName) const = 0;

	protected:
		TShaderMacroMaskMap				mShaderMacroMaskMap;
		IRenderResourceFactory*			mRenderResourceFactory = nullptr;
		std::unique_ptr<ITextureManager>	mTextureManager;
		std::unique_ptr<IMaterialManager>	mMaterialManager;
		std::unique_ptr<IMeshManager>		mMeshManager;
	};



}


