#pragma once

#include "GrindECS.h"
#include "tinyxml2.h"

namespace grind
{
	class IEngineCore;
	class ISceneLoadListener;

	class IGameFramework
	{
	public:

		using TCreateFunction = IGameFramework * (*)();

		// event
		virtual void OnCreate() = 0;
		virtual void OnBeforeInitialize() = 0;
		virtual void OnAfterInitialize() = 0;
		virtual void OnFrameUpdate(float dt) = 0;
		virtual void OnExitGame() = 0;
		virtual void OnDestroy() = 0;
		virtual void OnAfterCreateWindow() = 0;
		virtual void OnSceneLoaded(ISceneManager* pSceneManager) = 0;
		
		void Release() 
		{
			delete this;
		}

		// create listener
		virtual ISceneLoadListener* GetSceneLoadListener() = 0;
	};


}