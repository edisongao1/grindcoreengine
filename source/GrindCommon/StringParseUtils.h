#pragma once
#include <vector>
#include <string>
#include <algorithm>
#include "GrindMath.h"
#include <Windows.h>

namespace grind
{
	namespace StringParseUtils
	{
		inline void SplitString(const char* s, char separator, std::vector<std::string>& results)
		{
			if (s == nullptr)
				return;

			const char* p1 = s;
			const char* p2 = nullptr;

			while (*p1 != '\0') {
				p2 = p1;
				while (*p2 != separator && *p2 != '\0') p2++;
				std::string str(p1, p2 - p1);
				results.push_back(std::move(str));
				if (*p2 == '\0')
					break;
				p1 = p2 + 1;
			}
		}

		inline void SplitStringIgnoreSpace(const char* s, char separator, std::vector<std::string>& results)
		{
			if (s == nullptr)
				return;

			const char* p1 = s;
			const char* p2 = nullptr;

			while (*p1 != '\0') {
				while (*p1 == ' ' || *p1 == separator) p1++;
				if (*p1 == '\0')
					break;
				p2 = p1;
				while (*p2 != ' ' && *p2 != separator && *p2 != '\0') p2++;
				std::string str(p1, p2 - p1);
				results.push_back(std::move(str));
				p1 = p2;
			}
		}

		// 返回数组的有效元素个数
		template<typename T>
		inline int ParseArray(const char* str, T arr[], int maxNum);

		

		// 返回float数组的有效元素个数
		template<>
		inline int ParseArray<float>(const char* str, float arr[], int maxNum)
		{
			if (!str)
				return 0;

			char buff[128] = { 0 };
			const char* p = str;
			int i = 0, j = 0;
			while (*p != '\0') {
				if ((*p >= '0' && *p <= '9') || *p == '.' || *p == '-') {
					buff[i++] = *p;
				}
				else if (*p == ',') {
					buff[i] = '\0';
					arr[j++] = (float)atof(buff);
					i = 0;
					if (j == maxNum)
						return maxNum;
				}
				p += 1;
			}

			if (i != 0) {
				buff[i] = '\0';
				arr[j++] = (float)atof(buff);
			}
			return j;
		}


		// 返回float数组的有效元素个数
		template<>
		inline int ParseArray<int>(const char* str, int arr[], int maxNum)
		{
			if (!str)
				return 0;

			char buff[128] = { 0 };
			const char* p = str;
			int i = 0, j = 0;
			while (*p != '\0') {
				if ((*p >= '0' && *p <= '9') || *p == '.' || *p == '-') {
					buff[i++] = *p;
				}
				else if (*p == ',') {
					buff[i] = '\0';
					arr[j++] = atoi(buff);
					i = 0;
					if (j == maxNum)
						return maxNum;
				}
				p += 1;
			}

			if (i != 0) {
				buff[i] = '\0';
				arr[j++] = atoi(buff);
			}
			return j;
		}

		template<typename T>
		inline T Parse(const char* str);

		template<>
		inline const char* Parse<const char*>(const char* str)
		{
			return str;
		}

		template<>
		inline Vector2f Parse<Vector2f>(const char* str)
		{
			Vector2f v(0);
			if (!str)
				return v;
			float arr[2] = { 0 };
			ParseArray<float>(str, arr, 2);
			v.x = arr[0]; v.y = arr[1];
			return v;
		}

		template<>
		inline Vector3f Parse<Vector3f>(const char* str)
		{
			Vector3f v(0);
			if (!str)
				return v;

			float arr[3] = { 0 };
			ParseArray<float>(str, arr, 3);
			v.x = arr[0]; v.y = arr[1]; v.z = arr[2];
			return v;
		}

		template<>
		inline Vector4f Parse<Vector4f>(const char* str)
		{
			Vector4f v(0);
			if (!str)
				return v;
			float arr[4] = { 0 };
			ParseArray<float>(str, arr, 4);
			//v.set(arr[0], arr[1], arr[2], arr[3]);
			v.x = arr[0]; v.y = arr[1]; v.z = arr[2]; v.w = arr[3];
			return v;
		}

		template<>
		inline float Parse<float>(const char* str)
		{
			if (!str)
				return 0;
			return (float)atof(str);
		}

		template<>
		inline int Parse<int>(const char* str)
		{
			if (!str)
				return 0;
			return atoi(str);
		}

		template<>
		inline Vector2i Parse<Vector2i>(const char* str)
		{
			Vector2i v(0);
			if (!str)
				return v;
			int arr[2] = { 0 };
			ParseArray<int>(str, arr, 2);
			//v.set(arr[0], arr[1]);
			v.x = arr[0]; v.y = arr[1];
			return v;
		}

		template<>
		inline Vector3i Parse<Vector3i>(const char* str)
		{
			Vector3i v(0);
			if (!str)
				return v;
			int arr[3] = { 0 };
			ParseArray<int>(str, arr, 3);
			//v.set(arr[0], arr[1], arr[2]);
			v.x = arr[0]; v.y = arr[1]; v.z = arr[2];
			return v;
		}

		template<>
		inline Vector4i Parse<Vector4i>(const char* str)
		{
			Vector4i v(0);
			if (!str)
				return v;
			int arr[4] = { 0 };
			ParseArray<int>(str, arr, 4);
			//v.set(arr[0], arr[1], arr[2], arr[3]);
			v.x = arr[0]; v.y = arr[1]; v.z = arr[2]; v.w = arr[3];
			return v;
		}

		template<>
		inline bool Parse<bool>(const char* str)
		{
			if (_stricmp(str, "true") == 0 || _stricmp(str, "1") == 0)
				return true;
			return false;
		}

		inline std::string RemoveFileExtension(const std::string& filename)
		{
			size_t pos = filename.find_first_of('.');
			if (pos == std::string::npos)
				return filename;
			return filename.substr(0, pos);
		}

		inline std::wstring AnsiToWString(const std::string& str)
		{
			WCHAR buffer[512];
			MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, buffer, 512);
			return std::wstring(buffer);
		}

		inline std::wstring AnsiToWString(const char* str)
		{
			WCHAR buffer[512];
			MultiByteToWideChar(CP_ACP, 0, str, -1, buffer, 512);
			return std::wstring(buffer);
		}

		
	}
}

