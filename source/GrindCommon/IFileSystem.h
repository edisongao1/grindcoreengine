#pragma once
#ifdef _WIN32
#include "Windows.h"
#endif

namespace grind
{

#ifdef _WIN32
	struct handle_closer { void operator()(HANDLE h) { if (h) CloseHandle(h); } };

	using ScopedHandle = std::unique_ptr<void, handle_closer>;

	inline HANDLE safe_handle(HANDLE h) { return (h == INVALID_HANDLE_VALUE) ? 0 : h; }
#endif

	class IFileSystem
	{
	public:
		virtual ~IFileSystem(){}
		virtual FILE* OpenFile(const char* filename, const char* mode) = 0;
		virtual void CloseFile(FILE* fp) = 0;
		virtual void GetFileFullPath(const char* filename, char szFullPath[]) = 0;
		virtual const char* GetBasePath() const = 0;

		virtual void IterateFilesInDirectory(const char* dir, const char* pattern, std::function<void(const char*)> cb) = 0;
		virtual void IterateFilesInDirectory(const char* dir, const char* suffix, bool bSearchSubDir, std::function<void(const char*)> cb) = 0;

	};
}

