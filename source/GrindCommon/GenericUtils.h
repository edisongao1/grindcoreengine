#pragma once
#include "Math/Common.h"
#include <string.h>

namespace grind
{
	namespace Generic
	{
		template<typename T>
		bool IsEqual(T v1, T v2)
		{
			return v1 == v2;
		}

		template<>
		bool IsEqual<const char*>(const char* s1, const char* s2)
		{
			return strcmp(s1, s2) == 0;
		}

		template<>
		bool IsEqual<float>(float v1, float v2)
		{
			return glm::detail::compute_equal<float, std::numeric_limits<float>::is_iec559>::call(v1, v2);
		}

		template<>
		bool IsEqual<double>(double v1, double v2)
		{
			return glm::detail::compute_equal<double, std::numeric_limits<double>::is_iec559>::call(v1, v2);
		}

		// if x is any of those values given in the parameter list
		template<typename T, typename...Args>
		inline bool IsAnyOf(T v, Args...args);

		template<typename T>
		inline bool IsAnyOf(T v)
		{
			return false;
		}

		template<typename T, typename U, typename...Args>
		inline bool IsAnyOf(T v, U arg1, Args... args)
		{
			return IsEqual<T>(v, (T)arg1) || IsAnyOf<T, Args...>(v, args...);
		}
	}

}
