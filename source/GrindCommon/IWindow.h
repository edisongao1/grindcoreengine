#ifndef __IWINDOW_H__
#define __IWINDOW_H__

#include "GrindMath.h"

namespace grind
{
	enum EWindowStyle
	{
		eWS_NONE = 0,
		eWS_FULLSCREEN = 1 << 0,
		eWS_FULLRESOLUTION = 1 << 1
	};

	struct SWindowSettings
	{
		unsigned			Width;
		unsigned			Height;
		uint32_t			Style;
		intptr_t			ParentHandle;
		intptr_t			FrameWindowHandle;
		intptr_t			BackBufferWindowHandle;
		void*				ParentWindowPtr;
		void*				FrameWindowPtr;
		void*				BackBufferWindowPtr;
		void*				WindowsProcedure;

		SWindowSettings()
		{
			Width = 800;
			Height = 600;
			Style = eWS_NONE;
			ParentHandle = 0;
			FrameWindowHandle = 0;
			BackBufferWindowHandle = 0;
			WindowsProcedure = NULL;
			ParentWindowPtr = nullptr;
			FrameWindowPtr = nullptr;
			BackBufferWindowPtr = nullptr;
		}
	};

	class IWindow
	{
	public:
		IWindow() 
			:m_frameFunction(nullptr)
		{

		}

		virtual ~IWindow(){}

		virtual bool Initialize(SWindowSettings& settings) = 0;

		virtual void StartLoop() = 0;
		
		template<typename T>
		void SetFrameFunction(T&& func) 
		{
			m_frameFunction = std::forward<T>(func);
		}

		virtual intptr_t GetWindowHandle() const = 0;

		virtual void SetCaption(const char* szText) = 0;

		int GetWidth() const { return (int)m_settings.Width; }
		int GetHeight() const { return (int)m_settings.Height; }
		int GetViewportWidth() const { return (int)m_settings.Width; }
		int GetViewportHeight() const { return (int)m_settings.Height; }
		bool IsFullScreen() const { return m_settings.Style & eWS_FULLSCREEN; }

		virtual Vector2i GetPosFromScreenToClient(const Vector2i& spos) const = 0;
		virtual Vector2i GetPosFromClientToScreen(const Vector2i& cpos) const = 0;
		

	protected:
		SWindowSettings				m_settings;
		std::function<bool()>		m_frameFunction;
	};
}

#endif
