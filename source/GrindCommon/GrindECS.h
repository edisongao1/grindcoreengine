#pragma once

// Component Index Table Type, the values you can specify:
// 0: use sequential array, the search speed is log(n), n is the number of components that the current entity has
// 1: use hashmap as a container, with the same search speed as std::unordered_map
// 2: use direct array, the search speed is log(1), but remember to give the component and unique id using DefineComponentWithID
//	  instead of DefineComponent when you define your own component, and make sure this id doesn't excceed
//    FASTECS_MAX_COMPONENT_COUNT whose default value is 128.
#define FASTECS_COMPONENT_INDEX_TABLE_TYPE 2

// if FASTECS_COMPONENT_INDEX_TABLE_TYPE is set to 2, then enable FASTECS_USE_CUSTOM_COMPONENT_TYPE_ID
#if FASTECS_COMPONENT_INDEX_TABLE_TYPE == 2
#define FASTECS_USE_CUSTOM_COMPONENT_TYPE_ID 1
#endif

#define FASTECS_MAX_BLOCK_COUNT_BITS 3

#include "FastECS/FastECS.hpp"
#include "GrindMath.h"

using namespace FastECS;

namespace grind
{
	class IMesh;
	class IMaterial;

	DefineComponentWithID(TransformComponent, 1)
	{
		bool		bActive = true;
		bool		bStatic = true;

		Vector3f	Position;
		Quaternion	Rotation;
		Vector3f	Scale;

		inline Matrix4x4 ToMatrix() const
		{
			Matrix4x4 T(1.0f), R(1.0f), S(1.0f);
			T = glm::translate(T, Position);
			S = glm::scale(S, Scale);
			R = glm::toMat4(Rotation);
			return T * R * S;
		}
	};

	DefineComponentWithID(MeshComponent, 2)
	{
		bool			bVisible = true;
		bool			bReceiveShadow = true;
		bool			bCastShadow = true;
		
		IMesh*			pMesh = nullptr;
		IMaterial**		pMaterialList = nullptr;
	};

	DefineComponentWithID(BoundingBoxComponent, 3)
	{
		AABB			aabb;
	};

	DefineComponentWithID(CameraComponent, 4)
	{
		bool		bOrthogonal;
		float		NearZ;
		float		FarZ;
		float		Fov;
		Vector2f	ProjectionSize;
	};

	/*class StaticMeshEntityClass
		: public EntityClass<TransformComponent, MeshComponent, BoundingBoxComponent> {};*/


}


