#pragma once
#include "Common.h"
#include "GrindMath.h"

namespace grind
{
	//struct EAxis
	//{
	//	enum { X, Y, Z };
	//};

	enum class EAxis
	{
		X, Y, Z
	};

	enum EShaderType {
		VERTEX_SHADER_TYPE = 0,
		PIXEL_SHADER_TYPE,
		GEOMETRY_SHADER_TYPE,
		HULL_SHADER_TYPE,
		DOMAIN_SHADER_TYPE,
		COMPUTE_SHADER_TYPE,
		SHADER_TYPE_COUNT,
	};

	enum ETechniquePassStage {
		ePass_Forward,
		ePass_ShadowMap,
		ePass_GBuffer,
		ePass_DeferredShading,
		ePass_Count,
	};


	enum class EFVF : uint8_t
	{
		Unknown = 99,
		Object3D = 0,
		Object2D,
		PostEffectQuad,
		Count,
	};

	inline EFVF StringToFVF(const char* str)
	{
		__RETURN_IF_STRING_MATCH_ENUM(str, EFVF, Object3D);
		__RETURN_IF_STRING_MATCH_ENUM(str, EFVF, Object2D);
		__RETURN_IF_STRING_MATCH_ENUM(str, EFVF, PostEffectQuad);
		return EFVF::Unknown;
	}

	class E_FVF_Flag {
	public:
		enum type : uint8_t {
			ContainTangent = 1,
			ContainTexcoord1 = (1 << 1)
		};

		const static int FlagCount = 2;
	};


	enum class EVariableType
	{
		Bool,
		Float,
		Float2,
		Float3,
		Float4,
		Int,
		Int2,
		Int3,
		Int4,
		Color4,
		Color3,
		String,
		CharSequence,
	};

	template<typename T>
	inline EVariableType GetVariableType();

	template<> inline EVariableType GetVariableType<bool>() { return EVariableType::Bool; }
	template<> inline EVariableType GetVariableType<float>() { return EVariableType::Float; }
	template<> inline EVariableType GetVariableType<Vector2f>() { return EVariableType::Float2; }
	template<> inline EVariableType GetVariableType<Vector3f>() { return EVariableType::Float3; }
	template<> inline EVariableType GetVariableType<Vector4f>() { return EVariableType::Float4; }
	template<> inline EVariableType GetVariableType<int>() { return EVariableType::Int; }
	template<> inline EVariableType GetVariableType<Vector2i>() { return EVariableType::Int2; }
	template<> inline EVariableType GetVariableType<Vector3i>() { return EVariableType::Int3; }
	template<> inline EVariableType GetVariableType<Vector4i>() { return EVariableType::Int4; }
	template<> inline EVariableType GetVariableType<std::string>() { return EVariableType::String; }
	template<> inline EVariableType GetVariableType<char[]>() { return EVariableType::CharSequence; }
	template<> inline EVariableType GetVariableType<const char*>() { return EVariableType::CharSequence; }

	template<typename T>
	inline T GetDefaultValueOfType();

	template<> inline bool GetDefaultValueOfType<bool>() { return false; }
	template<> inline int GetDefaultValueOfType<int>() { return 0; }
	template<> inline float GetDefaultValueOfType<float>() { return 0.0f; }
	template<> inline double GetDefaultValueOfType<double>() { return 0.0; }
	template<> inline const char* GetDefaultValueOfType<const char*>() { return ""; }
}


