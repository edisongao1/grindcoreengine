#pragma once

#include "GrindECS.h"
#include "GrindEnums.h"

namespace grind
{
	class ISceneManager;

	class IEntityAction
	{
	public:
		IEntityAction(ISceneManager* pSceneManager)
			:mSceneManager(pSceneManager)
		{

		}

		ISceneManager* GetSceneManager() { return mSceneManager; }


		Vector3f GetLocalAxis(Entity* pEntity, EAxis axis)
		{
			TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
			Matrix3x3 m = glm::toMat3(pTransform->Rotation);
			return m[(int)axis];
		}

		void GetLocalAxes(Entity* pEntity, Vector3f localAxes[])
		{
			TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
			Matrix3x3 m = glm::toMat3(pTransform->Rotation);
			localAxes[0] = m[0];
			localAxes[1] = m[1];
			localAxes[2] = m[2];
		}

		void SetLocalAxes(Entity* pEntity, Vector3f localAxes[])
		{
			TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
			Matrix3x3 m = Matrix3x3(1.0f);
			m[0] = localAxes[0];
			m[1] = localAxes[1];
			m[2] = localAxes[2];
			pTransform->Rotation = glm::quat_cast(m);
		}

		void SetActive(Entity* pEntity, bool bActive)
		{
			TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
			if (pTransform)
				pTransform->bActive = bActive;
		}

		void SetPosition(Entity*& pEntity, const Vector3f& pos)
		{
			TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
			if (pTransform == nullptr)
				return;

			pTransform->Position = pos;
			Relocate(pEntity, pTransform);
		}

		void SetRotation(Entity*& pEntity, const Quaternion& rotation)
		{
			TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
			if (pTransform == nullptr)
				return;

			pTransform->Rotation = rotation;
			Relocate(pEntity, pTransform);
		}

		void SetScale(Entity*& pEntity, const Vector3f& scale)
		{
			TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
			if (pTransform == nullptr)
				return;

			pTransform->Scale = scale;
			Relocate(pEntity, pTransform);
		}

		void SetTranformation(Entity* pEntity, const Vector3f& pos, const Quaternion& rotation, const Vector3f& scale)
		{
			TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
			if (pTransform == nullptr)
				return;

			pTransform->Position = pos;
			pTransform->Rotation = rotation;
			pTransform->Scale = scale;
			Relocate(pEntity, pTransform);
		}

		//virtual AABB ComputeWorldBoundingBox(Entity* pEntity) const = 0;
		virtual AABB GetWorldBoundingBox(Entity* pEntity) const = 0;

		// if the position (or scale, rotation) of a static entity is changed, remember to call this method
		virtual void Relocate(Entity*& pEntity, TransformComponent* pTransform) = 0;

		virtual void Relocate(Entity*& pEntity)
		{
			TransformComponent* pTransform = pEntity->GetComponent<TransformComponent>();
			Relocate(pEntity, pTransform);
		}

	protected:
		ISceneManager*		mSceneManager = nullptr;
	};


	//class ITransformAction : public IEntityAction
	//{
	//public:
	//	virtual void Relocate(Entity*& pEntity, TransformComponent* pTransform);
	//};

	class IMeshEntityAction : public IEntityAction
	{
	public:
		IMeshEntityAction(ISceneManager* pSceneManager):IEntityAction(pSceneManager){}

		//virtual AABB ComputeWorldBoundingBox(Entity* pEntity) const;
		
	};

	struct SCameraViewTransformation;
	class ICameraAction : public IEntityAction
	{
	public:
		ICameraAction(ISceneManager* pSceneManager) :IEntityAction(pSceneManager) {}
		virtual SCameraViewTransformation GetCameraTransformations(Entity* pEntity) = 0;
		virtual void LookAt(Entity* pEntity, const Vector3f& target, const Vector3f& up = Vector3f(0, 1, 0)) = 0;
	};
}

