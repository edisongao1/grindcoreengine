#pragma once

#include "GrindEnums.h"
#include "GrindMath.h"
#include "StringParseUtils.h"
#include "Common.h"

namespace grind
{
	struct SMaterialTemplate;
	class ITechnique;

	enum class EFrameConstBuffer {
		ForwardObject3D,
		Test,
		Count,
		None = 99,
	};

	enum class EObjectConstBuffer {
		ForwardObject3D,
		Test,
		Count,
		None = 99,
	};

	//enum class EConstBufferType {
	//	Object = 0,
	//	Frame,
	//	Material,
	//	Count,
	//};

	inline EFrameConstBuffer StringToFrameConstBufferType(const char* str) {
		__RETURN_IF_STRING_MATCH_ENUM(str, EFrameConstBuffer, ForwardObject3D);
		__RETURN_IF_STRING_MATCH_ENUM(str, EFrameConstBuffer, Test);
		return EFrameConstBuffer::None;
	}

	inline EObjectConstBuffer StringToObjectConstBufferType(const char* str) {
		__RETURN_IF_STRING_MATCH_ENUM(str, EObjectConstBuffer, ForwardObject3D);
		__RETURN_IF_STRING_MATCH_ENUM(str, EObjectConstBuffer, Test);
		return EObjectConstBuffer::None;
	}

	struct SFrameConstBuffer
	{
		struct SForwardObject3D
		{
			Matrix4x4	View;
			Matrix4x4	InvView;
			Matrix4x4	Proj;
			Matrix4x4	InvProj;
			Matrix4x4	ViewProj;
			Matrix4x4	InvViewProj;
			Vector3f	CameraPos;
			float		_pad0;
			Vector4f	RenderTargetSize;
			float		NearZ;
			float		FarZ;
			float		TotalTime;
			float		DeltaTime;
		};

		struct STest
		{
			Matrix4x4		ViewProj;
			Vector4f		Color1;
			Vector4f		Color2;
		};
	};

	struct SObjectConstBuffer
	{
		struct SForwardObject3D
		{
			Matrix4x4	World;
			Matrix4x4	WorldViewProj;
		};

		struct STest
		{
			Matrix4x4	World;
			Vector4f	Color1;
			Vector4f	Color2;
		};
	};

	class IRenderPass
	{
	public:
		IRenderPass(ETechniquePassStage stage, ITechnique* pTech)
			: mStage(stage), mTechinque(pTech) {}
		
		ETechniquePassStage GetStage() const { return mStage; }
		ITechnique* GetTechnique() { return mTechinque; }

		EObjectConstBuffer GetObjectConstBufferType() const { return mObjectConstBufferType; }
		EFrameConstBuffer GetFrameConstBufferType() const { return mFrameConstBufferType; }
		//GetObjectConstBufferType();
	public:
		ETechniquePassStage		mStage;
		//EObjectConstantBuffer	mObjectConstantsType;
		//EFrameConstantBuffer	mFrameConstantsType;
		EObjectConstBuffer		mObjectConstBufferType = EObjectConstBuffer::None;
		EFrameConstBuffer		mFrameConstBufferType = EFrameConstBuffer::None;
		ITechnique*				mTechinque;
	};




}