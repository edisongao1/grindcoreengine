#ifndef __DEFINED_FASTECS_HEADER_HPP__
#define __DEFINED_FASTECS_HEADER_HPP__

#include <typeinfo>
#include <map>
#include <unordered_map>
#include <assert.h>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <tuple>
#include <cstring>
#include <atomic>
#include <thread>
#include <cstdlib>

namespace FastECS
{

typedef unsigned char byte;

#define FASTECS_STR_1(S,I) (I < sizeof(S) ? S[I] : '\0')
#define FASTECS_STR_2(S,I) FASTECS_STR_1(S,I), FASTECS_STR_1(S,I+1)
#define FASTECS_STR_4(S,I) FASTECS_STR_2(S,I), FASTECS_STR_2(S,I+2)
#define FASTECS_STR_8(S,I) FASTECS_STR_4(S,I), FASTECS_STR_4(S,I+4)
#define FASTECS_STR_16(S,I) FASTECS_STR_8(S,I), FASTECS_STR_8(S,I+8)
#define FASTECS_STR_32(S,I) FASTECS_STR_16(S,I), FASTECS_STR_16(S,I+16)
#define FASTECS_STR(S) FASTECS_STR_32(S,0)

#ifndef SAFE_DELETE
#define SAFE_DELETE(x) { if(x){ delete x; x = 0; } }
#endif

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(x) { if(x){ x->Release(); x = 0; } }
#endif

#define FASTECS_MAX_LONG_PTR_MASK (0xFFFF000000000000LL)

#ifdef FASTECS_MAX_THREAD_COUNT
enum { MAX_THREAD_COUNT = FASTECS_MAX_THREAD_COUNT };
#else
enum { MAX_THREAD_COUNT = 32 };
#endif

#ifdef FASTECS_MAX_COMPONENT_COUNT_PER_ENTITY
#define MAX_COMPONENT_COUNT_PER_ENTITY FASTECS_MAX_COMPONENT_COUNT_PER_ENTITY
enum { MAX_COMPONENT_COUNT_PER_ENTITY = FASTECS_MAX_COMPONENT_COUNT_PER_ENTITY };
#else
enum { MAX_COMPONENT_COUNT_PER_ENTITY = 32 };
#endif

//enum { INVALID_COMPONENT_INDEX = MAX_COMPONENT_COUNT_PER_ENTITY + 1};
enum { INVALID_COMPONENT_INDEX = -1 };
enum { INVALID_EVENT_INDEX = -1 };

#ifdef FASTECS_MAX_COMPONENT_COUNT
enum { MAX_COMPONENT_COUNT = FASTECS_MAX_COMPONENT_COUNT };
#else
enum { MAX_COMPONENT_COUNT = 128 };
#endif

#ifdef FASTECS_MAX_EVENT_COUNT
enum { MAX_EVENT_COUNT = FASTECS_MAX_EVENT_COUNT + 2 };
#else 
enum {MAX_EVENT_COUNT = 16 };
#endif

#ifdef FASTECS_MAX_STORAGE_CHUNK_SIZE
enum { MAX_STORAGE_CHUNK_SIZE = FASTECS_MAX_STORAGE_CHUNK_SIZE };
#else
enum { MAX_STORAGE_CHUNK_SIZE = 64 * 1024 * 1024 }; // 64k
#endif

#ifdef FASTECS_MAX_CONTEXT_COUNT
enum { MAX_CONTEXT_COUNT = FASTECS_MAX_CONTEXT_COUNT };
#else
enum { MAX_CONTEXT_COUNT = 256 };
#endif

#ifdef FASTECS_MAX_BLOCK_COUNT_BITS
enum { MAX_BLOCK_COUNT_BITS = FASTECS_MAX_BLOCK_COUNT_BITS };
#else
enum { MAX_BLOCK_COUNT_BITS = 10 };
#endif

enum { MAX_CHUNK_COUNT_BITS = 15 };
enum { MAX_STORAGE_COUNT_BITS = 10 };

enum { MAX_ENTITY_COUNT_PER_CHUNK = (1 << MAX_BLOCK_COUNT_BITS) };
enum { MAX_CHUNK_COUNT_PER_STORAGE = (1 << MAX_CHUNK_COUNT_BITS) };
enum { MAX_STORAGE_COUNT_PER_CONTEXT = (1 << MAX_STORAGE_COUNT_BITS) };

enum { BLOCK_INDEX_MASK = MAX_ENTITY_COUNT_PER_CHUNK - 1 };
enum { CHUNK_INDEX_MASK = MAX_CHUNK_COUNT_PER_STORAGE - 1};
enum { STORAGE_INDEX_MASK = MAX_STORAGE_COUNT_PER_CONTEXT - 1};


#ifdef FASTECS_USE_CUSTOM_COMPONENT_TYPE_ID
#define USE_CUSTOM_COMPONENT_TYPE_ID FASTECS_USE_CUSTOM_COMPONENT_TYPE_ID
#else
#define USE_CUSTOM_COMPONENT_TYPE_ID 0
#endif

#ifdef FASTECS_USE_CUSTOM_EVENT_TYPE_ID
#define USE_CUSTOM_EVENT_TYPE_ID FASTECS_USE_CUSTOM_EVENT_TYPE_ID
#else
#define USE_CUSTOM_EVENT_TYPE_ID 0
#endif

#ifdef FASTECS_COMPONENT_INDEX_TABLE_TYPE
#define COMPONENT_INDEX_TABLE_TYPE FASTECS_COMPONENT_INDEX_TABLE_TYPE
#else
#define COMPONENT_INDEX_TABLE_TYPE 0
#endif

#ifdef FASTECS_EVENT_INDEX_TABLE_TYPE
#define EVENT_INDEX_TABLE_TYPE FASTECS_EVENT_INDEX_TABLE_TYPE
#else
#define EVENT_INDEX_TABLE_TYPE 0
#endif

// 0: don't divide each chunk, 1: can divide one chunk. 
#ifdef FASTECS_DIVIDE_PARALLEL_JOB_METHOD
#define DIVIDE_PARALLEL_JOB_METHOD FASTECS_DIVIDE_PARALLEL_JOB_METHOD
#else
#define DIVIDE_PARALLEL_JOB_METHOD 1
#endif

constexpr uint32_t crc_table[256] = {
	0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x76dc419, 0x706af48f, 0xe963a535, 0x9e6495a3,
	0xedb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988, 0x9b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91,
	0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
	0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5,
	0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172, 0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
	0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
	0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423, 0xcfba9599, 0xb8bda50f,
	0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924, 0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,
	0x76dc4190, 0x1db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x6b6b51f, 0x9fbfe4a5, 0xe8b8d433,
	0x7807c9a2, 0xf00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x86d3d2d, 0x91646c97, 0xe6635c01,
	0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457,
	0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
	0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb,
	0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0, 0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9,
	0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
	0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad,
	0xedb88320, 0x9abfb3b6, 0x3b6e20c, 0x74b1d29a, 0xead54739, 0x9dd277af, 0x4db2615, 0x73dc1683,
	0xe3630b12, 0x94643b84, 0xd6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0xa00ae27, 0x7d079eb1,
	0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7,
	0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
	0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
	0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79,
	0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236, 0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f,
	0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
	0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x26d930a, 0x9c0906a9, 0xeb0e363f, 0x72076785, 0x5005713,
	0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0xcb61b38, 0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0xbdbdf21,
	0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
	0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45,
	0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db,
	0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
	0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf,
	0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94, 0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};

template<size_t _n> 
constexpr size_t string_length(const char(&s)[_n]) noexcept
{
	size_t n = _n;
	while (s[n - 1] == 0 && n > 0)
	{
		--n;
	}
	return n;
}

template<size_t _len, size_t _n>
constexpr uint32_t string_crc(const char(&s)[_n])
{
	uint32_t x = 0xffffffff;
	for (size_t i = 0; i < _len; i++) {
		x = (x >> 8) ^ crc_table[(x ^ s[i]) & 0x000000ff];
	}
	return x ^ 0xffffffff;
}

template<typename T, T...>
struct sum_helper_class;

template<typename T, T x, T...Args>
struct sum_helper_class<T, x, Args...> {
	constexpr static const T value = x + sum_helper_class<T, Args...>::value;
};
template<typename T>
struct sum_helper_class<T> {
	constexpr static const T value = 0;
};

template<typename T, T... Args>
constexpr T sum_v = sum_helper_class<T, Args...>::value;

using ComponentHash = uint32_t;
using ComponentTypeID = ComponentHash;
using EventHash = uint32_t;
using EventTypeID = EventHash;

#define INVALID_COMPONENT_TYPE_ID 0xffffffff

struct ComponentBase { };

template<ComponentTypeID CustomId, char..._Char>
struct constant_name_class
{
public:
	static constexpr char const __className_string[sizeof...(_Char) + 1] = { _Char..., '\0' };
	static constexpr size_t __className_len = string_length(__className_string);
	static constexpr uint32_t __crc = string_crc<__className_len>(__className_string);
	//static constexpr uint32_t xxx = __className_len - 1;

	static constexpr const char* class_name() { return &__className_string[0]; }
	//static constexpr size_t classNameLength() { return __className_len; }
	static constexpr ComponentHash hash_code() { return __crc; }
};

template<ComponentTypeID CustomId, char..._Char>
struct component_name_class : public constant_name_class<CustomId, _Char...> , public ComponentBase
{
#if USE_CUSTOM_COMPONENT_TYPE_ID
	static constexpr ComponentTypeID type_id()
	{
		static_assert(CustomId != INVALID_COMPONENT_TYPE_ID, "Must assign a value when using custom component type id.");
		static_assert(CustomId < MAX_COMPONENT_COUNT, "CustomId must smaller than the maximum of components");
		return CustomId;
	}
#else
	static constexpr ComponentTypeID type_id() { return constant_name_class<CustomId, _Char...>::__crc; }
#endif
};

template<EventTypeID CustomId, char..._Char>
struct event_name_class : public constant_name_class<CustomId, _Char...> 
{
#if USE_CUSTOM_EVENT_TYPE_ID
	static constexpr EventTypeID type_id()
	{
		static_assert(CustomId != INVALID_COMPONENT_TYPE_ID, "Must assign a value when using custom event type id.");
		static_assert(CustomId < MAX_EVENT_COUNT, "CustomId must smaller than the maximum of events");
		return CustomId;
	}
#else
	static constexpr EventTypeID type_id() { return constant_name_class<CustomId, _Char...>::__crc; }
#endif
};

#define ComponentBaseClass(s, customId) component_name_class<customId, FASTECS_STR(#s)>
#define DefineComponent(className) \
	struct className : public component_name_class<INVALID_COMPONENT_TYPE_ID, FASTECS_STR(#className)>
#define DefineComponentWithID(className, id) \
	struct className : public component_name_class<id, FASTECS_STR(#className)>

#define EventBaseClass(s, customId) event_name_class<customId, FASTECS_STR(#s)>
#define DefineEvent(className) \
	struct className : public event_name_class<INVALID_COMPONENT_TYPE_ID, FASTECS_STR(#className)>
#define DefineEventWithID(className, id) \
	struct className : public event_name_class<id, FASTECS_STR(#className)>

template<typename T, typename...OtherTypes>
struct is_type_duplicate;

template<typename T, typename U, typename...OtherTypes>
struct is_type_duplicate<T, U, OtherTypes...>
{
	constexpr static const bool value = std::is_same<std::decay_t<T>, std::decay<U>>::value ? true : is_type_duplicate<T, OtherTypes...>::value;
};

template<typename T>
struct is_type_duplicate<T>
{
	constexpr static const bool value = false;
};

template<typename T, typename...OtherTypes>
constexpr bool is_type_duplicate_v = is_type_duplicate<T, OtherTypes...>::value;



template<typename...T>
struct sum_component_hashcodes;

#pragma warning( push )
#pragma warning( disable : 4307 ) // warning C4307: '+': integral constant overflow

template<typename T, typename...U>
struct sum_component_hashcodes<T, U...> {
	constexpr static const uint32_t value = (is_type_duplicate_v<T, U...> ? 0 : T::hash_code()) + (uint32_t)sum_component_hashcodes<U...>::value;
};

template<>
struct sum_component_hashcodes<> {
	constexpr static const uint32_t value = 0;
};
#pragma warning( pop )

template<typename...T>
constexpr uint32_t sum_component_hashcodes_v = sum_component_hashcodes<T...>::value;

class IChunkMemoryAllocator
{
public:
	virtual void* Malloc(size_t sizeBytes) = 0;
	virtual void* Realloc(void* ptr, std::size_t new_size) = 0;
	virtual void Free(void* p) = 0;
};

class StandardChunkMemoryAllocator : public IChunkMemoryAllocator
{
public:
	virtual void* Malloc(size_t sizeBytes) override
	{
		void* p = std::malloc(sizeBytes);
		return p;
	}
	virtual void* Realloc(void* ptr, std::size_t new_size) override
	{
		void* new_ptr = std::realloc(ptr, new_size);
		return new_ptr;
	}
	virtual void Free(void* p) override
	{
		std::free(p);
	}
};

struct DummyEntityClass {};

struct EntityClassBase {};

template<typename...ComponentTypes>
struct EntityClass;

template<typename T, typename...U>
struct EntityClass<T, U...> : public EntityClassBase
{
	constexpr static const uint32_t hashCode = sum_component_hashcodes_v<T, U...>;
	static constexpr uint32_t hash_code() { return hashCode; }
	using type = T;
	using next_type = EntityClass<U...>;
};

template<>
struct EntityClass<> : public EntityClassBase
{
	constexpr static const uint32_t hashCode = 0;
	using type = DummyEntityClass;
	//using next_type = EntityClass<U...>;
};

class World;

using ArchetypeID = uint32_t;

using ComponentConstructor = std::function<void(void*)>;
using ComponentDestructor = std::function<void(void*)>;
using ComponentAssignment = std::function<void(void*, const void*)>;

struct ComponentMeta
{
	const char*				name;
	ComponentHash			hashCode;
	ComponentTypeID			typeId;
	size_t					size;
	ComponentConstructor	constructor = nullptr;
	ComponentDestructor		destructor = nullptr;
	ComponentAssignment		assignment = nullptr;
};

using ComponentMetaMap = std::map<ComponentTypeID, ComponentMeta*>;

//MAX_COMPONENT_COUNT_PER_ENTITY
template<int MaxCount> 
class LinearComponentIndexTable
{
public:
	int Add(ComponentTypeID id) 
	{
		auto count = mComponentCount;
		mComponentTypeIds[mComponentCount++] = id; 
		return count;
	}
	template<typename T> int Add() { return Add(std::decay_t<T>::type_id()); }
	int Get(ComponentTypeID id) const
	{
		for (int i = 0; i < mComponentCount; i++) {
			if (mComponentTypeIds[i] == id)
				return i;
		}
		return INVALID_COMPONENT_INDEX;
	}
	template<typename T>
	int Get() const { return Get(std::decay_t<T>::type_id()); }
private:
	int					mComponentCount = 0;
	ComponentTypeID		mComponentTypeIds[MaxCount] = { 0 };
};

class HashMapComponentIndexTable
{
public:
	int Add(ComponentTypeID id)
	{
		assert(mIndexMap.size() == mComponentCount);
		assert(mIndexMap.find(id) == mIndexMap.end());
		auto count = mComponentCount;
		mIndexMap.insert({ id, mComponentCount });
		mComponentCount += 1;
		return count;
	}
	template<typename T> int Add() { return Add(std::decay_t<T>::type_id()); }
	int Get(ComponentTypeID id) const
	{
		auto it = mIndexMap.find(id);
		if (it == mIndexMap.end())
			return INVALID_COMPONENT_INDEX;
		return it->second;
	}
	template<typename T> int Get() const { return Get(std::decay_t<T>::type_id()); }
private:
	int					mComponentCount = 0;
	std::unordered_map<ComponentTypeID, int>	mIndexMap;
};

// MAX_COMPONENT_COUNT
template<int MaxCount>
class DirectComponentIndexTable
{
public:
	DirectComponentIndexTable() 
	{
		for (int i = 0; i < MaxCount; i++) {
			mIndexTable[i] = INVALID_COMPONENT_INDEX;
		}
	}
	int Add(ComponentTypeID id)
	{
		assert(mIndexTable[id] == INVALID_COMPONENT_INDEX);
		mIndexTable[id] = mComponentCount;
		auto count = mComponentCount;
		mComponentCount += 1;
		return count;
	}
	template<typename T> int Add() { return Add(std::decay_t<T>::type_id()); }
	int Get(ComponentTypeID id) const { return mIndexTable[id]; }
	template<typename T> int Get() const { return Get(std::decay_t<T>::type_id()); }
private:
	int			mComponentCount = 0;
	int			mIndexTable[MaxCount];
};

#if COMPONENT_INDEX_TABLE_TYPE == 0
	using ComponentIndexTable = LinearComponentIndexTable<MAX_COMPONENT_COUNT_PER_ENTITY>;
#elif COMPONENT_INDEX_TABLE_TYPE == 1
	using ComponentIndexTable = HashMapComponentIndexTable;
#elif COMPONENT_INDEX_TABLE_TYPE == 2
#if USE_CUSTOM_COMPONENT_TYPE_ID
	using ComponentIndexTable = DirectComponentIndexTable<MAX_COMPONENT_COUNT>;
#else
	using ComponentIndexTable = LinearComponentIndexTable<MAX_COMPONENT_COUNT_PER_ENTITY>;
#endif
#endif

template<typename...ComponentTypes>
struct ContainComponentsHelperClass;

template<typename ComponentType, typename...Rest>
struct ContainComponentsHelperClass<ComponentType, Rest...>
{
	static bool All(const ComponentIndexTable& componentIndexTable)
	{
		return componentIndexTable.Get<ComponentType>() != INVALID_COMPONENT_INDEX &&
			ContainComponentsHelperClass<Rest...>::All(componentIndexTable);
	}
	static bool Any(const ComponentIndexTable& componentIndexTable)
	{
		return componentIndexTable.Get<ComponentType>() != INVALID_COMPONENT_INDEX ||
			ContainComponentsHelperClass<Rest...>::Any(componentIndexTable);
	}
};

template<>
struct ContainComponentsHelperClass<>
{
	static bool All(const ComponentIndexTable& componentIndexTable) { return true; }
	static bool Any(const ComponentIndexTable& componentIndexTable) { return false; }
};

class EntityArchetypeManager;
class EntityComponentStorage;
class EntityContext;
class EntityComponentChunk;

class EntityArchetype
{
	friend class EntityArchetypeManager;
	friend class EntityComponentStorage;
	friend class EntityComponentChunk;
	friend class EntityContext;
private:

public:
	EntityArchetype(EntityArchetypeManager* pArchetypeManager, ArchetypeID id, const ComponentMetaMap& metaMap)
		:mArchetypeManager(pArchetypeManager), mArchetypeId(id), mComponentMetaMap(metaMap)
	{
		mComponentCount = (int)metaMap.size();
		int i = 0;
		size_t currentOffset = 0;
		for (auto it : metaMap) 
		{
			ComponentMeta* meta = it.second;
			mComponentNames[i] = meta->name;
			mComponentHashes[i] = meta->hashCode;
			mComponentTypeIds[i] = meta->typeId;
			mComponentSizes[i] = meta->size;
			mComponentOffsets[i] = currentOffset;

			mComponentConstructors[i] = &meta->constructor;
			mComponentDestructors[i] = &meta->destructor;
			mComponentAssignments[i] = &meta->assignment;

			mComponentIndexTable.Add(meta->typeId);
			currentOffset += meta->size;
			i += 1;
		}
	}

	EntityArchetype(const EntityArchetype&) = delete;
	EntityArchetype& operator=(const EntityArchetype&) = delete;

	template<typename ComponentType>
	bool ContainComponent() const
	{
		return  mComponentIndexTable.Get<ComponentType>() != INVALID_COMPONENT_INDEX;
	}

	template<typename... ComponentTypes>
	bool ContainAllComponents() const
	{
		return ContainComponentsHelperClass<ComponentTypes...>::All(mComponentIndexTable);
	}

	template<typename... ComponentTypes>
	bool ContainAnyComponents() const
	{
		return ContainComponentsHelperClass<ComponentTypes...>::Any(mComponentIndexTable);
	}

	template<typename ComponentType>
	int GetComponentIndex() const
	{
		return mComponentIndexTable.Get<ComponentType>();
	}

	int GetComponentIndex(ComponentTypeID componentTypeID) const
	{
		return mComponentIndexTable.Get(componentTypeID);
	}

	ArchetypeID GetID() const {
		return mArchetypeId;
	}

	template<typename...ComponentTypes>
	inline EntityArchetype* Extend();

private:
	EntityArchetypeManager*		mArchetypeManager = nullptr;
	ArchetypeID			mArchetypeId = 0;
	ComponentMetaMap	mComponentMetaMap;
	
	int					mComponentCount = 0;
	const char*			mComponentNames[MAX_COMPONENT_COUNT_PER_ENTITY] = { 0 };
	ComponentTypeID		mComponentHashes[MAX_COMPONENT_COUNT_PER_ENTITY] = { 0 };
	ComponentHash		mComponentTypeIds[MAX_COMPONENT_COUNT_PER_ENTITY] = { 0 };
	size_t				mComponentSizes[MAX_COMPONENT_COUNT_PER_ENTITY] = { 0 };
	size_t				mComponentOffsets[MAX_COMPONENT_COUNT_PER_ENTITY] = { 0 };
	ComponentConstructor*	mComponentConstructors[MAX_COMPONENT_COUNT_PER_ENTITY] = { 0 };
	ComponentDestructor*	mComponentDestructors[MAX_COMPONENT_COUNT_PER_ENTITY] = { 0 };
	ComponentAssignment*	mComponentAssignments[MAX_COMPONENT_COUNT_PER_ENTITY] = { 0 };

	ComponentIndexTable	mComponentIndexTable;

	// map from context to storage, just for fast indexing.
	EntityComponentStorage*		mStoragesInContext[MAX_CONTEXT_COUNT] = { 0 };
};

class EntityArchetypeManager
{
public:
	EntityArchetypeManager()
	{

	}

	template<typename...T>
	EntityArchetype* CreateArchetype();

	template<typename EntityClassType>
	EntityArchetype* CreateArchetypeByEntityClass()
	{
		const ArchetypeID id = EntityClassType::hashCode;
		auto it = mArchetypesMap.find(id);
		if (it != mArchetypesMap.end()) {
			return it->second;
		}
		ComponentMetaMap metaMap;
		GetComponentsMetaFromEntityClass<EntityClassType>(metaMap);
		EntityArchetype* pEntityArchetype = new EntityArchetype(this, id, metaMap);
		mArchetypesMap.insert({ id, pEntityArchetype });
		return pEntityArchetype;
	}

	template<typename...ComponentTypes>
	EntityArchetype* CreateArchetypeByComponentTypes();

	template<typename...ComponentTypes>
	EntityArchetype* AddComponents(const EntityArchetype* pArchetype);

	template<typename... ComponentTypes>
	EntityArchetype* RemoveComponents(const EntityArchetype* pArchetype);

	ArchetypeID GetArchetypeIDFromComponentMetaMap(const ComponentMetaMap& metaMap)
	{
		ArchetypeID id = 0;
		for (const auto& it : metaMap) {
			id += it.second->hashCode;
		}
		return id;
	}

	template<typename EntityClassType>
	void GetComponentsMetaFromEntityClass(ComponentMetaMap& metaMap)
	{
		if constexpr (std::is_same_v<typename EntityClassType::type, DummyEntityClass>)
		{
			return;
		}
		else
		{
			using ComponentType = typename EntityClassType::type;
			ComponentTypeID componentTypeId = ComponentType::type_id();
			if (metaMap.find(componentTypeId) == metaMap.end()) 
			{
				ComponentMeta* meta = GetComponentMeta<ComponentType>();
				metaMap.insert({ componentTypeId, meta });
			}
			GetComponentsMetaFromEntityClass<typename EntityClassType::next_type>(metaMap);
		}
	}
	
	template<typename T>
	ComponentMeta* GetComponentMeta()
	{
		using ComponentType = std::decay_t<T>;
		ComponentHash hashcode = ComponentType::hash_code();
		auto it = mComponentMetas.find(hashcode);
		if (it != mComponentMetas.end())
			return it->second;

		ComponentMeta* meta = new ComponentMeta();
		meta->name = ComponentType::class_name();
		meta->hashCode = ComponentType::hash_code();
		meta->typeId = ComponentType::type_id();;
		meta->size = sizeof(ComponentType);
		meta->constructor = [](void* pMem) {
			new (pMem) ComponentType();
		};
		meta->destructor = [](void* pMem) {
			ComponentType* pComponent = reinterpret_cast<ComponentType*>(pMem);
			pComponent->~ComponentType();
		};
		meta->assignment = [](void* pDst, const void* pSrc) {
			//ComponentType* pDstComponent = reinterpret_cast<ComponentType*>(pDst);
			const ComponentType* pSrcComponent = reinterpret_cast<const ComponentType*>(pSrc);
			new (pDst) ComponentType(*pSrcComponent);
		};

		mComponentMetas.insert({ hashcode, meta });
		return meta;
	}


private:
	//World*												mWorld;
	std::unordered_map<ComponentHash, ComponentMeta*>	mComponentMetas;
	std::unordered_map<ArchetypeID, EntityArchetype*>	mArchetypesMap;
};

template<typename...T>
struct CreateArchetypeHelperClass;

template<typename T, typename...U>
struct CreateArchetypeHelperClass<T, U...>
{
	static EntityArchetype* Create(EntityArchetypeManager* pManager)
	{
		if constexpr (std::is_base_of<EntityClassBase, T>::value) {
			return pManager->CreateArchetypeByEntityClass<T>();
		}
		else {
			return pManager->CreateArchetypeByComponentTypes<T, U...>();
		}
	}
};

template<>
struct CreateArchetypeHelperClass<>
{
	static EntityArchetype* Create(EntityArchetypeManager* pManager)
	{
		return pManager->CreateArchetypeByComponentTypes<>();
	}
};

template<typename...ComponentTypes>
struct GetComponentsMetaHelperClass;

template<typename ComponentType, typename...OtherTypes>
struct GetComponentsMetaHelperClass<ComponentType, OtherTypes...>
{
	static void Call(EntityArchetypeManager* pArchetypeManager, ComponentMetaMap& metaMap)
	{
		ComponentTypeID componentTypeId = std::decay_t<ComponentType>::type_id();
		if (metaMap.find(componentTypeId) == metaMap.end()) {
			ComponentMeta* meta = pArchetypeManager->GetComponentMeta<ComponentType>();
			metaMap.insert({ componentTypeId, meta });
		}
		GetComponentsMetaHelperClass<OtherTypes...>::Call(pArchetypeManager, metaMap);
	}
};

template<>
struct GetComponentsMetaHelperClass<>
{
	static void Call(EntityArchetypeManager* pArchetypeManager, ComponentMetaMap& metaMap) { }
};

template<typename...ComponentTypes>
struct RemoveComponentsFromMetaMap;

template<typename ComponentType, typename...OtherTypes>
struct RemoveComponentsFromMetaMap<ComponentType, OtherTypes...>
{
	static void Call(EntityArchetypeManager* pArchetypeManager, ComponentMetaMap& metaMap)
	{
		ComponentTypeID componentTypeId = std::decay_t<ComponentType>::type_id();
		if (metaMap.find(componentTypeId) != metaMap.end()) {
			metaMap.erase(componentTypeId);
		}
		RemoveComponentsFromMetaMap<OtherTypes...>::Call(pArchetypeManager, metaMap);
	}
};

template<>
struct RemoveComponentsFromMetaMap<>
{
	static void Call(EntityArchetypeManager* pArchetypeManager, ComponentMetaMap& metaMap) {}
};



template<typename...T>
EntityArchetype* EntityArchetypeManager::CreateArchetype()
{
	return CreateArchetypeHelperClass<T...>::Create(this);
}

template<typename...ComponentTypes>
EntityArchetype* EntityArchetypeManager::CreateArchetypeByComponentTypes()
{
	const ArchetypeID id = sum_component_hashcodes_v<ComponentTypes...>;
	auto it = mArchetypesMap.find(id);
	if (it != mArchetypesMap.end()) {
		return it->second;
	}

	ComponentMetaMap metaMap;
	GetComponentsMetaHelperClass<ComponentTypes...>::Call(this, metaMap);
	EntityArchetype* pEntityArchetype = new EntityArchetype(this, id, metaMap);
	mArchetypesMap.insert({ id, pEntityArchetype });
	return pEntityArchetype;
}

template<typename...ComponentTypes>
EntityArchetype* EntityArchetypeManager::AddComponents(const EntityArchetype* pArchetype)
{
	ComponentMetaMap metaMap(pArchetype->mComponentMetaMap);
	GetComponentsMetaHelperClass<ComponentTypes...>::Call(this, metaMap);
	if (metaMap.size() == pArchetype->mComponentMetaMap.size()) {
		return nullptr;
	}
	ArchetypeID id = GetArchetypeIDFromComponentMetaMap(metaMap);
	auto it = mArchetypesMap.find(id);
	if (it != mArchetypesMap.end()) {
		return it->second;
	}
	EntityArchetype* pEntityArchetype = new EntityArchetype(this, id, metaMap);
	mArchetypesMap.insert({ id, pEntityArchetype });
	return pEntityArchetype;
}

template<typename... ComponentTypes>
EntityArchetype* EntityArchetypeManager::RemoveComponents(const EntityArchetype* pArchetype)
{
	ComponentMetaMap metaMap(pArchetype->mComponentMetaMap);
	RemoveComponentsFromMetaMap<ComponentTypes...>::Call(this, metaMap);
	if (metaMap.size() == pArchetype->mComponentMetaMap.size()) {
		return nullptr;
	}
	ArchetypeID id = GetArchetypeIDFromComponentMetaMap(metaMap);
	auto it = mArchetypesMap.find(id);
	if (it != mArchetypesMap.end()) {
		return it->second;
	}
	EntityArchetype* pEntityArchetype = new EntityArchetype(this, id, metaMap);
	mArchetypesMap.insert({ id, pEntityArchetype });
	return pEntityArchetype;
}

template<typename...ComponentTypes>
inline EntityArchetype* EntityArchetype::Extend()
{
	return mArchetypeManager->AddComponents<ComponentTypes...>(this);
}

using EntityID = uint64_t;

class Entity
{
	friend class EntityComponentChunk;
	friend class EntityComponentStorage;
	friend class EntityContext;
public:
	Entity() : mValid(false), mGenID(0)
	{

	}

	bool operator==(const Entity& ent) const
	{
		return mChunkIndex == ent.mChunkIndex
			&& mBlockIndex == ent.mBlockIndex
			&& mGenID == ent.mGenID
			&& mStorage == ent.mStorage;
	}

	/*static EntityID ToEntityID(Entity* pEntity)
	{

	}*/

	inline EntityID GetEntityID() const;

	inline EntityContext* GetContext();

	inline static void ParseEntityID(EntityID eid, uint16_t* genid, 
		uint8_t* contextId, uint16_t* storageIndex,
		uint16_t* chunkIndex, uint16_t* blockIndex);

	inline static uint8_t ExtractContextIdFromEntityID(EntityID eid);

	inline void Release();
	bool IsValid() const { return mValid; }

	template<typename ComponentType>
	inline ComponentType* GetComponent();

	template<typename ComponentType>
	inline const ComponentType* GetComponent() const;

	template<typename ComponentType>
	inline int GetComponentIndex() const;

	inline int GetComponentIndex(ComponentTypeID componentTypeID) const;

	template<typename T=byte>
	inline T* GetComponentByIndex(int index);

	template<typename T=byte>
	inline T* GetComponentByTypeID(ComponentTypeID typeId);

	template<typename T = byte>
	inline const T* GetComponentByIndex(int index) const;

	template<typename T = byte>
	inline const T* GetComponentByTypeID(ComponentTypeID typeId) const;

	template<typename ComponentType>
	inline bool ContainComponent() const;

	template<typename... ComponentTypes>
	inline bool ContainAllComponents() const;

	template<typename... ComponentTypes>
	inline bool ContainAnyComponents() const;

	template<typename ComponentType>
	inline bool SetComponent(ComponentType&& component);

	inline EntityArchetype* GetArchetype();
	inline const EntityArchetype* GetArchetype() const;

	inline int GetComponentCount() const;

	inline Entity* Clone() const;

	template<typename...ComponentTypes>
	inline Entity* Extend(ComponentTypes&&... Args) const;

	template<typename...ComponentTypes>
	inline Entity* Extend() const;

	template<typename...ComponentTypes>
	inline Entity* Remove() const;


private:
	bool					mValid;
	uint16_t				mGenID;
	uint16_t				mChunkIndex = 0;
	uint16_t				mBlockIndex = 0;
	EntityComponentStorage*	mStorage = nullptr;
};


template<typename...ComponentTypes>
struct GetComponentIndexesHelperClass;

template<typename ComponentType, typename...Rest>
struct GetComponentIndexesHelperClass<ComponentType, Rest...>
{
	static void Call(EntityArchetype* pArchetype, int* indexArray, int currentIndex)
	{
		indexArray[currentIndex] = pArchetype->GetComponentIndex<ComponentType>();
		GetComponentIndexesHelperClass<Rest...>::Call(pArchetype, indexArray, currentIndex + 1);
	}
};

template<>
struct GetComponentIndexesHelperClass<>
{
	static void Call(EntityArchetype* pArchetype, int* indexArray, int currentIndex) {}
};

class EntityComponentChunk
{
public:
	EntityComponentChunk(uint16_t chunkId, EntityComponentStorage* pStorage, 
		EntityArchetype* pArchetype,
		size_t n, size_t blockSize)
		: mChunkId(chunkId)
		, mEntityComponentStorage(pStorage)
		, mArchetype(pArchetype)
		, mBlockCount((uint16_t)n)
		, mBlockSize(blockSize)
		, mComponentCount((int)pArchetype->mComponentCount)
		, mUsedCount(0)
	{
		//mMem = (byte*)malloc(mBlockCount * mBlockSize);
		void* pMem = GetMemoryAllocator()->Malloc(mBlockCount * mBlockSize);
		//void* pMem = malloc(mBlockCount * mBlockSize);

		mFreeList = reinterpret_cast<uint16_t*>(pMem);
		mEntitiesBuffer = reinterpret_cast<Entity*>(mFreeList + mBlockCount);
		mComponentsBuffer = reinterpret_cast<byte*>(mEntitiesBuffer + mBlockCount);

		// init free list
		for (uint16_t i = 0; i < mBlockCount; i++) {
			mFreeList[i] = i + 1;
		}
		mFreeHead = 0;
		mFreeTail = mBlockCount;

		for (uint16_t i = 0; i < mBlockCount; i++) {
			Entity* pEntity = &mEntitiesBuffer[i];
			pEntity->mValid = false;
			pEntity->mGenID = 0;
			pEntity->mBlockIndex = i;
			pEntity->mChunkIndex = mChunkId;
			pEntity->mStorage = mEntityComponentStorage;
		}
	}

	inline IChunkMemoryAllocator* GetMemoryAllocator();

	Entity* Allocate(bool bCallConstruct)
	{
		assert(mFreeHead != mFreeTail);
		uint16_t head = mFreeHead;
		mFreeHead = mFreeList[head];
		Entity* pEntity = &mEntitiesBuffer[head];
		pEntity->mValid = true;
		pEntity->mGenID = (pEntity->mGenID + 1) & 0x0000FFFF; // mGenID just has 16 bits
		assert(pEntity->mBlockIndex == head);
		// construct components
		if (bCallConstruct)
			ConstructComponents(pEntity);
		mUsedCount++;
		return pEntity;
	}

	void ConstructComponents(Entity* pEntity)
	{
		for (int i = 0; i < mComponentCount; i++) {
			byte* pMem = GetComponentByIndex(pEntity, i);
			ComponentConstructor* constructor = mArchetype->mComponentConstructors[i];
			(*constructor)(pMem);
		}
	}

	void Deallocate(Entity* pEntity, bool bCallDestructor)
	{
		assert(pEntity->mChunkIndex == mChunkId);
		if (bCallDestructor)
			DestructComponents(pEntity);
		mFreeList[pEntity->mBlockIndex] = mFreeHead;
		mFreeHead = pEntity->mBlockIndex;
		pEntity->mValid = false;
		mUsedCount--;
	}

	void DestructComponents(Entity* pEntity)
	{
		for (int i = 0; i < mComponentCount; i++) {
			byte* pMem = GetComponentByIndex(pEntity, i);
			ComponentDestructor* destructor = mArchetype->mComponentDestructors[i];
			(*destructor)(pMem);
		}
	}

	bool IsFull() const 
	{
		return mFreeHead == mFreeTail;
	}

	Entity* GetEntity(uint16_t blockIndex)
	{
		return &mEntitiesBuffer[blockIndex];
	}

	static size_t CalculateBlockSize(EntityArchetype* pArchetype)
	{
		int n = (int)pArchetype->mComponentCount;
		size_t blockSize = 0;
		blockSize += sizeof(uint16_t); // freeList
		blockSize += sizeof(Entity); // entity
		// all components
		for (int i = 0; i < n; i++) {
			blockSize += pArchetype->mComponentSizes[i];
		}
		return blockSize;
	}

	template<typename ComponentType>
	ComponentType* GetComponent(Entity* pEntity)
	{
		int index = mArchetype->GetComponentIndex<ComponentType>();
		if (index == INVALID_COMPONENT_INDEX)
			return nullptr;
		size_t offset = mArchetype->mComponentOffsets[index];
		size_t size = mArchetype->mComponentSizes[index];
		return reinterpret_cast<ComponentType*>(mComponentsBuffer + (offset * mBlockCount) + (size * pEntity->mBlockIndex));
	}

	template<typename ComponentType>
	const ComponentType* GetComponent(const Entity* pEntity) const
	{
		int index = mArchetype->GetComponentIndex<ComponentType>();
		if (index == INVALID_COMPONENT_INDEX)
			return nullptr;
		size_t offset = mArchetype->mComponentOffsets[index];
		size_t size = mArchetype->mComponentSizes[index];
		return reinterpret_cast<const ComponentType*>(mComponentsBuffer + (offset * mBlockCount) + (size * pEntity->mBlockIndex));
	}

	template<typename T = byte>
	T* GetComponentByIndex(Entity* pEntity, int index)
	{
		assert(index < mComponentCount);
		size_t offset = mArchetype->mComponentOffsets[index];
		size_t size = mArchetype->mComponentSizes[index];
		return reinterpret_cast<T*>(mComponentsBuffer + (offset * mBlockCount) + (size * pEntity->mBlockIndex));
	}

	template<typename T = byte>
	const T* GetComponentByIndex(const Entity* pEntity, int index) const
	{
		assert(index < mComponentCount);
		size_t offset = mArchetype->mComponentOffsets[index];
		size_t size = mArchetype->mComponentSizes[index];
		return reinterpret_cast<const T*>(mComponentsBuffer + (offset * mBlockCount) + (size * pEntity->mBlockIndex));
	}

	template<typename T = byte>
	T* GetComponentByTypeID(Entity* pEntity, ComponentTypeID componentTypeID)
	{
		int index = mArchetype->GetComponentIndex(componentTypeID);
		if (index == INVALID_COMPONENT_INDEX) {
			return nullptr;
		}
		return GetComponentByIndex<T>(pEntity, index);
	}

	template<typename T = byte>
	const T* GetComponentByTypeID(const Entity* pEntity, ComponentTypeID componentTypeID) const
	{
		int index = mArchetype->GetComponentIndex(componentTypeID);
		if (index == INVALID_COMPONENT_INDEX) {
			return nullptr;
		}
		return GetComponentByIndex<T>(pEntity, index);
	}

	template<typename TupleType, int startIndex, int currentIndex, typename...ComponentTypes>
	struct GetComponentsTupleHelperClass;

	template<typename TupleType, int startIndex, int currentIndex, typename ComponentType, typename...Rest>
	struct GetComponentsTupleHelperClass<TupleType, startIndex, currentIndex, ComponentType, Rest...>
	{
		static void Call(TupleType& componentsTuple, byte* componentsByteArray[]) {
			// use 'currentIndex + 1', since position 0 is reserved for Entity pointer
			std::get<currentIndex + startIndex>(componentsTuple) = reinterpret_cast<ComponentType*>(componentsByteArray[currentIndex]);
			GetComponentsTupleHelperClass<TupleType, startIndex, currentIndex + 1, Rest...>::Call(componentsTuple, componentsByteArray);
		}
	};

	template<typename TupleType, int startIndex, int currentIndex>
	struct GetComponentsTupleHelperClass<TupleType, startIndex, currentIndex>
	{
		static void Call(TupleType& componentsTuple, byte* componentsByteArray[]) {}
	};

	template<int startIndex, int currentIndex, typename TupleType>
	void AdvanceComponentsTuple(TupleType& componentTuple)
	{
		if constexpr (currentIndex >= startIndex)
		{
			std::get<currentIndex>(componentTuple) += 1;
			AdvanceComponentsTuple<startIndex, currentIndex - 1>(componentTuple);
		}
	}

	template<typename...ComponentTypes, typename F, typename RuntimeArg>
	void ForEach(F&& f, RuntimeArg* pArg, int startBlockIndex, int endBlockIndex)
	{
		using ComponentTuple = std::tuple<RuntimeArg*, Entity*, std::decay_t<ComponentTypes>*...>;
		const int n = sizeof...(ComponentTypes);
		int componentIndexes[n];
		GetComponentIndexesHelperClass<ComponentTypes...>::Call(mArchetype, componentIndexes, 0);
		Entity* pEntity = &mEntitiesBuffer[startBlockIndex];

		byte* componentsBytes[n] = { 0 };
		for (int i = 0; i < n; i++) {
			int index = componentIndexes[i];
			size_t offset = mArchetype->mComponentOffsets[index];
			size_t componentSize = mArchetype->mComponentSizes[index];
			componentsBytes[i] = mComponentsBuffer + mBlockCount * offset + startBlockIndex * componentSize;
		}

		ComponentTuple componentTuple;
		std::get<0>(componentTuple) = pArg;
		std::get<1>(componentTuple) = pEntity;
		GetComponentsTupleHelperClass<ComponentTuple, 2, 0, std::decay_t<ComponentTypes>...>::Call(componentTuple, componentsBytes);

		for (int i = 0; i < endBlockIndex - startBlockIndex; i++) {
			if (pEntity->IsValid()) {
				std::apply(std::forward<F>(f), componentTuple);
			}
			AdvanceComponentsTuple<2, n + 1>(componentTuple);
			pEntity++;
			std::get<1>(componentTuple) = pEntity;
		}
	}

	template<typename...ComponentTypes, typename F>
	void ForEach(F&& f, int startBlockIndex, int endBlockIndex)
	{
		using ComponentTuple = std::tuple<Entity*, std::decay_t<ComponentTypes>*...>;
		const int n = sizeof...(ComponentTypes);
		int componentIndexes[n];
		GetComponentIndexesHelperClass<ComponentTypes...>::Call(mArchetype, componentIndexes, 0);
		Entity* pEntity = &mEntitiesBuffer[startBlockIndex];

		byte* componentsBytes[n] = { 0 };
		for (int i = 0; i < n; i++) {
			int index = componentIndexes[i];
			size_t offset = mArchetype->mComponentOffsets[index];
			size_t componentSize = mArchetype->mComponentSizes[index];
			componentsBytes[i] = mComponentsBuffer + mBlockCount * offset + startBlockIndex * componentSize;
		}

		ComponentTuple componentTuple;
		std::get<0>(componentTuple) = pEntity;
		GetComponentsTupleHelperClass<ComponentTuple, 1, 0, std::decay_t<ComponentTypes>...>::Call(componentTuple, componentsBytes);

		for (int i = 0; i < endBlockIndex - startBlockIndex; i++) {
			if (pEntity->IsValid()) {
				std::apply(std::forward<F>(f), componentTuple);
			}
			AdvanceComponentsTuple<1, n>(componentTuple);
			pEntity++;
			std::get<0>(componentTuple) = pEntity;
		}
	}


	template<typename F, typename...ComponentTypes>
	void ForEach(F&& f, int* componentIndexes)
	{
		using ComponentTuple = std::tuple<Entity*, std::decay_t<ComponentTypes>*...>;
		const int n = sizeof...(ComponentTypes);
		Entity* pEntity = &mEntitiesBuffer[0];

		byte* componentsBytes[n] = { 0 };
		for (int i = 0; i < n; i++) {
			int index = componentIndexes[i];
			size_t offset = mArchetype->mComponentOffsets[index];
			componentsBytes[i] = mComponentsBuffer + mBlockCount * offset;
		}
	
		ComponentTuple componentTuple;
		std::get<0>(componentTuple) = pEntity;
		GetComponentsTupleHelperClass<ComponentTuple, 1, 0, std::decay_t<ComponentTypes>...>::Call(componentTuple, componentsBytes);

		for (int i = 0; i < mBlockCount; i++)
		{
			if (pEntity->IsValid()) {
				std::apply(std::forward<F>(f), componentTuple);
			}
			AdvanceComponentsTuple<1, n>(componentTuple);
			pEntity++;
			std::get<0>(componentTuple) = pEntity;
		}
	}

	bool IsEmpty() const { return mUsedCount == 0; }

	~EntityComponentChunk()
	{
		// release all entities
		for (int i = 0; i < mBlockCount; i++)
		{
			Entity* pEntity = &mEntitiesBuffer[i];
			if (pEntity->IsValid())
			{
				pEntity->Release();
			}
		}

		if (mFreeList) {
			//free((void*)mFreeList);
			GetMemoryAllocator()->Free(mFreeList);
			mFreeList = nullptr;
			mEntitiesBuffer = nullptr;
			mComponentsBuffer = nullptr;
		}
	}
private:
	uint16_t			mChunkId;
	EntityComponentStorage*	mEntityComponentStorage;
	EntityArchetype*	mArchetype;
	uint16_t			mBlockCount;
	size_t				mBlockSize;
	int					mComponentCount;
	uint16_t			mUsedCount = 0;

	uint16_t			mFreeHead;
	uint16_t			mFreeTail;

	uint16_t*			mFreeList = nullptr;
	Entity*				mEntitiesBuffer = nullptr;
	byte*				mComponentsBuffer = nullptr;
	//byte*				mMem = nullptr;
};



class EntityComponentStorage
{
	//using EntityComponentChunkPtr = EntityComponentChunk * ;
	friend class Entity;
	template<typename...T>
	friend class ParallelJobBase;
public:
	EntityComponentStorage(EntityContext* pContext, uint16_t index, EntityArchetype* pArchetype)
		: mContext(pContext)
		, mIndex(index)
		, mArchetype(pArchetype)
		, mChunkArrayCapacity(16)
	{
		mComponentCountPerEntity = (int)pArchetype->mComponentCount;
		mEntityBlockSize = EntityComponentChunk::CalculateBlockSize(pArchetype);

		mChunkSize = MAX_STORAGE_CHUNK_SIZE;
		mEntityCountPerChunk = mChunkSize / mEntityBlockSize;
		if (mEntityCountPerChunk > MAX_ENTITY_COUNT_PER_CHUNK)
		{
			mEntityCountPerChunk = MAX_ENTITY_COUNT_PER_CHUNK;
			mChunkSize = MAX_ENTITY_COUNT_PER_CHUNK * mEntityBlockSize;
		}

		//mChunkFreeList = (uint16_t*)malloc(sizeof(uint16_t) * mChunkArrayCapacity);
		//mChunks = (EntityComponentChunk*)malloc(sizeof(EntityComponentChunk) * mChunkArrayCapacity);
		mChunkFreeList = (uint16_t*)GetChunkMemoryAllocator()->Malloc(sizeof(uint16_t) * mChunkArrayCapacity);
		mChunks = (EntityComponentChunk*)GetChunkMemoryAllocator()->Malloc(sizeof(EntityComponentChunk) * mChunkArrayCapacity);
		
		memset(mChunkFreeList, 0, sizeof(sizeof(uint16_t) * mChunkArrayCapacity));
		memset(mChunks, 0, sizeof(EntityComponentChunk) * mChunkArrayCapacity);
		mChunkFreeHead = mChunkCount = 0;

	}

	Entity* Allocate(bool bCallConstructor)
	{
		// find chunk that is not full
		//uint16_t freeChunkIndex = -1;
		if (mChunkFreeHead == mChunkCount) // free list is full
		{
			// don't have enough capacity
			if (mChunkCount >= mChunkArrayCapacity) {
				IncreaseCapacity();
			}
			EntityComponentChunk* pChunk = &mChunks[mChunkCount];
			//pChunk->EntityComponentChunk::EntityComponentChunk(mChunkCount, this, mArchetype, mEntityCountPerChunk, mEntityBlockSize);
			new (pChunk) EntityComponentChunk(mChunkCount, this, mArchetype, mEntityCountPerChunk, mEntityBlockSize);
			mChunkCount += 1;
			mChunkFreeList[mChunkFreeHead] = mChunkCount;
		}
		EntityComponentChunk* pChunk = &mChunks[mChunkFreeHead];
		Entity* pEntity = pChunk->Allocate(bCallConstructor);
		if (pChunk->IsFull()) {
			mChunkFreeHead = mChunkFreeList[mChunkFreeHead];
		}
		return pEntity;
	}

	void Deallocate(Entity* pEntity, bool bCallDestructor)
	{
		EntityComponentChunk* pChunk = &mChunks[pEntity->mChunkIndex];
		bool bFull = pChunk->IsFull();
		pChunk->Deallocate(pEntity, bCallDestructor);
		if (bFull) {
			mChunkFreeList[pEntity->mChunkIndex] = mChunkFreeHead;
			mChunkFreeHead = pEntity->mChunkIndex;
		}
	}

	Entity* GetEntity(uint16_t chunkIndex, uint16_t blockIndex)
	{
		if (chunkIndex >= mChunkCount)
			return nullptr;

		Entity* pEntity = mChunks[chunkIndex].GetEntity(blockIndex);
		if (!pEntity->mValid)
			return nullptr;
		return pEntity;
	}

	Entity* CloneEntity(const Entity* pEntity)
	{
		assert(mArchetype == pEntity->GetArchetype());
		Entity* pClonedEntity = Allocate(false);
		for (int i = 0; i < mComponentCountPerEntity; i++)
		{
			const byte* pSrcMem = GetComponentByIndex(pEntity, i);
			byte* pDstMem = GetComponentByIndex(pClonedEntity, i);
			ComponentAssignment* pAssignment = mArchetype->mComponentAssignments[i];
			(*pAssignment)(pDstMem, pSrcMem);
		}
		return pClonedEntity;
	}

	~EntityComponentStorage()
	{
		for (uint16_t i = 0; i < mChunkCount; i++)
		{
			mChunks[i].~EntityComponentChunk();
		}
		//free(mChunkFreeList);
		//free(mChunks);
		GetChunkMemoryAllocator()->Free(mChunkFreeList);
		GetChunkMemoryAllocator()->Free(mChunks);
	}

	EntityArchetype* GetArchetype() { return mArchetype; }
	const EntityArchetype* GetArchetype() const { return mArchetype; }

	template<typename ComponentType>
	ComponentType* GetComponent(Entity* pEntity)
	{
		return mChunks[pEntity->mChunkIndex].GetComponent<ComponentType>(pEntity);
	}

	template<typename ComponentType>
	const ComponentType* GetComponent(const Entity* pEntity) const
	{
		const EntityComponentChunk* pChunk = &mChunks[pEntity->mChunkIndex];
		return pChunk->GetComponent<ComponentType>(pEntity);
	}

	template<typename T = byte>
	T* GetComponentByIndex(Entity* pEntity, int index)
	{
		return mChunks[pEntity->mChunkIndex].GetComponentByIndex<T>(pEntity, index);
	}

	template<typename T = byte>
	const T* GetComponentByIndex(const Entity* pEntity, int index) const
	{
		return mChunks[pEntity->mChunkIndex].GetComponentByIndex<T>(pEntity, index);
	}

	template<typename T = byte>
	T* GetComponentByTypeID(Entity* pEntity, ComponentTypeID componentTypeID)
	{
		return mChunks[pEntity->mChunkIndex].GetComponentByTypeID<T>(pEntity, componentTypeID);
	}

	template<typename T = byte>
	const T* GetComponentByTypeID(const Entity* pEntity, ComponentTypeID componentTypeID) const
	{
		return mChunks[pEntity->mChunkIndex].GetComponentByTypeID<T>(pEntity, componentTypeID);
	}

	uint16_t GetIndex() const { return mIndex; }

	template<typename F, typename...ComponentTypes>
	void ForEach(F&& f)
	{
		const int n = sizeof...(ComponentTypes);
		int componentIndexes[n];
		GetComponentIndexesHelperClass<ComponentTypes...>::Call(mArchetype, componentIndexes, 0);
		
		for (int i = 0; i < mChunkCount; i++) {
			if (!mChunks[i].IsEmpty()) {
				mChunks[i].ForEach<F, ComponentTypes...>(std::forward<F>(f), componentIndexes);
			}
		}
	}

	EntityComponentChunk* GetChunk(int index) 
	{
		return &mChunks[index];
	}

	inline IChunkMemoryAllocator* GetChunkMemoryAllocator();

private:

	void IncreaseCapacity()
	{
		mChunkArrayCapacity *= 2;
		//mChunkFreeList = (uint16_t*)realloc(mChunkFreeList, sizeof(uint16_t) * mChunkArrayCapacity);
		//mChunks = (EntityComponentChunk*)realloc(mChunks, sizeof(EntityComponentChunk) * mChunkArrayCapacity);
		mChunkFreeList = (uint16_t*)GetChunkMemoryAllocator()->Realloc(mChunkFreeList, sizeof(uint16_t) * mChunkArrayCapacity);
		mChunks = (EntityComponentChunk*)GetChunkMemoryAllocator()->Realloc(mChunks, sizeof(EntityComponentChunk) * mChunkArrayCapacity);
	}

private:
	EntityContext*				mContext;
	uint16_t					mIndex;
	EntityArchetype*			mArchetype;
	size_t						mEntityCountPerChunk;
	int							mComponentCountPerEntity;
	size_t						mChunkSize;
	size_t						mEntityBlockSize;

	uint16_t*					mChunkFreeList;
	EntityComponentChunk*		mChunks;
	uint16_t					mChunkArrayCapacity;
	uint16_t					mChunkCount;
	uint16_t					mChunkFreeHead;

	//uint16_t					mChunkFreeTail;

	//ComponentHash				mComponentTypeIds[MAX_COMPONENT_COUNT_PER_ENTITY] = { 0 };
	//size_t					mComponentSizes[MAX_COMPONENT_COUNT_PER_ENTITY] = { 0 };
	//size_t					mComponentOffsets[MAX_COMPONENT_COUNT_PER_ENTITY] = { 0 };
};

#if EVENT_INDEX_TABLE_TYPE == 0
using EventIndexTable = LinearComponentIndexTable<MAX_EVENT_COUNT>;
#elif EVENT_INDEX_TABLE_TYPE == 1
using EventIndexTable = HashMapComponentIndexTable;
#elif EVENT_INDEX_TABLE_TYPE == 2
#if USE_CUSTOM_EVENT_TYPE_ID
using EventIndexTable = DirectComponentIndexTable<MAX_EVENT_COUNT>;
#else
using EventIndexTable = LinearComponentIndexTable<MAX_EVENT_COUNT>;
#endif
#endif

struct CreateEntityEvent : EventBaseClass(CreateEntityEvent, MAX_EVENT_COUNT - 1)
{
	Entity* pEntity = nullptr;
	CreateEntityEvent() {}
	CreateEntityEvent(Entity* pEntity) : pEntity(pEntity){}
};

struct DeleteEntityEvent : EventBaseClass(DeleteEntityEvent, MAX_EVENT_COUNT - 2)
{
	Entity* pEntity = nullptr;
	DeleteEntityEvent() {}
	DeleteEntityEvent(Entity* pEntity) : pEntity(pEntity) {}
};

using EventCallbackFunction = std::function<void(const void*)>;
using EventCallbackHandle = uint64_t;
struct EventCallbackObject
{
	EventCallbackHandle			id;
	EventCallbackFunction		callback;
};

using EventCallbackList = std::list<EventCallbackObject>;

class EventManager
{
public:
	template<typename EventType, typename F>
	EventCallbackHandle Subscrible(F&& cb)
	{
		int index = mIndexTable.Get<EventType>();
		if (index == INVALID_EVENT_INDEX) {
			index = mIndexTable.Add<EventType>();
		}
		uint32_t genid = GenID();
		EventCallbackObject callbackObject;
		callbackObject.id = genid;
		callbackObject.callback = [&cb](const void* pMem) {
			const EventType* pEvent = reinterpret_cast<const EventType*>(pMem);
			cb(pEvent);
		};
		mEventCallbacks[index].push_back(std::move(callbackObject));
		return ((uint64_t)index << 32) | genid;
	}

	void Unsubscribe(EventCallbackHandle handle) 
	{
		uint32_t index = (uint32_t)(handle >> 32);
		uint32_t id = handle & 0x0ffffffff;
		EventCallbackList& callbackList = mEventCallbacks[index];
		for (auto it = callbackList.begin(); it != callbackList.end(); it++) {
			if (it->id == id) {
				callbackList.erase(it);
				break;
			}
		}
	}

	template<typename EventType>
	void Unsubscribe()
	{
		int index = mIndexTable.Get<EventType>();
		if (index == INVALID_EVENT_INDEX)
			return;
		EventCallbackList& callbackList = mEventCallbacks[index];
		callbackList.clear();
	}

	template<typename EventType>
	void TriggerEvent(EventType& evt)
	{
		int index = mIndexTable.Get<EventType>();
		if (index == INVALID_EVENT_INDEX) {
			return;
		}
		EventCallbackList& callbackList = mEventCallbacks[index];
		for (auto it = callbackList.begin(); it != callbackList.end(); it++) {
			it->callback(&evt);
		}
	}

	void Release()
	{
		delete this;
	}

private:
	static uint32_t GenID()
	{
		static uint32_t id = 0;
		return ++id;
	}

private:
	EventIndexTable		mIndexTable;
	EventCallbackList	mEventCallbacks[MAX_EVENT_COUNT];
};


template<typename...ComponentTypes>
struct ComponentTypesHelperClass;

template<typename ComponentType, typename...Rest>
struct ComponentTypesHelperClass<ComponentType, Rest...>
{
	static void ConstructEntity(Entity* pEntity, ComponentType&& data, Rest&&... args)
	{
		using T = std::decay_t<ComponentType>;
		if constexpr (!is_type_duplicate_v<T, Rest...>)
		{
			T* pComponentMem = pEntity->GetComponent<T>();
			if (pComponentMem) {
				new (pComponentMem) T(std::forward<ComponentType>(data));
			}
		}
		ComponentTypesHelperClass<Rest...>::ConstructEntity(pEntity, std::forward<Rest>(args)...);
	}

	static void ConstructEntity(Entity* pEntity)
	{
		using T = std::decay_t<ComponentType>;
		if constexpr (!is_type_duplicate_v<T, Rest...>)
		{
			T* pComponentMem = pEntity->GetComponent<T>();
			if (pComponentMem) {
				new (pComponentMem) T();
			}
		}
		ComponentTypesHelperClass<Rest...>::ConstructEntity(pEntity);
	}

	static bool Contain(ComponentTypeID componentTypeId)
	{
		return componentTypeId == std::decay_t<ComponentType>::type_id() || ComponentTypesHelperClass<Rest...>::Contain(componentTypeId);
	}
};

template<>
struct ComponentTypesHelperClass<>
{
	static void ConstructEntity(Entity* pEntity) {}
	static bool Contain(ComponentTypeID) { return false; }
};

class EntityContext
{
	friend class World;
	friend class EntityArchetype;
	friend class Entity;

	template<typename...ComponentTypes>
	friend class ParallelJobBase;
public:
	EntityContext(int id, World* pWorld, EntityArchetypeManager* pArchetypeManager)
		:mContextId(id), mWorld(pWorld), mArchetypeManager(pArchetypeManager)
	{

	}

	Entity* CreateEntity(EntityArchetype* pArchetype)
	{
		EntityComponentStorage* pStorage = GetEntityComponentStorage(pArchetype);
		Entity* pEntity = pStorage->Allocate(true);
		OnEntityCreated(pEntity);
		return pEntity;
	}

	template<typename...Args>
	Entity* CreateEntity(EntityArchetype* pArchetype, Args&&...args)
	{
		EntityComponentStorage* pStorage = GetEntityComponentStorage(pArchetype);
		Entity* pEntity = pStorage->Allocate(false);
		int componentCount = pArchetype->mComponentCount;
		
		// construct those components whose values aren't provided through parameters
		for (int i = 0; i < componentCount; i++) {
			ComponentTypeID componentTypeId = pArchetype->mComponentTypeIds[i];
			if (!ComponentTypesHelperClass<Args...>::Contain(componentTypeId))
			{
				byte* pComponentBytes = pStorage->GetComponentByIndex(pEntity, i);
				ComponentConstructor* pConstructor = pArchetype->mComponentConstructors[i];
				(*pConstructor)(pComponentBytes);
			}
		}

		// construct those components in parameters
		ComponentTypesHelperClass<Args...>::ConstructEntity(pEntity, std::forward<Args>(args)...);
		OnEntityCreated(pEntity);
		return pEntity;
	}

	template<typename...ComponentTypes>
	Entity* CreateEntity()
	{
		EntityArchetype* pArchetype = mArchetypeManager->CreateArchetype<ComponentTypes...>();
		return CreateEntity(pArchetype);
	}

	template<typename...ComponentTypes, typename...Args>
	Entity* CreateEntity(Args&&... args)
	{
		// 1,if component types are given, then use these types to create entity.
		// then the component values to initialize entity are given as arguments
		// the types whose values are not specified in arguments will be given a default value 
		// by calling the component's default constructor.

		// 2,if component types are not given, the deduce types from the arguments

		if constexpr (sizeof...(ComponentTypes) != 0)
		{
			EntityArchetype* pArchetype = mArchetypeManager->CreateArchetype<ComponentTypes...>();
			return CreateEntity(pArchetype, std::forward<Args>(args)...);
		}
		else
		{
			EntityArchetype* pArchetype = mArchetypeManager->CreateArchetype<std::decay_t<Args>...>();
			EntityComponentStorage* pStorage = GetEntityComponentStorage(pArchetype);
			Entity* pEntity = pStorage->Allocate(false);

			ComponentTypesHelperClass<Args...>::ConstructEntity(pEntity, std::forward<Args>(args)...);
			OnEntityCreated(pEntity);
			return pEntity;
		}
	}

	//template<typename...ComponentTypes>
	//Entity* CreateEntity(ComponentTypes&&... args)
	//{
	//	EntityArchetype* pArchetype = mArchetypeManager->CreateArchetype<std::decay_t<ComponentTypes>...>();
	//	EntityComponentStorage* pStorage = GetEntityComponentStorage(pArchetype);
	//	Entity* pEntity = pStorage->Allocate(false);

	//	ComponentTypesHelperClass<ComponentTypes...>::ConstructEntity(pEntity, std::forward<ComponentTypes>(args)...);
	//	OnEntityCreated(pEntity);
	//	return pEntity;
	//}

	inline void Release();
	
	EntityComponentStorage* GetEntityComponentStorage(uint16_t index)
	{
		assert(index < mEntityComponentStorageList.size());
		return mEntityComponentStorageList[index];
	}

	Entity* GetEntity(EntityID eid)
	{
		uint16_t genid;
		uint8_t contextId;
		uint16_t chunkIndex, blockIndex, storageIndex;
		Entity::ParseEntityID(eid, &genid, &contextId, &storageIndex, &chunkIndex, &blockIndex);
		if (storageIndex >= mEntityComponentStorageList.size()) {
			return nullptr;
		}
		EntityComponentStorage* pStorage = mEntityComponentStorageList[storageIndex];
		Entity* pEntity = pStorage->GetEntity(chunkIndex, blockIndex);
		if (pEntity == nullptr)
			return nullptr;
		if (pEntity->mGenID != genid) {
			return nullptr;
		}
		return pEntity;
	}

	EntityComponentStorage* GetEntityComponentStorage(EntityArchetype* pArchetype) 
	{
		EntityComponentStorage* pStorage = pArchetype->mStoragesInContext[mContextId];
		if (!pStorage) {
			uint16_t index = (uint16_t)mEntityComponentStorageList.size();
			assert(index < MAX_STORAGE_COUNT_PER_CONTEXT);
			pStorage = new EntityComponentStorage(this, index, pArchetype);
			mEntityComponentStorageList.push_back(pStorage);
			pArchetype->mStoragesInContext[mContextId] = pStorage;
		}
		return pStorage;
	}

	template<typename... ComponentTypes>
	Entity* ExtendEntity(const Entity* pSrcEntity, ComponentTypes&&... args)
	{
		const EntityArchetype* pSrcArchetype = pSrcEntity->GetArchetype();
		if (pSrcArchetype->ContainAnyComponents<ComponentTypes...>()) {
			return nullptr;
		}
		
		EntityArchetype* pDstArchetype = mArchetypeManager->AddComponents<std::decay_t<ComponentTypes>...>(pSrcArchetype);
		EntityComponentStorage* pDstStorage = GetEntityComponentStorage(pDstArchetype);
		Entity* pDstEntity = pDstStorage->Allocate(false);
		CopyEntityData(pDstEntity, pSrcEntity);
		// construct new added components
		ComponentTypesHelperClass<ComponentTypes...>::ConstructEntity(pDstEntity, std::forward<ComponentTypes>(args)...);
		OnEntityCreated(pDstEntity);
		return pDstEntity;
	}

	template<typename... ComponentTypes>
	Entity* ExtendEntity(const Entity* pSrcEntity)
	{
		const EntityArchetype* pSrcArchetype = pSrcEntity->GetArchetype();
		if (pSrcArchetype->ContainAnyComponents<ComponentTypes...>()) {
			return nullptr;
		}

		EntityArchetype* pDstArchetype = mArchetypeManager->AddComponents<std::decay_t<ComponentTypes>...>(pSrcArchetype);
		EntityComponentStorage* pDstStorage = GetEntityComponentStorage(pDstArchetype);
		Entity* pDstEntity = pDstStorage->Allocate(false);
		CopyEntityData(pDstEntity, pSrcEntity);
		// construct new added components
		ComponentTypesHelperClass<ComponentTypes...>::ConstructEntity(pDstEntity);
		OnEntityCreated(pDstEntity);
		return pDstEntity;
	}

	template<typename...ComponentTypes>
	Entity* RemoveComponentsFromEntity(const Entity* pSrcEntity) 
	{
		const EntityArchetype* pSrcArchetype = pSrcEntity->GetArchetype();
		if (!pSrcArchetype->ContainAllComponents<ComponentTypes...>()) {
			return nullptr;
		}
		EntityArchetype* pDstArchetype = mArchetypeManager->RemoveComponents<std::decay_t<ComponentTypes>...>(pSrcArchetype);
		EntityComponentStorage* pDstStorage = GetEntityComponentStorage(pDstArchetype);
		Entity* pDstEntity = pDstStorage->Allocate(false);
		CopyEntityData(pDstEntity, pSrcEntity);
		OnEntityCreated(pDstEntity);
		return pDstEntity;
	}

	template<typename...ComponentTypes, typename F>
	void ForEach(F&& f)
	{
		for (EntityComponentStorage* pStorage : mEntityComponentStorageList) {
			if (pStorage->GetArchetype()->ContainAllComponents<ComponentTypes...>()) {
				pStorage->ForEach<F, ComponentTypes...>(std::forward<F>(f));
			}
		}
	}

	void SetEventManager(EventManager* pEventManager) { mEventManager = pEventManager; }
	EventManager* GetEventManager() { return mEventManager; }
	const EventManager* GetEventManager() const { return mEventManager; }

	template<typename EventType>
	void TriggerEvent(const EventType& evt)
	{
		if (mEventManager != nullptr)
		{
			mEventManager->TriggerEvent(evt);
		}
	}

	World* GetWorld() { return mWorld; }
	int GetContextId() { return mContextId; }

private:
	
	void OnEntityCreated(Entity* pEntity)
	{
		CreateEntityEvent evt(pEntity);
		TriggerEvent(evt);
	}

	void OnEntityDeleted(Entity* pEntity)
	{
		DeleteEntityEvent evt(pEntity);
		TriggerEvent(evt);
	}

public:

	void CopyEntityData(Entity* pDstEntity, const Entity* pSrcEntity)
	{
		//int srcComponetCount = pSrcEntity->GetComponentCount();
		const EntityArchetype* pSrcArchetype = pSrcEntity->GetArchetype();
		for (int i = 0; i < pSrcArchetype->mComponentCount; i++) {
			ComponentTypeID componentTypeID = pSrcArchetype->mComponentTypeIds[i];
			byte* pDstComponentMem = pDstEntity->GetComponentByTypeID(componentTypeID);
			if (pDstComponentMem) {
				const byte* pSrcComponentMem = pSrcEntity->GetComponentByIndex(i);
				ComponentAssignment* pAssignment = pSrcArchetype->mComponentAssignments[i];
				(*pAssignment)(pDstComponentMem, pSrcComponentMem);
			}
		}
	}

private:
	int					mContextId;
	std::vector<EntityComponentStorage*>		mEntityComponentStorageList;
	World*						mWorld;
	EntityArchetypeManager*		mArchetypeManager;
	EventManager*				mEventManager = nullptr;
};


struct ParallelJobChunkSegement
{
	EntityComponentChunk*		pChunk = nullptr;
	int							RangeStart; // inclusive
	int							RangeEnd; // exclusive
};

using ParallelJobChunkSegementList = std::list<ParallelJobChunkSegement>;

enum class ParallJobState
{
	Unprepared,
	Prepared,
	Executing,
};

template<typename...ComponentTypes>
class ParallelJobBase
{
public:
	void Prepare(EntityContext* pContext, int threadCount)
	{
		assert(threadCount <= MAX_THREAD_COUNT);
		assert(mState != ParallJobState::Executing);
		mContext = pContext;
		mThreadCount = threadCount;
		mStartCounter.store(0);
		mCompleteCounter.store(0);

		for (int i = 0; i < MAX_THREAD_COUNT; i++) {
			mDividedJobChunkSegmentArray[i].clear();
		}

		int threadTaskCounts[MAX_THREAD_COUNT] = { 0 };
		std::vector<EntityComponentStorage*> vecStorages;
		for (EntityComponentStorage* pStorage : pContext->mEntityComponentStorageList) {
			if (pStorage->GetArchetype()->ContainAllComponents<ComponentTypes...>()) {
				vecStorages.push_back(pStorage);
			}
		}
		if (vecStorages.empty())
			return;

#if DIVIDE_PARALLEL_JOB_METHOD == 0
		// sort storages according to their chunkSize
		std::sort(vecStorages.begin(), vecStorages.end(), [](EntityComponentStorage* a, EntityComponentStorage* b) {
			return a->mEntityCountPerChunk > b->mEntityCountPerChunk;
		});

		for (EntityComponentStorage* pStorage : vecStorages) {
			uint16_t chunkCount = pStorage->mChunkCount;
			for (uint16_t i = 0; i < chunkCount; i++) {
				auto pChunk = pStorage->GetChunk((int)i);
				auto threadIndexSelected = std::min_element(threadTaskCounts, threadTaskCounts + threadCount) - threadTaskCounts;
				ParallelJobChunkSegement chunkSegment;
				chunkSegment.pChunk = pChunk;
				chunkSegment.RangeStart = 0;
				chunkSegment.RangeEnd = (int)(pStorage->mEntityCountPerChunk);
				mDividedJobChunkSegmentArray[threadIndexSelected].push_back(chunkSegment);
				threadTaskCounts[threadIndexSelected] += (int)(pStorage->mEntityCountPerChunk);
			}
		}
#elif DIVIDE_PARALLEL_JOB_METHOD == 1
		// divide one chunk only if its entities exceed this threshold
		//int entityCountThreshold = 16 * threadCount;
		for (EntityComponentStorage* pStorage : vecStorages)
		{
			int chunkCount = (int)(pStorage->mChunkCount);
			int entityCountPerChunk = (int)pStorage->mEntityCountPerChunk;
			// divide the entire chunk into many segments, each segment per thread
			int entityCountPerThread = entityCountPerChunk / threadCount;
			entityCountPerThread = entityCountPerThread / 16 * 16;
			
			for (int i = 0; i < chunkCount; i++)
			{
				// the last segment may have more than 'entityCountPerThread' entities
				// we give the last segment to the specific thread with the least tasks currently. 
				auto pChunk = pStorage->GetChunk(i);

				// find the thread with the least tasks
				auto threadIndexSelected = std::min_element(threadTaskCounts, threadTaskCounts + threadCount) - threadTaskCounts;
				int currentStartIndex = 0;

				for (int j = 0; j < threadCount; j++)
				{
					ParallelJobChunkSegement chunkSegment;
					chunkSegment.pChunk = pChunk;

					// assign the last segment to this particular thread
					if (j == threadIndexSelected)
					{
						chunkSegment.RangeStart = entityCountPerThread * (threadCount - 1);
						chunkSegment.RangeEnd = entityCountPerChunk;
					}
					else
					{
						chunkSegment.RangeStart = currentStartIndex;
						chunkSegment.RangeEnd = currentStartIndex + entityCountPerThread;
						currentStartIndex += entityCountPerThread;
					}

					auto rangeCount = chunkSegment.RangeEnd - chunkSegment.RangeStart;
					if (rangeCount > 0)
					{
						mDividedJobChunkSegmentArray[j].push_back(chunkSegment);
						threadTaskCounts[j] += rangeCount;
					}
				}
			}

		}
#endif
	}
	
	ParallelJobBase GetState() const { return mState; }

protected:
	//std::atomic<int>							mThreadCount = 0;
	//bool										mPrepared;
	ParallJobState								mState = ParallJobState::Unprepared;
	int											mThreadCount = 0;
	std::atomic<int>							mStartCounter = 0;
	std::atomic<int>							mCompleteCounter = 0;
	//std::atomic<int>							mThreadCount = 0;
	EntityContext*								mContext = nullptr;
	ParallelJobChunkSegementList				mDividedJobChunkSegmentArray[MAX_THREAD_COUNT];
};


template<bool IsThreadLocalArgRequired, typename...T>
class ParallelJob;

template<typename ThreadLcoalArg, typename...ComponentTypes>
class ParallelJob<true, ThreadLcoalArg, ComponentTypes...> : public ParallelJobBase<ComponentTypes...>
{
public:
	using base = ParallelJobBase<ComponentTypes...>;

	ParallelJob():mFunction(nullptr){}

	template<typename F>
	ParallelJob(F&& f) : mFunction(std::forward<F>(f))
	{

	}

	template<typename F>
	void SetJobFunction(F&& f)
	{
		mFunction = std::forward<F>(f);
	}

	void Execute(ThreadLcoalArg* pThreadLocalArg);
private:
	std::function<void(ThreadLcoalArg*, Entity*, ComponentTypes*...)>	mFunction;
};

template<typename...ComponentTypes>
class ParallelJob<false, ComponentTypes...> : public ParallelJobBase<ComponentTypes...>
{
public:
	using base = ParallelJobBase<ComponentTypes...>;

	ParallelJob() :mFunction(nullptr) {}

	template<typename F>
	ParallelJob(F&& f) : mFunction(std::forward<F>(f))
	{

	}

	template<typename F>
	void SetJobFunction(F&& f) 
	{
		mFunction = std::forward<F>(f);
	}

	void Execute();
private:
	std::function<void(Entity*, ComponentTypes*...)>		mFunction;
};


void Entity::Release()
{
	assert(mValid);
	this->mStorage->mContext->OnEntityDeleted(this);
	mStorage->Deallocate(this, true);
}

class World
{
	friend class EntityContext;
public:
	static World* GetInstance()
	{
		static World _inst;
		return &_inst;
	}

	EntityContext* CreateContext()
	{
		auto id = FindAvaibableContextId();
		assert(id != -1);
		auto pContext = new EntityContext(id, this, mArchetypeManager);
		mEntityContexts[id] = pContext;
		return pContext;
	}

	template<typename...ComponentTypes, typename F>
	void ForEach(F&& f)
	{
		for (int i = 0; i < MAX_CONTEXT_COUNT; i++)
		{
			EntityContext* pContext = mEntityContexts[i];
			if (pContext)
			{
				pContext->ForEach<ComponentTypes...>(std::forward<F>(f));
			}
		}
	}

	~World()
	{
		SAFE_DELETE(mArchetypeManager);
	}

	template<typename...ComponentTypes>
	EntityArchetype* CreateArchetype()
	{
		return mArchetypeManager->CreateArchetype<ComponentTypes...>();
	}

	EventManager* CreateEventManager() 
	{
		return new EventManager();
	}

	IChunkMemoryAllocator* GetChunkMemoryAllocator() { return m_pChunkMemoryAllocator; }
	void SetChunkMemoryAllocator(IChunkMemoryAllocator* pAllocator) 
	{
		if (pAllocator == nullptr)
			m_pChunkMemoryAllocator = &mStandardChunkMemoryAllocator;
		else 
			m_pChunkMemoryAllocator = pAllocator; 
	}

	World()
	{
		memset(mEntityContexts, 0, sizeof(mEntityContexts));
		mArchetypeManager = new EntityArchetypeManager();
		m_pChunkMemoryAllocator = &mStandardChunkMemoryAllocator;
	}

	Entity* GetEntity(EntityID eid)
	{
		// get context id 
		uint8_t contextId = Entity::ExtractContextIdFromEntityID(eid);
		if (mEntityContexts[contextId] == nullptr)
			return nullptr;
		return mEntityContexts[contextId]->GetEntity(eid);
	}

private:
	int FindAvaibableContextId() const
	{
		for (int i = 0; i < MAX_CONTEXT_COUNT; i++) {
			if (mEntityContexts[i] == nullptr) {
				return i;
			}
		}
		return -1;
	}

	void RemoveContext(EntityContext* pContext)
	{
		mEntityContexts[pContext->mContextId] = nullptr;
	}
	EntityArchetypeManager*			mArchetypeManager;
	//std::list<EntityContext*>		mEntityContextList;
	EntityContext*					mEntityContexts[MAX_CONTEXT_COUNT] = { 0 };
	//bool							mContextIdsUsed[MAX_CONTEXT_COUNT] = { false };
	IChunkMemoryAllocator*			m_pChunkMemoryAllocator;
	StandardChunkMemoryAllocator	mStandardChunkMemoryAllocator;
};

void EntityContext::Release()
{
	for (auto pEntityComponentStorage : mEntityComponentStorageList) {
		auto pArchetype = pEntityComponentStorage->GetArchetype();
		pArchetype->mStoragesInContext[mContextId] = nullptr;
		SAFE_DELETE(pEntityComponentStorage);
	}
	mEntityComponentStorageList.clear();
	mWorld->RemoveContext(this);
	delete this;
}

template<typename ComponentType>
ComponentType* Entity::GetComponent()
{
	return mStorage->GetComponent<ComponentType>(this);
}

template<typename ComponentType>
const ComponentType* Entity::GetComponent() const
{
	return mStorage->GetComponent<ComponentType>(this);
}

template<typename ComponentType>
int Entity::GetComponentIndex() const
{
	return mStorage->GetArchetype()->GetComponentIndex<ComponentType>();
}

int Entity::GetComponentIndex(ComponentTypeID componentTypeID) const
{
	return mStorage->GetArchetype()->GetComponentIndex(componentTypeID);
}

template<typename T/*=byte*/>
T* Entity::GetComponentByIndex(int index)
{
	return mStorage->GetComponentByIndex<T>(this, index);
}

template<typename T/*=byte*/>
T* Entity::GetComponentByTypeID(ComponentTypeID typeId)
{
	return mStorage->GetComponentByTypeID<T>(this, typeId);
}

template<typename T/*=byte*/>
const T* Entity::GetComponentByIndex(int index) const
{
	return mStorage->GetComponentByIndex<T>(this, index);
}

template<typename T/*=byte*/>
const T* Entity::GetComponentByTypeID(ComponentTypeID typeId) const
{
	return mStorage->GetComponentByTypeID<T>(this, typeId);
}

template<typename ComponentType>
bool Entity::ContainComponent() const
{
	return mStorage->GetArchetype()->ContainComponent<ComponentType>();
}

template<typename... ComponentTypes>
bool Entity::ContainAllComponents() const
{
	return mStorage->GetArchetype()->ContainAllComponents<ComponentTypes...>();
}

template<typename... ComponentTypes>
bool Entity::ContainAnyComponents() const
{
	return mStorage->GetArchetype()->ContainAnyComponents<ComponentTypes...>();
}

EntityID Entity::GetEntityID() const
{
	/*
	return ((uint64_t)mGenID << 40)
		| ((uint64_t)mStorage->GetIndex() << (MAX_CHUNK_COUNT_BITS + MAX_BLOCK_COUNT_BITS))
		| ((uint64_t)mChunkIndex << MAX_BLOCK_COUNT_BITS)
		| (uint64_t)mBlockIndex;
		*/
	// change for this engine
	return ((uint64_t)mGenID << 48)
		| ((uint64_t)mStorage->mContext->GetContextId() << 40)
		| ((uint64_t)mStorage->GetIndex() << (MAX_CHUNK_COUNT_BITS + MAX_BLOCK_COUNT_BITS))
		| ((uint64_t)mChunkIndex << MAX_BLOCK_COUNT_BITS)
		| (uint64_t)mBlockIndex;
}

EntityContext* Entity::GetContext()
{
	return mStorage->mContext;
}

void Entity::ParseEntityID(EntityID eid, uint16_t* genid,
	uint8_t* contextId,
	uint16_t* storageIndex,
	uint16_t* chunkIndex, uint16_t* blockIndex)
{
	*genid = (uint16_t)((eid >> 48) & 0x0FFFF);
	*contextId = (uint8_t)((eid >> 40) & 0x0FF);
	*storageIndex = (uint16_t)((eid >> (MAX_CHUNK_COUNT_BITS + MAX_BLOCK_COUNT_BITS)) & STORAGE_INDEX_MASK);
	*chunkIndex = (uint16_t)((eid >> MAX_BLOCK_COUNT_BITS) & CHUNK_INDEX_MASK);
	*blockIndex = (uint16_t)(eid & BLOCK_INDEX_MASK);
}

uint8_t Entity::ExtractContextIdFromEntityID(EntityID eid)
{
	return (uint8_t)((eid >> 40) & 0x0FF);
}

EntityArchetype* Entity::GetArchetype() 
{
	return mStorage->GetArchetype();
}

const EntityArchetype* Entity::GetArchetype() const
{
	return mStorage->GetArchetype();
}

int Entity::GetComponentCount() const
{
	return mStorage->mComponentCountPerEntity;
}

template<typename...ComponentTypes>
Entity* Entity::Extend(ComponentTypes&&... args) const
{
	return mStorage->mContext->ExtendEntity<ComponentTypes...>(this, std::forward<ComponentTypes>(args)...);
}

template<typename...ComponentTypes>
Entity* Entity::Extend() const
{
	return mStorage->mContext->ExtendEntity<ComponentTypes...>(this);
}

template<typename ComponentType>
bool Entity::SetComponent(ComponentType&& data)
{
	using T = std::decay_t<ComponentType>;
	T* pComponent = GetComponent<T>();
	if (!pComponent) {
		return false;
	}
	*pComponent = std::forward<ComponentType>(data);
	return true;
}

Entity* Entity::Clone() const
{
	return mStorage->CloneEntity(this);
}

template<typename...ComponentTypes>
Entity* Entity::Remove() const
{
	return mStorage->mContext->RemoveComponentsFromEntity<ComponentTypes...>(this);
}

IChunkMemoryAllocator* EntityComponentStorage::GetChunkMemoryAllocator()
{
	return mContext->GetWorld()->GetChunkMemoryAllocator();
}


IChunkMemoryAllocator* EntityComponentChunk::GetMemoryAllocator()
{
	return mEntityComponentStorage->GetChunkMemoryAllocator();
}


template<typename ThreadLcoalArg, typename...ComponentTypes>
void ParallelJob<true, ThreadLcoalArg, ComponentTypes...>::Execute(ThreadLcoalArg* pThreadLocalArg)
{
	int threadIndex = base::mStartCounter.fetch_add(1);
	assert(threadIndex < base::mThreadCount);
	base::mState = ParallJobState::Executing;

	ParallelJobChunkSegementList& chunkList = base::mDividedJobChunkSegmentArray[threadIndex];
	for (auto& it : chunkList) {
		EntityComponentChunk* pChunk = it.pChunk;
		pChunk->ForEach<ComponentTypes...>(mFunction, pThreadLocalArg, it.RangeStart, it.RangeEnd);
	}
	if (base::mCompleteCounter.fetch_add(1) == base::mThreadCount - 1) {
		base::mState = ParallJobState::Unprepared;
	}
}

template<typename...ComponentTypes>
void ParallelJob<false, ComponentTypes...>::Execute()
{
	int threadIndex = base::mStartCounter.fetch_add(1);
	assert(threadIndex < base::mThreadCount);
	base::mState = ParallJobState::Executing;

	ParallelJobChunkSegementList& chunkList = base::mDividedJobChunkSegmentArray[threadIndex];
	for (auto& it : chunkList) {
		EntityComponentChunk* pChunk = it.pChunk;
		pChunk->ForEach<ComponentTypes...>(mFunction, it.RangeStart, it.RangeEnd);
	}
	if (base::mCompleteCounter.fetch_add(1) == base::mThreadCount - 1) {
		base::mState = ParallJobState::Unprepared;
	}
}

} // end namespace

#endif
