#pragma once

#include "RenderItem.h"

namespace grind
{
	class IRenderContext
	{
	public:
		virtual ~IRenderContext() {}

		virtual void BeginFrame() = 0;
		virtual void EndFrame() = 0;

		virtual void Render() = 0;
		virtual void BeginCommands() = 0;
		virtual void EndCommands() = 0;
		int GetDrawcallCount() const { return mDrawcallCounter; }

		virtual void RenderMeshItemQueue(CMeshRenderItemQueue* pMeshItemQueue) = 0;

	protected:
		int		mDrawcallCounter = 0;
	};

}


