//***************************************************************************************
// GeometryGenerator.cpp by Frank Luna (C) 2011 All Rights Reserved.
//***************************************************************************************

//#include "Mesh.h"

namespace grind
{
	namespace GeometryGenerator
	{
		template<typename Vertex, typename IndiceType>
		inline void CreateBox(__out SGeometryData<Vertex, IndiceType>& meshData, float width, float height, float depth, int numSubdivisions)
		{
			float w2 = 0.5f * width;
			float h2 = 0.5f * height;
			float d2 = 0.5f * depth;

			Vertex v[24];
			// Fill in the front face vertex data.
			v[0] = Vertex(Vector3f(-w2, -h2, -d2), Vector3f(0.0f, 0.0f, -1.0f), Vector3f(1.0f, 0.0f, 0.0f), Vector2f(0.0f, 1.0f));
			v[1] = Vertex(Vector3f(-w2, +h2, -d2), Vector3f(0.0f, 0.0f, -1.0f), Vector3f(1.0f, 0.0f, 0.0f), Vector2f(0.0f, 0.0f));
			v[2] = Vertex(Vector3f(+w2, +h2, -d2), Vector3f(0.0f, 0.0f, -1.0f), Vector3f(1.0f, 0.0f, 0.0f), Vector2f(1.0f, 0.0f));
			v[3] = Vertex(Vector3f(+w2, -h2, -d2), Vector3f(0.0f, 0.0f, -1.0f), Vector3f(1.0f, 0.0f, 0.0f), Vector2f(1.0f, 1.0f));

			// Fill in the back face vertex data.
			v[4] = Vertex(Vector3f(-w2, -h2, +d2), Vector3f(0.0f, 0.0f, 1.0f), Vector3f(-1.0f, 0.0f, 0.0f), Vector2f(1.0f, 1.0f));
			v[5] = Vertex(Vector3f(+w2, -h2, +d2), Vector3f(0.0f, 0.0f, 1.0f), Vector3f(-1.0f, 0.0f, 0.0f), Vector2f(0.0f, 1.0f));
			v[6] = Vertex(Vector3f(+w2, +h2, +d2), Vector3f(0.0f, 0.0f, 1.0f), Vector3f(-1.0f, 0.0f, 0.0f), Vector2f(0.0f, 0.0f));
			v[7] = Vertex(Vector3f(-w2, +h2, +d2), Vector3f(0.0f, 0.0f, 1.0f), Vector3f(-1.0f, 0.0f, 0.0f), Vector2f(1.0f, 0.0f));

			// Fill in the top face vertex data.
			v[8] = Vertex(Vector3f(-w2, +h2, -d2), Vector3f(0.0f, 1.0f, 0.0f), Vector3f(1.0f, 0.0f, 0.0f), Vector2f(0.0f, 1.0f));
			v[9] = Vertex(Vector3f(-w2, +h2, +d2), Vector3f(0.0f, 1.0f, 0.0f), Vector3f(1.0f, 0.0f, 0.0f), Vector2f(0.0f, 0.0f));
			v[10] = Vertex(Vector3f(+w2, +h2, +d2), Vector3f(0.0f, 1.0f, 0.0f), Vector3f(1.0f, 0.0f, 0.0f), Vector2f(1.0f, 0.0f));
			v[11] = Vertex(Vector3f(+w2, +h2, -d2), Vector3f(0.0f, 1.0f, 0.0f), Vector3f(1.0f, 0.0f, 0.0f), Vector2f(1.0f, 1.0f));

			// Fill in the bottom face vertex data.
			v[12] = Vertex(Vector3f(-w2, -h2, -d2), Vector3f(0.0f, -1.0f, 0.0f), Vector3f(-1.0f, 0.0f, 0.0f), Vector2f(1.0f, 1.0f));
			v[13] = Vertex(Vector3f(+w2, -h2, -d2), Vector3f(0.0f, -1.0f, 0.0f), Vector3f(-1.0f, 0.0f, 0.0f), Vector2f(0.0f, 1.0f));
			v[14] = Vertex(Vector3f(+w2, -h2, +d2), Vector3f(0.0f, -1.0f, 0.0f), Vector3f(-1.0f, 0.0f, 0.0f), Vector2f(0.0f, 0.0f));
			v[15] = Vertex(Vector3f(-w2, -h2, +d2), Vector3f(0.0f, -1.0f, 0.0f), Vector3f(-1.0f, 0.0f, 0.0f), Vector2f(1.0f, 0.0f));

			// Fill in the left face vertex data.
			v[16] = Vertex(Vector3f(-w2, -h2, +d2), Vector3f(-1.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, -1.0f), Vector2f(0.0f, 1.0f));
			v[17] = Vertex(Vector3f(-w2, +h2, +d2), Vector3f(-1.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, -1.0f), Vector2f(0.0f, 0.0f));
			v[18] = Vertex(Vector3f(-w2, +h2, -d2), Vector3f(-1.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, -1.0f), Vector2f(1.0f, 0.0f));
			v[19] = Vertex(Vector3f(-w2, -h2, -d2), Vector3f(-1.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, -1.0f), Vector2f(1.0f, 1.0f));

			// Fill in the right face vertex data.
			v[20] = Vertex(Vector3f(+w2, -h2, -d2), Vector3f(1.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f), Vector2f(0.0f, 1.0f));
			v[21] = Vertex(Vector3f(+w2, +h2, -d2), Vector3f(1.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f), Vector2f(0.0f, 0.0f));
			v[22] = Vertex(Vector3f(+w2, +h2, +d2), Vector3f(1.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f), Vector2f(1.0f, 0.0f));
			v[23] = Vertex(Vector3f(+w2, -h2, +d2), Vector3f(1.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f), Vector2f(1.0f, 1.0f));

			meshData.Vertices.assign(&v[0], &v[24]);

			//
			// Create the indices.
			//

			IndiceType i[36];

			// Fill in the front face index data
			i[0] = 0; i[1] = 1; i[2] = 2;
			i[3] = 0; i[4] = 2; i[5] = 3;

			// Fill in the back face index data
			i[6] = 4; i[7] = 5; i[8] = 6;
			i[9] = 4; i[10] = 6; i[11] = 7;

			// Fill in the top face index data
			i[12] = 8; i[13] = 9; i[14] = 10;
			i[15] = 8; i[16] = 10; i[17] = 11;

			// Fill in the bottom face index data
			i[18] = 12; i[19] = 13; i[20] = 14;
			i[21] = 12; i[22] = 14; i[23] = 15;

			// Fill in the left face index data
			i[24] = 16; i[25] = 17; i[26] = 18;
			i[27] = 16; i[28] = 18; i[29] = 19;

			// Fill in the right face index data
			i[30] = 20; i[31] = 21; i[32] = 22;
			i[33] = 20; i[34] = 22; i[35] = 23;

			meshData.Indices.assign(&i[0], &i[36]);

			// Put a cap on the number of subdivisions.
			numSubdivisions = std::min<int>(numSubdivisions, 6);

			for (int i = 0; i < numSubdivisions; ++i)
				Subdivide(meshData);
		}

		template<typename Vertex, typename IndiceType>
		inline void CreateSphere(__out SGeometryData<Vertex, IndiceType>& meshData,
			float radius, int sliceCount, int stackCount)
		{
			
			// Compute the vertices stating at the top pole and moving down the stacks.
			//

			// Poles: note that there will be texture coordinate distortion as there is
			// not a unique point on the texture map to assign to the pole when mapping
			// a rectangular texture onto a sphere.
			Vertex topVertex(Vector3f(0.0f, +radius, 0.0f), Vector3f(0.0f, +1.0f, 0.0f), Vector3f(1.0f, 0.0f, 0.0f), Vector2f(0.0f, 0.0f));
			Vertex bottomVertex(Vector3f(0.0f, -radius, 0.0f), Vector3f(0.0f, -1.0f, 0.0f), Vector3f(1.0f, 0.0f, 0.0f), Vector2f(0.0f, 1.0f));

			meshData.Vertices.push_back(topVertex);

			float phiStep = XM_PI / stackCount;
			float thetaStep = 2.0f*XM_PI / sliceCount;

			// Compute vertices for each stack ring (do not count the poles as rings).
			for (int i = 1; i <= stackCount - 1; ++i)
			{
				float phi = i * phiStep;

				// Vertices of ring.
				for (int j = 0; j <= sliceCount; ++j)
				{
					float theta = j * thetaStep;

					//Vertex v;

					Vector3f pos;
					// spherical to cartesian
					pos.x = radius * sinf(phi)*cosf(theta);
					pos.y = radius * cosf(phi);
					pos.z = radius * sinf(phi)*sinf(theta);

					// Partial derivative of P with respect to theta
					Vector3f tangent;
					tangent.x = -radius * sinf(phi)*sinf(theta);
					tangent.y = 0.0f;
					tangent.z = +radius * sinf(phi)*cosf(theta);
					tangent.normalize();

					Vector3f normal = pos.getNormalized();
					
					Vector2f tc;
					tc.x = theta / irr::core::_2PI;
					tc.y = phi / irr::core::PI;

					Vertex v(pos, normal, tangent, tc);
					meshData.Vertices.push_back(v);
				}
			}

			meshData.Vertices.push_back(bottomVertex);

			//
			// Compute indices for top stack.  The top stack was written first to the vertex buffer
			// and connects the top pole to the first ring.
			//

			for (int i = 1; i <= sliceCount; ++i)
			{
				meshData.Indices.push_back(0);
				meshData.Indices.push_back((IndiceType)(i + 1));
				meshData.Indices.push_back((IndiceType)i);
			}

			//
			// Compute indices for inner stacks (not connected to poles).
			//

			// Offset the indices to the index of the first vertex in the first ring.
			// This is just skipping the top pole vertex.
			int baseIndex = 1;
			int ringVertexCount = sliceCount + 1;
			for (int i = 0; i < stackCount - 2; ++i)
			{
				for (int j = 0; j < sliceCount; ++j)
				{
					meshData.Indices.push_back(IndiceType(baseIndex + i * ringVertexCount + j));
					meshData.Indices.push_back(IndiceType(baseIndex + i * ringVertexCount + j + 1));
					meshData.Indices.push_back(IndiceType(baseIndex + (i + 1)*ringVertexCount + j));

					meshData.Indices.push_back(IndiceType(baseIndex + (i + 1)*ringVertexCount + j));
					meshData.Indices.push_back(IndiceType(baseIndex + i * ringVertexCount + j + 1));
					meshData.Indices.push_back(IndiceType(baseIndex + (i + 1)*ringVertexCount + j + 1));
				}
			}

			//
			// Compute indices for bottom stack.  The bottom stack was written last to the vertex buffer
			// and connects the bottom pole to the bottom ring.
			//

			// South pole vertex was added last.
			int southPoleIndex = (int)meshData.Vertices.size() - 1;

			// Offset the indices to the index of the first vertex in the last ring.
			baseIndex = southPoleIndex - ringVertexCount;

			for (int i = 0; i < sliceCount; ++i)
			{
				meshData.Indices.push_back(IndiceType(southPoleIndex));
				meshData.Indices.push_back(IndiceType(baseIndex + i));
				meshData.Indices.push_back(IndiceType(baseIndex + i + 1));
			}
		}

		template<typename Vertex, typename IndiceType>
		inline void Subdivide(_Inout_ SGeometryData<Vertex, IndiceType>& meshData)
		{
			// Save a copy of the input geometry.
			SGeometryData<Vertex, IndiceType> inputCopy(std::move(meshData));

			meshData.Vertices.resize(0);
			meshData.Indices.resize(0);

			//       v1
			//       *
			//      / \
			//     /   \
			//  m0*-----*m1
			//   / \   / \
			//  /   \ /   \
			// *-----*-----*
			// v0    m2     v2

			int numTris = (int)inputCopy.Indices.size() / 3;
			for (int i = 0; i < numTris; ++i)
			{
				Vertex v0 = inputCopy.Vertices[inputCopy.Indices[i * 3 + 0]];
				Vertex v1 = inputCopy.Vertices[inputCopy.Indices[i * 3 + 1]];
				Vertex v2 = inputCopy.Vertices[inputCopy.Indices[i * 3 + 2]];

				//
				// Generate the midpoints.
				//

				Vertex m0 = MidPoint(v0, v1);
				Vertex m1 = MidPoint(v1, v2);
				Vertex m2 = MidPoint(v0, v2);

				//
				// Add new geometry.
				//

				meshData.Vertices.push_back(v0); // 0
				meshData.Vertices.push_back(v1); // 1
				meshData.Vertices.push_back(v2); // 2
				meshData.Vertices.push_back(m0); // 3
				meshData.Vertices.push_back(m1); // 4
				meshData.Vertices.push_back(m2); // 5

				meshData.Indices.push_back(i * 6 + 0);
				meshData.Indices.push_back(i * 6 + 3);
				meshData.Indices.push_back(i * 6 + 5);

				meshData.Indices.push_back(i * 6 + 3);
				meshData.Indices.push_back(i * 6 + 4);
				meshData.Indices.push_back(i * 6 + 5);

				meshData.Indices.push_back(i * 6 + 5);
				meshData.Indices.push_back(i * 6 + 4);
				meshData.Indices.push_back(i * 6 + 2);

				meshData.Indices.push_back(i * 6 + 3);
				meshData.Indices.push_back(i * 6 + 1);
				meshData.Indices.push_back(i * 6 + 4);
			}
		}

		template<typename Vertex>
		Vertex MidPoint(const Vertex& v0, const Vertex& v1)
		{
			return v0.Lerp(v1, 0.5f);
		}

		template<typename Vertex, typename IndiceType>
		inline void CreateGeosphere(__out SGeometryData<Vertex, IndiceType>& meshData,
			float radius, int numSubdivisions)
		{
			// Put a cap on the number of subdivisions.
			numSubdivisions = std::min<int>(numSubdivisions, 6);

			// Approximate a sphere by tessellating an icosahedron.
			const float X = 0.525731f;
			const float Z = 0.850651f;

			Vector3f pos[12] =
			{
				Vector3f(-X, 0.0f, Z),  Vector3f(X, 0.0f, Z),
				Vector3f(-X, 0.0f, -Z), Vector3f(X, 0.0f, -Z),
				Vector3f(0.0f, Z, X),   Vector3f(0.0f, Z, -X),
				Vector3f(0.0f, -Z, X),  Vector3f(0.0f, -Z, -X),
				Vector3f(Z, X, 0.0f),   Vector3f(-Z, X, 0.0f),
				Vector3f(Z, -X, 0.0f),  Vector3f(-Z, -X, 0.0f)
			};

			IndiceType k[60] =
			{
				1,4,0,  4,9,0,  4,5,9,  8,5,4,  1,8,4,
				1,10,8, 10,3,8, 8,3,5,  3,2,5,  3,7,2,
				3,10,7, 10,6,7, 6,11,7, 6,0,11, 6,1,0,
				10,1,6, 11,0,9, 2,11,9, 5,2,9,  11,2,7
			};

			meshData.Vertices.resize(12);
			meshData.Indices.assign(&k[0], &k[60]);

			for (int i = 0; i < 12; ++i)
				meshData.Vertices[i].Pos = pos[i];

			for (int i = 0; i < numSubdivisions; ++i)
				Subdivide(meshData);

			// Project vertices onto sphere and scale.
			for (int i = 0; i < meshData.Vertices.size(); ++i)
			{
				// Project onto unit sphere.
				Vector3f n = meshData.Vertices[i].Pos.getNormalized();

				// Project onto sphere.
				Vector3f p = radius * n;

				// Derive texture coordinates from spherical coordinates.
				float theta = atan2f(p.z, p.x);

				// Put in [0, 2pi].
				if (theta < 0.0f)
					theta += irr::core::_2PI;

				float phi = acosf(p.y / radius);

				Vector2f uv(theta / irr::core::_2PI, phi / irr::core::_2PI);

				// Partial derivative of P with respect to theta
				Vector3f tangent;
				tangent.x = -radius * sinf(phi)*sinf(theta);
				tangent.y = 0.0f;
				tangent.z = +radius * sinf(phi)*cosf(theta);
				tangent.normalize();

				meshData.Vertices[i] = Vertex(p, n, tangent, uv);

			}
		}

		template<typename Vertex, typename IndiceType>
		void BuildCylinderTopCap(__out SGeometryData<Vertex, IndiceType>& meshData,
			float bottomRadius, float topRadius, float height, int sliceCount, int stackCount)
		{
			int baseIndex = (int)meshData.Vertices.size();

			float y = 0.5f*height;
			float dTheta = 2.0f*XM_PI / sliceCount;

			// Duplicate cap ring vertices because the texture coordinates and normals differ.
			for (int i = 0; i <= sliceCount; ++i)
			{
				float x = topRadius * cosf(i*dTheta);
				float z = topRadius * sinf(i*dTheta);

				// Scale down by the height to try and make top cap texture coord area
				// proportional to base.
				float u = x / height + 0.5f;
				float v = z / height + 0.5f;

				Vertex vertex(Vector3f(x, y, z), Vector3f(0.0f, 1.0f, 0.0f), Vector3f(1.0f, 0.0f, 0.0f), Vector2f(u, v));
				meshData.Vertices.push_back(vertex);
			}

			// Cap center vertex.
			meshData.Vertices.push_back(Vertex(Vector3f(0.0f, y, 0.0f), Vector3f(0.0f, 1.0f, 0.0f), Vector3f(1.0f, 0.0f, 0.0f), Vector2f(0.5f, 0.5f)));

			// Index of center vertex.
			int centerIndex = (int)meshData.Vertices.size() - 1;

			for (int i = 0; i < sliceCount; ++i)
			{
				meshData.Indices.push_back((IndiceType)centerIndex);
				meshData.Indices.push_back((IndiceType)(baseIndex + i + 1));
				meshData.Indices.push_back((IndiceType)(baseIndex + i));
			}
		}

		template<typename Vertex, typename IndiceType>
		void BuildCylinderBottomCap(__out SGeometryData<Vertex, IndiceType>& meshData,
			float bottomRadius, float topRadius, float height, int sliceCount, int stackCount)
		{
			int baseIndex = (int)meshData.Vertices.size();
			float y = -0.5f*height;

			// vertices of ring
			float dTheta = 2.0f*XM_PI / sliceCount;
			for (int i = 0; i <= sliceCount; ++i)
			{
				float x = bottomRadius * cosf(i*dTheta);
				float z = bottomRadius * sinf(i*dTheta);

				// Scale down by the height to try and make top cap texture coord area
				// proportional to base.
				float u = x / height + 0.5f;
				float v = z / height + 0.5f;

				meshData.Vertices.push_back(Vertex(Vector3f(x, y, z), Vector3f(0.0f, -1.0f, 0.0f), 
					Vector3f(1.0f, 0.0f, 0.0f), Vector2f(u, v)));
			}

			// Cap center vertex.
			meshData.Vertices.push_back(Vertex(Vector3f(0.0f, y, 0.0f), Vector3f(0.0f, -1.0f, 0.0f), 
				Vector3f(1.0f, 0.0f, 0.0f), Vector2f(0.5f, 0.5f)));

			// Cache the index of center vertex.
			int centerIndex = (int)meshData.Vertices.size() - 1;

			for (int i = 0; i < sliceCount; ++i)
			{
				meshData.Indices.push_back((IndiceType)centerIndex);
				meshData.Indices.push_back((IndiceType)(baseIndex + i));
				meshData.Indices.push_back((IndiceType)(baseIndex + i + 1));
			}
		}

		
		template<typename Vertex, typename IndiceType>
		inline void CreateCylinder(__out SGeometryData<Vertex, IndiceType>& meshData,
			float bottomRadius, float topRadius, float height, int sliceCount, int stackCount)
		{
			float stackHeight = height / stackCount;

			// Amount to increment radius as we move up each stack level from bottom to top.
			float radiusStep = (topRadius - bottomRadius) / stackCount;

			int ringCount = stackCount + 1;

			// Compute vertices for each stack ring starting at the bottom and moving up.
			for (int i = 0; i < ringCount; ++i)
			{
				float y = -0.5f*height + i * stackHeight;
				float r = bottomRadius + i * radiusStep;

				// vertices of ring
				float dTheta = 2.0f * irr::core::PI / sliceCount;
				for (int j = 0; j <= sliceCount; ++j)
				{
					float c = cosf(j*dTheta);
					float s = sinf(j*dTheta);

					Vector3f pos(r*c, y, r*s);
					Vector2f uv((float)j / sliceCount, 1.0f - (float)i / stackCount);

					//vertex.Position = XMFLOAT3(r*c, y, r*s);

					//vertex.TexC.x = (float)j / sliceCount;
					//vertex.TexC.y = 1.0f - (float)i / stackCount;

					// Cylinder can be parameterized as follows, where we introduce v
					// parameter that goes in the same direction as the v tex-coord
					// so that the bitangent goes in the same direction as the v tex-coord.
					//   Let r0 be the bottom radius and let r1 be the top radius.
					//   y(v) = h - hv for v in [0,1].
					//   r(v) = r1 + (r0-r1)v
					//
					//   x(t, v) = r(v)*cos(t)
					//   y(t, v) = h - hv
					//   z(t, v) = r(v)*sin(t)
					// 
					//  dx/dt = -r(v)*sin(t)
					//  dy/dt = 0
					//  dz/dt = +r(v)*cos(t)
					//
					//  dx/dv = (r0-r1)*cos(t)
					//  dy/dv = -h
					//  dz/dv = (r0-r1)*sin(t)

					// This is unit length.
					//vertex.TangentU = XMFLOAT3(-s, 0.0f, c);
					Vector3f tangent(-s, 0.0f, c);

					float dr = bottomRadius - topRadius;
					//XMFLOAT3 bitangent(dr*c, -height, dr*s);
					Vector3f bitangent(dr*c, -height, dr*s);

					Vector3f normal = tangent.crossProduct(bitangent).getNormalized();

					//XMVECTOR T = XMLoadFloat3(&vertex.TangentU);
					//XMVECTOR B = XMLoadFloat3(&bitangent);
					//XMVECTOR N = XMVector3Normalize(XMVector3Cross(T, B));
					//XMStoreFloat3(&vertex.Normal, N);

					Vertex v(pos, normal, tangent, uv);

					meshData.Vertices.push_back(v);
				}
			}

			// Add one because we duplicate the first and last vertex per ring
			// since the texture coordinates are different.
			int ringVertexCount = sliceCount + 1;

			// Compute indices for each stack.
			for (int i = 0; i < stackCount; ++i)
			{
				for (int j = 0; j < sliceCount; ++j)
				{
					meshData.Indices.push_back((IndiceType)(i*ringVertexCount + j));
					meshData.Indices.push_back((IndiceType)((i + 1)*ringVertexCount + j));
					meshData.Indices.push_back((IndiceType)((i + 1)*ringVertexCount + j + 1));

					meshData.Indices.push_back((IndiceType)(i*ringVertexCount + j));
					meshData.Indices.push_back((IndiceType)((i + 1)*ringVertexCount + j + 1));
					meshData.Indices.push_back((IndiceType)(i*ringVertexCount + j + 1));
				}
			}

			BuildCylinderTopCap(meshData, bottomRadius, topRadius, height, sliceCount, stackCount);
			BuildCylinderBottomCap(meshData, bottomRadius, topRadius, height, sliceCount, stackCount);

		}

		template<typename Vertex, typename IndiceType>
		inline void CreateGrid(__out SGeometryData<Vertex, IndiceType>& meshData,
			float width, float depth, int m, int n)
		{
			m = m + 1;
			n = n + 1;
			int vertexCount = m * n;
			int faceCount = (m - 1)*(n - 1) * 2;

			//
			// Create the vertices.
			//

			float halfWidth = 0.5f*width;
			float halfDepth = 0.5f*depth;

			float dx = width / (n - 1);
			float dz = depth / (m - 1);

			float du = 1.0f / (n - 1);
			float dv = 1.0f / (m - 1);

			meshData.Vertices.resize(vertexCount);
			for (int i = 0; i < m; ++i)
			{
				float z = halfDepth - i * dz;
				for (int j = 0; j < n; ++j)
				{
					float x = -halfWidth + j * dx;

					Vector3f pos(x, 0, z);
					Vector3f normal(0.0f, 1.0f, 0.0f);
					Vector3f tangent(1.0f, 0.0f, 0.0f);
					Vector2f uv(j * du, i * dv);

					meshData.Vertices[i*n + j] = Vertex(pos, normal, tangent, uv);

					//meshData.Vertices[i*n + j].Position = XMFLOAT3(x, 0.0f, z);
					//meshData.Vertices[i*n + j].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
					//meshData.Vertices[i*n + j].TangentU = XMFLOAT3(1.0f, 0.0f, 0.0f);

					//// Stretch texture over grid.
					//meshData.Vertices[i*n + j].TexC.x = j * du;
					//meshData.Vertices[i*n + j].TexC.y = i * dv;
				}
			}

			//
			// Create the indices.
			//

			meshData.Indices.resize(faceCount * 3); // 3 indices per face

			// Iterate over each quad and compute indices.
			int k = 0;
			for (int i = 0; i < m - 1; ++i)
			{
				for (int j = 0; j < n - 1; ++j)
				{
					meshData.Indices[k] = (IndiceType)(i * n + j);
					meshData.Indices[k + 1] = (IndiceType)(i * n + j + 1);
					meshData.Indices[k + 2] = (IndiceType)((i + 1)*n + j);

					meshData.Indices[k + 3] = (IndiceType)((i + 1)*n + j);
					meshData.Indices[k + 4] = (IndiceType)(i * n + j + 1);
					meshData.Indices[k + 5] = (IndiceType)((i + 1)*n + j + 1);

					k += 6; // next quad
				}
			}

		}

	}
}
