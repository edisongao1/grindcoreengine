#pragma once

#include "IRenderDevice.h"

namespace grind
{
	class IGraphicsFactory
	{
	public:
		using TCreateFunction = IGraphicsFactory * (*)();
		virtual ~IGraphicsFactory(){}

		virtual IRenderDevice* CreateRenderDevice() = 0;
		virtual void ReleaseRenderDevice(IRenderDevice* p) = 0;
	};

}

