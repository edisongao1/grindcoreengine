#pragma once

namespace grind
{
	enum EAntiAliasMethod {
		eAA_NotUse,
		eAA_MSAA,
	};

	struct SGraphicsSettings
	{
		intptr_t				hWindowHandle;
		unsigned				ViewportWidth;
		unsigned				ViewportHeight;
		unsigned				MsaaSampleCount;
		EAntiAliasMethod		AntiAliasMethod;
		bool					WindowMode;
	};

	class IRenderContext;

	class IRenderDevice
	{
	public:
		virtual ~IRenderDevice() {}
		virtual bool Initialize() = 0;
		virtual void OnResize() = 0;
		virtual void Render() = 0;

		const SGraphicsSettings& GetSettings() const { return m_Settings; }

		virtual IRenderContext* GetRenderContext(int index) = 0;
		virtual int GetRenderContextCount() const { return mRenderContextCount; }
		virtual void FlushCommandQueue() = 0;

		virtual void BeginFrame() = 0;
		virtual void EndFrame() = 0;
		int GetResourceFrameIndex() const { return mFrameResourceIndex; }

	protected:
		SGraphicsSettings	m_Settings;
		int					mFrameResourceIndex = 0;
		int					mRenderContextCount = 0;
		//unsigned			m_ClientWidth;
		//unsigned			m_ClientHeight;
	};

	class ILocalFrameResource
	{
	public:
		virtual uint64_t SetObjectConstBuffer(void* data, uint32_t size) = 0;
	};
}
