#include "stdafx.h"
#include "Core/GrindCore.h"
#include "IGraphicsFactory.h"
#include "IGameFramework.h"
#include "GrindException.h"
#include <Windows.h>
#include <cxxopts.hpp>
#include <filesystem>
#include <tinyxml2.h>

using namespace grind;

namespace grind
{
	IEngineCore* g_pEngineCore = nullptr;
	IConsole* g_pConsole = nullptr;
}

void SaveMeshDataToFile(const char* szFbxFilePath, SMeshData& meshData, uint32_t fvf)
{
	IFileSystem* pFileSystem = g_pEngineCore->GetFileSystem();

	auto name = std::filesystem::path(szFbxFilePath).stem();
	auto gmeshPath = std::filesystem::path(szFbxFilePath).replace_extension("gmesh");
	auto gmeshMetaPath = std::filesystem::path(szFbxFilePath).replace_extension("gmesh.xml");
	
	std::string strName = name.generic_string();
	std::string strGmeshPath = gmeshPath.generic_string();
	std::string strGmeshMetaPath = gmeshMetaPath.generic_string();

	char szGmeshMetaFullPath[260];
	pFileSystem->GetFileFullPath(strGmeshMetaPath.c_str(), szGmeshMetaFullPath);

	tinyxml2::XMLDocument doc;
	


	doc.SaveFile(szGmeshMetaFullPath);

	
}

int main(int argc, char *argv[])
{
	cxxopts::Options options(argv[0], "tools command line options");
	options
		.positional_help("[optional args]")
		.show_positional_help();

	std::string strFileName;
	int bContainTangent = true;
	int iTexcoordCount = 1;

	options
		.allow_unrecognised_options()
		.add_options()
		("f, file", "File", cxxopts::value<std::string>(strFileName), "FILE")
		("tangent", "Contain Tangent?", cxxopts::value<int>()->default_value("1"))
		("texcoord", "Contain Texcoord1?", cxxopts::value<int>()->default_value("1"))
		("help", "Print help")
		;

	auto optionResult = options.parse(argc, argv);

	IEngineCore* pEngineCore = nullptr;
	IConfiguration* pConfiguration = nullptr;
	IConsole* pConsole = nullptr;

	// create engine
	{
		HINSTANCE hCoreDll = LoadLibraryA("GrindCore");
		if (hCoreDll == 0) {
			throw CException("Failed in loading enginecore module.");
		}

		auto pCreateEngineCore = (IEngineCore::TCreateFunction)GetProcAddress(hCoreDll, "CreateEngineCore");
		pEngineCore = pCreateEngineCore();
		pConfiguration = pEngineCore->GetConfiguration();
		pConsole = pEngineCore->GetConsole();

		g_pEngineCore = pEngineCore;
		g_pConsole = pConsole;
	}

	if (optionResult.count("f") == 0)
	{
		pConsole->Print(IConsole::EConsoleTextColor::RED, true, "No File is given");
		exit(EXIT_FAILURE);
	}

	if (optionResult.count("tangent"))
	{
		bContainTangent = optionResult["tangent"].as<int>();
	}

	if (optionResult.count("texcoord"))
	{
		iTexcoordCount = optionResult["texcoord"].as<int>();
	}

	uint32_t fvf = 0;
	if (bContainTangent)
	{
		fvf |= E_FVF_Flag::ContainTangent;
	}

	if (iTexcoordCount == 2)
	{
		fvf |= E_FVF_Flag::ContainTexcoord1;
	}

	IRenderResourceManager* pResourceMgr = pEngineCore->GetRenderResourceManager();
	IMeshManager* pMeshManager = pResourceMgr->GetMeshManager();

	SMeshData meshData;
	pMeshManager->LoadStaticMeshData(strFileName.c_str(), meshData, fvf);

	//CModelFileLoader modelFileLoader2;
	//SMeshData meshData2;
	//modelFileLoader2.LoadStaticMesh("Models/cokecan.fbx", meshData2,
	SaveMeshDataToFile(strFileName.c_str(), meshData, fvf);

	pEngineCore->Release();
	return 0;
}
