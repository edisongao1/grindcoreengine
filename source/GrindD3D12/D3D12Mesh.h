#pragma once
#include "IMesh.h"

using Microsoft::WRL::ComPtr;

namespace grind
{
	class IRenderContext;

	class CD3D12MeshBuffer: public IMeshBuffer
	{
	public:
		CD3D12MeshBuffer(IMesh* pMesh, ID3D12Device* pd3dDevice);
		void Create(IRenderContext* pRenderContext, const SMeshData* meshData);
		virtual void OnCreateCompleted() override;
		virtual void Bind(ID3D12GraphicsCommandList* pd3dCmdList);

	private:
		ID3D12Device*			md3dDevice;
		// GPU Buffer
		ComPtr<ID3D12Resource>	mVertexBuffer = nullptr;
		ComPtr<ID3D12Resource>	mIndexBuffer = nullptr;
		// An intermediate buffer used to transfer data from CPU to GPU
		// only be used while loading, and release afterwards
		ComPtr<ID3D12Resource>	mVertexUploadBuffer = nullptr; 
		ComPtr<ID3D12Resource>	mIndexUploadBuffer = nullptr;

		D3D12_VERTEX_BUFFER_VIEW	mVertexBufferView;
		D3D12_INDEX_BUFFER_VIEW		mIndexBufferView;

	};

	class CD3D12MeshBufferManager
	{
	public:
		CD3D12MeshBufferManager(ID3D12Device* d3dDevice);

	private:
		ID3D12Device*			md3dDevice;
	};

}

