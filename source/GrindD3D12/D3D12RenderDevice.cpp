#include "stdafx.h"
#include "D3D12RenderDevice.h"
#include "D3D12Util.h"
#include "D3D12Buffer.h"
#include "D3D12RenderContext.h"
#include "GrindMath.h"
#include "IEngineCore.h"
#include "IConsole.h"
#include "D3D12RenderResourceFactory.h"
#include "IMesh.h"
#include "ICamera.h"

#pragma comment(lib,"d3dcompiler.lib")
#pragma comment(lib, "D3D12.lib")
#pragma comment(lib, "dxgi.lib")

namespace grind {

	SD3D12ConstantValues g_d3d12ConstantValues;

	CD3D12RenderDevice::CD3D12RenderDevice()
	{
		//mRenderContexts.fill(nullptr);
		auto pConsole = GetEngineCore()->GetConsole();
		pConsole->RegisterCommand("runningMask", [this](IConsole::SConsoleCommand* pCommand) {
			bool bShadow = (bool)pCommand->GetArg<int>(1);
			bool bGI = (bool)pCommand->GetArg<int>(2);
			mRunningMask = 0;
			if (bShadow) {
				mRunningMask |= EShaderRunningMacro::ReceiveShadow;
			}
			if (bGI) {
				mRunningMask |= EShaderRunningMacro::ReceiveGI;
			}
		});

		pConsole->RegisterCommand("wire", [this](IConsole::SConsoleCommand* pCommand) {
			mWireMode = (bool)pCommand->GetArg<int>(1);
		});
	}

	CD3D12RenderDevice::~CD3D12RenderDevice()
	{
		//int a = 1;
	}

	
	bool CD3D12RenderDevice::Initialize()
	{
		//SGraphicsSettings settings;
		//settings.MsaaSampleCount = 1;
		//settings.ClientWidth = SCREEN_WIDTH;
		//settings.ClientHeight = SCREEN_HEIGHT;
		//settings.WindowMode = true;
		//settings.hWindowHandle = pWindow->GetWindowHandle();


		//m_Settings = settings;
		IConfiguration* pConfiguration = GetEngineCore()->GetConfiguration();
		IWindow* pWindow = GetEngineCore()->GetMainWindow();

		m_hWnd = (HWND)pWindow->GetWindowHandle();
		m_Settings.ViewportWidth = pWindow->GetWidth();
		m_Settings.ViewportHeight = pWindow->GetHeight();
		m_Settings.MsaaSampleCount = pConfiguration->GetProperty<int>("msaa_sample_count", 1);
		//m_Settings.WindowMode = pWindow->IsFullScreen() ? false : true;
		
		// 目前创建MSAA会报错 
		//m_Settings.MsaaSampleCount = 1;
		m_Settings.WindowMode = true;

		//m_hWnd = (HWND)m_Settings.hWindowHandle;
		IConsole::Print("CD3D12RenderDevice::Init");
		
		HRESULT hResult;

#if defined(DEBUG) || defined(_DEBUG) 
		// Enable the D3D12 debug layer.
		if (pConfiguration->GetProperty<bool>("renderer_debug_enabled"))
		{
			Microsoft::WRL::ComPtr<ID3D12Debug> debugController;
			ThrowIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController)));
			debugController->EnableDebugLayer();
		}
#endif

		hResult = CreateDXGIFactory1(IID_PPV_ARGS(&m_pDxgiFactory4));
		ThrowIfFailed(hResult);

		IDXGIAdapter * pDefaultAdapter = nullptr;
		{
			// print all graphics adapters
			UINT i = 0;
			IDXGIAdapter * pAdapter;
			std::vector<IDXGIAdapter*> vAdapters;
			while (m_pDxgiFactory4->EnumAdapters(i, &pAdapter) != DXGI_ERROR_NOT_FOUND)
			{
				vAdapters.push_back(pAdapter);
				++i;
			}

			for (IDXGIAdapter* pAdapter : vAdapters) {
				DXGI_ADAPTER_DESC adapterDesc;
				pAdapter->GetDesc(&adapterDesc);
				//wprintf(L"Graphics Adapter: %s GPUMemory: %ld\n", adapterDesc.Description, adapterDesc.DedicatedVideoMemory);
				IConsole::PrintW(L"Graphics Adapter: %s GPUMemory: %ld", adapterDesc.Description, adapterDesc.DedicatedVideoMemory);
				GRIND_DEBUG_LOG("Helloworld");
				GRIND_WARNING_LOG("Hello:%f", 1.2f);
			}
			pDefaultAdapter = vAdapters[0];
		}
		
		hResult = D3D12CreateDevice(pDefaultAdapter, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&m_pd3dDevice));
		ThrowIfFailed(hResult);

		// Get descriptors Size
		//m_RtvDescriptorSize = m_pd3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		//m_DsvDescriptorSize = m_pd3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
		//m_SrvDescriptorSize = m_pd3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
		g_d3d12ConstantValues.RtvDescriptorSize = m_pd3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		g_d3d12ConstantValues.DsvDescriptorSize = m_pd3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
		g_d3d12ConstantValues.SrvDescriptorSize = m_pd3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

		// Check MSAA Quality Support
		D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS msQualityLevels;
		msQualityLevels.Format = m_BackBufferFormat;
		msQualityLevels.SampleCount = m_Settings.MsaaSampleCount;
		msQualityLevels.Flags = D3D12_MULTISAMPLE_QUALITY_LEVELS_FLAG_NONE;
		msQualityLevels.NumQualityLevels = 0;
		ThrowIfFailed(m_pd3dDevice->CheckFeatureSupport(D3D12_FEATURE_MULTISAMPLE_QUALITY_LEVELS, 
			&msQualityLevels, sizeof(msQualityLevels)));

		m_MsaaQuality = msQualityLevels.NumQualityLevels;
		assert(m_MsaaQuality > 0);

		// create fence
		ThrowIfFailed(m_pd3dDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE,
			IID_PPV_ARGS(&m_Fence)));

		// create frame resources
		for (int i = 0; i < FRAME_RESOURCE_COUNT; i++) {
			mGlobalFrameResources[i].Init(m_pd3dDevice.Get());
			mLocalFrameResources[i].Init(&mGlobalFrameResources[i], m_pd3dDevice.Get());
		}

		// create command queue and command list
		{
			D3D12_COMMAND_QUEUE_DESC queueDesc = {};
			queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
			queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
			ThrowIfFailed(m_pd3dDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_pd3dCommandQueue)));

			//ThrowIfFailed(m_pd3dDevice->CreateCommandAllocator(
			//	D3D12_COMMAND_LIST_TYPE_DIRECT,
			//	IID_PPV_ARGS(m_pd3dCommandAllocator.GetAddressOf())));

			//ThrowIfFailed(m_pd3dDevice->CreateCommandAllocator(
			//	D3D12_COMMAND_LIST_TYPE_DIRECT,
			//	IID_PPV_ARGS(m_pd3dCommandAllocator2.GetAddressOf())));

			auto pCommandAllocator = mLocalFrameResources[mFrameResourceIndex].GetCommandAllocator();

			ThrowIfFailed(m_pd3dDevice->CreateCommandList(
				0,
				D3D12_COMMAND_LIST_TYPE_DIRECT,
				pCommandAllocator, // Associated command allocator
				nullptr,                   // Initial PipelineStateObject
				IID_PPV_ARGS(m_pd3dCommandList.GetAddressOf())));

			m_pd3dCommandList->Close();
		}

		// create swap chain
		{
			DXGI_SWAP_CHAIN_DESC scd;
			
			scd.BufferDesc.Width = m_Settings.ViewportWidth;
			scd.BufferDesc.Height = m_Settings.ViewportHeight;
			scd.BufferDesc.Format = m_BackBufferFormat;
			scd.BufferDesc.RefreshRate.Numerator = 60;
			scd.BufferDesc.RefreshRate.Denominator = 1;
			scd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
			scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			
			scd.SampleDesc.Count = m_Settings.MsaaSampleCount;
			scd.SampleDesc.Quality = m_MsaaQuality - 1;
			
			scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			scd.BufferCount = SWAP_CHAIN_BUFFER_COUNT;
			scd.OutputWindow = m_hWnd;
			scd.Windowed = m_Settings.WindowMode;
			scd.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
			scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

			ThrowIfFailed(m_pDxgiFactory4->CreateSwapChain(m_pd3dCommandQueue.Get(), &scd, m_pSwapChain.GetAddressOf()));
		}

		// create descriptor heaps
		{
			D3D12_DESCRIPTOR_HEAP_DESC rtvDesc;
			rtvDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
			rtvDesc.NumDescriptors = SWAP_CHAIN_BUFFER_COUNT;
			rtvDesc.NodeMask = 0;
			rtvDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
			ThrowIfFailed(m_pd3dDevice->CreateDescriptorHeap(&rtvDesc, IID_PPV_ARGS(m_RtvHeap.GetAddressOf())));

			D3D12_DESCRIPTOR_HEAP_DESC dsvDesc;
			dsvDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
			dsvDesc.NumDescriptors = 1;
			dsvDesc.NodeMask = 0;
			dsvDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
			ThrowIfFailed(m_pd3dDevice->CreateDescriptorHeap(&dsvDesc, IID_PPV_ARGS(m_DsvHeap.GetAddressOf())));
		}

		// 创建各种管理器
		mRenderResourceFactory = std::make_unique<CD3D12RenderResourceFactory>(m_pd3dDevice.Get());
		GetEngineCore()->GetRenderResourceManager()->_SetRenderResourceFactory(mRenderResourceFactory.get());

		OnResize();

		// Reset the command list to prep for initialization commands.
		//ThrowIfFailed(m_pd3dCommandList->Reset(m_pd3dCommandAllocator.Get(), nullptr));

		BuildShadersAndInputLayout();
		BuildBoxGeometry();

		ExecuteCommandList();

		FlushCommandQueue();

		mRenderContextCount = GetEngineCore()->GetWorkerThreadCount();
		for (int i = 0; i < mRenderContextCount; i++) {
			mRenderContexts[i] = std::make_shared<CD3D12RenderContext>(this, i);
		}

		FlushCommandQueue();

		auto pMaterialManager = GetEngineCore()->GetRenderResourceManager()->GetMaterialManager();
		pMaterialManager->InitializeAfterGraphicsCreated();

		return true;
	}

	D3D12_CPU_DESCRIPTOR_HANDLE CD3D12RenderDevice::GetCurrentBackBufferView()
	{
		return CD3DX12_CPU_DESCRIPTOR_HANDLE(
			m_RtvHeap->GetCPUDescriptorHandleForHeapStart(),
			m_CurrentBackBufferIndex,
			D3D12Util::GetRtvDescriptorSize()
			
		);
	}

	D3D12_CPU_DESCRIPTOR_HANDLE CD3D12RenderDevice::GetDepthStencilView()
	{
		return m_DsvHeap->GetCPUDescriptorHandleForHeapStart();
	}

	ID3D12Resource* CD3D12RenderDevice::GetCurrentBackBuffer()
	{
		return m_pBackBuffers[m_CurrentBackBufferIndex].Get();
	}

	void CD3D12RenderDevice::OnResize()
	{
		//FlushCommandQueue();

		auto pCommandAllocator = mLocalFrameResources[mFrameResourceIndex].GetCommandAllocator();

		// must reset before using commandList
		//ThrowIfFailed(m_pd3dCommandList->Reset(m_pd3dCommandAllocator.Get(), nullptr));
		ThrowIfFailed(m_pd3dCommandList->Reset(pCommandAllocator, nullptr));

		// Destroy extant resources before re-creating
		for (int i = 0; i < SWAP_CHAIN_BUFFER_COUNT; ++i)
			m_pBackBuffers[i].Reset();
		m_pDepthStencilBuffer.Reset();

		// resize back buffers
		ThrowIfFailed(m_pSwapChain->ResizeBuffers(2, m_Settings.ViewportWidth, m_Settings.ViewportHeight,
			m_BackBufferFormat, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH));

		// get back buffers from swap chain
		{
			CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHeapHandle(m_RtvHeap->GetCPUDescriptorHandleForHeapStart());
			for (UINT i = 0; i < SWAP_CHAIN_BUFFER_COUNT; i++)
			{
				ThrowIfFailed(m_pSwapChain->GetBuffer(i, IID_PPV_ARGS(m_pBackBuffers[i].GetAddressOf())));

				m_pd3dDevice->CreateRenderTargetView(m_pBackBuffers[i].Get(), nullptr, rtvHeapHandle);

				rtvHeapHandle.Offset(1, D3D12Util::GetRtvDescriptorSize());
			}
		}

		// create depth-stencil-view
		{
			D3D12_RESOURCE_DESC desc;
			desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
			desc.Alignment = 0;
			desc.Width = m_Settings.ViewportWidth;
			desc.Height = m_Settings.ViewportHeight;
			desc.DepthOrArraySize = 1;
			desc.MipLevels = 1;
			desc.Format = m_DepthStencilFormat;
			desc.SampleDesc.Count = m_Settings.MsaaSampleCount;
			desc.SampleDesc.Quality = m_MsaaQuality - 1;
			desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
			desc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

			D3D12_CLEAR_VALUE clearValue;
			clearValue.Format = m_DepthStencilFormat;
			clearValue.DepthStencil.Depth = 1.0f;
			clearValue.DepthStencil.Stencil = 0;

			CD3DX12_HEAP_PROPERTIES heapProperties(D3D12_HEAP_TYPE_DEFAULT);

			ThrowIfFailed(m_pd3dDevice->CreateCommittedResource(
				&heapProperties,
				D3D12_HEAP_FLAG_NONE,
				&desc,
				D3D12_RESOURCE_STATE_COMMON,
				&clearValue,
				IID_PPV_ARGS(m_pDepthStencilBuffer.GetAddressOf())));

			m_pd3dDevice->CreateDepthStencilView(m_pDepthStencilBuffer.Get(), nullptr, GetDepthStencilView());

			CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(
				m_pDepthStencilBuffer.Get(),
				D3D12_RESOURCE_STATE_COMMON,
				D3D12_RESOURCE_STATE_DEPTH_WRITE
			);
			m_pd3dCommandList->ResourceBarrier(1, &barrier);
		}

		// set viewport and scissor rect
		{
			m_Viewport.Width = static_cast<float>(m_Settings.ViewportWidth);
			m_Viewport.Height = static_cast<float>(m_Settings.ViewportHeight);
			m_Viewport.TopLeftX = 0;
			m_Viewport.TopLeftY = 0;
			m_Viewport.MinDepth = 0;
			m_Viewport.MaxDepth = 1.0f;

			m_ScissorRect.top = 0;
			m_ScissorRect.left = 0;
			m_ScissorRect.right = static_cast<LONG>(m_Settings.ViewportWidth);
			m_ScissorRect.bottom = static_cast<LONG>(m_Settings.ViewportHeight);
		}

		// execute command list
		//ThrowIfFailed(m_pd3dCommandList->Close());
		//ID3D12CommandList* commandLists[] = { m_pd3dCommandList.Get() };
		//m_pd3dCommandQueue->ExecuteCommandLists(1, commandLists);
		//FlushCommandQueue();
	}

	void CD3D12RenderDevice::ExecuteCommandList()
	{
		m_pd3dCommandList->Close();
		ID3D12CommandList* commandLists[] = { m_pd3dCommandList.Get() };
		m_pd3dCommandQueue->ExecuteCommandLists(1, commandLists);
	}

	void CD3D12RenderDevice::ExecuteCommandList(ID3D12GraphicsCommandList* pCommandList)
	{
		pCommandList->Close();
		ID3D12CommandList* commandLists[] = { pCommandList };
		m_pd3dCommandQueue->ExecuteCommandLists(1, commandLists);
	}

	//IRenderContext* CD3D12RenderDevice::CreateRenderContext(int index)
	//{
	//	//auto pRenderContext = new CD3D12RenderContext(this, index);
	//	//mRenderContextCount++;
	//	//mRenderContexts[index] = pRenderContext;
	//	//pRenderContext->OnCreate();
	//	//return pRenderContext;
	//}

	void CD3D12RenderDevice::Render()
	{
		//std::for_each(mRenderContexts.begin(), mRenderContexts.end(), [](CD3D12RenderContext* pRenderContext) {
		//	if (pRenderContext)
		//	{
		//		pRenderContext->Render();
		//	}
		//});
	}

	void CD3D12RenderDevice::UpdateFrameConstBuffers(ETechniquePassStage stage)
	{
		SFrameConstBuffer::SForwardObject3D frameBuffer;
		IGraphicsSystem* pGraphicsSystem = GetEngineCore()->GetIGraphicsSystem();

		const SCameraViewTransformation& camera = pGraphicsSystem->GetSceneCameraTransformation();

		frameBuffer.View = camera.ViewMatrix;
		frameBuffer.InvView = camera.InvViewMatrix;
		frameBuffer.Proj = camera.ProjMatrix;
		frameBuffer.InvProj = camera.InvProjMatrix;
		frameBuffer.ViewProj = camera.ViewProjMatrix;
		frameBuffer.InvViewProj = camera.InvViewProjMatrix;

		frameBuffer.RenderTargetSize = Vector4f(
			(float)m_Settings.ViewportWidth, 
			(float)m_Settings.ViewportHeight,
			1.0f / m_Settings.ViewportWidth, 1.0f / m_Settings.ViewportHeight);

		frameBuffer.NearZ = camera.NearZ;
		frameBuffer.FarZ = camera.FarZ;
		frameBuffer.TotalTime = 0;
		frameBuffer.DeltaTime = 0;
		frameBuffer.CameraPos = camera.Position;
		mGlobalFrameResources[mFrameResourceIndex].SetFrameConstBufferData(EFrameConstBuffer::ForwardObject3D, frameBuffer);
	
		SFrameConstBuffer::STest testBuffer;
		//testBuffer.ViewProj = pCamera->GetViewProjTransform().getTransposed();
		testBuffer.ViewProj = camera.ViewProjMatrix;
		testBuffer.Color1 = Vector4f(1, 0, 0, 1);
		testBuffer.Color2 = Vector4f(0, 1, 0, 1);
		mGlobalFrameResources[mFrameResourceIndex].SetFrameConstBufferData(EFrameConstBuffer::Test, testBuffer);
	}

	void CD3D12RenderDevice::BeginFrame()
	{
		auto frameFence = mGlobalFrameResources[mFrameResourceIndex].mFenceValue;
		if (frameFence != 0 && m_Fence->GetCompletedValue() < frameFence)
		{
			HANDLE eventHandle = CreateEventEx(nullptr, 0, 0, EVENT_ALL_ACCESS);
			ThrowIfFailed(m_Fence->SetEventOnCompletion(frameFence, eventHandle));
			WaitForSingleObject(eventHandle, INFINITE);
			CloseHandle(eventHandle);
		}

		mGlobalFrameResources[mFrameResourceIndex].Reset();

		UpdateFrameConstBuffers(ePass_Forward);

		auto pCommandAllocator = mLocalFrameResources[mFrameResourceIndex].GetCommandAllocator();

		m_pd3dCommandList->Reset(pCommandAllocator, nullptr);


		CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(
			GetCurrentBackBuffer(),
			D3D12_RESOURCE_STATE_PRESENT,
			D3D12_RESOURCE_STATE_RENDER_TARGET
		);
		m_pd3dCommandList->ResourceBarrier(1, &barrier);

		D3D12_CPU_DESCRIPTOR_HANDLE backBufferView = GetCurrentBackBufferView();
		D3D12_CPU_DESCRIPTOR_HANDLE depthStencilView = GetDepthStencilView();

		m_pd3dCommandList->OMSetRenderTargets(1, &backBufferView, true, &depthStencilView);
		m_pd3dCommandList->RSSetViewports(1, &m_Viewport);
		m_pd3dCommandList->RSSetScissorRects(1, &m_ScissorRect);

		float clearColor[] = { 0.529f, 0.807f, 0.980f, 1.0f };
		//float clearColor[] = { 0, 0, 0, 1 };
		m_pd3dCommandList->ClearRenderTargetView(backBufferView,
			clearColor, 0, nullptr);

		m_pd3dCommandList->ClearDepthStencilView(depthStencilView,
			D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);

		ExecuteCommandList();
	}

	void CD3D12RenderDevice::EndFrame()
	{
		auto pCommandAllocator = mLocalFrameResources[mFrameResourceIndex].GetCommandAllocator();

		CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(
			GetCurrentBackBuffer(),
			D3D12_RESOURCE_STATE_RENDER_TARGET,
			D3D12_RESOURCE_STATE_PRESENT
		);

		m_pd3dCommandList->Reset(pCommandAllocator, nullptr);
		m_pd3dCommandList->ResourceBarrier(1, &barrier);

		ExecuteCommandList();

		m_pSwapChain->Present(0, 0);

		m_CurrentBackBufferIndex = (m_CurrentBackBufferIndex + 1) % SWAP_CHAIN_BUFFER_COUNT;

		FlushCommandQueue();
	}

	void CD3D12RenderDevice::FlushCommandQueue()
	{
		m_CurrentFence++;
		//ThrowIfFailed(m_pd3dCommandQueue->Signal(m_Fence.Get(), m_CurrentFence));

		//if (m_Fence->GetCompletedValue() < m_CurrentFence)
		//{
		//	HANDLE eventHandle = CreateEventEx(nullptr, false, false, EVENT_ALL_ACCESS);
		//	ThrowIfFailed(m_Fence->SetEventOnCompletion(m_CurrentFence, eventHandle));
		//	WaitForSingleObject(eventHandle, INFINITE);
		//	CloseHandle(eventHandle);
		//}

		mGlobalFrameResources[mFrameResourceIndex].mFenceValue = m_CurrentFence;
		ThrowIfFailed(m_pd3dCommandQueue->Signal(m_Fence.Get(), m_CurrentFence));

		//mGlobalFrameResources[mFrameResourceIndex].FinishFrame(m_pd3dCommandQueue.Get());
		mFrameResourceIndex = (mFrameResourceIndex + 1) % FRAME_RESOURCE_COUNT;
	}


	void CD3D12RenderDevice::BuildShadersAndInputLayout()
	{
		//HRESULT hr = S_OK;

		//CD3D12RenderResourceFactory* pResourceFactory = (CD3D12RenderResourceFactory*)mRenderResourceFactory.get();
		//auto pShaderManager = pResourceFactory->GetShaderManager();
		auto pResourceManager = GetEngineCore()->GetRenderResourceManager();

		//std::vector<std::string> defines;
		//defines.push_back("RED_FLAG");

		//ITechnique* pTechInfo = GetEngineCore()->GetRenderResourceManager()->GetTechniqueByName("PBR");
		//auto pRenderPass = (CD3D12RenderPass*)pResourceFactory->CreateRenderPass(&pTechInfo->mPassMetas[ePass_Forward], defines);
		//mVertexShader = pRenderPass->GetShaderInstance(VERTEX_SHADER_TYPE);
		//mPixelShader = pRenderPass->GetShaderInstance(PIXEL_SHADER_TYPE);

		//ITechnique* pTechInfo2 = GetEngineCore()->GetRenderResourceManager()->GetTechniqueByName("PBR");
		//auto pRenderPass2 = (CD3D12RenderPass*)pResourceFactory->CreateRenderPass(&pTechInfo2->mPassMetas[ePass_Forward], TShaderMacroList());
		//auto VertexShader2 = pRenderPass2->GetShaderInstance(VERTEX_SHADER_TYPE);
		//auto PixelShader2 = pRenderPass2->GetShaderInstance(PIXEL_SHADER_TYPE);
		auto pMaterialManager = pResourceManager->GetMaterialManager();
		auto pPbrMat = pMaterialManager->GetMaterial("PBRCube");
		auto pPureColorMat = pMaterialManager->GetMaterial("PureColorCube");

		for (size_t i = 0; i < CUBE_NUM_X * CUBE_NUM_Y; i++) {
			IMaterial* pMat = nullptr;
			if (MathHelper::Rand(0, 1) == 0) {
				pMat = pMaterialManager->CopyMaterial(pPbrMat);
				Vector4f diffuseColor(MathHelper::RandF(), MathHelper::RandF(), MathHelper::RandF(), 1);
				pMat->SetParam("DiffuseAlbedo", diffuseColor);
			}
			else
			{
				pMat = pMaterialManager->CopyMaterial(pPureColorMat);
			}
			pMat->FlushParams();
			mCubeMaterials.push_back(pMat);
		}
	}

	void CD3D12RenderDevice::BuildBoxGeometry()
	{
		//std::array<SVertex_P3_N3_T2, 8> vertices =
		//{
		//	SVertex_P3_N3_T2({ Vector3f(-1.0f, -1.0f, -1.0f),  Vector4f(1, 1, 1, 1) }),
		//	SVertex_P3_N3_T2({ Vector3f(-1.0f, +1.0f, -1.0f), Vector4f(0, 0, 0, 1) }),
		//	SVertex_P3_N3_T2({ Vector3f(+1.0f, +1.0f, -1.0f), Vector4f(1, 0, 0, 1) }),
		//	SVertex_P3_N3_T2({ Vector3f(+1.0f, -1.0f, -1.0f), Vector4f(0, 1, 0, 1) }),
		//	SVertex_P3_N3_T2({ Vector3f(-1.0f, -1.0f, +1.0f), Vector4f(0, 0, 1, 1) }),
		//	SVertex_P3_N3_T2({ Vector3f(-1.0f, +1.0f, +1.0f), Vector4f(1, 1, 0, 1) }),
		//	SVertex_P3_N3_T2({ Vector3f(+1.0f, +1.0f, +1.0f), Vector4f(0, 1, 1, 1) }),
		//	SVertex_P3_N3_T2({ Vector3f(+1.0f, -1.0f, +1.0f), Vector4f(1, 0, 1, 1) })
		//};

		//std::array<std::uint16_t, 36> indices =
		//{
		//	// front face
		//	0, 1, 2,
		//	0, 2, 3,

		//	// back face
		//	4, 6, 5,
		//	4, 7, 6,

		//	// left face
		//	4, 5, 1,
		//	4, 1, 0,

		//	// right face
		//	3, 2, 6,
		//	3, 6, 7,

		//	// top face
		//	1, 5, 6,
		//	1, 6, 2,

		//	// bottom face
		//	4, 0, 3,
		//	4, 3, 7
		//};

		//const UINT vbByteSize = (UINT)vertices.size() * sizeof(Vertex);
		//const UINT ibByteSize = (UINT)indices.size() * sizeof(std::uint16_t);

		//SGeometryData<SVertex_P3_N3_T2, uint16_t>	meshGeom;
		SGeometryData<SVertex_P3_N3_TG3_T2> meshGeom;
		GeometryGenerator::CreateBox(meshGeom, 2, 2, 2, 1);
		SMeshData meshData = meshGeom.GetMeshData();

		auto pResourceManager = GetEngineCore()->GetRenderResourceManager();
		auto pMeshManager = pResourceManager->GetMeshManager();
		mBoxMesh = pMeshManager->CreateMesh("box", meshData);

		//GeometryGenerator::CreateSphere(meshData, 1.5f, 20, 20);
		//GeometryGenerator::CreateGeosphere(meshData, 1.5f, 3);
		//GeometryGenerator::CreateCylinder(meshData, 0.5f, 0.3f, 2.5f, 10, 10);
		//GeometryGenerator::CreateGrid(meshData, 2, 2, 1, 1);

		//auto& vertices = meshData.Vertices;
		//auto& indices = meshData.Indices;

		//const UINT vbByteSize = (UINT)vertices.size() * sizeof(SVertex_P3_N3_T2);
		//const UINT ibByteSize = (UINT)indices.size() * sizeof(std::uint16_t);

		//mBoxGeo = std::make_unique<MeshGeometry>();
		//mBoxGeo->Name = "boxGeo";

		////ThrowIfFailed(D3DCreateBlob(vbByteSize, &mBoxGeo->VertexBufferCPU));
		////CopyMemory(mBoxGeo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

		////ThrowIfFailed(D3DCreateBlob(ibByteSize, &mBoxGeo->IndexBufferCPU));
		////CopyMemory(mBoxGeo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

		//mBoxGeo->VertexBufferGPU = D3D12Util::CreateDefaultBuffer(m_pd3dDevice.Get(),
		//	m_pd3dCommandList.Get(), vertices.data(), vbByteSize, mBoxGeo->VertexBufferUploader);

		//mBoxGeo->IndexBufferGPU = D3D12Util::CreateDefaultBuffer(m_pd3dDevice.Get(),
		//	m_pd3dCommandList.Get(), indices.data(), ibByteSize, mBoxGeo->IndexBufferUploader);

		//mBoxGeo->VertexByteStride = sizeof(SVertex_P3_N3_T2);
		//mBoxGeo->VertexBufferByteSize = vbByteSize;
		//mBoxGeo->IndexFormat = DXGI_FORMAT_R16_UINT;
		//mBoxGeo->IndexBufferByteSize = ibByteSize;

		//SubmeshGeometry submesh;
		//submesh.IndexCount = (UINT)indices.size();
		//submesh.StartIndexLocation = 0;
		//submesh.BaseVertexLocation = 0;

		//mBoxGeo->DrawArgs["box"] = submesh;
	}

	IRenderContext* CD3D12RenderDevice::GetRenderContext(int index)
	{
		return mRenderContexts[index].get();
	}

}
