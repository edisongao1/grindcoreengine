#include "stdafx.h"
#include "D3D12Mesh.h"
#include "AsynchronizeTaskQueue.h"
#include "D3D12RenderContext.h"
#include "D3D12Util.h"

namespace grind
{


	CD3D12MeshBufferManager::CD3D12MeshBufferManager(ID3D12Device* pd3dDevice)
		:md3dDevice(pd3dDevice)
	{
		auto queue = GetEngineCore()->GetAsynchronizeTaskQueue();
		
		// ETaskExecuteTime::LoadRenderResources
		queue->SetRequestHandler<SCreateMeshBufferRequest>(
			ETaskType::CreateMeshBuffer,
			[this](SCreateMeshBufferRequest* req, STaskContext* context) {
		
			IMesh* pMesh = req->pMesh;
			const SMeshData* data = pMesh->GetMeshData();
			if (data == nullptr || data->VertexData == nullptr || data->IndiceData == nullptr) 
			{
				pMesh->OnCreateBufferFailed();
				return;
			}

			pMesh->_SetState(EMeshState::BufferCreating);
			CD3D12MeshBuffer* pMeshBuffer = new CD3D12MeshBuffer(pMesh, md3dDevice);
			pMeshBuffer->Create(context->pRenderContext, data);
			
			SCreateMeshBufferFinishedRequest finishedRequest;
			finishedRequest.pMesh = pMesh;
			finishedRequest.pMeshBuffer = pMeshBuffer;
			context->pQueue->PushRequest<ETaskExecuteTime::BeforeRenderScene_SyncResourceFrame>(ETaskType::CreateMeshBufferFinished, finishedRequest);
		});
	}

	CD3D12MeshBuffer::CD3D12MeshBuffer(IMesh* pMesh, ID3D12Device* pd3dDevice)
		:IMeshBuffer(pMesh)
		, md3dDevice(pd3dDevice)
	{

	}

	//bool CD3D12MeshBuffer::Create()
	//{
	//	return true;
	//}

	void CD3D12MeshBuffer::Create(IRenderContext* pRenderContext, const SMeshData* meshData)
	{
		auto pd3dRenderContext = dynamic_cast<CD3D12RenderContext*>(pRenderContext);
		ID3D12GraphicsCommandList* pd3dCommandList = pd3dRenderContext->GetD3D12CommandList();
		
		// create vertex buffer
		mVertexBuffer = D3D12Util::CreateDefaultBuffer(md3dDevice, pd3dCommandList, meshData->VertexData,
			meshData->VertexCount * meshData->VertexSize, mVertexUploadBuffer);

		// create index buffer
		mIndexBuffer = D3D12Util::CreateDefaultBuffer(md3dDevice, pd3dCommandList, meshData->IndiceData,
			meshData->IndiceCount * meshData->IndexSize, mIndexUploadBuffer);
	}

	void CD3D12MeshBuffer::OnCreateCompleted()
	{
		mIndexUploadBuffer = nullptr;
		mVertexUploadBuffer = nullptr;
	
		const SMeshData* meshData = mMesh->GetMeshData();
		mVertexBufferView.BufferLocation = mVertexBuffer->GetGPUVirtualAddress();
		mVertexBufferView.StrideInBytes = meshData->VertexSize;
		mVertexBufferView.SizeInBytes = meshData->VertexSize * meshData->VertexCount;

		mIndexBufferView.BufferLocation = mIndexBuffer->GetGPUVirtualAddress();
		mIndexBufferView.Format = meshData->IndexSize == 2 ? DXGI_FORMAT_R16_UINT : DXGI_FORMAT_R32_UINT;
		mIndexBufferView.SizeInBytes = meshData->IndexSize * meshData->IndiceCount;
	}

	void CD3D12MeshBuffer::Bind(ID3D12GraphicsCommandList* pd3dCmdList)
	{
		//m_pd3dCommandList->IASetVertexBuffers(0, 1, &vbView);
		//m_pd3dCommandList->IASetIndexBuffer(&ibView);
		pd3dCmdList->IASetVertexBuffers(0, 1, &mVertexBufferView);
		pd3dCmdList->IASetIndexBuffer(&mIndexBufferView);
	}

}

