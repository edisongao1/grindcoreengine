#ifndef PCH_H
#define PCH_H

#include <stdio.h>
#include <Windows.h>
#include <d3d12.h>
#include <dxgi.h>
#include <dxgi1_4.h>
#include "d3dx12.h"
#include <string>
#include <vector>
#include <string>
#include <array>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <functional>
#include <thread>
#include <DirectXMath.h>
#include <assert.h>
#include <DirectXColors.h>
#include <DirectXPackedVector.h>
#include <DirectXCollision.h>
#include <D3Dcompiler.h>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <wrl.h>
#include <comdef.h>

#endif
