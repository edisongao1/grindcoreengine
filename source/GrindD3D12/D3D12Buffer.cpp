#include "stdafx.h"
#include "D3D12Buffer.h"

namespace grind
{
	//CD3D12ConstantBuffer::CD3D12ConstantBuffer(UINT elementSize, 
	//	UINT elementCount, ID3D12Device* pd3dDevice)
	//	: mUploadBuffer(elementSize * elementCount, pd3dDevice)
	//	, mElementCount(elementCount)
	//	, mElementSize(elementSize)
	//{
	//	D3D12_DESCRIPTOR_HEAP_DESC cbvHeapDesc;
	//	cbvHeapDesc.NumDescriptors = elementCount;
	//	cbvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	//	cbvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	//	cbvHeapDesc.NodeMask = 0;

	//	ThrowIfFailed(pd3dDevice->CreateDescriptorHeap(&cbvHeapDesc,
	//		IID_PPV_ARGS(&mCbvHeap)));

	//	D3D12_GPU_VIRTUAL_ADDRESS cbAddress = mUploadBuffer.Resource()->GetGPUVirtualAddress();
	//	CD3DX12_CPU_DESCRIPTOR_HANDLE heapHandle(mCbvHeap->GetCPUDescriptorHandleForHeapStart());

	//	for (UINT i = 0; i < elementCount; i++)
	//	{
	//		auto cbViewAddress = cbAddress + i * elementCount;

	//		D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc;
	//		cbvDesc.BufferLocation = cbViewAddress;
	//		cbvDesc.SizeInBytes = elementSize;

	//		pd3dDevice->CreateConstantBufferView(
	//			&cbvDesc,
	//			heapHandle);

	//		heapHandle.Offset(1, D3D12Util::GetSrvDescriptorSize());
	//	}

	//	mHeapHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(mCbvHeap->GetGPUDescriptorHandleForHeapStart());
	//}	

	CD3D12FrameConstantBuffer::CD3D12FrameConstantBuffer(ID3D12Device* device)
		:md3dDevice(device)
	{
		mConstBufferSizes[(int)EFrameConstBuffer::ForwardObject3D] = D3D12Util::CalcConstantBufferByteSize<SFrameConstBuffer::SForwardObject3D>();
		mConstBufferSizes[(int)EFrameConstBuffer::Test] = D3D12Util::CalcConstantBufferByteSize<SFrameConstBuffer::STest>();

		UINT currentPos = 0;
		for (int i = 0; i < ConstBufferCount; i++) {
			mConstBufferPositions[i] = currentPos;
			currentPos += mConstBufferSizes[i];
		}
		mBufferSize = currentPos;

		mUploadBuffer = std::make_unique<CD3D12UploadBuffer>(mBufferSize, md3dDevice);
		mGPUAddress = mUploadBuffer->GetGPUVirtualAddress();
	}

	void CD3D12FrameConstantBuffer::Write(EFrameConstBuffer cbType, const void* data, UINT dataSize)
	{
		int i = (int)cbType;
		mUploadBuffer->CopyData(data, mConstBufferPositions[i], dataSize);
	}

	CD3D12BlockConstantBuffer::CD3D12BlockConstantBuffer(ID3D12Device* device,
		UINT elementSize)
		:mUploadBuffer(elementSize * BLOCK_COUNT, device)
		, mElementSize(elementSize)
	{
		memset(mBlockAllocated, 0, sizeof(mBlockAllocated));

		//D3D12_DESCRIPTOR_HEAP_DESC cbvHeapDesc;
		//cbvHeapDesc.NumDescriptors = ELEMENT_COUNT;
		//cbvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		//cbvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		//cbvHeapDesc.NodeMask = 0;
		//ThrowIfFailed(md3dDevice->CreateDescriptorHeap(&cbvHeapDesc,
		//	IID_PPV_ARGS(&mCbvHeap)));

		//for (UINT i = 0; i < ELEMENT_COUNT; i++) {
		//	D3D12_CONSTANT_BUFFER_VIEW_DESC	cbvDesc;
		//	cbvDesc.BufferLocation = i * elementSize;
		//	cbvDesc.SizeInBytes = elementSize;
		//	md3dDevice->CreateConstantBufferView(
		//		&cbvDesc,
		//		mCbvHeap->GetCPUDescriptorHandleForHeapStart());
		//}
	}

	void CD3D12BlockConstantBuffer::SetBlockData(int blockIndex, void* data)
	{
		assert(blockIndex < BLOCK_COUNT);
		mUploadBuffer.CopyData(data, blockIndex * mElementSize, mElementSize);
	}

	D3D12_GPU_VIRTUAL_ADDRESS CD3D12BlockConstantBuffer::GetGPUAddress(int blockIndex) const
	{
		return mUploadBuffer.GetGPUVirtualAddress() + blockIndex * mElementSize;
	}

	//int CD3D12BlockConstantBuffer::AllocateBlock()
	//{
	//	if (IsFull())
	//		return -1;

	//	int i = mCurrentAllocateIndex;
	//	while (mBlockAllocated[i]) {
	//		i = (i + 1) % BLOCK_COUNT;
	//	}

	//	mBlockAllocated[i] = true;
	//	mCurrentAllocateIndex = (i + 1) % BLOCK_COUNT;
	//	mAllocateBlockCount += 1;
	//	return i;
	//}

	//void CD3D12BlockConstantBuffer::ReleaseBlock(int i)
	//{
	//	mBlockAllocated[i] = false;
	//	if (mBlockAllocated[mCurrentAllocateIndex]) {
	//		mCurrentAllocateIndex = i;
	//	}
	//	mAllocateBlockCount -= 1;
	//}

	//bool CD3D12BlockConstantBuffer::IsFull() const
	//{
	//	return mAllocateBlockCount == BLOCK_COUNT;
	//}

	

}


