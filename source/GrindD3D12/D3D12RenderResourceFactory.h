#pragma once

#include "IRenderResourceFactory.h"
#include "D3D12Shader.h"
#include "D3D12PipelineStateObject.h"
#include "D3D12MaterialResourceManager.h"
#include "D3D12TextureResource.h"
#include "D3D12Mesh.h"

using Microsoft::WRL::ComPtr;

namespace grind
{


	class CD3D12InputLayoutManager
	{
	public:
		CD3D12InputLayoutManager();
		D3D12_INPUT_LAYOUT_DESC GetInputLayout(EFVF fvf, bool bContainTangent = false);

	private:
		D3D12_INPUT_LAYOUT_DESC					mInputLayoutCache[(uint32_t)EFVF::Count][E_FVF_Flag::FlagCount];
		enum { _ElementDescCacheSize = (1 << E_FVF_Flag::FlagCount) * (uint32_t)EFVF::Count };
		std::vector<D3D12_INPUT_ELEMENT_DESC>	mInputElementDescsCache[_ElementDescCacheSize];
	};

	struct SRootParameterIndexes
	{
		static const UINT INVALID_INDEX = 0xffffffff;
		UINT		ObjectConstBuffer = INVALID_INDEX;
		UINT		FrameConstBuffer = INVALID_INDEX;
		UINT		MaterialConstBuffer = INVALID_INDEX;
	};

	struct SD3D12RootSignature
	{
		ID3D12RootSignature*			pd3dRootSignature = nullptr;
		SRootParameterIndexes			RootParameterIndexes;
	};

	class CD3D12RootSignatureManager
	{
	public:
		CD3D12RootSignatureManager(ID3D12Device* device);
		~CD3D12RootSignatureManager();
		//ID3D12RootSignature* GetRootSignature(IRenderPass* pRenderPass);
		const SD3D12RootSignature* GetRootSignature(IRenderPass* pRenderPass) const;

	private:
		std::array<CD3DX12_STATIC_SAMPLER_DESC, 6>		mStaticSamplers;

		// binary code: 
		// 1 bit(object buffer) + 1 bit(frame buffer) + 
		// 1 bit(material buffer)
		// 4 bit(maximum to 16 material textures)
		enum { BINARY_CODE_BITS = 7 };
		//ComPtr<ID3D12RootSignature>		mRootSignatures[1 << BINARY_CODE_BITS];
		//SRootParameterIndexes			mRootParameterIndexes[1 << BINARY_CODE_BITS];
		std::array<SD3D12RootSignature, 1 << BINARY_CODE_BITS> mRootSignatures;
		//SD3D12RootSignature					mRootSignatures[1 << BINARY_CODE_BITS];

		static bool IsContainObjectBuffer(uint32_t code) { return code & (1U << 6); }
		static bool IsContainFrameBuffer(uint32_t code) { return code & (1U << 5); }
		static bool IsContainMaterialBuffer(uint32_t code) { return code & (1U << 4); }
		static int GetMaterialTexturesCount(uint32_t code) { return (int)(code & 0x0f); }
		void BuildStaticSamplers();
		void BuildAllRootSignatures(ID3D12Device* d3dDevice);
		
	};

	class CD3D12RenderResourceFactory : public IRenderResourceFactory
	{
	public:
		CD3D12RenderResourceFactory(ID3D12Device* device);
		virtual IMaterialResourceManager* GetMaterialResourceManager() override;

		virtual IRenderPass* CreateRenderPass(SRenderPassMeta* pTechData,
			const std::vector<std::string>& matFlags) override;
	
		CD3D12ShaderManager* GetShaderManager() { return mShaderManager.get(); }
		CD3D12PipelineStateManager* GetPipelineStateManager() { return mPipelineStateManager.get(); }
		D3D12_INPUT_LAYOUT_DESC GetInputLayoutDesc(EFVF fvf, bool bContainTangent = false);

		const SD3D12RootSignature* GetRootSignature(IRenderPass* pRenderPass) const;
		ID3D12Device* GetD3DDevice() const { return md3dDevice; }
	private:
		ID3D12Device*		md3dDevice;
		ComPtr<ID3D12RootSignature>						mRootSignature;

		std::unique_ptr<CD3D12ShaderManager>			mShaderManager;
		std::unique_ptr<CD3D12RenderPassManager>		mRenderPassManager;
		std::unique_ptr<CD3D12PipelineStateManager>		mPipelineStateManager;
		std::unique_ptr<CD3D12MaterialResourceManager>	mMaterialResourceManager;
		std::unique_ptr<CD3D12TextureResourceManager>	mTextureResourceManager;
		std::unique_ptr<CD3D12MeshBufferManager>		mMeshBufferManager;
		std::unique_ptr<CD3D12RootSignatureManager>		mRootSignatureManager;
		CD3D12InputLayoutManager						mInputLayoutManager;
	};
}