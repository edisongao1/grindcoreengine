#pragma once
#include "IMaterial.h"
#include "D3D12Shader.h"

using Microsoft::WRL::ComPtr;


namespace grind
{
	class CD3D12RenderResourceFactory;

	struct SPipelineStateObjectWrapper
	{
		ComPtr<ID3D12PipelineState>				pd3dPipelineStateObject;
		D3D12_GRAPHICS_PIPELINE_STATE_DESC		Desc;
	};

	class CD3D12PipelineStateManager
	{
	public:
		CD3D12PipelineStateManager(CD3D12RenderResourceFactory* pRenderResourceFactory);

		ID3D12PipelineState* GetOrCreatePipelineStateObject(const SRenderStatesDesc& desc, CD3D12RenderPass* pRenderPass, 
			ID3D12RootSignature* pRootSignature);


	private:
		D3D12_GRAPHICS_PIPELINE_STATE_DESC GetD3DPipelineStateDesc(const SRenderStatesDesc& renderState, 
			CD3D12RenderPass* pRenderPass, ID3D12RootSignature* pRootSignature);

		static uint64_t ComputeD3DPipelineStateDescHash(D3D12_GRAPHICS_PIPELINE_STATE_DESC& d3dDesc);

		static bool CompareD3DPipelineStateDescs(
			 D3D12_GRAPHICS_PIPELINE_STATE_DESC& psoDesc1,
			 D3D12_GRAPHICS_PIPELINE_STATE_DESC& psoDesc2);
		
		std::unordered_map<uint64_t, std::unique_ptr<SPipelineStateObjectWrapper>>	mStateObjectsMap;
		std::mutex		mMutex;

		CD3D12RenderResourceFactory*	mRenderResourceFactory = nullptr;
		ID3D12Device*					md3dDevice = nullptr;

	};
}