#pragma once

#include "LockFreeStack.h"
#include "D3D12Buffer.h"
#include "ThreadLocalContainer.h"
#include "Common.h"
#include "IMaterialResourceManager.h"
#include "IRenderDevice.h"

namespace grind
{
	enum { CONSTANT_BUFFER_SIZE = 256 * 2};
	

	//////////////////////////////////////////////////////////////////////////
	// for command allcotor
	struct SCommandAllocator 
		: public TContainerItemWithNextPointer<ID3D12CommandAllocator> 
	{
	public:
		void Reset() 
		{
			ThrowIfFailed(pObject->Reset());
		}

		~SCommandAllocator()
		{
			SAFE_RELEASE(pObject);
		}
	};

	using TCommandAllocatorStack = LockFreePointerStack<SCommandAllocator>;
	using TObjectConstantBufferStack = LockFreePointerStack<CD3D12ObjectConstantBuffer>;

	class CLocalFrameResource;

	class CGlobalFrameResource
	{
	public:
		enum { CB_COUNT = 256 };
		enum { CB_ELEMENT_MIN_SIZE = 256 };

		~CGlobalFrameResource();
		void Init(ID3D12Device* pd3dDevice);
		void Reset();

		template<typename T>
		void SetFrameConstBufferData(EFrameConstBuffer cbType, const T& data)
		{
			mFrameConstantBuffer->Write(cbType, &data, sizeof(T));
		}

		D3D12_GPU_VIRTUAL_ADDRESS GetFrameConstBufferGPUAddress(EFrameConstBuffer cbType)
		{
			if (cbType == EFrameConstBuffer::None)
				return 0;
			return mFrameConstantBuffer->GetGPUVirtualAddress(cbType);
		}

		CD3D12ObjectConstantBuffer* GetObjectConstantBuffer();
		SCommandAllocator* GetCommandAllocator();

		ID3D12Device*						md3dDevice;
		std::vector<CLocalFrameResource*>	mVecLoadFrameResources;

		TCommandAllocatorStack				mCommandAllocatorStack;
		TObjectConstantBufferStack			mObjectConstBufferStack;

		std::unique_ptr<CD3D12FrameConstantBuffer>	mFrameConstantBuffer;

		uint64_t							mFenceValue = 0;
	};

	class CLocalFrameResource : public ILocalFrameResource
	{
	public:
		enum { CB_COUNT = CGlobalFrameResource::CB_COUNT };
		enum { CB_ELEMENT_MIN_SIZE = CGlobalFrameResource::CB_ELEMENT_MIN_SIZE };

		CLocalFrameResource();
		~CLocalFrameResource();

		void Init(CGlobalFrameResource* pGlobalFrameResource, ID3D12Device* pd3dDevice);
		void Reset();

		ID3D12CommandAllocator* GetCommandAllocator();
		
		virtual uint64_t SetObjectConstBuffer(void* data, uint32_t size) override;

		//template<typename T>
		//D3D12_GPU_VIRTUAL_ADDRESS SetObjectConstantBufferData(T* data)
		//{
		//	return SetObjectConstantBufferData((void*)data, sizeof(T));
		//}

		CGlobalFrameResource* GetGlobalFrameResource() { return m_pGlobalFrameResource; }

	private:
		ID3D12Device*					m_pd3dDevice;
		CGlobalFrameResource*			m_pGlobalFrameResource;

		CD3D12ObjectConstantBuffer*		mCurrentObjectConstantBuffer;
		
		SCommandAllocator*				mCurrentCommandAllcator;
	};


}


