#include "stdafx.h"
#include "D3D12TextureResource.h"
#include "StringParseUtils.h"
#include "DDSTextureLoader.h"
#include "D3D12RenderResourceFactory.h"
#include "AsynchronizeTaskQueue.h"
#include "D3D12RenderContext.h"

namespace grind
{
	//bool CD3D12TextureResource2D::LoadFromFile(ID3D12GraphicsCommandList* pd3dCommandList)
	//{
	//	auto pFileSystem = GetEngineCore()->GetFileSystem();

	//	const auto& strFileName = mTexture->GetFileName();

	//	char szFullPath[MAX_PATH] = { 0 };
	//	pFileSystem->GetFileFullPath(strFileName.c_str(), szFullPath);
	//	std::wstring wstrFileName = StringParseUtils::AnsiToWString(szFullPath);

	//	auto hr = DirectX::CreateDDSTextureFromFile12(md3dDevice,
	//		pd3dCommandList, wstrFileName.c_str(),
	//		mResource, mUploadHeap);

	//	ThrowIfFailed(hr);
	//	return true;
	//}



	void CD3D12TextureResource2D::CreateFromDDSData(
		ID3D12GraphicsCommandList* cmdList,
		const DirectX::DDS_HEADER* header,
		const uint8_t* bitData,
		size_t bitSize)
	{
		auto hr = DirectX::CreateTextureFromDDS12(md3dDevice, cmdList, header, 
			bitData, bitSize, 0, false, mResource, mUploadHeap);

		ThrowIfFailed(hr);
	}

	void CD3D12TextureResource2D::CreateShaderResourceView(D3D12_CPU_DESCRIPTOR_HANDLE hDescriptor)
	{
		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.Format = mResource->GetDesc().Format;
		srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = mResource->GetDesc().MipLevels;
		srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;
		md3dDevice->CreateShaderResourceView(mResource.Get(), &srvDesc, hDescriptor);
	}

	void CD3D12TextureResource2D::OnCompleteLoadResource()
	{
		//mUploadHeap->Release();
		mUploadHeap = nullptr;
		mResourceDesc = mResource->GetDesc();
	}

	//ITextureResource* CD3D12RenderResourceFactory::CreateTextureResource(ITexture* pTexture, void* p)
	//{
	//	ID3D12GraphicsCommandList* pCommandList = (ID3D12GraphicsCommandList*)p;

	//	auto eTextureType = pTexture->GetTextureType();
	//	if (eTextureType == ETextureType::Texture2D)
	//	{
	//		auto pd3dTextureResource = new CD3D12TextureResource2D(pTexture, md3dDevice);
	//		pd3dTextureResource->LoadFromFile(pCommandList);
	//		return pd3dTextureResource;
	//	}
	//	return nullptr;
	//}


	CD3D12TextureResourceManager::CD3D12TextureResourceManager(ID3D12Device* device)
		:md3dDevice(device)
	{
		auto taskQueue = GetEngineCore()->GetAsynchronizeTaskQueue();
		taskQueue->SetRequestHandler<SCreateTextureResourceRequest>(
			ETaskType::CreateTextureResource, 
			[this](SCreateTextureResourceRequest* req, STaskContext* context) {

			auto pRenderContext = dynamic_cast<CD3D12RenderContext*>(context->pRenderContext);
			ITexture* pTexture = req->pTexture;
			ITextureResource* pTextureResource = nullptr;
			ID3D12GraphicsCommandList* pd3dCommandList = pRenderContext->GetD3D12CommandList();

			auto eTextureType = pTexture->GetTextureType();
			if (eTextureType == ETextureType::Texture2D)
			{
				CD3D12TextureResource2D* pd3dTextureResource = new CD3D12TextureResource2D(pTexture, md3dDevice);
				auto ddsHeader = reinterpret_cast<DirectX::DDS_HEADER*>(req->pddsHeader);
				pd3dTextureResource->CreateFromDDSData(pd3dCommandList, ddsHeader, req->pBitData, req->bitSize);
				pTextureResource = pd3dTextureResource;
			}

			if (!pTextureResource)
			{
				pTexture->OnFailLoadResource();
				return;
			}

			SCreateTextureResourceFinishedRequest ctrfReq;
			ctrfReq.pTexture = pTexture;
			ctrfReq.pTextureResource = pTextureResource;

			context->pQueue->PushRequest<ETaskExecuteTime::BeforeRenderScene_SyncResourceFrame>(
				ETaskType::CreateTextureResourceFinished, ctrfReq);
		});
	}

}

