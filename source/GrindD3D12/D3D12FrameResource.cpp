#include "stdafx.h"
#include "Common.h"
#include "D3D12FrameResource.h"

namespace grind
{

	CLocalFrameResource::CLocalFrameResource() 
		: m_pGlobalFrameResource(nullptr)
		, mCurrentObjectConstantBuffer(nullptr)
		, mCurrentCommandAllcator(nullptr)
	{
	}

	void CLocalFrameResource::Init(CGlobalFrameResource* pGlobalFrameResource, 
		ID3D12Device* pd3dDevice)
	{
		m_pGlobalFrameResource = pGlobalFrameResource;
		m_pd3dDevice = pd3dDevice;
		m_pGlobalFrameResource->mVecLoadFrameResources.push_back(this);
	}

	ID3D12CommandAllocator* CLocalFrameResource::GetCommandAllocator()
	{
		auto p = m_pGlobalFrameResource->GetCommandAllocator();
		p->next = (intptr_t)mCurrentCommandAllcator;
		mCurrentCommandAllcator = p;
		return p->pObject;
	}

	uint64_t CLocalFrameResource::SetObjectConstBuffer(void* data, uint32_t size)
	{
		UINT byteSize = D3D12Util::CalcConstantBufferByteSize(size);
		if (mCurrentObjectConstantBuffer == nullptr || mCurrentObjectConstantBuffer->IsFull(byteSize))
		{
			auto pNewConstantBuffer = m_pGlobalFrameResource->GetObjectConstantBuffer();
			pNewConstantBuffer->next = (intptr_t)mCurrentObjectConstantBuffer;
			mCurrentObjectConstantBuffer = pNewConstantBuffer;
		}
		return mCurrentObjectConstantBuffer->Write(data, size);
	}

	void CLocalFrameResource::Reset()
	{
		// reclaim command allocators
		while (mCurrentCommandAllcator) {
			auto p = mCurrentCommandAllcator;
			mCurrentCommandAllcator = (SCommandAllocator*)(p->next);
			p->Reset();
			m_pGlobalFrameResource->mCommandAllocatorStack.push(p);
		}

		// reclaim constant buffers
		while (mCurrentObjectConstantBuffer) {
			auto p = mCurrentObjectConstantBuffer;
			mCurrentObjectConstantBuffer = (CD3D12ObjectConstantBuffer*)(p->next);
			p->Reset();
			m_pGlobalFrameResource->mObjectConstBufferStack.push(p);
		}
	}

	CLocalFrameResource::~CLocalFrameResource()
	{
	}

	void CGlobalFrameResource::Init(ID3D12Device* pd3dDevice)
	{
		//mGlobalConstantBufferManager.Init(pd3dDevice);
		md3dDevice = pd3dDevice;
		mFrameConstantBuffer = std::make_unique<CD3D12FrameConstantBuffer>(pd3dDevice);
	}

	void CGlobalFrameResource::Reset()
	{
		for (auto pLocalFrameResource : mVecLoadFrameResources) {
			pLocalFrameResource->Reset();
		}
	}

	CD3D12ObjectConstantBuffer* CGlobalFrameResource::GetObjectConstantBuffer()
	{
		auto pConstantBuffer = mObjectConstBufferStack.pop();
		if (pConstantBuffer == nullptr) {
			pConstantBuffer = new CD3D12ObjectConstantBuffer(CONSTANT_BUFFER_SIZE, md3dDevice);
		}
		pConstantBuffer->next = 0;
		return pConstantBuffer;
	}

	SCommandAllocator* CGlobalFrameResource::GetCommandAllocator()
	{
		auto pCommandAllocator = mCommandAllocatorStack.pop();
		if (pCommandAllocator == nullptr) {
			pCommandAllocator = new SCommandAllocator();
			ThrowIfFailed(md3dDevice->CreateCommandAllocator(
				D3D12_COMMAND_LIST_TYPE_DIRECT,
				IID_PPV_ARGS(&pCommandAllocator->pObject)));
		}
		pCommandAllocator->next = 0;
		return pCommandAllocator;
	}

	CGlobalFrameResource::~CGlobalFrameResource()
	{
		Reset();
		while (SCommandAllocator* pCommandAllocator = mCommandAllocatorStack.pop()) {
			SAFE_DELETE(pCommandAllocator);
		}
		while (auto* pConstantBuffer = mObjectConstBufferStack.pop()) {
			SAFE_DELETE(pConstantBuffer);
		}
	}

}
