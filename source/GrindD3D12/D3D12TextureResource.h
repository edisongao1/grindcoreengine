#pragma once
#include "ITexture.h"
#include "DDSTextureLoader.h"

using Microsoft::WRL::ComPtr;


namespace grind
{
	class CD3D12TextureResourceBase : public ITextureResource
	{
	public:
		CD3D12TextureResourceBase(ITexture* pTexture)
			:ITextureResource(pTexture)
		{

		}

		//virtual D3D12_SHADER_RESOURCE_VIEW_DESC GetShaderResourceViewDesc() = 0;
		virtual void CreateShaderResourceView(D3D12_CPU_DESCRIPTOR_HANDLE hDescriptor) = 0;

	protected:
		D3D12_RESOURCE_DESC		mResourceDesc;
	};

	class CD3D12TextureResource2D : public CD3D12TextureResourceBase
	{
	public:
		CD3D12TextureResource2D(ITexture* pTexture, ID3D12Device* pd3dDevice)
			:CD3D12TextureResourceBase(pTexture), md3dDevice(pd3dDevice)
		{
			
		}

		//bool LoadFromFile(ID3D12GraphicsCommandList* pd3dCommandList);
		void CreateFromDDSData(
			ID3D12GraphicsCommandList* cmdList,
			const DirectX::DDS_HEADER* header,
			const uint8_t* bitData,
			size_t bitSize);

		virtual void CreateShaderResourceView(D3D12_CPU_DESCRIPTOR_HANDLE hDescriptor) override;
		virtual void OnCompleteLoadResource() override;
	private:
		ID3D12Device*				md3dDevice = nullptr;
		ComPtr<ID3D12Resource>		mResource = nullptr;
		ComPtr<ID3D12Resource>		mUploadHeap = nullptr;
	};

	class CD3D12TextureResourceManager
	{
	public:
		CD3D12TextureResourceManager(ID3D12Device* d3dDevice);
		CD3D12TextureResourceBase* GetTextureResource(const std::string& name)
		{
			auto it = mTextureResourceMap.find(name);
			if (it == mTextureResourceMap.end())
				return nullptr;
			return it->second.get();
		}

	private:
		ID3D12Device*			md3dDevice;
		using TTextureResourceMap = std::unordered_map<std::string, std::unique_ptr<CD3D12TextureResourceBase>>;
		TTextureResourceMap		mTextureResourceMap;
	};
}
