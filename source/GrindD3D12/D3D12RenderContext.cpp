#include "stdafx.h"
#include "D3D12RenderDevice.h"
#include "D3D12RenderContext.h"
#include "IEngineCore.h"
#include "Common.h"
#include "ICamera.h"
#include "D3D12Mesh.h"
#include "D3D12Util.h"

static const float CUBE_INTERVAL = 0.5f;


namespace grind
{
	CD3D12RenderContext::CD3D12RenderContext(CD3D12RenderDevice* pDevice, int id)
		: mId(id)
		, m_pDevice(pDevice)
		, m_pd3dDevice(m_pDevice->m_pd3dDevice)
		, m_pd3dCommandQueue(m_pDevice->m_pd3dCommandQueue)
	{
		float fStartX = -(CUBE_NUM_X - 1) * CUBE_INTERVAL * 0.5f;
		float fStartY = -(CUBE_NUM_Y - 1) * CUBE_INTERVAL * 0.5f;

		for (int i = 0; i < CUBE_NUM_Y; i++) {
			for (int j = 0; j < CUBE_NUM_X; j++) {
				int index = i * CUBE_NUM_X + j;

				if (index % GetEngineCore()->GetWorkerThreadCount() != mId)
					continue;

				float x = fStartX + j * CUBE_INTERVAL;
				float y = fStartY + i * CUBE_INTERVAL;

				m_arrCubeIndexs.push_back(index);
				m_arrPositions.push_back(Vector3f(x, y, 0));
				m_arrRotations.push_back(Vector3f(MathHelper::RandF(), MathHelper::RandF(), MathHelper::RandF()));
			}
		}

		m_vecWorldTransforms.resize(m_arrPositions.size());

		for (int i = 0; i < FRAME_RESOURCE_COUNT; i++) {
			mLocalFrameResources[i].Init(&m_pDevice->mGlobalFrameResources[i], m_pd3dDevice.Get());
		}

		auto pMaterialResourceManager = GetEngineCore()->GetRenderResourceManager()->GetMaterialResourceManager();
		m_pd3dMaterialResourceManager = dynamic_cast<CD3D12MaterialResourceManager*>(pMaterialResourceManager);
		mRenderResourceFactory = m_pDevice->mRenderResourceFactory.get();

		Init();
	}

	CD3D12RenderContext::~CD3D12RenderContext()
	{
	}

	bool CD3D12RenderContext::Init()
	{
		// create command allocator and command list
		mCurrentLocalFrameResource = &mLocalFrameResources[m_pDevice->mFrameResourceIndex];

		auto pCommandAllocator = mCurrentLocalFrameResource->GetCommandAllocator();

		ThrowIfFailed(m_pd3dDevice->CreateCommandList(
			0,
			D3D12_COMMAND_LIST_TYPE_DIRECT,
			pCommandAllocator, // Associated command allocator
			nullptr,                   // Initial PipelineStateObject
			IID_PPV_ARGS(m_pd3dCommandList.GetAddressOf())));

		m_pd3dCommandList->Close();

		ThrowIfFailed(m_pd3dCommandList->Reset(pCommandAllocator, nullptr));

		//BuildRootSignature();
		//BuildPSO();

		// Execute the initialization commands.
		ThrowIfFailed(m_pd3dCommandList->Close());
		ID3D12CommandList* cmdsLists[] = { m_pd3dCommandList.Get() };
		m_pd3dCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

		return true;
	}

	void CD3D12RenderContext::BeginCommands()
	{
		mCurrentLocalFrameResource = &mLocalFrameResources[m_pDevice->mFrameResourceIndex];
		auto pCommandAllocator = mCurrentLocalFrameResource->GetCommandAllocator();
		ThrowIfFailed(m_pd3dCommandList->Reset(pCommandAllocator, nullptr));
		mLastDrawcallState.Reset();
	}

	void CD3D12RenderContext::EndCommands()
	{
		m_pDevice->ExecuteCommandList(m_pd3dCommandList.Get());
	}


	bool CD3D12RenderContext::GetDrawcallState(IMaterial* pMaterial, ETechniquePassStage stage, uint64_t runningMask, __out SD3D12DrawcallState* pDrawcallState)
	{
		if (!pMaterial->IsCreated()) {
			pMaterial->TryCreateMaterialResource();
			return false;
		}

		CD3D12RenderPass* pRenderPass = (CD3D12RenderPass*)pMaterial->GetRenderPass(stage, runningMask);
		if (pRenderPass == nullptr) {
			return false;
		}

		const SD3D12RootSignature* pRootSignature = mRenderResourceFactory->GetRootSignature(pRenderPass);

		// TODO:
		//SRenderStatesDesc renderState;
		//renderState.WireFrame = m_pDevice->mWireMode;
		//renderState.ComputeHash();

		SRenderStatesDesc renderState = pMaterial->GetRenderStateDesc(stage);
		renderState.WireFrame = m_pDevice->mWireMode;
		renderState.ComputeHash();

		ID3D12PipelineState* pPipelineStateObject = pRenderPass->GetPipelineStateObject(renderState, pRootSignature->pd3dRootSignature);
		if (pPipelineStateObject == nullptr)
			return false;

		auto frameBufferAddress = mCurrentLocalFrameResource->GetGlobalFrameResource()->GetFrameConstBufferGPUAddress(pRenderPass->GetFrameConstBufferType());
		
		SMaterialResourceId materiaReslId = pMaterial->GetMaterialResourceId();
		ID3D12DescriptorHeap* pMatDescriptorHeap = nullptr;

		if (materiaReslId.IsValid()) {
			pDrawcallState->HasMaterialResource = true;
			pDrawcallState->MaterialResDescriptorHandle = m_pd3dMaterialResourceManager->GetGPUDescriptorHandle(materiaReslId, &pMatDescriptorHeap);
		}
		else {
			pDrawcallState->HasMaterialResource = false;
		}

		pDrawcallState->pRenderPass = pRenderPass;
		pDrawcallState->pRootSignature = pRootSignature;
		pDrawcallState->pPipelineStateObject = pPipelineStateObject;
		pDrawcallState->FrameBufferGPUAddress = frameBufferAddress;
		pDrawcallState->pDescriptorHeaps[0] = pMatDescriptorHeap;
		pDrawcallState->MaterialResourceId = materiaReslId;

		return true;
	}

	uint64_t CD3D12RenderContext::GetRunningMask(IMesh* pMesh) const
	{
		uint64_t mask = 0;
		uint8_t fvfFlags = pMesh->GetFVFFlags();
		if (fvfFlags & E_FVF_Flag::ContainTangent)
		{
			mask |= EShaderRunningMacro::Tangent;
		}
		
		// global flag
		mask |= m_pDevice->mRunningMask;
		return mask;
	}

	CD3D12MeshBuffer* GetMeshBuffer(IMesh* pMesh)
	{
		if (!pMesh->IsCreated())
		{
			pMesh->TryCreate();
			return nullptr;
		}
		return static_cast<CD3D12MeshBuffer*>(pMesh->GetMeshBuffer());
	}

	void CD3D12RenderContext::RenderMeshItemQueue(CMeshRenderItemQueue* pMeshItemQueue)
	{
		mCurrentLocalFrameResource = &mLocalFrameResources[m_pDevice->mFrameResourceIndex];
		auto pCommandAllocator = mCurrentLocalFrameResource->GetCommandAllocator();
		ThrowIfFailed(m_pd3dCommandList->Reset(pCommandAllocator, nullptr));
		mLastDrawcallState.Reset();
		mLastMeshBuffer = nullptr;

		m_pd3dCommandList->RSSetViewports(1, &m_pDevice->m_Viewport);
		m_pd3dCommandList->RSSetScissorRects(1, &m_pDevice->m_ScissorRect);

		D3D12_CPU_DESCRIPTOR_HANDLE backBufferViewHandle = m_pDevice->GetCurrentBackBufferView();
		D3D12_CPU_DESCRIPTOR_HANDLE depthViewHandle = m_pDevice->GetDepthStencilView();

		m_pd3dCommandList->OMSetRenderTargets(1, &backBufferViewHandle, true, &depthViewHandle);
		m_pd3dCommandList->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		//mNeedExecuteCommands = false;
		SMeshRenderItem* renderItem = pMeshItemQueue->GetNextToRead();
		while (renderItem != nullptr)
		{
			RenderMeshItem(renderItem);
			renderItem = pMeshItemQueue->GetNextToRead();
		}

		if (mDrawcallCounter > 0)
		{
			m_pDevice->ExecuteCommandList(m_pd3dCommandList.Get());
		}
		else
		{
			m_pd3dCommandList->Close();
		}
	}

	void CD3D12RenderContext::RenderMeshItem(SMeshRenderItem* pRenderItem)
	{
		IMaterial* pMaterial = pRenderItem->pMaterial;
		IMesh* pMesh = pRenderItem->pMesh;

		CD3D12MeshBuffer* pMeshBuffer = GetMeshBuffer(pRenderItem->pMesh);
		if (pMeshBuffer == nullptr) {
			return;
		}

		if (pMeshBuffer != mLastMeshBuffer)
		{
			mLastMeshBuffer = pMeshBuffer;
			pMeshBuffer->Bind(m_pd3dCommandList.Get());
		}
		
		uint64_t runningMask = GetRunningMask(pMesh);

		SD3D12DrawcallState drawcallState;
		if (!GetDrawcallState(pMaterial, ePass_Forward, runningMask, &drawcallState))
		{
			return;
		}
		
		if (drawcallState.pPipelineStateObject != mLastDrawcallState.pPipelineStateObject) {
			m_pd3dCommandList->SetPipelineState(drawcallState.pPipelineStateObject);
		}

		const SD3D12RootSignature* pRootSignature = drawcallState.pRootSignature;
		if (pRootSignature != mLastDrawcallState.pRootSignature) {
			m_pd3dCommandList->SetGraphicsRootSignature(pRootSignature->pd3dRootSignature);
		}

		auto objectCB = SetObjectConstBuffer(mCurrentLocalFrameResource, drawcallState.pRenderPass, pRenderItem->WorldMatrix);

		if (objectCB != 0) {
			m_pd3dCommandList->SetGraphicsRootConstantBufferView(pRootSignature->RootParameterIndexes.ObjectConstBuffer, objectCB);
		}

		if ((drawcallState.FrameBufferGPUAddress != mLastDrawcallState.FrameBufferGPUAddress || 
			pRootSignature->RootParameterIndexes.FrameConstBuffer != mLastDrawcallState.pRootSignature->RootParameterIndexes.FrameConstBuffer)
			&& drawcallState.FrameBufferGPUAddress != 0)
		{
			m_pd3dCommandList->SetGraphicsRootConstantBufferView(pRootSignature->RootParameterIndexes.FrameConstBuffer, drawcallState.FrameBufferGPUAddress);
		}

		if (drawcallState.pDescriptorHeaps[0] != mLastDrawcallState.pDescriptorHeaps[0]
			|| drawcallState.pDescriptorHeaps[1] != mLastDrawcallState.pDescriptorHeaps[1])
		{
			ID3D12DescriptorHeap* descriptorHeaps[2] = { 0 };
			int iDescriptorHeapCount = 0;
			if (drawcallState.pDescriptorHeaps[0]) {
				descriptorHeaps[iDescriptorHeapCount++] = drawcallState.pDescriptorHeaps[0];
			}
			if (drawcallState.pDescriptorHeaps[1]) {
				descriptorHeaps[iDescriptorHeapCount++] = drawcallState.pDescriptorHeaps[1];
			}

			m_pd3dCommandList->SetDescriptorHeaps(iDescriptorHeapCount, descriptorHeaps);
		}

		if (drawcallState.HasMaterialResource &&
			(drawcallState.MaterialResourceId != mLastDrawcallState.MaterialResourceId ||
				pRootSignature->RootParameterIndexes.MaterialConstBuffer != mLastDrawcallState.pRootSignature->RootParameterIndexes.MaterialConstBuffer))
		{
			m_pd3dCommandList->SetGraphicsRootDescriptorTable(pRootSignature->RootParameterIndexes.MaterialConstBuffer, drawcallState.MaterialResDescriptorHandle);
		}

		mLastDrawcallState = drawcallState;

		const auto pSubMesh = pMesh->GetSubMesh(pRenderItem->iSubMeshIndex);
		m_pd3dCommandList->DrawIndexedInstanced(pSubMesh->IndexCount, 1, pSubMesh->StartIndexLocation, 0, 0);

		//mNeedExecuteCommands = true;
		mDrawcallCounter += 1;
	}

	void CD3D12RenderContext::Render()
	{
		mCurrentLocalFrameResource = &mLocalFrameResources[m_pDevice->mFrameResourceIndex];

		Update();

		//if (mCurrentRenderPass == nullptr)
		//	return;
		
		auto pCommandAllocator = mCurrentLocalFrameResource->GetCommandAllocator();
		ThrowIfFailed(m_pd3dCommandList->Reset(pCommandAllocator, nullptr));
		mLastDrawcallState.Reset();
		mLastMeshBuffer = nullptr;

		m_pd3dCommandList->RSSetViewports(1, &m_pDevice->m_Viewport);
		m_pd3dCommandList->RSSetScissorRects(1, &m_pDevice->m_ScissorRect);

		D3D12_CPU_DESCRIPTOR_HANDLE backBufferViewHandle = m_pDevice->GetCurrentBackBufferView();
		D3D12_CPU_DESCRIPTOR_HANDLE depthViewHandle = m_pDevice->GetDepthStencilView();

		m_pd3dCommandList->OMSetRenderTargets(1, &backBufferViewHandle, true, &depthViewHandle);

		//m_pd3dCommandList->SetGraphicsRootSignature(pRootSignature);

		//D3D12_VERTEX_BUFFER_VIEW vbView = m_pDevice->mBoxGeo->VertexBufferView();
		//D3D12_INDEX_BUFFER_VIEW ibView = m_pDevice->mBoxGeo->IndexBufferView();

		//m_pd3dCommandList->IASetVertexBuffers(0, 1, &vbView);
		//m_pd3dCommandList->IASetIndexBuffer(&ibView);
		m_pd3dCommandList->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		bool bNeedExecute = false;

		//CD3DX12_GPU_DESCRIPTOR_HANDLE cbvHeapHandle(mCbvHeap->GetGPUDescriptorHandleForHeapStart());
		for (int i = 0; i < m_arrPositions.size(); i++)
		{
			int cubeIndex = m_arrCubeIndexs[i];
			// just simulate that each entity has its mesh
			IMesh* pMesh = m_pDevice->mBoxMesh;
			CD3D12MeshBuffer* pMeshBuffer = GetMeshBuffer(pMesh);
			if (pMeshBuffer == nullptr) {
				continue;
			}

			if (pMeshBuffer != mLastMeshBuffer)
			{
				mLastMeshBuffer = pMeshBuffer;
				pMeshBuffer->Bind(m_pd3dCommandList.Get());	
			}

			IMaterial* pMaterial = m_pDevice->mCubeMaterials[cubeIndex];
			uint64_t runningMask = GetRunningMask(pMesh);

			SD3D12DrawcallState drawcallState;
			if (!GetDrawcallState(pMaterial, ePass_Forward, runningMask, &drawcallState))
			{
				continue;
			}

			bNeedExecute = true;

			if (drawcallState.pPipelineStateObject != mLastDrawcallState.pPipelineStateObject) {
				m_pd3dCommandList->SetPipelineState(drawcallState.pPipelineStateObject);
			}
			
			const SD3D12RootSignature* pRootSignature = drawcallState.pRootSignature;
			if (pRootSignature != mLastDrawcallState.pRootSignature) {
				m_pd3dCommandList->SetGraphicsRootSignature(pRootSignature->pd3dRootSignature);
			}

			auto objectCB = SetObjectConstBuffer(mCurrentLocalFrameResource, drawcallState.pRenderPass, m_vecWorldTransforms[i]);
			
			if (objectCB != 0) {
				m_pd3dCommandList->SetGraphicsRootConstantBufferView(pRootSignature->RootParameterIndexes.ObjectConstBuffer, objectCB);
			}
			
			if (drawcallState.FrameBufferGPUAddress != mLastDrawcallState.FrameBufferGPUAddress
				&& drawcallState.FrameBufferGPUAddress != 0)
			{
				m_pd3dCommandList->SetGraphicsRootConstantBufferView(pRootSignature->RootParameterIndexes.FrameConstBuffer, drawcallState.FrameBufferGPUAddress);
			}

			if (drawcallState.pDescriptorHeaps[0] != mLastDrawcallState.pDescriptorHeaps[0]
				|| drawcallState.pDescriptorHeaps[1] != mLastDrawcallState.pDescriptorHeaps[1])
			{
				ID3D12DescriptorHeap* descriptorHeaps[2] = { 0 };
				int iDescriptorHeapCount = 0;
				if (drawcallState.pDescriptorHeaps[0]) {
					descriptorHeaps[iDescriptorHeapCount++] = drawcallState.pDescriptorHeaps[0];
				}
				if (drawcallState.pDescriptorHeaps[1]) {
					descriptorHeaps[iDescriptorHeapCount++] = drawcallState.pDescriptorHeaps[1];
				}

				m_pd3dCommandList->SetDescriptorHeaps(iDescriptorHeapCount, descriptorHeaps);
			}			

			if (drawcallState.HasMaterialResource && 
				drawcallState.MaterialResourceId != mLastDrawcallState.MaterialResourceId)
			{
				m_pd3dCommandList->SetGraphicsRootDescriptorTable(pRootSignature->RootParameterIndexes.MaterialConstBuffer, drawcallState.MaterialResDescriptorHandle);
			}

			mLastDrawcallState = drawcallState;

			const auto pSubMesh = pMesh->GetSubMesh(0);
			m_pd3dCommandList->DrawIndexedInstanced(pSubMesh->IndexCount, 1, pSubMesh->StartIndexLocation, 0, 0);

			//m_pd3dCommandList->DrawIndexedInstanced(
			//	m_pDevice->mBoxGeo->DrawArgs["box"].IndexCount,
			//	1, 0, 0, 0);

			
		}

		if (bNeedExecute)
		{
			m_pDevice->ExecuteCommandList(m_pd3dCommandList.Get());
		}
		else
		{
			m_pd3dCommandList->Close();
		}

		//m_pd3dCommandList->Close();
		//m_pDevice->ExecuteCommandList(m_pd3dCommandList.Get());
	}

	void CD3D12RenderContext::BeginFrame()
	{
		//mCurrentLocalFrameResource = &mLocalFrameResources[m_pDevice->mFrameResourceIndex];
		//mCurrentLocalFrameResource->Reset();
		mDrawcallCounter = 0;

		IGraphicsSystem* pGraphicsSystem = GetEngineCore()->GetIGraphicsSystem();
		mCameraTransformation = pGraphicsSystem->GetSceneCameraTransformation();
	}

	void CD3D12RenderContext::EndFrame()
	{
		
	}

	void CD3D12RenderContext::Update()
	{
		float dt = GetEngineCore()->GetTimer()->GetTickDelta();

		//IRenderPass* pPass = m_pDevice->mMaterial->GetRenderPass(ePass_Forward, m_pDevice->mRunningMask);
		//if (mCurrentRenderPass != pPass)
		//{
		//	mCurrentRenderPass = pPass;
		//	//BuildPSO();
		//}

		//UINT objCBByteSize = D3D12Util::CalcConstantBufferByteSize(sizeof(ObjectConstants));
		
		for (int i = 0; i < m_arrPositions.size(); i++) 
		{
			Vector3f rot = m_arrRotations[i];
			rot.x += dt * 1;
			rot.y += dt * 2;
			m_arrRotations[i] = rot;
			Vector3f pos = m_arrPositions[i];

			Matrix4x4 transM = glm::identity<Matrix4x4>();
			//transM.setTranslation(pos);
			transM = glm::translate(transM, pos);

			Matrix4x4 rotX = glm::identity<Matrix4x4>();
			rotX = glm::rotate(rotX, rot.x, Vector3f(1.0f, 0.0f, 0));
			Matrix4x4 rotY = glm::identity<Matrix4x4>();
			rotY = glm::rotate(rotY, rot.y, Vector3f(0.0f, 1.0f, 0));
			Matrix4x4 rotM = rotY * rotX;
			//rotM.setRotationRadians(rot);
			//rotM = glm::rotate()


			Matrix4x4 scaleM = glm::identity<Matrix4x4>();
			//scaleM.setScale(0.15f);
			scaleM = glm::scale(scaleM, Vector3f(0.15f, 0.15f, 0.15f));

			//Matrix4x4 world = scaleM * rotM * transM;
			Matrix4x4 world = transM * rotM * scaleM;
			
			Vector4f p1 = world * Vector4f(0, 0, 0, 1);

			m_vecWorldTransforms[i] = world;
		}
	}

	// 这个方法未来会改成Entity的一个成员函数
	uint64_t CD3D12RenderContext::SetObjectConstBuffer(ILocalFrameResource* pLocalFrameResource,
		IRenderPass* pRenderPass, const Matrix4x4& worldMatrix)
	{
		//ICamera* pCamera = GetEngineCore()->GetCamera();
		//ICamera* pCamera = GetEngineCore()->GetISceneManager()->GetMainCamera();

		auto cbType = pRenderPass->GetObjectConstBufferType();
		uint64_t handle = 0;
		if (cbType == EObjectConstBuffer::ForwardObject3D) {
			SObjectConstBuffer::SForwardObject3D objConstants;
			//objConstants.World = worldMatrix.getTransposed();
			objConstants.World = worldMatrix;
			//objConstants.WorldViewProj = pCamera->GetViewProjTransform() * worldMatrix;
			objConstants.WorldViewProj = mCameraTransformation.ViewProjMatrix * worldMatrix;

			handle = mCurrentLocalFrameResource->SetObjectConstBuffer(&objConstants, sizeof(objConstants));
		}
		else if (cbType == EObjectConstBuffer::Test) {
			SObjectConstBuffer::STest testBuffer;
			//testBuffer.World = worldMatrix.getTransposed();
			testBuffer.World = worldMatrix;
			testBuffer.Color1 = Vector4f(0, 0, 1, 1);
			testBuffer.Color2 = Vector4f(0, 1, 1, 1);
			handle = mCurrentLocalFrameResource->SetObjectConstBuffer(&testBuffer, sizeof(testBuffer));
		}
		return handle;
	}



}

