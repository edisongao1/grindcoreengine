#include "stdafx.h"
#include "D3D12GraphicsFactory.h"
#include "D3D12RenderDevice.h"
#include "IEngineCore.h"

namespace grind
{
	IEngineCore* g_pEngineCore;
	IConsole* g_pConsole;

	IRenderDevice* CD3D12GraphicsFactory::CreateRenderDevice()
	{
		return new CD3D12RenderDevice();
	}

	void CD3D12GraphicsFactory::ReleaseRenderDevice(IRenderDevice* p)
	{
		SAFE_DELETE(p);
		g_pEngineCore->_SetRenderDevice(nullptr);
	}

	IGraphicsFactory* CreateFactory()
	{
		return new CD3D12GraphicsFactory();
	}

	void DestroyFactory(IGraphicsFactory* pFactory)
	{
		if (pFactory) {
			delete pFactory;
		}
	}

	void SetGlobalEnvironment(IEngineCore* pEngineCore)
	{
		g_pEngineCore = pEngineCore;
		g_pConsole = pEngineCore->GetConsole();
	}

}

