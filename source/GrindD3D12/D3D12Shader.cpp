#include "stdafx.h"
#include "D3D12Util.h"
#include "D3D12Shader.h"
#include "IEngineCore.h"
#include "AsynchronizeTaskQueue.h"

namespace grind
{
	//TShaderMacroMaskMap CD3D12ShaderManager::mMacroMaskMap;

	static const char* GetShaderTarget(EShaderType eShaderType)
	{
		switch (eShaderType)
		{
		case VERTEX_SHADER_TYPE: return "vs_5_1";
		case PIXEL_SHADER_TYPE: return "ps_5_1";
		case GEOMETRY_SHADER_TYPE: return "gs_5_1";
		case HULL_SHADER_TYPE: return "hs_5_1";
		case DOMAIN_SHADER_TYPE: return "ds_5_1";
		case COMPUTE_SHADER_TYPE: return "cs_5_1";
		default: break;
		}
		return 0;
	}

	ComPtr<ID3DBlob> CD3D12ShaderFactory::CompileShader(const std::string& filename, 
		const std::vector<std::string>& defines, const std::string& entrypoint, EShaderType eShaderType)
	{
		D3D_SHADER_MACRO macros[65];
		for (size_t i = 0; i < defines.size(); i++) {
			macros[i].Name = defines[i].c_str();
			macros[i].Definition = "1";
		}
		macros[defines.size()].Name = NULL;
		macros[defines.size()].Definition = NULL;

		UINT compileFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)  
		compileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif
		HRESULT hr = S_OK;

		ComPtr<ID3DBlob> byteCode = nullptr;
		ComPtr<ID3DBlob> errors;

		wchar_t wszFileName[256];
		//mbstowcs(wszFileName, filename.c_str(), filename.length() + 1);
		size_t PtNumOfCharConverted;
		mbstowcs_s(&PtNumOfCharConverted, wszFileName, filename.c_str(), filename.length() + 1);

		hr = D3DCompileFromFile(wszFileName, macros, D3D_COMPILE_STANDARD_FILE_INCLUDE,
			entrypoint.c_str(), GetShaderTarget(eShaderType), compileFlags, 0, &byteCode, &errors);

		if (errors != nullptr)
			OutputDebugStringA((char*)errors->GetBufferPointer());

		ThrowIfFailed(hr);

		return byteCode;
	}

	CD3D12ShaderInstance* CD3D12ShaderFactory::CreateShaderInstance(CD3D12NamedShader* pNamedShader,
		const std::vector<std::string>& defines)
	{
		auto eShaderType = pNamedShader->GetShaderType();
		char szFilePath[MAX_PATH];
		sprintf_s(szFilePath, "%s/Shaders/%s", GetEngineCore()->GetFileSystem()->GetBasePath(),
			pNamedShader->GetFileName().c_str());

		//GetEngineCore()->GetFileSystem()->GetFileFullPath(pNamedShader->GetFileName().c_str(), szFilePath);

		ComPtr<ID3DBlob> blob = CompileShader(szFilePath,
			defines, pNamedShader->GetEntryPoint(), eShaderType);
		
		//uint64_t mask = GetEngineCore()->GetRenderResourceManager()->GetShaderMacroMask(defines);
		//uint64_t mask = CD3D12ShaderManager::GetDefineMask(eShaderType, defines);
		uint64_t mask = pNamedShader->GetShaderMacroMask(defines);
		CD3D12ShaderInstance* pShaderInstance = new CD3D12ShaderInstance(blob, mask);
		return pShaderInstance;
	}


	CD3D12ShaderManager::CD3D12ShaderManager()
	{
		//GetEngineCore()->GetRenderResourceManager()->GetShaderDefineMaskFromXml(mMacroMaskMap);
	}	

	CD3D12NamedShader* CD3D12ShaderManager::GetOrCreateNamedShader(const std::string& strShaderName, EShaderType eShaderType)
	{
		uint32_t crc = CRC::Calculate(strShaderName.c_str(), strShaderName.length(), CRC::CRC_32());

		std::unique_lock<std::mutex> lock(mMutex[eShaderType]);
		auto it = mNamedShadersMap[eShaderType].find(crc);
		if (it != mNamedShadersMap[eShaderType].end()) {
			return it->second.get();
		}

		auto pNamedShader = std::make_unique<CD3D12NamedShader>(strShaderName, eShaderType);
		auto pNamedShaderPtr = pNamedShader.get();
		mNamedShadersMap[eShaderType].insert(std::make_pair(crc, std::move(pNamedShader)));
		return pNamedShaderPtr;
	}

	CD3D12ShaderInstance* CD3D12ShaderManager::GetOrCreateShaderInstance(
		const std::string& filename,
		const std::vector<std::string>& defines,
		const std::string& entrypoint,
		EShaderType	eShaderType)
	{
		std::string strShaderName = StringParseUtils::RemoveFileExtension(filename) + "@" + entrypoint;
		CD3D12NamedShader* pNamedShader = GetOrCreateNamedShader(strShaderName, eShaderType);

		CD3D12ShaderInstance* pShader = pNamedShader->GetOrCreateShaderInstance(defines);
		return pShader;
	}

	CD3D12NamedShader::CD3D12NamedShader(const std::string& shaderName, 
		EShaderType shaderType) 
		: mShaderType(shaderType)
		, mNameStr(shaderName)
	{
		ExtractShaderFileNameAndEntry(mNameStr, mStrFileName, mStrEntryPoint);
		mValidateMacrosMask = GetEngineCore()->GetRenderResourceManager()->GetValidateShaderMacrosMask(shaderName);
	}

	CD3D12ShaderInstance* CD3D12NamedShader::GetOrCreateShaderInstance(const std::vector<std::string>& defines)
	{
		auto pResourceManager = GetEngineCore()->GetRenderResourceManager();
		uint64_t mask = GetShaderMacroMask(defines);
		std::unique_lock<std::mutex> lock(mMutex);
		auto it = mShaderInstances.find(mask);
		if (it != mShaderInstances.end()) {
			return it->second.get();
		}

		std::vector<std::string> filteredDefines = pResourceManager->FilterShaderMacroStrings(defines, mValidateMacrosMask);
		CD3D12ShaderInstance* pShaderInstance = CD3D12ShaderFactory::CreateShaderInstance(this, filteredDefines);
		mShaderInstances.insert(std::make_pair(mask, std::unique_ptr<CD3D12ShaderInstance>(pShaderInstance)));
		return pShaderInstance;
	}

	uint64_t CD3D12NamedShader::GetShaderMacroMask(const std::vector<std::string>& defines) const
	{
		uint64_t mask = GetEngineCore()->GetRenderResourceManager()->GetShaderMacroMask(defines);
		return mask & mValidateMacrosMask;
	}

	CD3D12ShaderInstance::CD3D12ShaderInstance(ComPtr<ID3DBlob> pd3dBlob, uint64_t mask)
		: md3dBlob(pd3dBlob)
		, mDefineMask(mask)
	{
		ComPtr<ID3D12ShaderReflection>	pShaderReflection = nullptr;
		ThrowIfFailed(D3DReflect(md3dBlob->GetBufferPointer(), md3dBlob->GetBufferSize(),
			IID_PPV_ARGS(&pShaderReflection)));

		D3D12_SHADER_DESC shaderDesc;
		pShaderReflection->GetDesc(&shaderDesc);

		auto pConstantBufferReflection = pShaderReflection->GetConstantBufferByName("cbPerObject");
		D3D12_SHADER_BUFFER_DESC shaderBuffDesc;
		pConstantBufferReflection->GetDesc(&shaderBuffDesc);
	}

	D3D12_SHADER_BYTECODE CD3D12ShaderInstance::GetByteCode() const
	{
		D3D12_SHADER_BYTECODE shaderByteCode;
		shaderByteCode.pShaderBytecode = md3dBlob->GetBufferPointer();
		shaderByteCode.BytecodeLength = md3dBlob->GetBufferSize();
		return shaderByteCode;
	}

	//void CD3D12ShaderInstance::ExtractMetaInfo(EFrameConstants& eFrameConstantsType, 
	//	EObjectConstants& eObjectsConstantsType)
	//{
	//	if (eFrameConstantsType != EFrameConstants::Unknown &&
	//		eObjectsConstantsType != EObjectConstants::Unknown)
	//	{
	//		return;
	//	}

	//	ComPtr<ID3D12ShaderReflection>	pShaderReflection = nullptr;
	//	ThrowIfFailed(D3DReflect(md3dBlob->GetBufferPointer(), md3dBlob->GetBufferSize(),
	//		IID_PPV_ARGS(&pShaderReflection)));

	//	D3D12_SHADER_DESC shaderDesc;
	//	pShaderReflection->GetDesc(&shaderDesc);

	//	auto ConstantBufferCount = shaderDesc.ConstantBuffers;
	//	for (UINT i = 0; i < ConstantBufferCount; i++) {
	//		auto pConstantBufferReflection = pShaderReflection->GetConstantBufferByIndex(i);
	//		D3D12_SHADER_BUFFER_DESC shaderBufferDesc;
	//		pConstantBufferReflection->GetDesc(&shaderBufferDesc);
	//		const char* cbName = shaderBufferDesc.Name;
	//		if (_stricmp(cbName, "cbCommonFrameConstants") == 0) {
	//			eFrameConstantsType = EFrameConstants::Common;
	//		}
	//		else if (_stricmp(cbName, "cbPBRObjectConstants") == 0) {
	//			eObjectsConstantsType = EObjectConstants::PBR;
	//		}
	//	}
	//}

	CD3D12RenderPass::CD3D12RenderPass(CD3D12ShaderInstance* pShaderInstances[], 
		ETechniquePassStage stage,
		ITechnique* pTech,
		uint64_t mask)
		:IRenderPass(stage, pTech)
		,mShaderMask(mask)
	{
		mIsPipelineStateConstructing.clear();

		const SRenderPassMeta* pPassMeta = &pTech->GetRenderPassMeta(stage);
		mObjectConstBufferType = pPassMeta->ObjectConstBufferType;
		mFrameConstBufferType = pPassMeta->FrameConstBufferType;

		memset(mShaderInstances, 0, sizeof(mShaderInstances));
		for (int i = 0; i < SHADER_TYPE_COUNT; i++) {
			mShaderInstances[i] = pShaderInstances[i];
		}
	}

	//void CD3D12RenderPass::ExtractShaderMetaInfo(EFrameConstants& eFrameConstantsType, 
	//	EObjectConstants& eObjectsConstantsType)
	//{
	//	eFrameConstantsType = EFrameConstants::Unknown;
	//	eObjectsConstantsType = EObjectConstants::Unknown;

	//	//ExtractShaderMetaInfo(mShaderInstances[PIXEL_SHADER_TYPE], eFrameConstantsType, eObjectsConstantsType);
	//	if (mShaderInstances[VERTEX_SHADER_TYPE]) {
	//		mShaderInstances[VERTEX_SHADER_TYPE]->ExtractMetaInfo(eFrameConstantsType, eObjectsConstantsType);
	//	}

	//	if (mShaderInstances[PIXEL_SHADER_TYPE]) {
	//		mShaderInstances[PIXEL_SHADER_TYPE]->ExtractMetaInfo(eFrameConstantsType, eObjectsConstantsType);
	//	}

	//	if (eFrameConstantsType == EFrameConstants::Unknown
	//		|| eObjectsConstantsType == EObjectConstants::Unknown)
	//	{
	//		// error:
	//	}
	//}

	ID3D12PipelineState* CD3D12RenderPass::GetPipelineStateObject(const SRenderStatesDesc& desc, ID3D12RootSignature* pRootSignature)
	{
		if (desc.IsCommon) {
			ID3D12PipelineState* pPSO = mCommonPipelineStateObjects[desc.CommonHash];
			if (pPSO) {
				return pPSO;
			}
		}
		else
		{
			auto it = mRarePipelineStateObjects.find(desc.FullHash);
			if (it != mRarePipelineStateObjects.end()) {
				return it->second;
			}
		}

		if (mIsPipelineStateConstructing.test_and_set() == false)
		{
			auto pTaskQueue = GetEngineCore()->GetAsynchronizeTaskQueue();
			SCreatePipelineStateRequest req;
			req.pRenderPass = this;
			req.RenderStateDesc = desc;
			req.pRootSignature = pRootSignature;
			pTaskQueue->PushRequest<ETaskExecuteTime::AsyncImmediately>(ETaskType::CreatePipelineState, req);
		}

		return nullptr;
	}

	void CD3D12RenderPass::SetPipelineStateObject(const SRenderStatesDesc& desc, ID3D12PipelineState* pPSO)
	{
		mIsPipelineStateConstructing.clear();

		if (desc.IsCommon) {
			if (mCommonPipelineStateObjects[desc.CommonHash] != nullptr)
				return;

			mCommonPipelineStateObjects[desc.CommonHash] = pPSO;
		}
		else {
			auto it = mRarePipelineStateObjects.find(desc.FullHash);
			if (it != mRarePipelineStateObjects.end()) {
				return;
			}
			mRarePipelineStateObjects.insert(std::make_pair(desc.FullHash, pPSO));
		}
	}

	CD3D12RenderPassManager::CD3D12RenderPassManager(CD3D12ShaderManager* pShaderManager)
	//	:mShaderManager(pShaderManager)
	{
		std::vector<ITechnique*> vecTech;
		GetEngineCore()->GetRenderResourceManager()->GetAllTechniques(vecTech);
		for (auto pTech : vecTech) {
			for (int i = 0; i < ePass_Count; i++) {
				if (pTech->GetRenderPassMeta(i).Valid && mTechPassCollectionMap[i].find(pTech->GetName()) == mTechPassCollectionMap[i].end())
				{
					auto p = std::make_unique<CTechniqueRenderPassCollection>(pTech, pShaderManager);
					mTechPassCollectionMap[i].insert(std::make_pair(pTech->GetName(), std::move(p)));
				}
			}
		}
	}

	CD3D12RenderPass* CD3D12RenderPassManager::GetOrCreateRenderPass(SRenderPassMeta* pPassData,
		const TShaderMacroList& shaderFlags)
	{
		auto stage = pPassData->Stage;
		return mTechPassCollectionMap[stage][pPassData->TechinqueName]->GetOrCreateRenderPass(pPassData, shaderFlags);
	}

	//CD3D12RenderPass* CD3D12RenderPassManager::CTechniquePassCollection::GetOrCreateRenderPass(
	//	SRenderPassMeta* pPassData, const TShaderMacroList& materialFlags,
	//	CD3D12ShaderManager* shaderManager)
	//{
	//	if (!pPassData->Valid)
	//		return nullptr;

	//	std::vector<std::string> combinedDefines(materialFlags);
	//	combinedDefines.insert(combinedDefines.end(), pPassData->Defines.begin(), pPassData->Defines.end());

	//	uint64_t mask = GetEngineCore()->GetRenderResourceManager()->GetShaderMacroMask(combinedDefines);
	//	{
	//		std::unique_lock<std::mutex> lock(mMutex);
	//		auto it = mPassMap.find(mask);
	//		if (it != mPassMap.end()) {
	//			return it->second.get();
	//		}
	//		return CreateRenderPass(pPassData, combinedDefines, shaderManager, mask);
	//	}
	//}

	//CD3D12RenderPass* CD3D12RenderPassManager::CTechniquePassCollection::CreateRenderPass(
	//	SRenderPassMeta* pPassData, const TShaderMacroList& combinedDefines,
	//	CD3D12ShaderManager* shaderManager, uint64_t mask)
	//{
	//	if (!pPassData->Valid)
	//		return nullptr;

	//	CD3D12ShaderInstance* pShaderInstances[SHADER_TYPE_COUNT] = { 0 };
	//	for (int i = 0; i < SHADER_TYPE_COUNT; i++) {
	//		if (pPassData->Shaders[i].Valid) {
	//			pShaderInstances[i] = shaderManager->GetOrCreateShaderInstance(pPassData->ShaderFileName.c_str(),
	//				combinedDefines, pPassData->Shaders[i].EntryPoint, (EShaderType)i);
	//		}
	//	}

	//	std::unique_ptr<CD3D12RenderPass> pPass = std::make_unique<CD3D12RenderPass>(pShaderInstances, pPassData->Stage, mTechnique);
	//	//if (!pPassData->bMetaInfoExtracted) {
	//	//	pPass->ExtractShaderMetaInfo(pPassData->mFrameConstantsType, pPassData->mObjectConstantsType);
	//	//	pPassData->bMetaInfoExtracted = true;
	//	//}
	//	//pPass->mFrameConstantsType = pPassData->mFrameConstantsType;
	//	//pPass->mObjectConstantsType = pPassData->mObjectConstantsType;

	//	auto pPassPtr = pPass.get();
	//	mPassMap.insert(std::make_pair(mask, std::move(pPass)));
	//	return pPassPtr;
	//}


	CD3D12RenderPass* CTechniqueRenderPassCollection::GetOrCreateRenderPass(SRenderPassMeta* pPassData, const TShaderMacroList& shaderFlags)
	{
		if (!pPassData->Valid)
			return nullptr;

		std::vector<std::string> combinedDefines(shaderFlags);
		combinedDefines.insert(combinedDefines.end(), pPassData->Defines.begin(), pPassData->Defines.end());

		uint64_t mask = GetEngineCore()->GetRenderResourceManager()->GetShaderMacroMask(combinedDefines);
		{
			std::unique_lock<std::mutex> lock(mMutex);
			auto it = mPassMap.find(mask);
			if (it != mPassMap.end()) {
				return it->second.get();
			}
			return CreateRenderPass(pPassData, combinedDefines, mask);
		}
	}

	CD3D12RenderPass* CTechniqueRenderPassCollection::CreateRenderPass(SRenderPassMeta* pPassData, const TShaderMacroList& combinedDefines, uint64_t mask)
	{
		if (!pPassData->Valid)
			return nullptr;

		CD3D12ShaderInstance* pShaderInstances[SHADER_TYPE_COUNT] = { 0 };
		for (int i = 0; i < SHADER_TYPE_COUNT; i++) {
			if (pPassData->Shaders[i].Valid) {
				pShaderInstances[i] = mShaderManager->GetOrCreateShaderInstance(pPassData->ShaderFileName.c_str(),
					combinedDefines, pPassData->Shaders[i].EntryPoint, (EShaderType)i);
			}
		}

		std::unique_ptr<CD3D12RenderPass> pPass = std::make_unique<CD3D12RenderPass>(pShaderInstances, pPassData->Stage, mTechnique, mask);
		auto pPassPtr = pPass.get();
		mPassMap.insert(std::make_pair(mask, std::move(pPass)));
		return pPassPtr;
	}

}

