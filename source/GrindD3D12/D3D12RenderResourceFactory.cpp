#include "stdafx.h"
#include "IMaterial.h"
#include "D3D12RenderResourceFactory.h"

namespace grind
{

	CD3D12RenderResourceFactory::CD3D12RenderResourceFactory(ID3D12Device* device):
		md3dDevice(device)
	{
		mShaderManager = std::make_unique<CD3D12ShaderManager>();
		mRenderPassManager = std::make_unique<CD3D12RenderPassManager>(mShaderManager.get());
		mPipelineStateManager = std::make_unique<CD3D12PipelineStateManager>(this);
		mMaterialResourceManager = std::make_unique<CD3D12MaterialResourceManager>(device);
		mTextureResourceManager = std::make_unique<CD3D12TextureResourceManager>(device);
		mMeshBufferManager = std::make_unique<CD3D12MeshBufferManager>(device);
		mRootSignatureManager = std::make_unique<CD3D12RootSignatureManager>(device);
	}

	IMaterialResourceManager* CD3D12RenderResourceFactory::GetMaterialResourceManager()
	{
		return mMaterialResourceManager.get();
	}

	IRenderPass* CD3D12RenderResourceFactory::CreateRenderPass(SRenderPassMeta* passMeta,
		const std::vector<std::string>& matFlags)
	{
		return mRenderPassManager->GetOrCreateRenderPass(passMeta, matFlags);
	}

	D3D12_INPUT_LAYOUT_DESC CD3D12RenderResourceFactory::GetInputLayoutDesc(EFVF fvf, bool bContainTangent /*= false*/)
	{
		return mInputLayoutManager.GetInputLayout(fvf, bContainTangent);
	}

	const SD3D12RootSignature* CD3D12RenderResourceFactory::GetRootSignature(IRenderPass* pRenderPass) const
	{
		return mRootSignatureManager->GetRootSignature(pRenderPass);
	}

	CD3D12InputLayoutManager::CD3D12InputLayoutManager()
	{
		static const char* s_POSITION = "POSITION";
		static const char* s_NORMAL = "NORMAL";
		//static const char* s_COLOR = "COLOR";
		static const char* s_TEXCOORD = "TEXCOORD";
		static const char* s_TANGENT = "TANGENT";

		uint32_t i = 0;

		// Object3D
		EFVF fvf = EFVF::Object3D;
		for (uint32_t flags = 0; flags < (1 << E_FVF_Flag::FlagCount); flags++) {
			auto& elements = mInputElementDescsCache[i++];
			uint32_t byteOffset = 0;

			elements.push_back({s_POSITION, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, byteOffset, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0});
			byteOffset += 12;

			elements.push_back({s_NORMAL, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, byteOffset, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 });
			byteOffset += 12;

			if (flags & E_FVF_Flag::ContainTangent) {
				elements.push_back({ s_TANGENT, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, byteOffset, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 });
				byteOffset += 12;
			}

			elements.push_back({ s_TEXCOORD, 0, DXGI_FORMAT_R32G32_FLOAT, 0, byteOffset, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 });
			byteOffset += 8;

			mInputLayoutCache[(uint32_t)fvf][flags] = { elements.data(), (UINT)elements.size() };
		}
	}

	//D3D12_INPUT_LAYOUT_DESC CD3D12InputLayoutManager::GetInputLayout(EFVF fvf, uint32_t flags)
	//{
	//	if (fvf == EFVF::Unknown) {
	//		// error:
	//	}
	//	return mInputLayoutCache[(uint32_t)fvf][flags];
	//}

	D3D12_INPUT_LAYOUT_DESC CD3D12InputLayoutManager::GetInputLayout(EFVF fvf, bool bContainTangent /*= false*/)
	{
		uint32_t flags = 0;
		if (bContainTangent) {
			flags |= E_FVF_Flag::ContainTangent;
		}
		return mInputLayoutCache[(uint32_t)fvf][flags];
	}

	CD3D12RootSignatureManager::CD3D12RootSignatureManager(ID3D12Device* device)
	{
		BuildStaticSamplers();
		BuildAllRootSignatures(device);
	}

	CD3D12RootSignatureManager::~CD3D12RootSignatureManager()
	{
		for (size_t i = 0; i < mRootSignatures.size(); i++)
		{
			SAFE_RELEASE(mRootSignatures[i].pd3dRootSignature);
		}
	}

	//ID3D12RootSignature* CD3D12RootSignatureManager::GetRootSignature(IRenderPass* pRenderPass)
	//{
	//	uint32_t bContainObjectBuffer = (pRenderPass->GetObjectConstBufferType() != EObjectConstBuffer::None);
	//	uint32_t bContainFrameBuffer = (pRenderPass->GetFrameConstBufferType() != EFrameConstBuffer::None);
	//	uint32_t iMaterialTextureCount = (uint32_t)pRenderPass->GetTechnique()->GetMaterialTextureCount();
	//	uint32_t bContainMaterialBuffer = (pRenderPass->GetTechnique()->GetMaterialBufferSize() != 0);


	//	// generate binary code:
	//	// 1 bit(object buffer) + 1 bit(frame buffer) + 
	//	// 1 bit(material buffer)
	//	// 4 bit(maximum to 16 material textures)
	//	uint32_t code = (bContainObjectBuffer << 6) | (bContainFrameBuffer << 5) |
	//		(bContainMaterialBuffer << 4) | (iMaterialTextureCount & 0x0f);

	//	return mRootSignatures[code].Get();
	//}

	const SD3D12RootSignature* CD3D12RootSignatureManager::GetRootSignature(IRenderPass* pRenderPass) const
	{
		uint32_t bContainObjectBuffer = (pRenderPass->GetObjectConstBufferType() != EObjectConstBuffer::None);
		uint32_t bContainFrameBuffer = (pRenderPass->GetFrameConstBufferType() != EFrameConstBuffer::None);
		uint32_t iMaterialTextureCount = (uint32_t)pRenderPass->GetTechnique()->GetMaterialTextureCount();
		uint32_t bContainMaterialBuffer = (pRenderPass->GetTechnique()->GetMaterialBufferSize() != 0);

		// generate binary code:
		// 1 bit(object buffer) + 1 bit(frame buffer) + 
		// 1 bit(material buffer)
		// 4 bit(maximum to 16 material textures)
		uint32_t code = (bContainObjectBuffer << 6) | (bContainFrameBuffer << 5) |
			(bContainMaterialBuffer << 4) | (iMaterialTextureCount & 0x0f);

		return &mRootSignatures[code];
	}

	void CD3D12RootSignatureManager::BuildStaticSamplers()
	{
		CD3DX12_STATIC_SAMPLER_DESC pointWrap(
			0, // shaderRegister
			D3D12_FILTER_MIN_MAG_MIP_POINT, // filter
			D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressU
			D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressV
			D3D12_TEXTURE_ADDRESS_MODE_WRAP); // addressW

		CD3DX12_STATIC_SAMPLER_DESC pointClamp(
			1, // shaderRegister
			D3D12_FILTER_MIN_MAG_MIP_POINT, // filter
			D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressU
			D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressV
			D3D12_TEXTURE_ADDRESS_MODE_CLAMP); // addressW

		CD3DX12_STATIC_SAMPLER_DESC linearWrap(
			2, // shaderRegister
			D3D12_FILTER_MIN_MAG_MIP_LINEAR, // filter
			D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressU
			D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressV
			D3D12_TEXTURE_ADDRESS_MODE_WRAP); // addressW

		CD3DX12_STATIC_SAMPLER_DESC linearClamp(
			3, // shaderRegister
			D3D12_FILTER_MIN_MAG_MIP_LINEAR, // filter
			D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressU
			D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressV
			D3D12_TEXTURE_ADDRESS_MODE_CLAMP); // addressW

		CD3DX12_STATIC_SAMPLER_DESC anisotropicWrap(
			4, // shaderRegister
			D3D12_FILTER_ANISOTROPIC, // filter
			D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressU
			D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressV
			D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressW
			0.0f,                             // mipLODBias
			8);                               // maxAnisotropy

		CD3DX12_STATIC_SAMPLER_DESC anisotropicClamp(
			5, // shaderRegister
			D3D12_FILTER_ANISOTROPIC, // filter
			D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressU
			D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressV
			D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressW
			0.0f,                              // mipLODBias
			8);                                // maxAnisotropy

		mStaticSamplers[0] = pointWrap;
		mStaticSamplers[1] = pointClamp;
		mStaticSamplers[2] = linearWrap;
		mStaticSamplers[3] = linearClamp;
		mStaticSamplers[4] = anisotropicWrap;
		mStaticSamplers[5] = anisotropicClamp;
	}

	void CD3D12RootSignatureManager::BuildAllRootSignatures(ID3D12Device* d3dDevice)
	{
		for (uint32_t code = 0; code < (1U << BINARY_CODE_BITS); code++) {
			SRootParameterIndexes rootParameterIndexes;
			ID3D12RootSignature* pd3dRootSignature = nullptr;

			bool bContainObjectBuffer = IsContainObjectBuffer(code);
			bool bContainFrameBuffer = IsContainFrameBuffer(code);
			int iMaterialTextureCount = GetMaterialTexturesCount(code);
			int bContainMaterialBuffer = (int)IsContainMaterialBuffer(code);

			CD3DX12_DESCRIPTOR_RANGE descriptorTables[2];

			int i = 0;
			CD3DX12_ROOT_PARAMETER slotRootParameter[3];

			if (bContainObjectBuffer) {
				// ObjectBuffer��ӦShader�е�b0
				rootParameterIndexes.ObjectConstBuffer = i;
				slotRootParameter[i++].InitAsConstantBufferView(0, 0, D3D12_SHADER_VISIBILITY_ALL); // object buffer
			}

			if (bContainMaterialBuffer + iMaterialTextureCount > 0) 
			{
				// material buffer
				int tableIndex = 0;
				
				if (bContainMaterialBuffer) {
					descriptorTables[tableIndex++].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
				}
				if (iMaterialTextureCount) {
					descriptorTables[tableIndex++].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, iMaterialTextureCount, 0);
				}
				rootParameterIndexes.MaterialConstBuffer = i;
				slotRootParameter[i++].InitAsDescriptorTable(tableIndex, descriptorTables);
			}
			
			if (bContainFrameBuffer) {
				rootParameterIndexes.FrameConstBuffer = i;
				slotRootParameter[i++].InitAsConstantBufferView(2, 0, D3D12_SHADER_VISIBILITY_ALL); // frame buffer
			}

			// A root signature is an array of root parameters.
			CD3DX12_ROOT_SIGNATURE_DESC rootSigDesc(i, slotRootParameter,
				(UINT)mStaticSamplers.size(), mStaticSamplers.data(),
				D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

			// create a root signature with a single slot which points to a descriptor range consisting of a single constant buffer
			ComPtr<ID3DBlob> serializedRootSig = nullptr;
			ComPtr<ID3DBlob> errorBlob = nullptr;
			HRESULT hr = D3D12SerializeRootSignature(&rootSigDesc, D3D_ROOT_SIGNATURE_VERSION_1,
				serializedRootSig.GetAddressOf(), errorBlob.GetAddressOf());

			if (errorBlob != nullptr)
			{
				::OutputDebugStringA((char*)errorBlob->GetBufferPointer());
			}
			ThrowIfFailed(hr);

			//ThrowIfFailed(d3dDevice->CreateRootSignature(
			//	0,
			//	serializedRootSig->GetBufferPointer(),
			//	serializedRootSig->GetBufferSize(),
			//	IID_PPV_ARGS(&mRootSignatures[code])));

			ThrowIfFailed(d3dDevice->CreateRootSignature(
				0,
				serializedRootSig->GetBufferPointer(),
				serializedRootSig->GetBufferSize(),
				IID_PPV_ARGS(&pd3dRootSignature)));

			mRootSignatures[code].pd3dRootSignature = pd3dRootSignature;
			mRootSignatures[code].RootParameterIndexes = rootParameterIndexes;
		}
	}

}

