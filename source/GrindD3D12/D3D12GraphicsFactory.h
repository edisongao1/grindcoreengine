#pragma once

#include "IGraphicsFactory.h"

namespace grind
{
	class IEngineCore;

	class CD3D12GraphicsFactory : public IGraphicsFactory
	{
	public:
		virtual IRenderDevice* CreateRenderDevice() override;
		virtual void ReleaseRenderDevice(IRenderDevice* p) override;
	};

	extern "C" __declspec(dllexport) IGraphicsFactory* CreateFactory();
	extern "C" __declspec(dllexport) void DestroyFactory(IGraphicsFactory*);
	extern "C" __declspec(dllexport) void SetGlobalEnvironment(IEngineCore* pEngineCore);

	
}



