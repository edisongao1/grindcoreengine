#pragma once

#include "IRenderContext.h"
#include "IRenderDevice.h"
#include "D3D12Buffer.h"
#include "MathHelper.h"
#include "GrindMath.h"
#include "D3D12FrameResource.h"
#include "RenderItem.h"
#include "IGraphicsSystem.h"

using Microsoft::WRL::ComPtr;
using namespace DirectX;
using namespace DirectX::PackedVector;

static const int CUBE_NUM_X = 10;
static const int CUBE_NUM_Y = 8;

namespace grind
{

	//struct ObjectConstants
	//{
	//	Matrix4x4 World;
	//	Matrix4x4	WorldViewProj;
	//};
	class CD3D12RenderPass;
	struct SD3D12RootSignature;
	class CD3D12MaterialResourceManager;
	class CD3D12RenderResourceFactory;
	class CD3D12MeshBuffer;

	struct SD3D12DrawcallState
	{
		CD3D12RenderPass*			pRenderPass = nullptr;
		ID3D12PipelineState*		pPipelineStateObject = nullptr;
		const SD3D12RootSignature*	pRootSignature;
		bool						HasMaterialResource = false;
		SMaterialResourceId			MaterialResourceId;
		D3D12_GPU_DESCRIPTOR_HANDLE	MaterialResDescriptorHandle;
		D3D12_GPU_VIRTUAL_ADDRESS	FrameBufferGPUAddress = 0;
		ID3D12DescriptorHeap*		pDescriptorHeaps[2] = { 0 };

		void Reset()
		{
			pRenderPass = nullptr;
			pPipelineStateObject = nullptr;
			pRootSignature = nullptr;
			MaterialResDescriptorHandle.ptr = 0;
			HasMaterialResource = false;
			FrameBufferGPUAddress = 0;
			pDescriptorHeaps[0] = pDescriptorHeaps[1] = nullptr;
			MaterialResourceId = SMaterialResourceId();
		}
	};

	class CD3D12RenderDevice;

	class CD3D12RenderContext : public IRenderContext
	{
	public:
		CD3D12RenderContext(CD3D12RenderDevice* pDevice, int id);
		virtual ~CD3D12RenderContext();

		bool Init();

		//void BuildRootSignature();
		
		//void BuildPSO();

		virtual void Render() override;

		virtual void BeginFrame() override;
		virtual void EndFrame() override;

		void Update();
		//virtual void Release() override;
		virtual void BeginCommands() override;
		virtual void EndCommands() override;

		virtual void RenderMeshItemQueue(CMeshRenderItemQueue* pMeshItemQueue) override;



		uint64_t SetObjectConstBuffer(ILocalFrameResource* pLocalFrameResource,
			IRenderPass* pRenderPass, const Matrix4x4& worldMatrix);

		ID3D12GraphicsCommandList* GetD3D12CommandList() { return m_pd3dCommandList.Get(); }

	private:
		void RenderMeshItem(SMeshRenderItem* pRenderItem);

		bool GetDrawcallState(IMaterial* pMaterial, ETechniquePassStage stage, uint64_t runningMask, __out SD3D12DrawcallState* pDrawcallState);
		uint64_t GetRunningMask(IMesh* pMesh) const;

		int			mId = 0;

		CD3D12RenderDevice*						m_pDevice;
		ComPtr<ID3D12Device>					m_pd3dDevice;

		ComPtr<ID3D12GraphicsCommandList>		m_pd3dCommandList;
		ComPtr<ID3D12CommandAllocator>			m_pd3dCommandAllocator;
		ComPtr<ID3D12CommandQueue>				m_pd3dCommandQueue;

		//ComPtr<ID3D12RootSignature> mRootSignature = nullptr;
		//ComPtr<ID3D12PipelineState> mPSO = nullptr;
		//IRenderPass*				mCurrentRenderPass = nullptr;

		std::vector<int>		m_arrCubeIndexs;
		std::vector<Vector3f>	m_arrPositions;
		std::vector<Vector3f>	m_arrRotations;

		std::vector<Matrix4x4>	m_vecWorldTransforms;

		CLocalFrameResource			mLocalFrameResources[FRAME_RESOURCE_COUNT];
		CLocalFrameResource*		mCurrentLocalFrameResource = nullptr;
		CD3D12MaterialResourceManager*	m_pd3dMaterialResourceManager = nullptr;
		CD3D12RenderResourceFactory*	mRenderResourceFactory = nullptr;
		SD3D12DrawcallState			mLastDrawcallState;
		CD3D12MeshBuffer*			mLastMeshBuffer = nullptr;

		SCameraViewTransformation	mCameraTransformation;
	};
}
