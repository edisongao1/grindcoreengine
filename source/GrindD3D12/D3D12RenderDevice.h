#pragma once

#include "IRenderDevice.h"
#include "D3D12Util.h"
#include "D3D12Shader.h"
#include "D3D12FrameResource.h"
#include "D3D12RenderResourceFactory.h"
#include "IMesh.h"
#include "IGraphicsSystem.h"


using Microsoft::WRL::ComPtr;


namespace grind
{
	class CD3D12RenderDevice : public IRenderDevice
	{
	public:

		friend class CD3D12RenderContext;

		CD3D12RenderDevice();
		~CD3D12RenderDevice();

		virtual bool Initialize() override;
		//virtual void Release() override;

		virtual void OnResize() override;

		virtual void Render() override;
		virtual void BeginFrame() override;
		virtual void EndFrame() override;

		void UpdateFrameConstBuffers(ETechniquePassStage stage);

		virtual void FlushCommandQueue() override;

		void ExecuteCommandList();
		void ExecuteCommandList(ID3D12GraphicsCommandList* pCommandList);
		virtual IRenderContext* GetRenderContext(int index) override;

	private:

		void BuildShadersAndInputLayout();
		void BuildBoxGeometry();

		enum { SWAP_CHAIN_BUFFER_COUNT = 2 };
		D3D12_CPU_DESCRIPTOR_HANDLE		GetCurrentBackBufferView();
		D3D12_CPU_DESCRIPTOR_HANDLE		GetDepthStencilView();

		ID3D12Resource*					GetCurrentBackBuffer();

		//std::unique_ptr<CD3D12ShaderManager>	mShaderManager;
		//CD3D12ShaderManager						mShaderManager;

		ComPtr<IDXGIFactory4>					m_pDxgiFactory4;
		ComPtr<ID3D12Device>					m_pd3dDevice;
		ComPtr<ID3D12CommandQueue>				m_pd3dCommandQueue;
		ComPtr<ID3D12GraphicsCommandList>		m_pd3dCommandList;
		ComPtr<IDXGISwapChain>					m_pSwapChain;

		ComPtr<ID3D12DescriptorHeap>			m_RtvHeap;
		ComPtr<ID3D12DescriptorHeap>			m_DsvHeap;

		ComPtr<ID3D12Resource>					m_pBackBuffers[SWAP_CHAIN_BUFFER_COUNT];
		ComPtr<ID3D12Resource>					m_pDepthStencilBuffer;

		ComPtr<ID3D12Fence>						m_Fence;

		DXGI_FORMAT		m_BackBufferFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
		DXGI_FORMAT		m_DepthStencilFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;

		UINT			m_MsaaQuality;
		HWND			m_hWnd;

		int				m_CurrentBackBufferIndex = 0;
		uint64_t		m_CurrentFence = 0;

		D3D12_VIEWPORT	m_Viewport;
		D3D12_RECT		m_ScissorRect;

		//ComPtr<ID3DBlob> mvsByteCode = nullptr;
		//ComPtr<ID3DBlob> mpsByteCode = nullptr;
		//IMaterial*		mMaterial = nullptr;
		std::vector<IMaterial*>		mCubeMaterials;

		uint64_t		mRunningMask = 0;
		bool			mWireMode = false;

		//CD3D12ShaderInstance*	mVertexShader = nullptr;
		//CD3D12ShaderInstance*	mPixelShader = nullptr;
		
		//std::vector<D3D12_INPUT_ELEMENT_DESC> mInputLayout;

		//std::unique_ptr<MeshGeometry>	mBoxGeo = nullptr;

		std::shared_ptr<CD3D12RenderContext>		mRenderContexts[32];

		
		CLocalFrameResource			mLocalFrameResources[FRAME_RESOURCE_COUNT];
		CGlobalFrameResource		mGlobalFrameResources[FRAME_RESOURCE_COUNT];
		
		std::unique_ptr<CD3D12RenderResourceFactory>	mRenderResourceFactory;

		//// camera:
		//Vector3f		mCamPos;
		//Vector3f		mCamTarget;
		//Matrix4x4			mViewMatrix;
		//Matrix4x4			mProjMatrix;
		IMesh*		mBoxMesh = nullptr;

		//SCameraViewTransformation		mCameraViewTransformation;
	};
}

