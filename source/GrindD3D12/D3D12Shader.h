#pragma once

#include <unordered_map>

// Shader中的宏分为静态标签和动态标签
// 静态标签是在材质编辑器中指定的
// 动态标签是程序运行时指定的

#include "GrindEnums.h"
#include "Common.h"
#include "IEngineCore.h"
#include "IMaterial.h"
#include "IRenderPass.h"

using Microsoft::WRL::ComPtr;

namespace grind
{
	class CD3D12NamedShader;
	class CD3D12ShaderInstance;
	class CD3D12RenderPass;

	inline void ExtractShaderFileNameAndEntry(const std::string& name, 
		std::string& fileName, std::string& entryName)
	{
		size_t pos = name.find_first_of('@');
		fileName = name.substr(0, pos) + ".hlsl";
		entryName = name.substr(pos + 1);
	}

	class CD3D12ShaderFactory
	{
	public:
		static ComPtr<ID3DBlob>	CompileShader(
			const std::string& filename,
			const std::vector<std::string>& defines,
			const std::string& entrypoint,
			EShaderType	eShaderType);


		static CD3D12ShaderInstance* CreateShaderInstance(
			CD3D12NamedShader* pNamedShader,
			const std::vector<std::string>& defines);

		static CD3D12ShaderInstance* CreateShaderInstance(
			CD3D12NamedShader* pNamedShader,
			uint64_t mask);
	};

	class CD3D12ShaderManager
	{
	public:
		const static uint64_t RUNNING_FLAG_MASK = 0xffff000000000000;
		const static uint64_t MATERIAL_FLAG_MASK = 0xffffffffff00;
		const static uint64_t TECH_FLAG_MASK = 0xff;

		CD3D12ShaderManager();

		CD3D12ShaderInstance* GetOrCreateShaderInstance(
			const std::string& filename,
			const std::vector<std::string>& defines,
			const std::string& entrypoint,
			EShaderType	eShaderType);

		//static uint64_t GetDefineMask(EShaderType eShaderType, const std::vector<std::string>& defines)
		//{
		//	auto pResourceManager = GetEngineCore()->GetRenderResourceManager();
		//	return pResourceManager->GetShaderMacroMask(eShaderType, defines);
		//}

		CD3D12NamedShader* GetOrCreateNamedShader(const std::string& strShaderName, EShaderType	eShaderType);

	public:
		//using TDefineMap = std::unordered_map<std::string, uint64_t>;
		//static TDefineMap mDefineMap[SHADER_TYPE_COUNT];
		//static TShaderMacroMaskMap	mMacroMaskMap;

	private:
		using TMap = std::unordered_map<uint32_t, std::unique_ptr<CD3D12NamedShader>>;

		TMap					mNamedShadersMap[SHADER_TYPE_COUNT];
		std::mutex				mMutex[SHADER_TYPE_COUNT];
	};

	// 拥有文件和入口函数名字的一组Shader
	class CD3D12NamedShader
	{
	public:
		CD3D12NamedShader(const std::string& shaderName, EShaderType shaderType);

		EShaderType GetShaderType() const { return mShaderType; }
		CD3D12ShaderInstance* GetOrCreateShaderInstance(const std::vector<std::string>& defines);
		//std::vector<std::string> FilterShaderMacroStringList(const std::vector<std::string>& defines);
		const std::string& GetFileName() const { return mStrFileName; }
		const std::string& GetEntryPoint() const { return mStrEntryPoint; }
		uint64_t GetShaderMacroMask(const std::vector<std::string>& defines) const;

	private:
		using TMap = std::unordered_map<uint64_t, std::unique_ptr<CD3D12ShaderInstance>>;
		TMap				mShaderInstances;
		EShaderType			mShaderType;
		std::string			mNameStr;
		std::mutex			mMutex;

		std::string			mStrFileName;
		std::string			mStrEntryPoint;
		uint64_t			mValidateMacrosMask;
	};

	class CD3D12ShaderInstance
	{
	public:
		CD3D12ShaderInstance(ComPtr<ID3DBlob> pd3dBlob, uint64_t mask);
		D3D12_SHADER_BYTECODE GetByteCode() const;

		uint64_t GetDefineMask() const { return mDefineMask; }
		//void ExtractMetaInfo(EFrameConstants& eFrameConstantsType,
		//	EObjectConstants& eObjectsConstantsType);

	private:
		ComPtr<ID3DBlob>		md3dBlob;
		//CD3D12NamedShader*		mNamedShader = nullptr;
		uint64_t				mDefineMask = 0;
	};

	class CD3D12RenderPass : public IRenderPass
	{
	public:
		CD3D12RenderPass(CD3D12ShaderInstance* pShaderInstances[], 
			ETechniquePassStage stage,
			ITechnique* pTech,
			uint64_t mask);

		//void UpdateRunningMask(uint64_t runningMask);

		//void ExtractShaderMetaInfo(EFrameConstants& eFrameConstantsType,
		//	EObjectConstants& eObjectsConstantsType);

		CD3D12ShaderInstance* GetShaderInstance(EShaderType eShaderType)
		{
			return mShaderInstances[eShaderType];
		}

		ID3D12PipelineState* GetPipelineStateObject(const SRenderStatesDesc& desc, ID3D12RootSignature* pRootSignature);

		// must called on main thread
		void SetPipelineStateObject(const SRenderStatesDesc& desc, ID3D12PipelineState* pPSO);

		//uint64_t GetRunningMask() const { return mRunningMask; }
		uint64_t GetShaderMask() const { return mShaderMask; }

	private:
		CD3D12ShaderInstance*		mShaderInstances[SHADER_TYPE_COUNT];

		ID3D12PipelineState*		mCommonPipelineStateObjects[1 << 8] = {0};
		std::unordered_map<uint64_t, ID3D12PipelineState*>	mRarePipelineStateObjects;

		DECLARE_ATOMIC_FLAG(mIsPipelineStateConstructing)
		//std::atomic_flag			mIsPipelineStateConstructing = ATOMIC_FLAG_INIT;
		//uint64_t					mRunningMask;
		uint64_t					mShaderMask;
	};

	// create and manage all the render pass objects in a certain technique object
	class CTechniqueRenderPassCollection
	{
	public:
		CTechniqueRenderPassCollection(ITechnique* pTech, CD3D12ShaderManager* shaderManager) 
			:mTechnique(pTech),mShaderManager(shaderManager) 
		{
		}

		CD3D12RenderPass* GetOrCreateRenderPass(SRenderPassMeta* pPassData, const TShaderMacroList& shaderFlags);
	private:
		// mask parameter equals to the mask of combinedDefines, in order to avoid duplicate calculations
		CD3D12RenderPass*  CreateRenderPass(SRenderPassMeta* pPassData, const TShaderMacroList& combinedDefines, uint64_t mask);

		std::unordered_map<uint64_t, std::unique_ptr<CD3D12RenderPass>>	mPassMap;
		std::mutex	mMutex;
		ITechnique* mTechnique;
		CD3D12ShaderManager*		mShaderManager = nullptr;
	};

	class CD3D12RenderPassManager
	{
	public:
		//class CTechniquePassCollection
		//{
		//public:
		//	CTechniquePassCollection(ITechnique* pTech):mTechnique(pTech){}

		//	CD3D12RenderPass* GetOrCreateRenderPass(SRenderPassMeta* pPassData,
		//		const TShaderMacroList& materialFlags, CD3D12ShaderManager* shaderManager);

		//private:
		//	CD3D12RenderPass*  CreateRenderPass(SRenderPassMeta* pPassData,
		//		const TShaderMacroList& combinedDefines, CD3D12ShaderManager* shaderManager, uint64_t mask);

		//	std::unordered_map<uint64_t, std::unique_ptr<CD3D12RenderPass>>	mPassMap;
		//	std::mutex	mMutex;
		//	ITechnique* mTechnique;
		//};

		CD3D12RenderPassManager(CD3D12ShaderManager* pShaderManager);
		CD3D12RenderPass* GetOrCreateRenderPass(SRenderPassMeta* pPassData,
			const TShaderMacroList& shaderFlags);
	private:
		std::unordered_map<std::string, std::unique_ptr<CTechniqueRenderPassCollection>>	mTechPassCollectionMap[ePass_Count];
		//CD3D12ShaderManager*		mShaderManager = nullptr;

	};

	struct SCreatePipelineStateRequest
	{
		SRenderStatesDesc			RenderStateDesc;
		CD3D12RenderPass*			pRenderPass = nullptr;
		ID3D12RootSignature*		pRootSignature = nullptr;
	};
}

