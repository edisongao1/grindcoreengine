#pragma once
#include "D3D12Util.h"
#include "MathHelper.h"
#include "IRenderPass.h"
#include "IMaterialResourceManager.h"

using namespace DirectX;
using namespace DirectX::PackedVector;

namespace grind
{
	class CD3D12UploadBuffer
	{
	public:
		CD3D12UploadBuffer(UINT bufferSize, ID3D12Device* device)
			:mBufferSize(bufferSize)
		{
			CD3DX12_HEAP_PROPERTIES heapProperties(D3D12_HEAP_TYPE_UPLOAD);
			CD3DX12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Buffer(mBufferSize);

			ThrowIfFailed(device->CreateCommittedResource(
				&heapProperties,
				D3D12_HEAP_FLAG_NONE,
				&desc,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&md3dResource)));

			ThrowIfFailed(md3dResource->Map(0, nullptr, reinterpret_cast<void**>(&mMappedData)));
		}

		ID3D12Resource* Resource()const
		{
			return md3dResource.Get();
		}

		D3D12_GPU_VIRTUAL_ADDRESS GetGPUVirtualAddress() const 
		{
			return md3dResource->GetGPUVirtualAddress();
		}

		CD3D12UploadBuffer(const CD3D12UploadBuffer& rhs) = delete;
		CD3D12UploadBuffer& operator=(const CD3D12UploadBuffer& rhs) = delete;
		~CD3D12UploadBuffer()
		{
			if (md3dResource != nullptr)
				md3dResource->Unmap(0, nullptr);

			mMappedData = nullptr;
		}

		void CopyData(const void* data, UINT pos, UINT dataSize)
		{
			assert(pos + dataSize <= mBufferSize);
			//BYTE* byteData = reinterpret_cast<BYTE*>(data);
			memcpy(mMappedData + pos, data, dataSize);
		}

		UINT GetBufferSize() const { return mBufferSize; }

	private:
		UINT											mBufferSize;
		Microsoft::WRL::ComPtr<ID3D12Resource>			md3dResource;
		BYTE*											mMappedData = nullptr;
	};	

	// 这种Constant Buffer是每个物体都有一个
	// 使用Root Descriptor （SetGraphicsRootConstantBufferView）的方式传入DX
	class CD3D12ObjectConstantBuffer
	{
	public:
		CD3D12ObjectConstantBuffer(UINT bufferSize, ID3D12Device* device)
			: mUploadBuffer(bufferSize, device)
			, mBufferSize(bufferSize)
			, mCurrentPos(0)
		{
			mGPUAddress = mUploadBuffer.Resource()->GetGPUVirtualAddress();
		}

		bool IsFull(UINT sizeToWrite) const
		{
			return mCurrentPos + sizeToWrite > mBufferSize;
		}

		UINT GetBufferSize() const { return mBufferSize; }

		D3D12_GPU_VIRTUAL_ADDRESS Write(void* data, UINT size)
		{
			//assert(size % 256 == 0);
			mUploadBuffer.CopyData(data, mCurrentPos, size);
			D3D12_GPU_VIRTUAL_ADDRESS gpuAddress = mGPUAddress + mCurrentPos;
			mCurrentPos += D3D12Util::CalcConstantBufferByteSize(size);
			return gpuAddress;
		}

		void Reset()
		{
			mCurrentPos = 0;
		}

	private:
		CD3D12UploadBuffer			mUploadBuffer;
		UINT						mBufferSize;
		UINT						mCurrentPos;
		D3D12_GPU_VIRTUAL_ADDRESS	mGPUAddress;

	public:
		intptr_t					next = 0;
	};

	// 每帧一个
	// 整个游戏只有三个FrameBuffer
	class CD3D12FrameConstantBuffer
	{
		static const uint32_t ConstBufferCount = (uint32_t)EFrameConstBuffer::Count;

	public:
		CD3D12FrameConstantBuffer(ID3D12Device* device);

		D3D12_GPU_VIRTUAL_ADDRESS GetGPUVirtualAddress(EFrameConstBuffer cbType)
		{
			return mGPUAddress + mConstBufferPositions[(uint32_t)cbType];
		}

		void Write(EFrameConstBuffer cbType, const void* data, UINT dataSize);

	private:
		ID3D12Device*				md3dDevice;
		UINT						mBufferSize;
		std::unique_ptr<CD3D12UploadBuffer>		mUploadBuffer;
		D3D12_GPU_VIRTUAL_ADDRESS	mGPUAddress;
		UINT						mConstBufferSizes[ConstBufferCount];
		UINT						mConstBufferPositions[ConstBufferCount];
	};
	
	class CD3D12BlockConstantBuffer
		: public TBlockArray<16>
	{
	public:
		enum { BLOCK_COUNT = 16 };
		CD3D12BlockConstantBuffer(ID3D12Device* device, UINT elementSize);
		void SetBlockData(int blockIndex, void* data);
		D3D12_GPU_VIRTUAL_ADDRESS GetGPUAddress(int blockIndex) const;

	private:
		CD3D12UploadBuffer			mUploadBuffer;
		UINT						mElementSize;
	};
}
