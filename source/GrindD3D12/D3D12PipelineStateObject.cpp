#include "stdafx.h"
#include "IEngineCore.h"
#include "D3D12Util.h"
#include "AsynchronizeTaskQueue.h"
#include "D3D12PipelineStateObject.h"
#include "D3D12Shader.h"
#include "D3D12RenderResourceFactory.h"

namespace grind
{
	struct SCreatePipelineStateResult 
	{
		SRenderStatesDesc			RenderStateDesc;
		CD3D12RenderPass*			pRenderPass = nullptr;
		ID3D12PipelineState*		pd3dPipelineState = nullptr;
	};

	CD3D12PipelineStateManager::CD3D12PipelineStateManager(CD3D12RenderResourceFactory* pRenderResourceFactory)
		: mRenderResourceFactory(pRenderResourceFactory)
		, md3dDevice(pRenderResourceFactory->GetD3DDevice())
	{
		auto pTaskQueue = GetEngineCore()->GetAsynchronizeTaskQueue();
		pTaskQueue->SetRequestHandler<SCreatePipelineStateRequest>
			(ETaskType::CreatePipelineState, [this](SCreatePipelineStateRequest* req, STaskContext* context) {

			ID3D12PipelineState* pd3dPipelineState = GetOrCreatePipelineStateObject(req->RenderStateDesc, req->pRenderPass, req->pRootSignature);

			SCreatePipelineStateResult res;
			res.pd3dPipelineState = pd3dPipelineState;
			res.pRenderPass = req->pRenderPass;
			res.RenderStateDesc = req->RenderStateDesc;

			context->pQueue->PushRequest<ETaskExecuteTime::BeforeRenderScene>(ETaskType::CreatePipelineStateResult, res);
		});

		pTaskQueue->SetRequestHandler<SCreatePipelineStateResult>
			(ETaskType::CreatePipelineStateResult, [](SCreatePipelineStateResult* res) {
			
			CD3D12RenderPass* pRenderPass = res->pRenderPass;
			pRenderPass->SetPipelineStateObject(res->RenderStateDesc, res->pd3dPipelineState);
		});
	}

	ID3D12PipelineState* CD3D12PipelineStateManager::GetOrCreatePipelineStateObject(
		const SRenderStatesDesc& renderState, CD3D12RenderPass* pRenderPass, ID3D12RootSignature* pRootSignature)
	{
		//GetOrCreatePipelineStateObject()
		D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = GetD3DPipelineStateDesc(renderState, pRenderPass, pRootSignature);
		uint64_t hash = ComputeD3DPipelineStateDescHash(psoDesc);
		
		std::unique_lock<std::mutex>	lock(mMutex);
		auto it = mStateObjectsMap.find(hash);
		if (it != mStateObjectsMap.end()) {
			SPipelineStateObjectWrapper* psoWrapper = it->second.get();
			auto pPSO = psoWrapper->pd3dPipelineStateObject.Get();
#ifdef CHECK_HASH_COLLISION
			assert(CompareD3DPipelineStateDescs(psoDesc, psoWrapper->Desc));
#endif // CHECK_HASH_COLLISION
			return pPSO;
		}

		auto psoWrapper = std::make_unique<SPipelineStateObjectWrapper>();
		psoWrapper->Desc = psoDesc;
		ThrowIfFailed(md3dDevice->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&psoWrapper->pd3dPipelineStateObject)));
		auto pPSO = psoWrapper->pd3dPipelineStateObject.Get();
		mStateObjectsMap.insert(std::make_pair(hash, std::move(psoWrapper)));
		return pPSO;
	}

	D3D12_GRAPHICS_PIPELINE_STATE_DESC CD3D12PipelineStateManager::GetD3DPipelineStateDesc(
		const SRenderStatesDesc& renderState, 
		CD3D12RenderPass* pRenderPass,
		ID3D12RootSignature* pRootSignature)
	{
		D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc;
		ZeroMemory(&psoDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));

		ITechnique* pTech = pRenderPass->GetTechnique();
		EFVF fvf = pTech->GetFVF();
		auto stage = pRenderPass->GetStage();

		// input layout
		uint64_t shaderMask = pRenderPass->GetShaderMask();
		bool bContainTangent = (shaderMask & EShaderRunningMacro::Tangent);
		psoDesc.InputLayout = mRenderResourceFactory->GetInputLayoutDesc(fvf, bContainTangent);

		psoDesc.pRootSignature = pRootSignature;
		
		// set shaders
		auto pVS = pRenderPass->GetShaderInstance(VERTEX_SHADER_TYPE);
		if (pVS) {
			psoDesc.VS = pVS->GetByteCode();
		}
		auto pPS = pRenderPass->GetShaderInstance(PIXEL_SHADER_TYPE);
		if (pPS) {
			psoDesc.PS = pPS->GetByteCode();
		}
		auto pGS = pRenderPass->GetShaderInstance(GEOMETRY_SHADER_TYPE);
		if (pGS) {
			psoDesc.GS = pGS->GetByteCode();
		}
		auto pHS = pRenderPass->GetShaderInstance(HULL_SHADER_TYPE);
		if (pHS) {
			psoDesc.HS = pHS->GetByteCode();
		}
		auto pDS = pRenderPass->GetShaderInstance(DOMAIN_SHADER_TYPE);
		if (pDS) {
			psoDesc.DS = pDS->GetByteCode();
		}

		// rasterization
		CD3DX12_RASTERIZER_DESC rasterizerDesc(D3D12_DEFAULT);
		if (renderState.WireFrame) {
			rasterizerDesc.FillMode = D3D12_FILL_MODE_WIREFRAME;
		}

		if (renderState.CullBack != ECullMode::Back) {
			rasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;
		}

		rasterizerDesc.FrontCounterClockwise = true;
		psoDesc.RasterizerState = rasterizerDesc;

		// blend states
		CD3DX12_BLEND_DESC blendDesc(D3D12_DEFAULT);
		switch (renderState.BlendState)
		{
		case EBlendState::Disabled:
			blendDesc.RenderTarget[0].BlendEnable = FALSE;
			break;
		case EBlendState::SrcAlphaAdd:
			blendDesc.RenderTarget[0].BlendEnable = TRUE;
			blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
			blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
			blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
			break;
		case EBlendState::ColorAdd:
			blendDesc.RenderTarget[0].BlendEnable = TRUE;
			blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
			blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
			blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ONE;
			break;
		default:
			break;
		}
		psoDesc.BlendState = blendDesc;

		// depth state
		CD3DX12_DEPTH_STENCIL_DESC depthStencilDesc(D3D12_DEFAULT);
		switch (renderState.DepthTest) {
		case EDepthTest::Disabled:
			depthStencilDesc.DepthEnable = FALSE;
			break;
		case EDepthTest::Less:
			depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
			break;
		case EDepthTest::Equal:
			depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_EQUAL;
			break;
		case EDepthTest::LessEqual:
			depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
			break;
		case EDepthTest::Greater:
			depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_GREATER;
			break;
		case EDepthTest::NotEqual:
			depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_NOT_EQUAL;
			break;
		case EDepthTest::GreaterEqual:
			depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_GREATER_EQUAL;
			break;
		case EDepthTest::Never:
			depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_NEVER;
			break;
		case EDepthTest::Always:
			depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_ALWAYS;
			break;
		default:
			break;
		}
		psoDesc.DepthStencilState = depthStencilDesc;

		psoDesc.SampleMask = UINT_MAX;
		// PrimitiveTopologyType和FVF有关
		psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

		// 不启用MSAA
		psoDesc.SampleDesc.Count = 1;
		psoDesc.SampleDesc.Quality = 0;
		psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;

		// RenderTarget的设置和Stage有关
		if (stage == ePass_Forward) {
			psoDesc.NumRenderTargets = 1;
			psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		}
		return psoDesc;
	}

	uint64_t CD3D12PipelineStateManager::ComputeD3DPipelineStateDescHash(D3D12_GRAPHICS_PIPELINE_STATE_DESC& psoDesc)
	{
		D3D12_INPUT_LAYOUT_DESC inputLayout = psoDesc.InputLayout;
		psoDesc.InputLayout = { 0, 0 };
		uint64_t hash = CRC::Calculate(&psoDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC), CRC::CRC_64());
		psoDesc.InputLayout = inputLayout;
		return hash;
	}

	bool CD3D12PipelineStateManager::CompareD3DPipelineStateDescs(
		D3D12_GRAPHICS_PIPELINE_STATE_DESC& psoDesc1,
		D3D12_GRAPHICS_PIPELINE_STATE_DESC& psoDesc2)
	{
		auto inputLayout1 = psoDesc1.InputLayout;
		auto inputLayout2 = psoDesc2.InputLayout;
		psoDesc1.InputLayout = { 0, 0 };
		psoDesc2.InputLayout = { 0, 0 };

		auto ret = memcmp(&psoDesc1, &psoDesc2, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));

		psoDesc1.InputLayout = inputLayout1;
		psoDesc2.InputLayout = inputLayout2;

		return ret == 0;
	}

}

