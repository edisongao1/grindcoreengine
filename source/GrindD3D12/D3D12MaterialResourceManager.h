#pragma once
#include "IMaterialResourceManager.h"
#include "D3D12Buffer.h"

namespace grind
{
	class CD3D12BlockConstBufferAllocator
		: public TBlockBufferAllocator<CD3D12BlockConstantBuffer, 1024>
	{
	public:
		CD3D12BlockConstBufferAllocator(ID3D12Device* device, UINT elementSize)
			:md3dDevice(device)
			, mElementSize(elementSize)
		{

		}

		void SetBlockData(SChunkBlockIndex index, void* data)
		{
			mChunks[index.BufferIndex]->SetBlockData(index.BlockIndex, data);
		}

		D3D12_GPU_VIRTUAL_ADDRESS GetGPUAddress(SChunkBlockIndex index) const
		{
			return mChunks[index.BufferIndex]->GetGPUAddress(index.BlockIndex);
		}

		virtual CD3D12BlockConstantBuffer* CreateChunkBuffer();

	private:
		ID3D12Device*	md3dDevice;
		UINT			mElementSize;
	};

	class CD3D12MaterialConstBufferManager
	{
	public:
		enum { CB_SIZE_COUNT = 16 };
		CD3D12MaterialConstBufferManager(ID3D12Device* device)
		{
			mAllocators[0] = nullptr;
			for (int i = 1; i < CB_SIZE_COUNT; i++) {
				mAllocators[i] = std::make_unique<CD3D12BlockConstBufferAllocator>(device, i * 256);
			}
		}

		SMaterialConstBufferIndex Allocate(UINT byteSize);
		void Release(SMaterialConstBufferIndex index);
		void SetConstBufferData(SMaterialConstBufferIndex index, void* data);

		D3D12_GPU_VIRTUAL_ADDRESS GetGPUAddress(SMaterialConstBufferIndex index) const
		{
			return mAllocators[index.SizeIndex]->GetGPUAddress(index.SubIndex);
		}

	private:
		std::unique_ptr<CD3D12BlockConstBufferAllocator>		mAllocators[CB_SIZE_COUNT];
	};

	class CD3D12MaterialDescriptorHeap
		: public TBlockArray<8>
	{
	public:
		enum { BLOCK_COUNT = 8 };
		CD3D12MaterialDescriptorHeap(ID3D12Device* device, int blockDescriptorCount);
		D3D12_CPU_DESCRIPTOR_HANDLE GetCPUDescriptorHandle(int blockIndex, int offset) const;
		D3D12_GPU_DESCRIPTOR_HANDLE GetGPUDescriptorHandle(int i) const;
		ID3D12DescriptorHeap* GetD3D12DescriptorHeap() { return mCbvHeap.Get(); }

	private:
		ID3D12Device*	md3dDevice;
		int				mBlockDescriptorCount;
		ComPtr<ID3D12DescriptorHeap> mCbvHeap = nullptr;
	};

	class CD3D12MaterialDescriptorHeapAllocator
		: public TBlockBufferAllocator<CD3D12MaterialDescriptorHeap, 1024>
	{
	public:
		CD3D12MaterialDescriptorHeapAllocator(ID3D12Device* device, UINT descriptorCount)
			:md3dDevice(device)
			, mDescriptorCountPerElement(descriptorCount)
		{

		}

		D3D12_CPU_DESCRIPTOR_HANDLE GetCPUDescriptorHandle(SChunkBlockIndex index, int offset);
		D3D12_GPU_DESCRIPTOR_HANDLE GetGPUDescriptorHandle(SChunkBlockIndex index, ID3D12DescriptorHeap** ppd3dDescriptorHeap);
		virtual CD3D12MaterialDescriptorHeap* CreateChunkBuffer();
	private:
		ID3D12Device*	md3dDevice;
		UINT			mDescriptorCountPerElement;
	};

	class CD3D12MaterialDescriptorHeapManager
	{
	public:
		enum { DESCRIPTOR_SIZE_COUNT = 16 };
		CD3D12MaterialDescriptorHeapManager(ID3D12Device* device)
		{
			mAllocators[0] = nullptr;
			for (int i = 1; i < DESCRIPTOR_SIZE_COUNT; i++) {
				mAllocators[i] = std::make_unique<CD3D12MaterialDescriptorHeapAllocator>(device, i * 256);
			}
		}

		SMaterialDescriptorSetIndex Allocate(int descriptorCount);
		void Release(SMaterialDescriptorSetIndex index);
		D3D12_CPU_DESCRIPTOR_HANDLE GetCPUDescriptorHandle(SMaterialDescriptorSetIndex index, int offset);
		D3D12_GPU_DESCRIPTOR_HANDLE GetGPUDescriptorHandle(SMaterialDescriptorSetIndex index, ID3D12DescriptorHeap** ppd3dDescriptorHeap);
	private:
		std::unique_ptr<CD3D12MaterialDescriptorHeapAllocator>		mAllocators[DESCRIPTOR_SIZE_COUNT];
	};

	class CD3D12MaterialResourceManager : public IMaterialResourceManager
	{
	public:
		enum { CB_SIZE_COUNT = 16 };

		CD3D12MaterialResourceManager(ID3D12Device* device);

		virtual SMaterialResourceId	AllocateMaterialResources(
			int cbSize, void* cbData,
			int texCount, ITexture* textures[]
		) override;

		//void ApplyMaterialResources(ID3D12CommandList* pCommandList, SMaterialResourceId materialId);
		D3D12_GPU_DESCRIPTOR_HANDLE GetGPUDescriptorHandle(SMaterialResourceId resourceId,
			ID3D12DescriptorHeap** ppd3dDescriptorHeap);

		virtual void SetConstBufferData(SMaterialResourceId resourceId, void* data) override;

		//ID3D12DescriptorHeap* GetD3D12DescriptorHeap(SMaterialResourceId materialId);

		~CD3D12MaterialResourceManager()
		{
			//int a = 1;
		}

	private:
		ID3D12Device*	md3dDevice;
		std::unique_ptr<CD3D12MaterialDescriptorHeapManager>	mDescriptorHeapManager;
		std::unique_ptr<CD3D12MaterialConstBufferManager>		mConstBufferManager;
	};

}

