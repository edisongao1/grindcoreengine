#include "stdafx.h"
#include "D3D12MaterialResourceManager.h"
#include "D3D12TextureResource.h"

namespace grind
{
	SMaterialConstBufferIndex CD3D12MaterialConstBufferManager::Allocate(UINT byteSize)
	{
		SMaterialConstBufferIndex index;
		if (byteSize == 0) {
			index.SizeIndex = 0;
			return index;
		}

		index.SizeIndex = (int)((byteSize + 255) >> 8);
		assert(index.SizeIndex < CB_SIZE_COUNT);
		index.SubIndex = mAllocators[index.SizeIndex]->Allocate();
		return index;
	}

	void CD3D12MaterialConstBufferManager::Release(SMaterialConstBufferIndex index)
	{
		if (index.SizeIndex == 0)
			return;
		mAllocators[index.SizeIndex]->Release(index.SubIndex);
	}

	void CD3D12MaterialConstBufferManager::SetConstBufferData(SMaterialConstBufferIndex index, void* data)
	{
		if (index.SizeIndex == 0)
			return;
		mAllocators[index.SizeIndex]->SetBlockData(index.SubIndex, data);
	}

	CD3D12BlockConstantBuffer* CD3D12BlockConstBufferAllocator::CreateChunkBuffer()
	{
		return new CD3D12BlockConstantBuffer(md3dDevice, mElementSize);
	}

	CD3D12MaterialDescriptorHeap::CD3D12MaterialDescriptorHeap(ID3D12Device* device, int descriptorCount)
		:md3dDevice(device)
		, mBlockDescriptorCount(descriptorCount)
	{
		D3D12_DESCRIPTOR_HEAP_DESC cbvHeapDesc;
		cbvHeapDesc.NumDescriptors = mBlockDescriptorCount * BLOCK_COUNT;
		cbvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		cbvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		cbvHeapDesc.NodeMask = 0;
		ThrowIfFailed(md3dDevice->CreateDescriptorHeap(&cbvHeapDesc,
			IID_PPV_ARGS(&mCbvHeap)));
	}

	D3D12_CPU_DESCRIPTOR_HANDLE CD3D12MaterialDescriptorHeap::GetCPUDescriptorHandle(int blockIndex, int offset) const
	{
		return CD3DX12_CPU_DESCRIPTOR_HANDLE(
			mCbvHeap->GetCPUDescriptorHandleForHeapStart(),
			blockIndex * mBlockDescriptorCount + offset, D3D12Util::GetSrvDescriptorSize());
	}

	D3D12_GPU_DESCRIPTOR_HANDLE CD3D12MaterialDescriptorHeap::GetGPUDescriptorHandle(int i) const
	{
		return CD3DX12_GPU_DESCRIPTOR_HANDLE(
			mCbvHeap->GetGPUDescriptorHandleForHeapStart(),
			i * mBlockDescriptorCount, D3D12Util::GetSrvDescriptorSize());
	}

	D3D12_CPU_DESCRIPTOR_HANDLE CD3D12MaterialDescriptorHeapAllocator::GetCPUDescriptorHandle(
		SChunkBlockIndex index, int offset)
	{
		return mChunks[index.BufferIndex]->GetCPUDescriptorHandle(index.BlockIndex, offset);
	}


	D3D12_GPU_DESCRIPTOR_HANDLE CD3D12MaterialDescriptorHeapAllocator::GetGPUDescriptorHandle(
		SChunkBlockIndex index, ID3D12DescriptorHeap** ppd3dDescriptorHeap)
	{
		*ppd3dDescriptorHeap = mChunks[index.BufferIndex]->GetD3D12DescriptorHeap();
		return mChunks[index.BufferIndex]->GetGPUDescriptorHandle(index.BlockIndex);
	}

	CD3D12MaterialDescriptorHeap* CD3D12MaterialDescriptorHeapAllocator::CreateChunkBuffer()
	{
		return new CD3D12MaterialDescriptorHeap(md3dDevice, mDescriptorCountPerElement);
	}

	SMaterialDescriptorSetIndex CD3D12MaterialDescriptorHeapManager::Allocate(int descriptorCount)
	{
		if (descriptorCount == 0) {
			SMaterialDescriptorSetIndex index;
			index.DescriptorCount = 0;
			return index;
		}

		assert(descriptorCount < DESCRIPTOR_SIZE_COUNT);
		SMaterialDescriptorSetIndex index;
		index.DescriptorCount = descriptorCount;
		index.SubIndex = mAllocators[descriptorCount]->Allocate();
		return index;
	}

	void CD3D12MaterialDescriptorHeapManager::Release(SMaterialDescriptorSetIndex index)
	{
		mAllocators[index.DescriptorCount]->Release(index.SubIndex);
	}

	D3D12_CPU_DESCRIPTOR_HANDLE CD3D12MaterialDescriptorHeapManager::GetCPUDescriptorHandle(
		SMaterialDescriptorSetIndex index, int offset)
	{
		return mAllocators[index.DescriptorCount]->GetCPUDescriptorHandle(index.SubIndex, offset);
	}

	D3D12_GPU_DESCRIPTOR_HANDLE CD3D12MaterialDescriptorHeapManager::GetGPUDescriptorHandle(
		SMaterialDescriptorSetIndex index, ID3D12DescriptorHeap** ppd3dDescriptorHeap)
	{
		return mAllocators[index.DescriptorCount]->GetGPUDescriptorHandle(index.SubIndex, ppd3dDescriptorHeap);
	}

	CD3D12MaterialResourceManager::CD3D12MaterialResourceManager(ID3D12Device* device)
		:md3dDevice(device)
	{
		mDescriptorHeapManager = std::make_unique<CD3D12MaterialDescriptorHeapManager>(device);
		mConstBufferManager = std::make_unique<CD3D12MaterialConstBufferManager>(device);
	}

	SMaterialResourceId CD3D12MaterialResourceManager::AllocateMaterialResources(
		int cbSize, void* cbData, int texCount, ITexture* textures[])
	{
		SMaterialResourceId resourceId;

		int cbCount = cbSize == 0 ? 0 : 1;

		SMaterialDescriptorSetIndex desIndex = mDescriptorHeapManager->Allocate(cbCount + texCount);
		resourceId.DescriptorSetIndex = desIndex;

		if (cbCount)
		{
			UINT cbByteSize = D3D12Util::CalcConstantBufferByteSize((UINT)cbSize);
			SMaterialConstBufferIndex cbIndex = mConstBufferManager->Allocate(cbByteSize);
			mConstBufferManager->SetConstBufferData(cbIndex, cbData);
			resourceId.ConstBufferIndex = cbIndex;

			D3D12_GPU_VIRTUAL_ADDRESS cbAddress = mConstBufferManager->GetGPUAddress(cbIndex);
			D3D12_CPU_DESCRIPTOR_HANDLE handle = mDescriptorHeapManager->GetCPUDescriptorHandle(desIndex, 0);
			D3D12_CONSTANT_BUFFER_VIEW_DESC	cbvDesc;
			cbvDesc.BufferLocation = cbAddress;
			cbvDesc.SizeInBytes = cbByteSize;
			md3dDevice->CreateConstantBufferView(&cbvDesc, handle);
		}

		for (int i = 0; i < texCount; i++)
		{
			if (textures[i] == nullptr)
				continue;

			auto pTextureResource = textures[i]->GetTextureResource();
			if (pTextureResource == nullptr)
				continue;

			auto pd3dTextureResource = dynamic_cast<CD3D12TextureResourceBase*>(pTextureResource);
			D3D12_CPU_DESCRIPTOR_HANDLE handle = mDescriptorHeapManager->GetCPUDescriptorHandle(desIndex, cbCount + i);
			pd3dTextureResource->CreateShaderResourceView(handle);
		}

		//resourceId.IsCreated = true;
		resourceId.ComputeHash();
		return resourceId;
	}

	D3D12_GPU_DESCRIPTOR_HANDLE CD3D12MaterialResourceManager::GetGPUDescriptorHandle(
		SMaterialResourceId resourceId, ID3D12DescriptorHeap** ppd3dDescriptorHeap)
	{
		return mDescriptorHeapManager->GetGPUDescriptorHandle(resourceId.DescriptorSetIndex, ppd3dDescriptorHeap);
	}

	void CD3D12MaterialResourceManager::SetConstBufferData(SMaterialResourceId resourceId, void* data)
	{
		mConstBufferManager->SetConstBufferData(resourceId.ConstBufferIndex, data);
	}

}



