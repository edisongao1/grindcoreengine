#include "Common.hlsl"

ConstantBuffer<cbObject3D> gObjectCB : register(b0);

struct VertexIn
{
	float3 PosL  	: POSITION;
	float3 Normal 	: NORMAL;
#ifdef _RT_TANGENT
	float3 Tangent  : TANGENT;
#endif
	float2 BaseTC 	: TEXCOORD;
};

struct VertexOut
{
	float4 PosH  : SV_POSITION;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout = (VertexOut)0;
    //vout.PosH = mul(float4(vin.PosL, 1.0f), gObjectCB.WorldViewProj);
    vout.PosH = mul(gObjectCB.WorldViewProj, float4(vin.PosL, 1.0f));
    return vout;
}

float4 PS(VertexOut pin) : SV_Target
{
	return float4(1, 0, 0, 1);
}
