//***************************************************************************************
// color.hlsl by Frank Luna (C) 2015 All Rights Reserved.
//
// Transforms and colors geometry.
//***************************************************************************************

#include "Common.hlsl"

struct cbPBRMaterial
{
    float4 DiffuseAlbedo;
    float3 FresnelR0;
    float Shininess;
};


ConstantBuffer<cbObject3D> gObjectCB : register(b0);
//ConstantBuffer<cbTestObject> gTestObjectCB: register(b0);

ConstantBuffer<cbObject3DFrame> gFrameCB : register(b2);
//ConstantBuffer<cbTestFrame> gTestFrameCB: register(b2);

ConstantBuffer<cbPBRMaterial> gMaterialCB: register(b1);


Texture2D    gDiffuseMap : register(t0);


struct VertexIn
{
	float3 PosL  	: POSITION;
	float3 Normal 	: NORMAL;
#ifdef _RT_TANGENT
	float3 Tangent  : TANGENT;
#endif
	float2 BaseTC 	: TEXCOORD;
};

struct VertexOut
{
	float4 PosH  : SV_POSITION;
    //float4 Color : COLOR;
    float3 PosW    : POSITION;
    float3 NormalW : NORMAL;
#ifdef _RT_TANGENT
	float3 Tangent  : TANGENT;
#endif
    float2 BaseTC : TEXCOORD;

};

VertexOut VS(VertexIn vin)
{
	VertexOut vout = (VertexOut)0;
  //	float4 PosW = mul(float4(vin.PosL, 1.0f), gObjectCB.World);
  float4 PosW = mul(gObjectCB.World, float4(vin.PosL, 1.0f));
  //  vout.PosH = mul(PosW, gTestFrameCB.ViewProj);

  //  vout.PosH = mul(PosW, mul(gFrameCB.View, gFrameCB.Proj));
  vout.PosH = mul(mul(gFrameCB.Proj, gFrameCB.View), PosW);

	// Transform to homogeneous clip space.
    //vout.PosH = mul(float4(vin.PosL, 1.0f), gObjectCB.WorldViewProj);
	vout.BaseTC = vin.BaseTC;
	// Just pass vertex color into the pixel shader.
    //vout.Color = vin.Color;


    
    return vout;
}

float4 PS(VertexOut pin) : SV_Target
{
    float4 Color = float4(1, 0, 0, 1);
#ifdef _MT_GREEN_FLAG
    Color = float4(0, 1, 0, 1);
#endif

#if _RT_RECEIVE_SHADOW
    float illum = dot(Color.rgb, float3( 0.2126, 0.7152, 0.0722 ));
    Color.rgb = float3(illum, illum, illum);
#endif

#if _RT_RECEIVE_GI
    Color.rgb *= 2;
#endif

#if _TT_TECH_FLAG_1
    Color.rgb = float3(0, 1, 1);
#endif

    Color.rgb = gMaterialCB.DiffuseAlbedo.rgb;

#ifdef _MT_RED_FLAG
    Color.rgb = Color.rgb * 0.5 + float3(0.5, 0, 0);
#endif 

    float4 diffuseColor = gDiffuseMap.Sample(gsamLinearWrap, pin.BaseTC);
    //float4 diffuseColor = float4(1, 1, 1, 1);
    //return diffuseColor;
    return Color * diffuseColor;
}


