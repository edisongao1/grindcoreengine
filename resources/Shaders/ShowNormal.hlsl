//***************************************************************************************
// color.hlsl by Frank Luna (C) 2015 All Rights Reserved.
//
// Transforms and colors geometry.
//***************************************************************************************

#include "Common.hlsl"

ConstantBuffer<cbObject3D> gObjectCB : register(b0);
ConstantBuffer<cbObject3DFrame> gFrameCB : register(b2);

struct VertexIn
{
	float3 PosL  	: POSITION;
	float3 Normal 	: NORMAL;
#ifdef _RT_TANGENT
	float3 Tangent  : TANGENT;
#endif
	float2 BaseTC 	: TEXCOORD;
};

struct VertexOut
{
	float4 PosH  : SV_POSITION;
    float3 NormalW : NORMAL;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout = (VertexOut)0;
    vout.PosH = mul(gObjectCB.WorldViewProj, float4(vin.PosL, 1.0f));
	// Transform to homogeneous clip space.
    //vout.PosH = mul(float4(vin.PosL, 1.0f), gObjectCB.WorldViewProj);
	float3 NormalW = mul(gObjectCB.World, float4(vin.Normal, 0.0f)).xyz;
    vout.NormalW = normalize(NormalW);

    // Just pass vertex color into the pixel shader.
    //vout.Color = vin.Color;


    
    return vout;
}

float4 PS(VertexOut pin) : SV_Target
{
    float3 normalW = normalize(pin.NormalW);

    normalW = normalW * 0.5f + 0.5f;
    return float4(normalW, 1);
    //return float4(0, 1, 0, 1);
}


