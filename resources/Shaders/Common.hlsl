struct cbTestObject
{
    float4x4 World;
    float4  TestColor1;
    float4  TestColor2;
};

struct cbObject3D
{
    float4x4 World;
    float4x4 WorldViewProj; 
};

struct cbObject3DFrame
{
	float4x4 View;
    float4x4 InvView;
    float4x4 Proj;
    float4x4 InvProj;
    float4x4 ViewProj;
    float4x4 InvViewProj;
    float3 	 EyePos;
    float    _Pad1;
    float4 	RenderTargetSize;
    float 	NearZ;
    float 	FarZ;
    float 	TotalTime;
    float 	DeltaTime;
};

struct cbTestFrame
{
    float4x4 ViewProj;
    float4  TestColor1;
    float4  TestColor2;
};


SamplerState gsamPointWrap        : register(s0);
SamplerState gsamPointClamp       : register(s1);
SamplerState gsamLinearWrap       : register(s2);
SamplerState gsamLinearClamp      : register(s3);
SamplerState gsamAnisotropicWrap  : register(s4);
SamplerState gsamAnisotropicClamp : register(s5);
