if not exist .\build (mkdir build)
if exist .\build\CMakeCache.txt del .\build\CMakeCache.txt
cd build
cmake .. -G "Visual Studio 15 2017 Win64"
pause
