if not exist .\build (mkdir build)
if not exist .\build\msvc2017 (mkdir build\msvc2017)
if exist .\build\msvc2017\CMakeCache.txt del .\build\msvc2017\CMakeCache.txt
cd .\build\msvc2017
cmake ..\.. -G "Visual Studio 15 2017 Win64"
pause
