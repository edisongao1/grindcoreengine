include(CheckCXXCompilerFlag)
if(MSVC)
	if (MSVC_VERSION GREATER_EQUAL "1900")
		CHECK_CXX_COMPILER_FLAG("/std:c++latest" _cpp_latest_flag_supported)
		if (_cpp_latest_flag_supported)
			add_compile_options("/std:c++latest")
		endif()
	endif()
	
	add_compile_options("/Yustdafx.h")
	message(STATUS "Set stdafx.h as precompiled header")
	
endif()
